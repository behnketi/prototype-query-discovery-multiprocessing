#!/usr/bin/python3
"""Contains functions for discovering queries from samples by using variants of pattern-type-split (BU) algorithm."""
import logging
import time
from math import ceil
import numpy as np
from query import Query
from error import EmptySampleError, InvalidQuerySupportError
from discovery import combine_all 
from sample import Sample
from hyper_linked_tree import HyperLinkedTree
#from discovery_bottom_up import matching_onedim

PATTER_TYPE_SPLIT_DISCOVERY_ORDER = [
        'type_first'
        ]
FULL_CHECK_ON_SAMPLE = False

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def bu_pattern_type_split_discovery(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first', find_all_matching_queries = False, max_query_length = -1):
    """
        Query Discovery by using a variant of pattern-type-split (BU) algorithm.
        Creates separate structures for pattern- and type-queries and combines them afterwards.
        Lastly tries to syntactically prune non-descriptive queries.

        Args:
            sample: Sample instance.

            supp: Float between 0 and 1 which describes the requested support.

            use_tree_structure [= False]: Optional parameter to decide whether the result is stored into a hyperlinked tree or not.

            discovery_order [= "type_first"]: Optional order to decide whether the starting to discover type-queries first ("type-first") or in any other order (not implemented
            yet).

            use_smart_matching [= False]: Uses a iterative dictionary based approach to solve matchings instead of regex

            multidim [= False]: Optional parameter to determine whether the algorithm was called from multidim approach.
        Returns:
            A list of all discovered queries and a dictionary of stat's.

        Raises:
            EmptySampleError: The given sample is empty.
            InvalidQuerySupportError: Supp is less than 0 or greater than 1.
            NotImplementedError: Selected discovery_order is not available

        Passes:
            IndexError: from _build_mixed_query_tree
            ValueError: from _build_type_tree
            ValueError: from _build_pattern_tree
            ValueError: from _build_mixed_query_tree
    """
    LOGGER.info('Pattern-Type-Split (BU) - Started')

    if not isinstance(sample, Sample):
        raise EmptySampleError('No sample given.')
    if not sample._sample or sample._sample_size < 1:
        raise EmptySampleError('The given sample is empty. (Sample size: ' + str(sample._sample_size) + ')')
    if supp < 0.0 or supp > 1.0:
        raise InvalidQuerySupportError(f'Support {supp} has to be between 0 and 1.')

    # Init statistics
    stats = {}
    stats["param match test count"] = 0
    stats["match_test_runtime_list"] = []

    # Init smart matching
    if use_smart_matching:
        param_smart_matching = ({}, [], {})
    else:
        param_smart_matching = None

    # Init typeset
    sample.set_sample_typeset()

    # Start of core algorithm
    return_value = None
    if discovery_order =='type_first':
        LOGGER.info(">> S type tree")
        time_build_type_tree_start = time.time()
        type_tree = _build_type_tree(stats, sample, supp, use_tree_structure, param_smart_matching, max_query_length=max_query_length)
        time_build_type_tree = time.time() - time_build_type_tree_start
        try:
            LOGGER.info(">> F type tree [%s] with %s", str(len(type_tree.vertices_to_list())),str(time_build_type_tree))
        except AttributeError:
            LOGGER.info(">> F types [%s] with %s",str(len(type_tree)), str(time_build_type_tree))

        LOGGER.info(">> S pattern tree")
        time_build_pattern_tree_start = time.time()
        pattern_tree = _build_pattern_tree(stats, sample, supp, use_tree_structure, param_smart_matching, max_query_length=max_query_length)
        time_build_pattern_tree = time.time() - time_build_pattern_tree_start
        try:
            LOGGER.info(">> F pattern tree [%s] with %s", str(len(pattern_tree.vertices_to_list())), str(time_build_pattern_tree))
        except AttributeError:
            LOGGER.info(">> F patterns [%s] with %s", str(len(pattern_tree)), str(time_build_pattern_tree))

        LOGGER.info(">> S mixed_query tree")
        time_build_mixed_query_tree_start = time.time()
        mixed_query_tree = _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree, param_smart_matching)
        time_build_mixed_query_tree = time.time() - time_build_mixed_query_tree_start
        try:
            LOGGER.info(">> F mixed_query tree [%s] with %s", str(len(mixed_query_tree.vertices_to_list())), str(time_build_mixed_query_tree))
        except AttributeError:
            LOGGER.info(">> F mixed queries [%s] with %s", str(len(mixed_query_tree)), str(time_build_mixed_query_tree))

        if use_tree_structure:
            if find_all_matching_queries:
                return_value = mixed_query_tree.query_strings_to_set(frequent_items_only=True)
            else:
                LOGGER.info(">> S descriptive process")
                time_extract_descriptive_start = time.time()
                descriptive_set = set()
                for vertex in mixed_query_tree.vertices_to_list():
                    if vertex.is_frequent(ceil(supp*sample._sample_size)):
                        if vertex.descriptive:
                            descriptive_set.add(vertex.query_string)
                typeset = list(sample.get_supported_typeset(supp))
                final_descriptive_mixed_query_set = []
                for query_string in descriptive_set:
                    variable_num_list = []
                    variable_num = 0
                    descriptive = True
                    while True:
                        variable = "$x" + str(variable_num)
                        if variable in query_string:
                            variable_num_list.append(variable_num)
                            variable_num += 1
                        else:
                            break
                    for variable_num in variable_num_list:
                        for new_element in typeset + [str("$x" + str(var_num)) for var_num in range(0, variable_num)]:
                            new_query_string = query_string.replace("$x"+str(variable_num), new_element)
                            old_var_list = [str("$x" +str(var_num)) for var_num in range (variable_num, max(variable_num_list)+1)]
                            for i in range(1, len(old_var_list)):
                                new_query_string = new_query_string.replace(old_var_list[i], old_var_list[i-1])
                            if new_query_string in descriptive_set:
                                descriptive = False
                                break
                        if not descriptive:
                            break
                    if descriptive:
                        final_descriptive_mixed_query_set.append(query_string)
                return_value = final_descriptive_mixed_query_set
                time_extract_descriptive = time.time() - time_extract_descriptive_start
                LOGGER.info(">> F descriptive process with %s", str(time_extract_descriptive))
        else:
            return_value = mixed_query_tree
        time_total = time.time() - time_build_type_tree_start
        LOGGER.info("> F total process with %s", str(time_total))
    else:
        # TODO: spezifizieren!
        raise NotImplementedError("No exception for wrong input defined!")


    # Save collected statistics
    stats["time_build_type_tree"] = time_build_type_tree
    stats["time_build_pattern_tree"] = time_build_pattern_tree
    stats["time_build_mixed_query_tree"] = time_build_mixed_query_tree
    stats["time_total"] = time_total
    #match_test_runtime_average = (sum(match_test_runtime_list)/len(match_test_runtime_list))
    #stats["param avg match test runtime for single position"] = match_test_runtime_average
    #stats["param avg match test runtime for whole query"] = match_test_runtime_average*query._query_string_length
    #stats["param supported typeset"] = sample._get_supported_typeset(supp)

    LOGGER.info('Pattern-Type-Split (BU) - Finished')
    return (return_value,stats)

def _search_type(sample, pat, s_n, supp, already_existing_tree=None, current_vertex=None, param_smart_matching=None, max_query_length=-1):
    """
        Recursive iteration to add further frequent symbols, checking if they are still frequent and iterating the process until support condition can no longer be achieved.
        In the case of hyperlinked trees, a current_vertex is required to store the found results.

        Args:
            sample: Sample instance.

            pat: Pattern to increment with frequent symbols from s_n

            s_n: Set of frequent items, which can be appended to pat

            supp: Float between 0 and 1 which describes the requested support.

            already_existing_tree [= None]: An hyperlinked tree can be given to store all found queries there

            current_vertex [= None]: If a hyperlinked tree is given, then a vertex has to be given as well to store the results to this vertex.

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

        Returns:
            type_queries: Set of type-queries fitting support.

        Raises:
            None

        Passes:
            ValueError: from matching_smarter
    """
    LOGGER.debug("Begin _search for '%s' with %s", str(pat), str(sorted(s_n)))
    s_temp = set()
    type_queries = set()

    # s-extension
    for j in s_n:
        new_pattern = pat + ' ' + j
        new_query = Query()
        new_query.set_query_string(new_pattern)
        if not param_smart_matching:
            pattern_is_frequent = new_query.match_sample(sample, supp)
        else:
            new_query.set_query_matchtest('smarter')
            parentstring = pat
            parent = Query()
            parent.set_query_string(parentstring)
            param_smart_matching[2][new_pattern] = parent
            pattern_is_frequent = new_query.match_sample(sample, supp,
                    dict_iter = param_smart_matching[0], patternset = param_smart_matching[1], parent_dict = param_smart_matching[2], max_query_length=max_query_length)
        matched_traces = new_query._query_matched_traces
        if pattern_is_frequent:
            s_temp.add(j)
            if already_existing_tree:
                new_query_vertex = already_existing_tree.insert_query_string(current_vertex, new_pattern, search_for_parents=False, set_descriptive_property=True)
                new_query_vertex.query_next_insert_index[0] = current_vertex.query_next_insert_index[0] + 1
                already_existing_tree.set_match_results(new_query_vertex, matched_traces)
            else:
                type_queries.add(new_pattern)
    if already_existing_tree:
        for vertex in current_vertex.child_vertices:
            _search_type(sample, vertex.query_string, s_temp, supp, already_existing_tree, vertex, param_smart_matching=param_smart_matching, max_query_length=max_query_length)
    else:
        for j in s_temp:
            s_pat = pat + ' ' + j
            type_queries |= _search_type(sample, s_pat, s_temp, supp, param_smart_matching=param_smart_matching, max_query_length=max_query_length)

    # i-extension not necessary
    # for j in s_n:
    #    ...
    LOGGER.debug("End _search")
    return type_queries

def _build_type_tree(stats, sample, supp, use_tree_structure=False, param_smart_matching=None, max_query_length=-1):
    """
        Builds a set with all queries containing only types and fitting the sample with the given support.
        If a tree structure shall be used, then the results are stored in a hyperlinked tree.

        Args:
            stats: Dictionary for statistical evaluation.

            sample: Sample instance.

            supp: Float between 0 and 1 which describes the requested support.

            use_tree_structure [= False]: Optional parameter to decide whether the result is stored into a hyperlinked tree or not.

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

        Returns:
            type_queries: Set of type-queries fitting support.

        Raises:
            None

        Passes:
            ValueError: from _search_type
    """
    vsdb = sample.get_vertical_sequence_database()

    s_init = {key for key in vsdb if len(vsdb[key]) >= ceil(sample._sample_size * supp)}
    if param_smart_matching:
        param_smart_matching[1].extend({key for key in vsdb for item in vsdb[key] if len(vsdb[key][item]) >= 2})

    type_queries = None
    if use_tree_structure:
        type_queries = HyperLinkedTree(ceil(supp*sample._sample_size))
        root_vertex = type_queries.get_root()
        root_vertex.query_next_insert_index = [0]
        for symbol in s_init:
            new_query_vertex = type_queries.insert_query_string(root_vertex, symbol, search_for_parents=False, set_descriptive_property=True)
            type_queries.set_match_results(new_query_vertex, list(vsdb[symbol].keys()))
            _search_type(sample, symbol, s_init, supp, type_queries, new_query_vertex, param_smart_matching=param_smart_matching, max_query_length=max_query_length)
    else:
        type_queries = {""} | s_init.copy()
        for symbol in s_init:
            type_queries |= _search_type(sample, symbol, s_init, supp, param_smart_matching=param_smart_matching)

    return type_queries

def _search_var(sample, next_var_number, pattern, s_n, supp, stats, allow_new_variables, already_existing_tree=None, current_vertex=None, param_smart_matching=None, max_query_length=-1):
    """
        Recursive iteration to add further variables, checking if they are still frequent and iterating the process until support condition can no longer be achieved.
        In the case of hyperlinked trees, a current_vertex is required to store the found results.

        Args:
            sample: Sample instance.

            next_var_number: The current number of the next inserted variable.

            pattern: Pattern to increment with frequent variables from s_n

            s_n: Set of frequent variables, which can be appended to pat

            supp: Float between 0 and 1 which describes the requested support.

            stats: Dictionary for statistical evaluation.

            allow_new_variables: Boolean, if the recursion is allowed to insert new variables. It is set to false, if a newly inserted variable can not fit support, then it should
            not try to insert a variable later.

            already_existing_tree [= None]: Optional parameter to decide whether the result is stored into a hyperlinked tree or not.

            current_vertex [= None]: The Current vertex to which the found queries will be added to.

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

        Returns:
            list: Set of pattern-queries containing only variables and fitting support.

        Raises:
            None

        Passes:
            ValueError: from matching_smarter
    """
    s_temp = set()
    pattern_queries = {""}

    # s-extension
    # new Variable possible?
    if allow_new_variables:
        single_var_pattern = pattern + ' $X' + str(next_var_number)
        double_var_pattern = pattern + ' $X' + str(next_var_number) + ' $X' + str(next_var_number)

        double_pat_query = Query()
        double_pat_query.set_query_string(double_var_pattern)

        if not param_smart_matching:
            pattern_is_frequent = double_pat_query.match_sample(sample, supp)
        else:
            double_pat_query.set_query_matchtest('smarter')
            parentstring = pattern
            parent = Query()
            parent.set_query_string(parentstring)
            param_smart_matching[2][double_pat_query] = parent
            pattern_is_frequent = double_pat_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                    parent_dict = param_smart_matching[2], max_query_length=max_query_length)
        matched_traces = double_pat_query._query_matched_traces
        if not pattern_is_frequent:
            allow_new_variables = False

    for j in s_n:
        new_pattern = pattern + ' ' + j
        new_query = Query()
        new_query.set_query_string(new_pattern)
        
        if not param_smart_matching:
            pattern_is_frequent = new_query.match_sample(sample, supp)
        else:
            new_query.set_query_matchtest('smarter')
            parentstring = pattern
            parent = Query()
            parent.set_query_string(parentstring)
            param_smart_matching[2][new_patern] = parent
            pattern_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                    parent_dict = param_smart_matching[2], max_query_length=max_query_length)
        matched_traces = new_query._query_matched_traces

        if pattern_is_frequent:
            s_temp.add(j)
            if already_existing_tree:
                new_query_vertex = already_existing_tree.insert_query_string(current_vertex, new_pattern, search_for_parents=False)
                already_existing_tree.set_match_results(new_query_vertex, matched_traces)
            else:
                pattern_queries.add(new_pattern)

    if already_existing_tree:
        for vertex in current_vertex.child_vertices:
            _search_type(sample, vertex.query_string, s_temp, supp, already_existing_tree, vertex, param_smart_matching=param_smart_matching)
        if allow_new_variables:
            s_temp.add("$x"+str(next_var_number))
            _search_var(sample, next_var_number+1, single_var_pattern, s_temp, supp, stats, True, already_existing_tree, current_vertex, max_query_length=max_query_length)
    else:
        for j in s_temp:
            new_pattern = pattern + ' ' + j
            pattern_queries |= _search_var(sample, next_var_number, new_pattern, s_temp, supp, stats, allow_new_variables, max_query_length=max_query_length)
        if allow_new_variables:
            s_temp.add("$x"+str(next_var_number))
            pattern_queries |= _search_var(sample, next_var_number+1, single_var_pattern, s_temp, supp, stats, True, max_query_length=max_query_length)

    # i-extension not necessary
    # for j in s_n:
    #    ...
    return pattern_queries

def _search_var_smart(stats, sample, supp, already_existing_tree, param_smart_matching=None, max_query_length=-1):
    """
        Non-recursive iteration to add further variables, checking if they are still frequent and iterating the process until support condition can no longer be achieved.
        This approach uses a smart version and only mines pattern in normal form.
        In the case of hyperlinked trees, a current_vertex is required to store the found results.

        Args:
            stats: Dictionary for statistical evaluation.

            sample: Sample instance.

            supp: Float between 0 and 1 which describes the requested support.

            already_existing_tree: Optional parameter, which should be a hyperlinked tree. It to decide whether the result is stored there or in a list.

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

        Returns:
            HyperLinkedTree: Set of pattern-queries containing only variables and fitting support.

        Raises:
            None

        Passes:
            ValueError: from matching_smarter
    """
    # setup
    query_string = "$x0 $x0"
    query_array  = ["$x0", "$x0"]
    current_var_num  = 0
    s_0 = {current_var_num}

    new_query = Query()
    new_query.set_query_string(query_string)
    
    if not param_smart_matching:
        pattern_is_frequent = new_query.match_sample(sample, supp)
    else:
        LOGGER.info("'%s'", new_query._query_string)
        LOGGER.info("%s,%s,%s", param_smart_matching[0], param_smart_matching[1], param_smart_matching[2])
        LOGGER.info("'%s'", new_query._query_string)
        new_query.set_query_matchtest('smarter')
        parent = Query()
        param_smart_matching[2][query_string] = parent
        pattern_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                parent_dict = param_smart_matching[2], max_query_length=max_query_length)
    matched_traces = new_query._query_matched_traces

    if not pattern_is_frequent:
        return

    next_vertex = already_existing_tree.insert_query_string(already_existing_tree.get_root(), query_string, query_array, search_for_parents=True)
    next_vertex.query_next_insert_index[0] = 1
    already_existing_tree.set_match_results(next_vertex, matched_traces)

    new_var_insert_index = 1
    query_next_insert_index_list = np.array([2])

    next_element_queue = [(next_vertex, query_array, query_next_insert_index_list, s_0, current_var_num, True, new_var_insert_index)]

    # optimisaion for query matching
    still_matches = True
    auto_match_length = 2
    if not param_smart_matching:
        auto_match_traces = new_query._query_matched_traces
        while still_matches:
            query_string += " $x0"
            query = Query()
            query.set_query_string(query_string)
            if not query.match_sample(sample, supp, complete_test=FULL_CHECK_ON_SAMPLE):
                break
            auto_match_length += 1
            auto_match_traces = query._query_matched_traces

    while len(next_element_queue)>0:
        next_element = next_element_queue.pop(0)
        (current_vertex, query_array, query_next_insert_index_list, s_n, current_var_num, new_var_allowed, new_var_insert_index) = next_element
        query_length = len(query_array)

    # s-extension
        #insert already inserted variable again
        s_temp = set()
        ready_to_queue_list = []
        for var_num in s_n:
            var_num_exists = False
            indices_to_increase = list(range(0,len(query_next_insert_index_list)))      # s_n should be enough as well
            indices_to_replace  = []
            for i in range(query_next_insert_index_list[var_num],query_length+1):
                new_query_array  = query_array[:i] + ['$x'+str(var_num)] + query_array[i:]
                new_query_string = ' '.join(new_query_array)
                new_query_string_is_frequent = False

                new_query = Query()
                new_query.set_query_string(new_query_string)
                
                if not param_smart_matching:
                    if len(new_query_array) <= auto_match_length:
                        new_query_string_is_frequent = True
                        matched_traces = auto_match_traces
                    else:
                        new_query_string_is_frequent = new_query.match_sample(sample, supp, complete_test=FULL_CHECK_ON_SAMPLE)
                        matched_traces = new_query._query_matched_traces
                else:
                    new_query.set_query_matchtest('smarter')
                    new_query_string_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                            parent_dict = param_smart_matching[2], max_query_length=max_query_length)
                    matched_traces = new_query._query_matched_traces

                if new_query_string_is_frequent:
                    var_num_exists = True

                    next_vertex = already_existing_tree.insert_query_string(current_vertex, new_query_string, new_query_array, search_for_parents=True)
                    next_vertex.query_next_insert_index[0] = i+1
                    already_existing_tree.set_match_results(next_vertex, matched_traces)

                    new_indices_to_increase = [index for index in indices_to_increase if query_next_insert_index_list[index] > i] # for growing i this set will decrease
                    indices_to_replace.extend(set(indices_to_increase) - set(new_indices_to_increase))
                    indices_to_increase = new_indices_to_increase
                    new_query_next_insert_index_list = query_next_insert_index_list.copy()
                    new_query_next_insert_index_list[indices_to_increase] = query_next_insert_index_list[indices_to_increase] + 1
                    new_query_next_insert_index_list[indices_to_replace]  = i + 1                                                 # by definition var_num is here

                    ready_to_queue_list.append((next_vertex, new_query_array, new_query_next_insert_index_list, current_var_num, i+1))
            if var_num_exists:
                s_temp.add(var_num)
        if new_var_allowed:
            new_var_allowed = False
            insert_symbol = '$x' + str(current_var_num + 1)
            for i in range(new_var_insert_index, query_length+1):
                indices_to_increase_1 = list(range(0,len(query_next_insert_index_list)))      # s_n should be enough as well
                indices_to_replace  = []
                for j in range(i, query_length+1):
                    new_query_array = query_array[:i] + [insert_symbol] + query_array[i:j] + [insert_symbol] + query_array[j:]
                    new_query_string = ' '.join(new_query_array)
                    new_query = Query()
                    new_query.set_query_string(new_query_string)
                    
                    if not param_smart_matching:
                        if len(new_query_array) <= auto_match_length:
                            new_query_string_is_frequent = True
                            matched_traces = auto_match_traces
                        else:
                            new_query_string_is_frequent = new_query.match_sample(sample, supp, complete_test=FULL_CHECK_ON_SAMPLE)
                            matched_traces = query._query_matched_traces
                    else:
                        parent = Query()
                        parent.set_query_string(current_vertex.query_string)
                        param_smart_matching[2][new_query_string] = parent
                        new_query.set_query_matchtest('smarter')
                        parentstring = current_vertex.query_string
                        parent = Query()
                        parent.set_query_string(parentstring)
                        param_smart_matching[2][new_query_string] = parent
                        new_query_string_is_frequent =  new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                                parent_dict = param_smart_matching[2], max_query_length=max_query_length)
                        matched_traces = new_query._query_matched_traces
                    
                    if new_query_string_is_frequent:
                        new_var_allowed = True

                        next_vertex = already_existing_tree.insert_query_string(current_vertex, new_query_string, new_query_array, search_for_parents=True)
                        next_vertex.query_next_insert_index[0] = i + 1
                        already_existing_tree.set_match_results(next_vertex, matched_traces)

                        new_indices_to_increase_1 = [index for index in indices_to_increase_1 if query_next_insert_index_list[index] > i] # for growing i this set will decrease
                        indices_to_replace.extend(set(indices_to_increase_1) - set(new_indices_to_increase_1))
                        indices_to_increase_2 = [index for index in new_indices_to_increase_1 if query_next_insert_index_list[index] > j]
                        indices_to_increase_1 = new_indices_to_increase_1

                        new_query_next_insert_index_list = query_next_insert_index_list.copy()
                        new_query_next_insert_index_list[indices_to_increase_1] = new_query_next_insert_index_list[indices_to_increase_1] + 1
                        new_query_next_insert_index_list[indices_to_increase_2] = new_query_next_insert_index_list[indices_to_increase_2] + 1
                        new_query_next_insert_index_list[indices_to_replace]  = i + 1                                                # by definition var_num is here
                        new_query_next_insert_index_list = np.append(new_query_next_insert_index_list, j+2)

                        ready_to_queue_list.append((next_vertex, new_query_array, new_query_next_insert_index_list, current_var_num+1, i+1))
        s_temp_extend = False
        for item in ready_to_queue_list:
            (next_vertex, new_query_array, new_query_next_insert_index_list, new_var_num, new_var_insert_index) = item
            if current_var_num != new_var_num and not s_temp_extend:
                s_temp.add(current_var_num+1)
                s_temp_extend = True
            next_element_queue.append((next_vertex, new_query_array, new_query_next_insert_index_list.copy(), s_temp.copy(), new_var_num, new_var_allowed, new_var_insert_index))

    # i-extension not necessary
    # for j in s_n:
    #    ...
    LOGGER.debug("End _search_var_smart")
    return

def _build_pattern_tree(stats, sample, supp, use_tree_structure=False, param_smart_matching=None, max_query_length=-1):
    """
        Builds a set with all queries containing only variables and fitting the sample with the given support.
        TODO: Use a more advanced data structure to enable the algorithm to perform redundancy checks.

        Args:
            stats: Dictionary for statistical evaluation.

            sample: Sample instance.

            supp: Float between 0 and 1 which describes the requested support.

            use_tree_structure [= False]: Optional parameter to decide whether the result should be stored in a hyperlinked tree or not.

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

            direction [= "bottom_up"]: Optional direction to decide whether the starting query is the empty query (bottom up) or if anything else (not implemented yet) should be
            done.

        Returns:
            Set of all pattern-queries fitting the support.

        Raises:
            None

        Passes:
            ValueError: from _search_var
            ValueError: from _search_var_smart
    """
    pattern_queries = None
    if use_tree_structure:
        pattern_queries = HyperLinkedTree(ceil(supp*sample._sample_size))
        _search_var_smart(stats, sample, supp, pattern_queries, param_smart_matching=param_smart_matching,
                          max_query_length=max_query_length)
    else:
        pattern_queries = set()
        s_init = {'$x0'}
        if len([trace for trace in sample._sample if len(trace) == 0]) < len(sample._sample)*supp:
            pattern_queries = s_init.copy()
            pattern_queries |= _search_var(sample, 1, "$x0", s_init, supp, stats, True, max_query_length=max_query_length)

        # normalize
        pattern_queries_normalform = set()
        for query_string in pattern_queries:
            new_query = Query()
            new_query.set_query_string(query_string)
            new_query.query_string_to_normalform()

            pattern_queries_normalform.add(new_query._query_string)
        pattern_queries = pattern_queries_normalform

    return pattern_queries

def _merge_query_vertices(mixed_query_tree, t_vertex, p_vertex, m_vertex, evolved_vertex):
    """
        The method creates a list of new vertices for mixed-query-vertex, when either the t(ype)-vertex or the p(attern)-vertex evolved (was increased).
        The goal is to create a spanning tree by creating each possible mixed-query only once.

        Args:
            mixed_query_tree: A hyperlinked tree to store the results to.

            t_vertex: the current type_vertex.

            p_vertex: the current pattern_vertex.

            m_vertex: the vertex contains the current mixed query of the t_vertex and the p_vertex in a specific manner.

            evolved_vertex: the vertex, which got one or two more symbols than either the t_vertex or p_vertex. The vertex will be used to create a child from m_vertex and store it
            in mixed_query_tree.

        Returns:
            A list of newly found queries

        Raises:
            IndexError: If t_index and p_index run out of bound, while iterating and comparing the type_string and the pattern_string with the mixed_query_string, and an "Undefined
            behavior" is raised, since this should normally never happen.
    """
    if evolved_vertex.query_string[0] != "$":
        m_array = m_vertex.query_array
        new_insert_position = m_vertex.query_next_insert_index[0]
        new_vertices = []
        for i in range(new_insert_position, len(m_array) + 1):
            new_query_array = m_array[:i]
            new_query_array += [evolved_vertex.query_array[-1]]
            new_query_array += m_array[i:]
            new_query_string = ' '.join(new_query_array)

            new_vertex = mixed_query_tree.insert_query_string(m_vertex, new_query_string, new_query_array, search_for_parents=True, break_when_missing_parent=True,
                    break_when_non_matching_parent=True)
            new_vertex.query_next_insert_index[0] = i + 1
            new_vertices.append(new_vertex)
        return new_vertices
    else:
        t_array = t_vertex.query_array
        t_index = 0
        t_length = len(t_array)
        p_array = p_vertex.query_array
        p_index = 0
        p_length = len(p_array)
        m_array = m_vertex.query_array
        m_index = 0
        m_next_insert_index = m_vertex.query_next_insert_index[0]
        e_array = evolved_vertex.query_array
        e_next_insert_index = evolved_vertex.query_next_insert_index[0]

        while m_index < m_next_insert_index:
            if t_index < t_length and m_array[m_index] == t_array[t_index]:
                t_index += 1
            elif p_index < p_length and m_array[m_index] == p_array[p_index]:
                if p_array[p_index] != e_array[p_index]:
                    return []
                p_index += 1
            else:
                raise IndexError("Undefined behavior")
            m_index += 1
        new_query_array = m_array[:m_index] #m_next_inser_index might be not big enough and must be replaced by m_index
        new_query_array += e_array[p_index:]
        new_query_string = ' '.join(new_query_array)
        new_vertex = mixed_query_tree.insert_query_string(m_vertex, new_query_string, new_query_array, search_for_parents=True, break_when_missing_parent=True,
                break_when_non_matching_parent=True)
        new_vertex.query_next_insert_index[0] = m_next_insert_index + (e_next_insert_index - p_index)
        return [new_vertex]

def _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=None, max_query_length=-1):
    """
        Construct all mixed queries from the set of all type-queries and all pattern-queries fitting the support requirements.

        Args:
            stats: Dictionary for statistical evaluation.

            sample: Sample instance.

            supp: Float between 0 and 1 which describes the requested support.

            type_tree: list of all type-queries (TODO: spanning tree over all matching queries only containing types).

            pattern_tree: list of all pattern-queries (TODO: spanning tree over all matching queries only containing variables).

            param_smart_matching [= None]: requires a tuple (Dictionary, Variable set) to evaluate the queries on the sample

            direction [= "bottom_up"]: Optional direction to decide whether the starting query is the empty query (bottom up) or if anything else (not implemented yet) should be
            done.

        Returns:
            A set of all queries fitting the sample.
            A tree_structure is used, when both the type_tree and pattern_tree are HyperLinkedTrees.

        Raises:
            None

        Passes:
            IndexError: from _merge_query_vertices
            ValueError: from matching_smarter
    """
    mixed_queries = {""}
    assert isinstance(type_tree, type(pattern_tree))
    if not isinstance(type_tree, HyperLinkedTree):
        for type_string in type_tree:
            for pattern_string in pattern_tree:
                if not type_string: # equals ""
                    mixed_queries.add(pattern_string)
                    continue
                if not pattern_string: # equals ""
                    mixed_queries.add(type_string)
                    continue
                type_array = type_string.split(' ')
                pattern_array = pattern_string.split(' ')

                combination = combine_all(type_array, pattern_array)

                for item in combination:
                    query_string = str(item[0])

                    for i in range(1, len(item)):
                        query_string += ' ' + str(item[i])

                    new_query = Query()
                    new_query.set_query_string(query_string)
                    
                    if not param_smart_matching:
                        pattern_is_frequent = new_query.match_sample(sample, supp)
                    else:
                        new_query.set_query_matchtest('smarter')

                        pattern_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                                parent_dict = param_smart_matching[2], max_query_length=max_query_length)
                    matched_traces = new_query._query_matched_traces

                    if pattern_is_frequent:
                        new_query.query_string_to_normalform()
                        mixed_queries.add(new_query._query_string)
    else:
        support = ceil(sample._sample_size*supp)
        mixed_query_tree = HyperLinkedTree(support)

        t_root = type_tree.get_root()
        p_root = pattern_tree.get_root()
        m_root = mixed_query_tree.get_root()
        m_root.query_next_insert_index[0] = 0

        if len(t_root.child_vertices) == 0:
            vertex_list = sorted(pattern_tree.vertices_to_list(), key=lambda item: (item.query_string.count(' '),item.query_string),reverse=True)
            for vertex in vertex_list:
                parent_set = pattern_tree.find_parent_vertices(vertex)
                for parent in parent_set:
                    parent.descriptive=False
            return pattern_tree
        if len(p_root.child_vertices) == 0:
            vertex_list = sorted(type_tree.vertices_to_list(), key=lambda item: (item.query_string.count(' '),item.query_string),reverse=True)
            for vertex in vertex_list:
                parent_set = type_tree.find_parent_vertices(vertex)
                for parent in parent_set:
                    parent.descriptive=False
            return type_tree

        new_vertex_queue = []
        for vertex in t_root.child_vertices:
            new_vertex_queue.append((t_root,p_root,m_root,vertex))
        for vertex in p_root.child_vertices:
            new_vertex_queue.append((t_root,p_root,m_root,vertex))
        while new_vertex_queue:
            (t_vertex, p_vertex, m_vertex, evolved_vertex) = new_vertex_queue.pop(0)

            new_vertices = _merge_query_vertices(mixed_query_tree, t_vertex, p_vertex, m_vertex, evolved_vertex)
            if evolved_vertex.query_string[0] != '$':
                for new_vertex in new_vertices:
                    new_vertex_is_frequent = new_vertex.is_frequent(support)
                    if new_vertex_is_frequent is None:
                        new_query = Query()
                        new_query.set_query_string(new_vertex.query_string)
                        if not param_smart_matching:
                            query_is_frequent = new_query.match_sample(sample, supp)
                        else:
                            new_query.set_query_matchtest('smarter')
                            if new_query._query_string in  param_smart_matching[2]:
                                pass
                            else:
                                parentstring = m_vertex.query_string
                                parent = Query()
                                parent.set_query_string(parentstring)
                                param_smart_matching[2][new_vertex.query_string] = parent
                            query_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                                    parent_dict = param_smart_matching[2], max_query_length=max_query_length)
                        mixed_query_tree.set_match_results(new_vertex, new_query._query_matched_traces)

                        if not query_is_frequent:
                            continue
                    elif new_vertex_is_frequent is False:
                        continue

                    for vertex in new_vertex.parent_vertices:
                        vertex.descriptive = False

                    for child_vertex in evolved_vertex.child_vertices:
                        new_vertex_queue.append((evolved_vertex,p_vertex,new_vertex,child_vertex))
                    for child_vertex in p_vertex.child_vertices:
                        new_vertex_queue.append((evolved_vertex,p_vertex,new_vertex,child_vertex))
            else:
                for new_vertex in new_vertices:
                    new_vertex_is_frequent = new_vertex.is_frequent(support)
                    if new_vertex_is_frequent is None:
                        new_query = Query()
                        new_query.set_query_string(new_vertex.query_string)
                        if not param_smart_matching:
                            query_is_frequent = new_query.match_sample(sample, supp)
                        else:
                            if new_query._query_string in  param_smart_matching[2]:
                                pass
                            else:
                                parentstring = m_vertex.query_string
                                parent = Query()
                                parent.set_query_string(parentstring)
                                param_smart_matching[2][new_vertex.query_string] = parent
                            new_query.set_query_matchtest('smarter')
                            query_is_frequent = new_query.match_sample(sample, supp, dict_iter = param_smart_matching[0], patternset = param_smart_matching[1],
                                    parent_dict = param_smart_matching[2], max_query_length=max_query_length)
                        mixed_query_tree.set_match_results(new_vertex, new_query._query_matched_traces)

                        if not query_is_frequent:
                            continue
                    elif new_vertex_is_frequent is False:
                        continue

                    for vertex in new_vertex.parent_vertices:
                        vertex.descriptive = False

                    for child_vertex in t_vertex.child_vertices:
                        new_vertex_queue.append((t_vertex,evolved_vertex,new_vertex,child_vertex))
                    for child_vertex in evolved_vertex.child_vertices:
                        new_vertex_queue.append((t_vertex,evolved_vertex,new_vertex,child_vertex))
        mixed_queries = mixed_query_tree
    return mixed_queries
