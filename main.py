#!/usr/bin/python3
"""Contains the main function of this prototype"""
import argparse
import logging
from tests.test_discovery_bu_multidim import test_discovery_bu_multidim_separated_01

def main():
    """
        Main method
    """
    log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logger = logging.getLogger(__name__)

    # To override the default severity of logging
    logger.setLevel('INFO')

    # Use FileHandler() to log to console; use FileHandler("mylogs.log") to log to a file
    file_handler = logging.StreamHandler()
    formatter = logging.Formatter(log_format)
    file_handler.setFormatter(formatter)

    # Add the file handler
    logger.addHandler(file_handler)
    logger.debug("Started main method of prototype for SFB 1404 FONDA Subproject A1")

    parser = argparse.ArgumentParser(description='Proof of concept for Query Discovery')
    parser.add_argument("--demo", help="Default. Runs a small discovery example.")
    args = parser.parse_args()

    if not (args.demo or args.generate_simulation or args.run_simulation):
        parser.error('No action requested, add --demo')

    if args.demo:
        test_discovery_bu_multidim_separated_01()

    logger.debug("Finished main method")


if __name__ == "__main__":
    main()
