#!/usr/bin/python3
"""
Contains functions for discovering queries
from samples by using bottom up algorithms.
"""
import logging
from itertools import combinations
from collections import deque
import time
import numpy as np
from query import Query, string_to_normalform
from discovery_bu_pattern_type_split import bu_pattern_type_split_discovery

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def bottom_up_discovery(sample,
                        supp=1.0,
                        max_query_length=-1,
                        matchtest='smarter',
                        vert_db=None,
                        statistics=False):
    """
    Query Discovery by using a bottom up depth-first search.

    Args:
        sample: Sample instance.
        supp: Float between 0 and 1 which describes the requested support.
        vert_db: dictionary containing instances of occuring types
        statistics: True iff statistics should be run. Default False.

    Returns:
        set of queries if a query has been discovered, None otherwise.
        if vert_db: dictionary containing occurences in traces of each query.
        if statistics: dictionary containing statistics.

    Raises:
        InvalidQuerySupportError: Supp is less than 0 or greater than 1.
        ValueError: Valid matchtests are 
        'smarter', 'pattern-split' or 'pattern-split-sep'
    """
    if max_query_length == -1:
        max_query_length = sample._sample[0].count(' ') + 1
    if matchtest == 'smarter':
        return _discover_bottom_up_smarter(sample=sample,
                                           supp=supp,
                                           max_query_length=max_query_length,
                                           vert_db=vert_db,
                                           statistics=statistics)

    if matchtest in ['pattern-split-sep', 'pattern-split']:

        results = (
            bu_pattern_type_split_discovery(sample,
                                            supp,
                                            use_tree_structure=True,
                                            discovery_order='type_first',
                                            use_smart_matching=True,
                                            find_all_matching_queries=True, max_query_length=max_query_length)[0])
        result_dict = {}
        result_dict['matchingset'] = {}
        result_dict['queryset'] = set()
        instance_dictionary = {}
        sample_size = sample._sample_size
        trace_list=list(range(sample_size))
        for result_querystring in results:
            result_query = Query()
            result_query.set_query_string(result_querystring,
                                          recalculate_attributes=False)
            instance_dictionary = result_query.query_pos_dict(
                vert_db,
                sample,
                instance_dictionary, trace_list=trace_list)

        result_dict['instance_dictionary'] = instance_dictionary
        result_dict['parent_dict'] = {}
        # for querystring in instance_dictionary.keys():
        for querystring in results:
            if querystring:
                query = Query()
                query.set_query_string(querystring)
                query.set_pos_last_type_and_variable()
                result_dict['matchingset'][querystring] = query
                result_dict['queryset'].add(querystring)
        return result_dict

    else:
        raise ValueError("Valid matchtests are 'smarter', 'pattern-split' \
                         or 'pattern-split-sep'")


def _discover_bottom_up_smarter(sample, supp, max_query_length,
                                vert_db=None,
                                statistics=False):
    """
    Query Discovery by using a bottom up depth-first search
    using the matching variant smarter.

    Args:
        sample: Sample instance.
        supp: Float between 0 and 1 which describes the requested support.
        vert_db: dictionary containing instances of occuring types
        statistics: True iff statistics should be run. Default False.

    Returns:
        Set of queries if a query has been discovered, None otherwise.
        if vert_db: dictionary containing occurences in traces of each query.
        if statistics: dictionary containing statistics.
    """
    if vert_db:
        vsdb = vert_db
    else:
        vsdb = sample.get_vertical_sequence_database()
    alphabet = sample.get_supported_typeset(supp)
    sample_size = sample._sample_size
    trace_list = list(range(sample_size))
    matching_queries = {}

    patternset = set([key for key in vsdb.keys() for item in vsdb[key].keys()
                      if len(vsdb[key][item]) >= 2])
    querycount = 1
    queryset = {}
    instance_dictionary = {}
    if not alphabet and not patternset:
        result_dict = {}
        result_dict['queryset'] = {''}
        result_dict['matchingset'] = queryset
        result_dict['parent_dict'] = {}
        if vert_db:
            result_dict['instance_dictionary'] = instance_dictionary
        return result_dict

    query = Query()
    # query.set_query_discovery_algorithm('bottom_up')
    querystring = query._query_string
    query.set_query_matchtest('smarter')
    stack = deque(querystring)
    matching = True
    matchcount = 0
    dictionary = {"": matching}
    parent_dict = {}
    parent_dict[querystring] = query
    children = _next_queries(query, alphabet, max_query_length)
    parent_dict.update({child._query_string: query for child in children})
    stack.extend(children)
    dict_iter = {}
    non_descriptive = set()

    LOGGER.info('Start bottom up discovery - smarter')
    start_time = time.time()
    last_print_time = start_time
    while stack:
        query = stack.pop()
        querystring = query._query_string
        query.set_query_matchtest('smarter')
        querycount += 1
        current_time = time.time()
        if current_time - last_print_time > 300:
            LOGGER.info('Current query: %s; current stack size: %i; \
                        Current Query count: %i', querystring, len(stack), querycount)
            last_print_time = current_time
        parent = parent_dict[querystring]
        parentstring = parent._query_string
        if querystring not in dictionary:

            matching = query.match_sample(sample=sample, supp=supp,
                                          dict_iter=dict_iter,
                                          patternset=patternset,
                                          parent_dict=parent_dict)
            matchcount += 1
            dictionary = _update_dictionary(query=query, matching=matching,
                                            parent_dict=parent_dict,
                                            dictionary=dictionary)
            # dictionary.update({querystring:matching})

        if not matching or querystring.count(' ') >= max_query_length:
            if parentstring not in non_descriptive:
                queryset[parentstring] = parent
        else:
            matching_queries[querystring] = query
            non_descriptive.add(parentstring)
            if vert_db:
                instance_dictionary = query.query_pos_dict(vert_db, sample,
                                                           instance_dictionary,
                                                           trace_list= trace_list)
            children = _next_queries(query, alphabet, max_query_length)

            if children:
                stack.extend(children)
                parent_dict.update({child._query_string:
                                    query for child in children})

            else:
                queryset[querystring] = query
    if not vert_db:
        for querystring, query in queryset.items():
            non_descriptive.update(non_descriptive_queries(query, parent_dict))

    LOGGER.debug('Match count: %i',  matchcount)
    LOGGER.debug('Query count: %i', querycount)
    # LOGGER.debug('Descriptive queries: %s' ,str(list(queryset.keys())))
    result_dict = {}
    result_dict['parent_dict'] = parent_dict

    result_dict['queryset'] = set(queryset.keys()) - non_descriptive
    result_dict['matchingset'] = matching_queries
    if vert_db:
        result_dict['instance_dictionary'] = instance_dictionary
    if statistics:
        result_dict['statistics'] = statistics_sample_queries(queryset, non_descriptive, sample)
    return result_dict

#########################################################################################


def _next_queries(query, alphabet, max_query_length=10):
    """Given a query the function calculates the children of that query which
        are the next queries that are more specialised adding one element
        following the rule set.

    Args:
        query (Query): an instance of Query
        alphabet (list): list of supported types
        max_query_length (int): maximal number of events in a query

    Returns:
        children of the given query: list of query strings

    """
    variables = query._query_repeated_variables
    sorted_variables = sorted(variables)
    typeset = query._query_typeset
    num_of_vars = len(variables)
    querystring = query._query_string
    querylength = query._query_string_length
    pos_last_type_and_variable = query._pos_last_type_and_variable

    children = []

    # special case: empty querystring

    if not querystring:
        if max_query_length >= 2:
            child = "$x0 $x0"
            child_query = Query()
            child_query._query_string = child
            child_query._query_repeated_variables = {'x0'}
            child_query._query_string_length = 2
            child_query._pos_last_type_and_variable = np.array([-1, 0, 1])
            children.append(child_query)
        if max_query_length >= 1:
            for letter in alphabet:
                child_query = Query()
                child = str(letter)
                child_query._query_string = child
                child_query._query_typeset = typeset | {letter}
                child_query._query_string_length = 1
                child_query._pos_last_type_and_variable = np.array([0, -1, -1])
                children.append(child_query)


    # querystrings without variables
    elif num_of_vars == 0:
        if max_query_length >= querylength + 2:
            new_var = 0
            child = querystring + ' $x'+str(new_var) + ' $x'+str(new_var)
            child_query = Query()
            child_query._query_string = child
            child_query._query_repeated_variables = {'x0'}
            child_query._query_typeset = typeset
            child_query._query_string_length = querylength + 2
            child_query._pos_last_type_and_variable = np.array([querylength-1,
                                                                querylength,
                                                                querylength + 1])
            children.append(child_query)
        if max_query_length >= querylength + 1:
            for letter in alphabet:
                child = querystring+' '+str(letter)
                child_query = Query()
                child_query._query_string = child
                child_query._query_typeset = typeset | {letter}
                child_query._query_string_length = querylength + 1
                child_query._pos_last_type_and_variable = np.array(
                    [querylength, -1, -1])
                children.append(child_query)

    # querystrings containing variables
    else:
        if max_query_length >= querylength + 1:
            pos_last_type = pos_last_type_and_variable[0]
            pos_first_var = pos_last_type_and_variable[1]
            pos_last_var = pos_last_type_and_variable[2]


            # insert new variable twice: after last occurence of type and
            # after first occurence of last variable
            if max_query_length >= querylength + 2:
                for i in range(max(pos_last_type, pos_first_var), querylength):
                    prefix = " ".join(querystring.split()[:i+1])
                    suffix = " ".join(querystring.split()[i+1:])
                    new_var = int(sorted_variables[-1][1]) + 1
                    child = prefix + ' $x'+str(new_var) + ' ' + suffix
                    for j in range(i + 1, querylength+1):
                        prefix = " ".join(child.split()[:j+1])
                        suffix = " ".join(child.split()[j+1:])

                        child2 = prefix + ' $x'+str(new_var) + ' ' + suffix
                        child_query = Query()
                        child_query._query_string = child2.strip()
                        child_query._query_typeset = typeset
                        child_query._query_repeated_variables = variables | {
                            'x' + str(new_var)}
                        child_query._query_string_length = querylength + 2
                        child_query._pos_last_type_and_variable = np.array(
                            [pos_last_type, i+1, j+1])
                        children.append(child_query)

            # insert last inserted variable again
            if pos_first_var > pos_last_type:
                for i in range(pos_last_var, querylength):
                    prefix = " ".join(querystring.split()[:i+1])
                    suffix = " ".join(querystring.split()[i+1:])
                    child = prefix + ' $x'+sorted_variables[-1][1] + ' ' + suffix
                    child_query = Query()
                    child_query._query_string = child.strip()
                    child_query._query_typeset = typeset
                    child_query._query_repeated_variables = variables
                    child_query._query_string_length = querylength + 1
                    child_query._pos_last_type_and_variable = np.array(
                        [pos_last_type,
                         pos_first_var,
                         i+1])
                    children.append(child_query)

            # insert types: after last occurence of type and after first
            # occurence of last variable
            for i in range(max(pos_last_type, pos_first_var), querylength):
                prefix = " ".join(querystring.split()[:i+1])
                suffix = " ".join(querystring.split()[i+1:])

                for letter in alphabet:
                    child = prefix + ' ' + letter + ' ' + suffix
                    child_query = Query()
                    child_query._query_string = child.strip()
                    child_query._query_typeset = typeset | {letter}
                    child_query._query_repeated_variables = variables
                    child_query._query_string_length = querylength + 1
                    if i <= pos_last_var:
                        child_query._pos_last_type_and_variable = np.array(
                            [i+1,
                             pos_first_var,
                             pos_last_var+1])
                    else:
                        child_query._pos_last_type_and_variable = np.array(
                            [i+1,
                             pos_first_var,
                             pos_last_var])
                    children.append(child_query)

    return children

#########################################################################################


def _update_dictionary(query, matching, dictionary, parent_dict):
    """Adds new queries and their matching behavior to the dictionary

    Args:
        query: an instance of Query
        matching (Boolean): matching behavior of the query
        dictionary: Dictionary containing already seen queries and their matching behavior

    Returns:
        Updated Dictionary
    """
    querystring = query._query_string
    dictionary.update({querystring: matching})

    variables = query._query_repeated_variables
    num_of_vars = len(variables)
    querystring = query._query_string
    typeset = query._query_typeset

    if matching:
        if variables:
            if querystring.count('$') == (querystring.count('$x0') and
                                          num_of_vars > 3):
                var_liste = [symbol for symbol in querystring.split()
                             if symbol[0] == '$']
                var_string = ' '.join(var_liste)
                if var_string not in dictionary:
                    next_pattern = Query()
                    pattern_stack = [next_pattern]
                    while pattern_stack:
                        next_pattern = pattern_stack.pop()
                        next_pattern_string = next_pattern._query_string
                        if next_pattern_string.count('$') == len(var_liste):
                            if (next_pattern_string not in dictionary and
                                next_pattern_string != var_string):
                                dictionary[next_pattern_string] = True
                        else:
                            children = _next_queries(next_pattern, alphabet=[])
                            pattern_stack.extend(children)
                            parent_dict.update({
                                child._query_string:
                                next_pattern for child in children})

    else:
        # more specialised queries won't match either
        if variables:
            for variable in variables:
                for letter in typeset:
                    gen_querystring = " ".join(
                        [letter if querystring.split()[i] == f"${variable}"
                         else querystring.split()[i]
                         for i in range(len(querystring.split()))])
                    counter2 = 0
                    seen = set()
                    for event in gen_querystring.split():
                        if '$x' in event and event not in seen:
                            gen_querystring = gen_querystring.replace(
                                event, f"$x_{counter2}")
                            seen.add(event)
                            counter2 += 1
                    gen_querystring = gen_querystring.replace('_', '')

                    dictionary.update({gen_querystring: matching})

    return dictionary


def non_descriptive_queries(query, parent_dict):
    """
    Given a querystring it generates more general
    querystring that are not descriptive

    Args:
        querystring (string): string representation of query

    Returns:
        Set containing non descriptive querystrings
    """
    querystring = query._query_string
    non_descriptive_set = set()
    if not querystring:
        return non_descriptive_set
    query_liste = querystring.split()
    query_length = len(query_liste)
    variables = query._query_repeated_variables
    num_of_vars = len(variables)
    typeset = query._query_typeset

    if variables:
        for variable in variables:
            # replace variables by new variables if they occur more than 4 times
            variable_count = querystring.count(variable)
            counter = 2
            pos_list = {i for i in range(len(query_liste)) if query_liste[i] == '$' + variable}
            pos_pairs = set()
            while counter <= variable_count-2:
                pos_pairs.update(set(combinations(pos_list, counter)))
                counter += 1

            for pos_pair in pos_pairs:
                gen_querystring = " ".join([f"$x{num_of_vars}" if i in pos_pair else querystring.split()[i] for i in range(len(querystring.split()))])
                counter = 0
                seen = set()
                for event in gen_querystring.split():
                    if '$x' in event and event not in seen:
                        gen_querystring = gen_querystring.replace(event, f"$x_{counter}")
                        seen.add(event)
                        counter += 1

                gen_querystring = gen_querystring.replace('_', '')
                if gen_querystring not in parent_dict:
                    gen_querystring = string_to_normalform(gen_querystring)[0]
                if gen_querystring != querystring:
                    non_descriptive_set.add(gen_querystring)
    if typeset:
        for letter in typeset:
            # replace type by new variables if they occur more than once
            letter_count = querystring.count(letter)
            counter = 2
            pos_list = {i for i in range(len(query_liste)) if query_liste[i] == letter}
            pos_pairs = set()
            while counter <= letter_count:
                pos_pairs.update(set(combinations(pos_list, counter)))
                counter += 1
                for pos_pair in pos_pairs:
                    gen_querystring = " ".join([f"$x{num_of_vars}" if i in pos_pair else querystring.split()[i] for i in range(len(querystring.split()))])
                    counter2 = 0
                    seen = set()
                    for event in gen_querystring.split():
                        if '$x' in event and event not in seen:
                            gen_querystring = gen_querystring.replace(event, f"$x_{counter2}")
                            seen.add(event)
                            counter2 += 1
                    gen_querystring = gen_querystring.replace('_', '')
                    if gen_querystring not in parent_dict:
                        gen_querystring = string_to_normalform(gen_querystring)[0]
                    if gen_querystring != querystring:
                        non_descriptive_set.add(gen_querystring)

    subqueries = combinations(query_liste, query_length-1)
    subquerystrings = {' '.join(subquery) for subquery in subqueries}
    for subquery_string in subquerystrings:
        if subquery_string.count('$') != 0:
            if subquery_string not in parent_dict:
                normal_form = string_to_normalform(subquery_string)[0]
            else:
                normal_form = subquery_string
            non_descriptive_set.add(normal_form)
        else:
            non_descriptive_set.add(subquery_string)
    return non_descriptive_set

#########################################################################################


def query_match_pos(matchtest, domain_query, domain_dictionary):
    """For a given query it returns the range of the first match for each trace

    Args:
        matchtest (String): Match variant: 'finditer' or 'smarter'
        domain_queries (list): list of queries of a domain
        domain_dictionary (dictionary): dictionary containing matching information for each query

    Returns:
        match_start: {trace_id : start_pos} or {trace_id : {matching_group: start_pos}}
        match_end: {trace_id: end_pos } or {trace_id : {matching_group: end_pos}}
    """

    match_end = domain_dictionary[domain_query.replace(';', '')]
    if domain_query.split()[0].count('$') == 0:
        match_start = domain_dictionary[domain_query.split()[0].replace(';', '')]
    else:
        if matchtest == 'finditer':
            match_start = {x: 0 for x in list(match_end.keys())}

        if matchtest == 'smarter':
            match_start = {}
            for trace_id in domain_dictionary['$x0 $x0']:
                match_start[trace_id] = {}
                for entries in match_end[trace_id].keys():
                    for entry in entries:
                        match_start[trace_id][entry[0]] = domain_dictionary[entry[0]][trace_id]
    return match_start, match_end


def statistics_sample_queries(queryset, non_descriptive, sample):
    """Calculates statistics on the resulting queryset and non-descriptive query set.

    Args:
        queryset (set): Set of discovered queries
        non_descriptive (set): set of non descriptive queries.
        sample (Sample): instance of Sample

    Returns:
        Set of descriptive queries and a dictionary containing statistics.
    """
    # stats= sample.sample_stats()
    stats = {}
    alphabet = sample.get_supported_typeset(1.0)
    patternset = []
    sample_set = sample._sample
    for letter in sample._sample_typeset:
        for trace in sample_set:
            if trace.split(' ').count(letter) >= 2:
                patternset.append(letter)
                break
    stats['Supported Types: '] = alphabet
    stats['Supported Pattern-Types: '] = patternset
    stats['min trace length: '] = sample.get_sample_min_trace().count(" ") + 1
    stats['max trace length: '] = sample.get_sample_max_trace().count(" ") + 1
    des_t = set()
    des_p = set()
    des_m = set()
    for querystring in queryset:
        if querystring.count('$') == 0:
            des_t.add(querystring)
        elif querystring.count('$') == querystring.count(' ') + 1:
            des_p.add(querystring)
        else:
            des_m.add(querystring)
    stats['Descriptive Queries'] = [len(des_t), len(des_p), len(des_m)]
    des_t = set()
    des_p = set()
    des_m = set()
    for querystring in non_descriptive:
        if querystring.count('$') == 0:
            des_t.add(querystring)
        elif querystring.count('$') == querystring.count(' ') + 1:
            des_p.add(querystring)
        else:
            des_m.add(querystring)
    stats['Non-Descriptive Queries'] = [len(des_t), len(des_p), len(des_m)]
    return stats
########################################################################################
