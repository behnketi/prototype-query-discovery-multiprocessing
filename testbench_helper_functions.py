#!/usr/bin/python3
"""Contains functions to read and write samples to / from files and generate experiments"""
import os
import re
import logging
import time
import gzip
import msgpack
from generator import SampleGenerator
from generator_multidim import MultidimSampleGenerator

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

FILENAME_REGEXP = '^(?P<sample_size>[^\\.]*)\\.(?P<min_trace_length>[0-9]*)\\.(?P<max_trace_length>[0-9]*)\\.(?P<type_length>[-]*[0-9]*)\\.(?P<event_dimension>[-]*[0-9]*)\\.(?P<dataset>[^\\.]*)\\.seq'

def _write_sample(sample:list, filename:str) -> None:
    """
        Packs a _sample, described in class (Multidim)Sample, in a binary file.

        Note that _sample is only an attribut of a (Multidim)Sample instance,
        i.e. a list of strings, where each string represents a trace.

        Args:
            sample: The sample, which has to be stored, as a list.

            filename: A string which stores the name of the binary file the
            user wants to create.
    """
    LOGGER.debug('_write_sample - Starting')
    if len(sample)==0:
        raise RuntimeError("No sample given")

    if not os.path.exists('samples'):
        os.mkdir('samples')

    with open('samples/'+filename+'.msgpack', 'wb') as outfile:
        msgpack.pack(sample, outfile, use_bin_type=True)
    LOGGER.debug('_write_sample - Finished')

def _read_sample(filename:str) -> list:
    """
        Loads and unpacks a binary file which contains a list of traces.

        Args:
            filename: A string which stores the name of the binary file the
            user wants to read.

        Returns:
            The list of traces which is stored in file.
    """
    LOGGER.debug('_read_sample - Starting')
    if filename is None:
        raise RuntimeError("No name of file given")
    with open('samples/'+filename+'.msgpack', 'rb') as read_file:
        sample = msgpack.unpack(read_file, raw=False)

    LOGGER.debug('_read_sample - Finished')
    return sample

def _dataset_to_sample(path_to_file:str, cutoff=-1, trace_len=-1, multidim:bool=True, types:list|None=None) -> None:
    """
        Loads a txt.gz file and writes a binary file.

        Args:
            path_to_file: String containing the path to a specific dataset
                which should be converted to a sample. Dataset should be stored
                as '*txt.gz'. We assume the txt.gz file to be in the correct
                format, i.e. each line consists of types, separated by a
                whitespace.

            cutoff [=-1]: If set to an integer >0, only the last cutoff events
                of each trace in file build the trace which is added to the
                sample.

            multidim [=True]: If True calculates event dimension for filename.

            types [=None]: List of types which have to be contained in a trace.
                A trace is only added to the sample, if it contains all types
                in this list.
    """
    LOGGER.info('_dataset_to_sample - Starting')
    LOGGER.info('_dataset_to_sample - Filename: %s', path_to_file)
    #path_to_filename = 'datasets/google/GoogleTraces_BTW23_using_ILMiner_Queries/'+filename
    path_to_filename = path_to_file
    if not os.path.exists(path_to_filename):
        LOGGER.info('_dataset_to_sample - Filename %s does not exist!', path_to_file)
        return
    file = gzip.open(path_to_filename, 'rb')
    sample = []
    line_counter = 0
    sample_min_trace = ""
    sample_max_trace = ""
    start = time.time()
    for trace in file:
        line_counter+=1
        # Only use the last cutoff events for the trace, if cutoff > -1
        if cutoff > -1:
            next_trace = ''.join(trace.decode().split()[-cutoff:])
        else:
            next_trace = ''.join(trace.decode())
        # Delete whitespace character at the end of next_trace if necessary
        alphabet = {'0','1','2','3','4','5','6','7','8','9','.',';'}
        while next_trace[len(next_trace)-1] not in alphabet:
            next_trace = next_trace[:-1]
        # Ignore trace if its length exceeds the given threshold
        if trace_len>-1 and next_trace.count(" ")+1 > trace_len:
            continue
        # Update min_trace and max_trace
        if next_trace.count(" ") < sample_min_trace.count(" ") or len(sample_min_trace) == 0:
            sample_min_trace = next_trace
        if next_trace.count(" ") > sample_max_trace.count(" ") or len(sample_max_trace) == 0:
            sample_max_trace = next_trace
        # Only append the trace if it contains all wanted types
        append = True
        if types is not None:
            assert isinstance(types,list)
            for wanted_type in types:
                if wanted_type not in next_trace:
                    append = False
                    break
        if append is True:
            sample.append(next_trace)
    file.close()
    end = time.time() - start
    LOGGER.info('_dataset_to_sample - Finished reading file %s: %i lines in %f seconds', path_to_file, line_counter, end)

    # Build the filename
    LOGGER.info('_dataset_to_sample - Build sample_filename')
    sample_size = len(sample)
    sample_min_trace_len = sample_min_trace.count(" ") + 1
    sample_max_trace_len = sample_max_trace.count(" ") + 1
    sample_type_len = -1
    sample_filename = str(sample_size) + "." + str(sample_min_trace_len) + "." + str(sample_max_trace_len) + "." + str(sample_type_len) + "."
    if multidim is True:
        sample_event_dimension = (sample[0].split(" "))[0].count(";")
        sample_filename = sample_filename + str(sample_event_dimension) + "."
    filename = path_to_file.split("/")[-1]
    sample_filename = sample_filename + filename.split(".")[0] + ".seq"

    # Write the sample to a file
    LOGGER.info('_dataset_to_sample - Write sample to /samples')
    _write_sample(sample, sample_filename)
    LOGGER.info('_dataset_to_sample - Finished')

def _generate_experiments(convert_datasets:bool=False, path_to_file:str="",params_sample_size:list|None=None, params_trace_length:list|None=None, params_type_length:int=-1, params_dimension:list|None=None) -> None:
    """
        Generates & writes samples with different size, trace- and typelength.

        Uses the SampleGenerator. Creates "done.txt" in the end. Creates a
        *.seq.mpack and a *.done.txt file for each experiment. Parameters can
        be extracted from the filename using FILENAME_REGEXP.

        Args:
            convert_datasets: Boolean which indicates whether datasets stored
                in 'datasets/' should be converted into samples. If set to True
                no synthetic samples are generated. Datasets should be stored
                as '*txt.gz'.

            path_to_file: String containing the path to a specific dataset
                which should be converted to a sample. Dataset should be stored
                as '*txt.gz'.

            params_sample_size: List of integer which represents the number of
                traces which has to be generated per sample.

            params_trace_length: List of integer tuples. The first element of
                each tuple defines the minimum trace length, the second defines
                the maximum trace length of each trace of the current sample.

            params_type_length: Integer which can be used to define the length
                of each type. Default -1 means that the length will be
                calculated depending on the maximal trace length and the size
                of the sample.

            params_dimension: List of integers representing the event dimension
                for each (multidimensional) sample. Default None implies that
                only onedimensional samples will be generated.
    """
    LOGGER.debug('_generate_experiments - Starting')

    if not os.path.exists('samples'):
        os.mkdir('samples')

    if os.path.exists('datasets') and convert_datasets is True:
        if len(path_to_file)>0 and os.path.exists(path_to_file):
            _dataset_to_sample(path_to_file)
        else:
            entries = os.listdir("datasets/")
            for entry in entries:
                if entry.split(".")[-1]=='gz':
                    entry_filename = "datasets/"+str(entry)
                    _dataset_to_sample(entry_filename)
        return

    multidim = True
    if params_sample_size is None:
        params_sample_size = [50,100,150,200,250,300,350,400,450,500]
    if params_trace_length is None:
        params_trace_length = [(10,50),(50,100),(100,150),(150,200)]
    if params_dimension is None:
        params_dimension = [1]
        multidim = False

    log_params_sample_size = '_generate_experiments - params sample sizes: '+" ".join(str(params_sample_size))
    log_params_trace_length = '_generate_experiments - params trace length: '+" ".join(str(params_trace_length))
    LOGGER.debug(log_params_sample_size)
    LOGGER.debug(log_params_trace_length)

    for sample_size, trace_length, event_dimension in [  (sample_size, trace_length, event_dimension)
                                        for sample_size in params_sample_size
                                        for trace_length in params_trace_length
                                        for event_dimension in params_dimension]:
        filename_base_random = f"{sample_size}.{str(trace_length[0])}.{str(trace_length[1])}.{str(params_type_length)}.{str(event_dimension)}.{'random'}"
        filename_base_fragmentation_gauss = f"{sample_size}.{str(trace_length[0])}.{str(trace_length[1])}.{str(params_type_length)}.{str(event_dimension)}.{'fragmentation-gauss'}"
        filename_base_fragmentation_quartered = f"{sample_size}.{str(trace_length[0])}.{str(trace_length[1])}.{str(params_type_length)}.{str(event_dimension)}.{'fragmentation-quartered'}"

        filename_random = filename_base_random + ".seq"
        filename_fragmentation_gauss = filename_base_fragmentation_gauss + ".seq"
        filename_fragmentation_quartered = filename_base_fragmentation_quartered + ".seq"

        filename_random_done = filename_base_random + ".done.txt"
        filename_fragmentation_gauss_done = filename_base_fragmentation_gauss + ".done.txt"
        filename_fragmentation_quartered_done = filename_base_fragmentation_quartered + ".done.txt"

        if os.path.isfile(filename_base_random + ".done.txt") and os.path.isfile(filename_base_fragmentation_gauss + ".done.txt"):
            continue

        if multidim is True:
            generator = MultidimSampleGenerator()
            sample_random = generator.generate_random_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length, event_dimension=event_dimension)
            sample_fragmentation_gauss = generator.generate_fragmentation_gauss_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length, event_dimension=event_dimension)
            sample_fragmentation_quartered = generator.generate_fragmentation_quartered_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length, event_dimension=event_dimension)
        else:
            generator = SampleGenerator()
            sample_random = generator.generate_random_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length)
            sample_fragmentation_gauss = generator.generate_fragmentation_gauss_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length)
            sample_fragmentation_quartered = generator.generate_fragmentation_quartered_sample(sample_size=sample_size, min_trace_length=trace_length[0], max_trace_length=trace_length[1], type_length=params_type_length)

        # check if filename is parsable
        parsed_params = re.match(FILENAME_REGEXP,filename_random)
        assert parsed_params is not None
        assert int(parsed_params.group('sample_size'))==sample_size
        assert int(parsed_params.group('min_trace_length'))==trace_length[0]
        assert int(parsed_params.group('max_trace_length'))==trace_length[1]
        assert int(parsed_params.group('type_length'))==params_type_length
        assert int(parsed_params.group('event_dimension'))==event_dimension
        parsed_params = re.match(FILENAME_REGEXP,filename_fragmentation_gauss)
        assert parsed_params is not None
        assert int(parsed_params.group('sample_size'))==sample_size
        assert int(parsed_params.group('min_trace_length'))==trace_length[0]
        assert int(parsed_params.group('max_trace_length'))==trace_length[1]
        assert int(parsed_params.group('type_length'))==params_type_length
        assert int(parsed_params.group('event_dimension'))==event_dimension
        parsed_params = re.match(FILENAME_REGEXP,filename_fragmentation_quartered)
        assert parsed_params is not None
        assert int(parsed_params.group('sample_size'))==sample_size
        assert int(parsed_params.group('min_trace_length'))==trace_length[0]
        assert int(parsed_params.group('max_trace_length'))==trace_length[1]
        assert int(parsed_params.group('type_length'))==params_type_length
        assert int(parsed_params.group('event_dimension'))==event_dimension

        _write_sample(sample_random._sample, filename_random)
        _write_sample(sample_fragmentation_gauss._sample, filename_fragmentation_gauss)
        _write_sample(sample_fragmentation_quartered._sample, filename_fragmentation_quartered)
        open('samples/' + filename_random_done, 'a', encoding='utf-8').close()
        open('samples/' + filename_fragmentation_gauss_done, 'a', encoding='utf-8').close()
        open('samples/' + filename_fragmentation_quartered_done, 'a', encoding='utf-8').close()

    done_file = open('samples/done.txt', 'w', encoding='utf-8')
    done_file.write("Generate experiment done!")
    done_file.close()
    LOGGER.debug('_generate_experiments - Finished')
