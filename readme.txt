README
=======

This is a prototype for SFB 1404 FONDA Subproject A1.

Currently Queries and Samples are implemented as classes. Query attributes can
be set and a query can be checked either against a trace or against a whole 
sample, using a support. Details on Queries and Samples can be found within the
class descriptions.

To check whether a trace or a sample matches a query, the query can be
transformed into normalform if neither gap constraints nor a global window size
is given. Currently the only implemented matchtest needs a regex, hence a query
string can be transformed into a regex as well.

Given a sample and a support the prototype is able to discover a query which is
matched by the sample considering the support. Different versions of Shinoharas
Algorithm are implemented, where shinohara_discovery_icdt is the most general
one, including the other three variants.

All implemented classes and functions should be documented. Please send a mail
to kleemeis@informatik.hu-berlin.de otherwise.


REQUIREMENTS
--------------------------------------------
Please make sure that you are using python3. You may need to install further
packages by using the python3 package installer pip3. If you haven't already
please install [1]
    - matplotlib
    - numpy
    - seaborn
    - iteration_utilities
    - regex
    - pandas
    - msgpack

Please send a mail to kleemeis@informatik.hu-berlin.de if further packages
should be added to this list.

If you are using Windows installing a WSL may be helpful, e.g. Ubuntu [4].


HOW TO USE THE PROTOTYPE VIA MAIN FUNCTION
--------------------------------------------
The current main function is able to run a small discovery example as a demo,
to generate and save samples in a directory 'samples/' and to run a simulation
which requires some samples.

    "python3 main.py --help"
    "python3 main.py --demo DEMO"
    "python3 main.py --generate_experiment GEN_EXPERIMENT"
    "python3 main.py --run_simulation RUN_SIMULATION"

Currently the program isn't able to handle further user inputs like parameters
for sample generating or query discovery. You can adapt the generated sample
size and the trace length in the main function, line 36, by assigning values to
the optional parameters, e.g.

    "_generate_experiments(  params_sample_size=[50,100,150],
                             params_trace_length=[(50,100),(100,150)])"

will generate 2 samples per sample size, where the first or second consists of
traces which length is between 50 and 100 or 100 and 150 types, respectively.

An alteration of the discovery parameters, e.g. the support or the choice of 
the discovery algorithm, is only possible by editing the global parameters in
run_simulation.py.


HOW TO USE THE PROTOTYPE VIA PYTEST
--------------------------------------------
If you like to run (some of) the implemented tests please install pytest [2]
and type 

    "[python -m] pytest [-s] [-k pattern]"

in your terminal. Please make sure that you use python3. Non-windows user do
not need the prefix "python -m". The option -s enforces the print-statements
contained within the tests. By using -k you can specify which tests you want
to execute. For example

    "[python -m] pytest -s -k test_shinohara_discovery_icdt_multidim_for_multidimensional_queries"

will execute each test which has a name which starts with the given pattern and
print all print-statements to your terminal. Hence

    "[python -m] pytest -s -k test_shinohara"

will execute all tests in all test files, which start with "test_shinohara". To
execute all implemented tests type

    "[python -m] pytest ."

which currently takes round about 5 minutes.

To check the test coverage of a specific file install pytest-cov [3] and use
the command

    "pytest -cov [filename]"

Note that no .py at the end of filename is needed.


Implementation details
--------------------------------------------
    - functions which start with an underscore are meant to be private
    - instead of print statements logger are used
    - please define tests if you add new functions
    - please try if pytest . is successful before creating a merge request
    - note that changes can not be pushed directly to master but to a (new)
      branch


Future work:
--------------------------------------------
    - Shinohara variants
    - further sample kinds
    - further match tests
    - improve plots
    - allow further user input like parameters for sample generating and query
      discovery


[1] pip3 install matplotlib
    pip3 install numpy
    pip3 install seaborn
    pip3 install iteration-utilities
    pip3 install pandas
    pip3 install msgpack
[2] pip3 install pytest
[3] pip3 install pytest-cov
[4] https://www.microsoft.com/de-de/p/ubuntu-2004-lts/9n6svws3rx71#activetab=pivot:overviewtab
