#!/usr/bin/python3
"""Contains a class for generating samples for experiments"""
import logging
import time
import gzip
import os.path
import sys
sys.path.append(".")
from experiments.testbench_multidim import match_algos, generate_plots
from experiments.evaluation import evaluate
from datasets.google.FirstPaper.google_trace_generation import query1, query2, query2_rl, query3

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def main():
    """Contains the main function of this file"""
    result_path = 'experiments/results/'
    if not os.path.isdir(result_path):
        os.mkdir(result_path)
    # test_queries()
    test_query2_rl()


def test_queries():
    results = []
    for i in range(1, 2):
        #data_file = open(f'experiments/google_{i}_results.txt', 'w', encoding="utf-8")
        LOGGER.info('Started file %i', i)
        # for j in [50, 100, 200, 500]:
        # LOGGER.info('Length of traces: %i', j)
        file = gzip.open(f'datasets/Sigmod2024/google_query{i}.txt.gz', 'rb')
        file_path = f'datasets/google/FirstPaper/samples/google_query{i}.txt.gz'
        if not os.path.isfile(file_path):
            if i == 1:
                query1()
            elif i == 2:
                query2()
            else:
                query3()
        file = gzip.open(file_path, 'rb')
        sample_set2 = []
        counter = 0
        start= time.time()
        for trace1 in file:

            counter+=1
            # sample_set2.append(' '.join(trace1.decode().split()))
            sample_set2.append(trace1.decode().replace('nan', ''))

        file.close()
        result = time.time() - start
        LOGGER.info('Finished reading file %i: %i lines in %f seconds', i, counter, result)

        results = match_algos(sample_list= sample_set2, results=results, iterations=3, j=1, mod='all', discovery=['unified', 'separated', 'separated_ps'])
    generate_plots(results=results, file_name='google_traces', y="time", x='iterations', hue="discovery algorithm",kind="scatter", col='iterations', col_wrap= 3)


def test_query2_rl():
    file_path = 'datasets/google/FirstPaper/samples/google_query2_rl.txt.gz'
    if not os.path.isfile(file_path):
        query2_rl()
    file = gzip.open(file_path, 'rb')
    sample_set2 = []
    start = time.time()
    counter = 0
    for _, trace1 in enumerate(file):

        # sample_set2.append(' '.join(trace1.decode().split()))
        sample_set2.append(' '.join(trace1.decode().split()))
        if counter == 125:
            break
        counter += 1

    file.close()
    result = time.time() - start
    LOGGER.info('Finished reading file: %i lines in %f seconds', counter, result)
    results = []
    results = match_algos(sample_list=sample_set2, results=results, iterations=3, j=1, mod='all', discovery=['unified'])
    querysets_dict = {}
    querysets_dict['base'] = {'4;; 4;; 4;; 4;;'}
    querysets_dict['rl'] = {'4;2;', '1;0;', '1;2;', '4;2; 1;2;', '1;9;'}
    querysets_dict['unified'] = results[0][7]
    querysets_dict['il-miner'] = results[1][7]
    evaluate(querysets_dict, sample_set2)


if __name__ == "__main__":
    main()
