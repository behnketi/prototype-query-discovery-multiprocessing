#!/usr/bin/python3
"""Contains functions to test perfomance of the different algorithms on syntatically created multidim-samples."""
import logging
import time
import os
import sys
sys.path.append(".")

from experiments.testbench_multidim import match_algos, generate_plots
from experiments.evaluation import evaluate
from datasets.finance.FirstPaper.finance_trace_generation import query3, traces_by_date, traces_by_ticker

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main():
    """
        Contains the main function of this file. Runs all the created test functions.
    """
    result_path = 'experiments/results/'
    if not os.path.isdir(result_path):
        os.mkdir(result_path)
    # test_by_date()
    # test_by_ticker()
    # evaluate_query3()
    query_sample()


def test_finance_traces(trace_length=15):
    results = []
    directory = 'datasets/finance/FirstPaper/samples'
    dataframe = None
    for i,filename in enumerate(os.listdir(directory)):
        file_path = os.path.join(directory, filename)
        if os.path.isfile(file_path):
            LOGGER.info('Started file %s', filename)
            with open(file_path, 'r', encoding="utf-8") as file:
                sample_list = []
                counter = 0
                start = time.time()
                for trace1 in file:

                    counter += 1
                    sample_list.append(' '.join(trace1.split()[-trace_length:]))
                    if counter == 1000:
                        break

            result = time.time() - start
            LOGGER.info('Finished reading file %i: %i lines in %f seconds', i, counter, result)


            results = match_algos(sample_list= sample_list, results=results, iterations=18, j=i, mod='all', discovery=['separated'], max_query_length=trace_length)
            dataframe = generate_plots(results=results, file_name='finance_query3', y="time", x="iterations", hue="discovery algorithm",kind="scatter")
    return dataframe

def test_by_date():
    results = []
    LOGGER.info('Started reading file')
    file_path = 'datasets/finance/FirstPaper/samples/finance_traces_by_date.txt'
    if not os.path.isfile(file_path):
        traces_by_date()
    with open(file_path, 'r', encoding="utf-8") as file:
        sample_list = []
        counter = 0
        start= time.time()
        for trace1 in file:

            counter+=1
            sample_list.append(' '.join(trace1.split()[:-1]))
            # if counter == 1000:
            #     break

    result = time.time() - start
    LOGGER.info('Finished reading file: %i lines in %f seconds', counter, result)

    sorted_list = sorted(sample_list, key=len)
    results = match_algos(sample_list= sorted_list, results=results, iterations=18, j=1, mod='all', discovery=['unified', 'separated', 'separated_ps'])
    return generate_plots(
        results=results,
        file_name='finance_traces_by_date',
        y="time",
        x="max query length",
        hue="discovery algorithm",
        kind="scatter",
    )

def test_by_ticker():
    results = []
    dataframe = None
    directory = 'datasets/finance/FirstPaper/samples/ticker'
    file_list = ['all_0', 'ew_100', 'ed_100', 'ew_10', 'ed_10']
    for _,filename in enumerate(file_list):
        file_path = f'{directory}finance_ticker_{filename}.txt'
        if not os.path.isfile(file_path):
            traces_by_ticker()
        LOGGER.info('Started file %s', filename)
        with open(file_path, 'r', encoding="utf-8") as file:
            sample_list = []
            LOGGER.info('Started reading file')
            sample_list = []
            counter = 0
            start= time.time()
            for trace1 in file:

                counter+=1
                sample_list.append(' '.join(trace1.split()))

        result = time.time() - start
        LOGGER.info('Finished reading file: %i lines in %f seconds', counter, result)

        sorted_list = sorted(sample_list, key=len)
        results = match_algos(sample_list= sorted_list, results=results, iterations=18, j=filename, mod='all', discovery=['unified', 'separated', 'separated_ps'])
        dataframe = generate_plots(results=results, file_name='finance_traces_by_ticker', y="time", x="iterations", hue="discovery algorithm",kind="scatter")
    return dataframe

def evaluate_query3():
    file_path = 'datasets/finance/FirstPaper/samples/finance_query3.txt'
    dir_path = 'datasets/finance/FirstPaper/samples'
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)
    if not os.path.isfile(file_path):
        query3()
    with open(file_path, 'r', encoding="utf-8") as file:
        sample_set2 = []
        start = time.time()
        counter = 0
        for trace1 in file:
            counter += 1
            sample_set2.append(' '.join(trace1.split()[:-100]))
            if counter == 5000:
                break

    result = time.time() - start
    LOGGER.info('Finished reading file: %i lines in %f seconds', counter, result)
    results = []
    results = match_algos(sample_list= sample_set2, results=results, iterations=3, j=1, mod='all', discovery=['unified', 'il-miner'])
    querysets_dict = {
        'base': {';;$x0;; ;;$x0;; ;;$x0;; ;;$x0;;'},
        'unified': results[0][7],
        'il-miner': results[1][7],
    }
    evaluate(querysets_dict, sample_set2)

def query_sample():
    discoveries = ['unified_ps']
    file_path = 'datasets/finance/FirstPaper/samples/finance_query3_100_10.txt'
    results = []
    with open(file_path, 'r', encoding="utf-8") as file:
        sample_list = list(file)
    results = match_algos(sample_list=sample_list, results=results, iterations=1, mod=1, j=1, discovery=discoveries)

if __name__ == "__main__":
    main()
