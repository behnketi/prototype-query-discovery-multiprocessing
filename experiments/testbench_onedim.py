#!/usr/bin/python3
"""Contains functions to test perfomance of the different algorithms on syntatically created samples."""
import logging
import time
import random
from copy import deepcopy
from datetime import datetime
import seaborn as sns
import pandas as pd
from generator import SampleGenerator
from discovery_bottom_up import bottom_up_discovery
from discovery_bu_pattern_type_split import bu_pattern_type_split_discovery
from sample import Sample

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def main():
    """
        Contains the main function of this file. Runs all the created test functions for different sample sizes and trace lengths.
    """
    LOGGER.info('Empty Querysets')
    # empty_queryset()

    sample_size = 2
    min_trace_length = 100

    generator = SampleGenerator()
    generator.generate_sample_w_empty_queryset(sample_size=sample_size, min_trace_length=min_trace_length, max_trace_length=min_trace_length)

    for j in [5, 10, 15]:
        LOGGER.info('Max query length: %i' , j)
        LOGGER.info('Max query length: %i Type Queries', j)
        type_queries(sample = generator._sample, max_query_length=j)
        LOGGER.info('Max query length: %i Type Queries 2', j)
        type_queries_shuffled(sample = generator._sample, max_query_length=j)
        LOGGER.info('Max query length: %i Pattern Queries', j)
        pattern_queries(sample = generator._sample, max_query_length=j)
        LOGGER.info('Max query length: %i Pattern Queries 2', j)
        pattern_queries_shuffled(sample = generator._sample, max_query_length=j)
        LOGGER.info('Max query length: %i Mixed Queries',j)
        mixed_queries(sample = generator._sample, max_query_length=j)

def empty_queryset():
    """
        Plots running time for unified and type-pattern discovery using various trace lengths and sample sizes where the resulting queryset is empty.
    """
    columns = ['sample size', 'trace length', 'matching', 'time']
    results = []
    min_trace_lengths= [10, 20, 50, 100]
    for min_trace_length in min_trace_lengths:
        for i in range(2,100):
            LOGGER.info('Empty Querysets - Sample size: %i Trace length: %i' , i, min_trace_length)
            generator = SampleGenerator()
            generator.generate_sample_w_empty_queryset(sample_size=i, min_trace_length=min_trace_length, max_trace_length=min_trace_length)
            sample1 = deepcopy(generator._sample)
            sample2 = deepcopy(generator._sample)

            start= time.time()
            queryset = bottom_up_discovery(sample1, supp=1., matchtest='smarter')
            result= time.time()-start
            results.append([i, min_trace_length, 'unified',result])
            assert queryset== {''}
            start= time.time()
            queryset = set(bu_pattern_type_split_discovery(sample2, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
            result= time.time()-start
            results.append([i, min_trace_length, 'type-pattern',result])
            assert queryset == {''}

    plot_results(results, columns, 'empty_queryset', x_axis='sample size', y_axis='time', hue='matching', col = 'trace length')

def type_queries(sample, max_query_length):
    """
        Plots running times of unified and type-pattern discovery for given sample size and trace length where queryset consists only of types.
        Runs generated samples on both (unified and separated) discoveries plotting execution time depending on query length.

    Args:
        sample_size (int): number of traces. Defaults to 15.
        trace_length (int): length of each trace. Defaults to 15.
    """
    columns = ['max_query_length', 'matching', 'time', 'supported types', 'queryset', 'max query length']
    results= []
    start= time.time()
    queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
    result= time.time()-start

    results.append([max_query_length, 'unified',result, 0, len(queryset)])

    start= time.time()
    queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
    result= time.time()-start
    results.append([max_query_length, 'type-pattern',result, 0, len(queryset)])

    trace0 = sample._sample[0]
    new_sample_set = sample._sample

    for pos, letter in enumerate(trace0.split()[-(max_query_length):]):
        LOGGER.info('Type Queries - Query length: %i Current position: %i' , max_query_length, pos)

        #for trace in new_sample_set:
        trace = new_sample_set[1]
        new_trace = ' '.join(trace.split()[:pos] + [letter] + trace.split()[pos+1:])
        new_sample_set[1] = new_trace
        sample1 = Sample()
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = bottom_up_discovery(sample1, supp=1., matchtest='smarter')
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'unified',result, pos+1, len(queryset), max_query])
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = set(bu_pattern_type_split_discovery(sample1, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'type-pattern',result, pos +1, len(queryset), max_query])
    filename= f'type_queries_{max_query_length}'

    plot_results(results, columns, file_name= filename, x_axis='max query length', y_axis='time', hue='matching')

def type_queries_shuffled(sample, max_query_length):
    """
        Plots running times of unified and type-pattern discovery for given sample size and trace length where queryset consists only of types.
        Runs generated samples on both (unified and separated) discoveries plotting execution time depending on query length.

    Args:
        sample_size (int): number of traces. Defaults to 15.
        trace_length (int): length of each trace. Defaults to 15.
    """
    columns = ['max query length', 'matching', 'time', 'supported types', 'queryset', 'query length']
    results= []

    start= time.time()
    queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
    result= time.time()-start

    results.append([max_query_length, 'unified',result, 0, len(queryset),0])

    start= time.time()
    queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
    result= time.time()-start
    results.append([max_query_length, 'type-pattern',result, 0, len(queryset),0])

    trace0 = sample._sample[0]
    trace0_list = trace0.split()
    trace0_length = len(trace0_list)
    pos_list = random.sample(range(trace0_length), trace0_length)
    sample1=Sample()
    new_sample_set = sample._sample
    for i, letter in enumerate(trace0_list[:max_query_length]):
        LOGGER.info('Type Queries Shuffled - Query length: %i Current position: %i' , max_query_length, i)
        pos = pos_list[i]

        #new_sample_set = []
        #for trace in sample_set:
        trace = new_sample_set[1]
        new_trace = ' '.join(trace.split()[:pos] + [letter] + trace.split()[pos+1:])
        new_sample_set[1] = new_trace
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = bottom_up_discovery(sample1, supp=1., matchtest='smarter')
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'unified',result, i+1, len(queryset), max_query])

        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = set(bu_pattern_type_split_discovery(sample1, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'type-pattern',result, i+1, len(queryset), max_query])
    filename= f'type_queries2_{max_query_length}'

    plot_results(results, columns, filename, x_axis='supported types', y_axis='time', hue='matching', size = 'query length')

def pattern_queries(sample, max_query_length):
    """
        Creates Samples of given sample size and trace length, generates pattern queries and records and plots performance for different matching algorithms.
        Runs generated samples on both (unified and separated) discoveries plotting execution time depending on query length.
    Args:
        sample_size (int): number of traces. Defaults to 5.
        trace_length (int): length of each trace. Defaults to 10.
    """

    columns = ['max query length', 'matching', 'time', 'patterns', 'queryset', 'query length']
    results= []
    start= time.time()
    queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
    result= time.time()-start

    results.append([max_query_length, 'unified',result, 0, len(queryset)])

    start= time.time()
    queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
    result= time.time()-start
    results.append([max_query_length, 'type-pattern',result, 0])
    new_sample_set = sample._sample
    sample1 = Sample()
    #pos_list = random.sample(range(trace0_length), trace0_length)
    for i in range(1,max_query_length):
        LOGGER.info('Pattern Queries - Query length: %i Current position: %i' , max_query_length, i)

        trace = new_sample_set[1]
        new_trace = ' '.join(trace.split()[:i] + [trace.split()[0]] + trace.split()[i+1:])
        new_sample_set[1] = new_trace
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'unified',result, i+1, len(queryset), max_query])

        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'type-pattern',result, i+1, len(queryset), max_query])
        filename= f'pattern_queries_{max_query_length}'

    plot_results(results, columns, filename, x_axis='patterns', y_axis='time', hue='matching')


def pattern_queries_shuffled(sample, max_query_length):
    """
        Creates Samples of given sample size and trace length, generates pattern queries and records and plots performance for different matching algorithms.
        Runs generated samples on both (unified and separated) discoveries plotting execution time depending on query length.

    Args:
        sample_size (int):  Defaults to 5.
        trace_length (int): Defaults to 10.
    """
    columns = ['max query length', 'matching', 'time', 'patterns', 'queryset', 'query length']
    results= []
    start= time.time()
    queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
    result= time.time()-start

    results.append([max_query_length, 'unified',result, 0, len(queryset)])

    start= time.time()
    queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
    result= time.time()-start
    results.append([max_query_length, 'type-pattern',result, 0])
    new_sample_set = sample._sample
    sample1 = Sample()
    trace_length= len(sample._sample[0].split())
    for i in range(max_query_length):
        LOGGER.info('Pattern Queries 2 - Query length: %i Current position: %i' , max_query_length, i)
        pos = random.randrange(trace_length)

        trace = new_sample_set[1]
        new_trace = ' '.join(trace.split()[:i] + [trace.split()[pos]] + trace.split()[i+1:])
        new_sample_set[1] = new_trace
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = bottom_up_discovery(sample1, supp=1., matchtest='smarter')
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'unified',result, i+1, len(queryset), max_query])
        sample1.set_sample(new_sample_set)
        sample1.set_sample_typeset()
        start= time.time()
        queryset = set(bu_pattern_type_split_discovery(sample1, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'type-pattern',result, i+1, len(queryset), max_query])
        filename= f'pattern_queries2_{max_query_length}'

    plot_results(results, columns, filename, x_axis='patterns', y_axis='time', hue='matching', size= 'query length')

def mixed_queries(sample, max_query_length):
    """
        Creates Samples of given sample size and trace length, generates mixed queries and records and plots performance for different matching algorithms.
        Runs generated samples on both (unified and separated) discoveries plotting execution time depending on query length.
    Args:
        sample_size (int, optional):  Defaults to 5.
        trace_length (int, optional): Defaults to 10.
    """
    columns = ['max query length', 'matching', 'time', 'patterns', 'queryset', 'query length']
    results= []
    start= time.time()
    queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
    result= time.time()-start

    results.append([max_query_length, 'unified',result, 0, len(queryset)])

    start= time.time()
    queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
    result= time.time()-start
    results.append([max_query_length, 'type-pattern',result, 0])

    trace_length= len(sample._sample[0].split())
    for i in range(max_query_length):
        LOGGER.info('Mixed Queries - Query length: %i Current position: %i' , max_query_length, i)
        pos = random.randrange(trace_length)
        pattern_type = random.random()
        new_sample_set = sample._sample
        trace = new_sample_set[1]
        #for trace in sample_set:
        if pattern_type < 0.5:
            new_trace = ' '.join(trace.split()[:i] + [trace.split()[pos]] + trace.split()[i+1:])
        else:
            trace0 = sample._sample[0]
            trace0_list = trace0.split()
            letter = trace0_list[i]
            new_trace = ' '.join(trace.split()[:pos] + [letter] + trace.split()[pos+1:])
            new_sample_set[1] = new_trace
        sample.set_sample(new_sample_set)
        sample.set_sample_typeset()
        start= time.time()
        queryset = bottom_up_discovery(sample, supp=1., matchtest='smarter')
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'unified',result, i+1, len(queryset), max_query])

        start= time.time()
        queryset = set(bu_pattern_type_split_discovery(sample, supp=1., use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
        max_query= sorted(queryset, key=len)[-1].count(' ') +1
        result= time.time()-start
        results.append([max_query_length, 'type-pattern',result, i+1, len(queryset), max_query])
        filename= f'mixed_queries_{max_query_length}'

    plot_results(results, columns, filename, x_axis='patterns', y_axis='time', hue='matching', size= 'query length')


def plot_results(results, columns, file_name, x_axis, y_axis, hue, size= None, col=None, style= None):
    """
        Creates plot from given datasets.

    Args:
        results (List): List of lists containing sample size, trace length, matching type, time of discovery for each disovery approach
        columns (List): String name for each feature in results
        file_name (String): Name of file where image and dataset should be saved
        x (String): Name and value of x-Achses
        y (String): Name and value of y-Achses
        hue (String): Dataframe column name
        size (integer, optional): Dataframe column name. Defaults to None
        col (String, optional): Dataframe column name. Defaults to None.
        style (String, optional): Dataframe column name. Defaults to None.
    """
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%Y%m%d%H%M%S")
    dataframe = pd.DataFrame(results, columns=columns)
    dataframe.to_csv(f'experiments/datasets/{str_date_time}_{file_name}.csv')
    dataframe = dataframe.astype({y_axis:'float'})
    # iterations = int(dataframe.iterations.values[-1]) +1
    grid=sns.relplot(data=dataframe, x=x_axis, y=y_axis, hue=hue, kind='scatter', size= size, col = col, style= style)

    grid.set(yscale='log')#, ylim=(0.0001,1.2*dataframe[y_axis].max()))
    # plt.xticks(range(0,iterations,int(iterations/10)))
    grid.savefig(f'experiments/plots/{str_date_time}_{file_name}.pdf')
    return dataframe


if __name__ == "__main__":
    main()
