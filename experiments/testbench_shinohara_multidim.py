#!/usr/bin/python3
"""Contains functions to test perfomance of shinohara_discovery_icdt on multidimensional queries and samples."""
from copy import deepcopy
import logging
import os
import re
import sys
import time
import pandas as pd
sys.path.append(".")
from sample_multidim import MultidimSample
from query_multidim import MultidimQuery
from testbench_helper_functions import _read_sample, _dataset_to_sample, _generate_experiments
from discovery_shinohara_multidim import shinohara_discovery_icdt, SHINOHARA_SELECT_POSITION, SHINOHARA_SELECT_OPERATION
from plot_statistics_tb_shin_multidim_discovery import _split_stats_df, _plot_testbench_shin_multi_querystringlength_to_runtime, _plot_testbench_shin_multi_querystringlength_to_matchtests, _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime, _plot_testbench_shin_multi_combinations_to_runtime
from plot_statistics_tb_shin_multidim_samples import _collect_and_plot_sample_statistics

SIMULATION_SUPPORT_LIST = [
    0.6,
    0.8,
    1.0
]

SIMULATION_DIM_L_W_TUPLE_LIST = [
    [1, 4, [(-1,-1),(0,1),(0,1),(0,1),(-1,-1)]],
    [1, 5, [(-1,-1),(0,1),(0,1),(0,1),(0,1),(-1,-1)]],
    [1, 6, [(-1,-1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1,-1)]],
    [2, 4, [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]],
    [2, 5, [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]],
    [2, 6, [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]],
    [3, 4, [(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)]],
    [3, 5, [(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)]],
    [3, 6, [(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)]]
]

SIMULATION_DIM_L_W_TUPLE_LIST_GOOGLE_BTW23 = [
    #query1 has query string length 3 and is defined over trace_length <=100
    [5, 3, [(-1,-1),(0,0),(0,0),(0,0),(0,0),(0,500),
            (0,0),(0,0),(0,0),(0,0),(0,500),
            (0,0),(0,0),(0,0),(0,0),(-1,-1)]
    ],
    #query2 has query string length 4 and is defined over trace_length <=100
    [5, 4, [(-1,-1),(0,0),(0,0),(0,0),(0,0),(0,500),
            (0,0),(0,0),(0,0),(0,0),(0,500),
            (0,0),(0,0),(0,0),(0,0),(0,500),
            (0,0),(0,0),(0,0),(0,0),(-1,-1)]
    ],
    #query3 has query string length 3 and is defined over trace_length <=200
    [5, 3, [(-1,-1),(0,0),(0,0),(0,0),(0,0),(0,1000),
            (0,0),(0,0),(0,0),(0,0),(0,1000),
            (0,0),(0,0),(0,0),(0,0),(-1,-1)]
    ]
]

COMBINATIONS =  [   (next_position, next_operation)
                    for next_position in SHINOHARA_SELECT_POSITION
                    for next_operation in SHINOHARA_SELECT_OPERATION
                ]

FILENAME_REGEXP = '^(?P<sample_size>[^\\.]*)\\.(?P<min_trace_length>[0-9]*)\\.(?P<max_trace_length>[0-9]*)\\.(?P<type_length>[-]*[0-9]*)\\.(?P<event_dimension>[-]*[0-9]*)\\.(?P<dataset>[^\\.]*)\\.seq'

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main(args):
    """
        This testbench is able to perform multiple query discoveries using
        the function shinohara_discovery_icdt() and different query and discovery
        parameters like string length, event dimension, support threshold, next
        position and next operation.

        The discovery is based on all samples in the directory '/samples'.
        Initially this directory contains the samples used for BTW23-Submission.
        The testbench is able to create synthetic data for query discovery as
        well.

        It collects statistics during the run which are stored and plotted.

        To use the testbench, use the following command:
        python3 experiments/testbench_shinohara_multidim.py [synthetic|google-btw23] [True|False]

        Args:
            args: Array containing a
                    - string defining which kind of samples will be available in
                      '/samples' for query discovery. Should be either
                      'synthetic' or 'google-btw23'. Affects mainly plots.
                    - boolean which indiactes whether samples should be generated.
                    If set to True it either generates synthetic samples or
                    converts datasets in '/datasets' to samples using the function
                    _dataset_to_sample(), depending on args[0].
    """
    LOGGER.info("testbench shin icdt multi - Start testbench for shinohara_discovery_icdt multidim")
    LOGGER.info("testbench shin icdt multi - Set simulation params")

    sim_supp_list = SIMULATION_SUPPORT_LIST
    sim_dim_l_w_list = []
    if args[0] == "synthetic":
        sim_dim_l_w_list = SIMULATION_DIM_L_W_TUPLE_LIST
    if args[0] == "google-btw23":
        sim_dim_l_w_list = SIMULATION_DIM_L_W_TUPLE_LIST_GOOGLE_BTW23

    if args[0] == "synthetic" and args[1] == "True":
        LOGGER.info('testbench shin icdt multi - Generate synthetic samples')
        #_generate_experiments(params_sample_size=[10,20,30,40,50], params_trace_length=[(5,10),(10,20)],params_type_length=1,params_dimension=[1,2,3])
        _generate_experiments(params_sample_size=[20,50], params_trace_length=[(10,20)],params_type_length=1,params_dimension=[2,3])
        LOGGER.info('testbench shin icdt multi - Finished building sythetic samples')
    if args[0] == "google-btw23" and args[1] == "True":
        LOGGER.info('testbench shin icdt multi - Build samples from google traces')
        _dataset_to_sample(path_to_file='datasets/google/GoogleTraces_BTW23_using_ILMiner_Queries/google_query1a.txt.gz',trace_len=100)
        _dataset_to_sample(path_to_file='datasets/google/GoogleTraces_BTW23_using_ILMiner_Queries/google_query2b.txt.gz',trace_len=100)
        _dataset_to_sample(path_to_file='datasets/google/GoogleTraces_BTW23_using_ILMiner_Queries/google_query3a.txt.gz')
        LOGGER.info('testbench shin icdt multi - Finished building samples from google traces')

    LOGGER.info('testbench shin icdt multi - Start simulation')
    # Init statistic dicts and lists
    stats_shin_icdt = {}
    stats_list_shin_icdt = []

    entries = os.listdir("samples/")
    for entry in entries:
        if entry.endswith(".msgpack"):
            filename = entry[0:len(entry)-8]
            # Load sample and store sample parameter
            LOGGER.info('testbench shin icdt multi - Load sample %s and parse sample params', filename)
            sample = MultidimSample(_read_sample(filename),uniform_dimension=True)
            sample.calc_sample_typeset()
            for supp in sim_supp_list:
                sample.calc_sample_supported_typeset(support=supp)

            parsed_params = re.match(FILENAME_REGEXP,filename)
            assert parsed_params is not None
            sample_size = int(parsed_params.group('sample_size'))
            min_trace_length = int(parsed_params.group('min_trace_length'))
            max_trace_length = int(parsed_params.group('max_trace_length'))
            type_length = int(parsed_params.group('type_length'))
            event_dimension = int(parsed_params.group('event_dimension'))
            dataset = parsed_params.group('dataset')

            # Start simulation for current sample
            LOGGER.info('testbench shin icdt multi - Start simulation for sample %s', filename)
            for dim_l_w_tuple in sim_dim_l_w_list:
                for supp in sim_supp_list:
                    for (next_position, next_operation) in COMBINATIONS:
                        dim_l_w_tuple_copy = deepcopy(dim_l_w_tuple)
                        query_event_dimension = dim_l_w_tuple_copy[0]
                        query_string_length = dim_l_w_tuple_copy[1]
                        local_window_sizes = dim_l_w_tuple_copy[2]

                        # Only init query and start discovery if dimensions of query and sample are equal
                        if event_dimension != query_event_dimension:
                            LOGGER.info('testbench shin icdt multi - Current query event dim and sample event dim are not equal. Skip.')
                            break
                        # Only init query and start discovery if local window sizes and sample trace length are compatible
                        if ('google_query3a' in dataset and (0,1000) not in local_window_sizes) or ('google_query3a' not in dataset and (0,1000) in local_window_sizes):
                            LOGGER.info('testbench shin icdt multi - Current query lws and sample trace length are not compatible. Skip.')
                            break
                        # If google data is used: sample '694.1.50.-1.5.google_query2a.seq'
                        # <-> only init query and start discovery for query string length of 4
                        if ('google_query2' in dataset and query_string_length != 4) or ('google_query2' not in dataset and query_string_length == 4):
                            LOGGER.info('testbench shin icdt multi - Current query string length and google sample are not compatible. Skip.')
                            break

                        # Init query
                        query_and_discovery_params = "dim: "+str(query_event_dimension)+", string length: "+str(query_string_length)+", supp: "+str(supp)+", next_pos: "+str(next_position)+", next_op: "+str(next_operation)
                        LOGGER.info('testbench shin icdt multi - Init discovery with params %s', query_and_discovery_params)
                        query:MultidimQuery = MultidimQuery()
                        query.init_most_general_query_for_shinohara(query_string_length,local_window_sizes,'shinohara_icdt', sample, event_dimension)

                        # Start discovery for current query, sample and support threshold
                        LOGGER.info('testbench shin icdt multi - Start discovery for sample %s and query %s', filename, query_and_discovery_params)
                        start_time_shinohara = time.time()
                        query_and_stats = shinohara_discovery_icdt(query, sample, supp, select_position=next_position, select_operation=next_operation)
                        runtime_shinohara = time.time()-start_time_shinohara
                        LOGGER.info('testbench shin icdt multi - Finished discovery for sample %s and query %s', filename, query_and_discovery_params)

                        # Store collected stats
                        LOGGER.info('testbench shin icdt multi - Start storing collected stats for sample %s and query %s', filename, query_and_discovery_params)
                        assert query_and_stats[0] is not None
                        query = query_and_stats[0]
                        discovery_stats = query_and_stats[1]

                        stats_shin_icdt["param sample_size"] = sample_size
                        stats_shin_icdt["param min_trace_length"] = min_trace_length
                        stats_shin_icdt["param max_trace_length"] = max_trace_length
                        stats_shin_icdt["param type_length"] = type_length
                        stats_shin_icdt["param support"] = supp
                        stats_shin_icdt["param dataset"] = dataset

                        if query_and_stats[0] is not None:
                            stats_shin_icdt["mined query"] = query._query_string
                            query.set_query_repeated_variables()
                            query.set_query_typeset()
                            stats_shin_icdt["param repeated variables"] = query._query_repeated_variables
                            stats_shin_icdt["param query typeset"] = query._query_typeset
                            stats_shin_icdt["param query class"] = query._query_class
                            stats_shin_icdt["param matchtest"] = query._query_matchtest
                            stats_shin_icdt["param discovery algorithm"] = query._query_discovery_algorithm
                        else:
                            stats_shin_icdt["mined query"] = ""
                            stats_shin_icdt["param repeated variables"] = set()
                            stats_shin_icdt["param query typeset"] = set()
                            stats_shin_icdt["param query class"] = "normal"
                            stats_shin_icdt["param matchtest"] = 'regex'
                            stats_shin_icdt["param discovery algorithm"] = 'shinohara_icdt'

                        stats_shin_icdt["mining runtime"] = runtime_shinohara
                        stats_shin_icdt["param query_string_length"] = query_string_length
                        stats_shin_icdt["param local_window_sizes"] = dim_l_w_tuple[2]
                        stats_shin_icdt["param position/operation choice"] = str(next_position)+"/"+str(next_operation)

                        if query_and_stats[1] is not None:
                            stats_shin_icdt["param match test count"] = discovery_stats["param match test count"]
                            stats_shin_icdt["param avg match test runtime for single position"] = discovery_stats["param avg match test runtime for single position"]
                            stats_shin_icdt["param avg match test runtime for whole query"] = discovery_stats["param avg match test runtime for whole query"]
                            stats_shin_icdt["param supported typeset"] = discovery_stats["param supported typeset"]
                        else:
                            stats_shin_icdt["param match test count"] = 0
                            stats_shin_icdt["param avg match test runtime for single position"] = 0
                            stats_shin_icdt["param avg match test runtime for whole query"] = 0
                            stats_shin_icdt["param supported typeset"] = set()

                        stats_list_shin_icdt.append(deepcopy(stats_shin_icdt))
                        LOGGER.info('testbench shin icdt multi - Finished storing collected stats for sample %s and query %s', filename, query_and_discovery_params)
    LOGGER.info('testbench shin icdt multi - Simulation finished')

    LOGGER.info('testbench shin icdt multi - Start building dataframe')
    shin_stats_dataframe = pd.DataFrame()
    for stats in stats_list_shin_icdt:
        stats_df = pd.DataFrame([stats])
        shin_stats_dataframe = shin_stats_dataframe.append(stats_df)

    LOGGER.info('testbench shin icdt multi - Writing shinohara icdt dataframe to plots/')
    shin_stats_dataframe.to_csv('plots/dataframe_shin_icdt')

    LOGGER.info('testbench shin icdt multi - Start plotting')
    if args[0] == "synthetic":
        _split_stats_df(shin_stats_dataframe)
        if os.path.exists('plots/dataframe_shin_icdt_random'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="random")
        if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="fragmentation-quartered")
        if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="fragmentation-gauss")
    if args[0] == "google-btw23":
        combination_names = [
            'random/type', 'random/var', 'random/random',
            'left_to_right/type', 'left_to_right/var', 'left_to_right/random',
            'right_to_left/type', 'right_to_left/var', 'right_to_left/random'
        ]
        #combination_names_shorter = [
        #    'r/t', 'r/v', 'r/r',
        #    'l2r/t', 'l2r/v', 'l2r/r',
        #    'r2l/t', 'r2l/v', 'r2l/r'
        #]
        if os.path.exists('plots/dataframe_shin_icdt'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST,combinations=combination_names)
            _plot_testbench_shin_multi_querystringlength_to_matchtests(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST,combinations=combination_names)
            _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST,combinations=combination_names)
            _plot_testbench_shin_multi_combinations_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST)

    LOGGER.info('testbench shin icdt multi - Start collecting and plotting samples statistics')
    _collect_and_plot_sample_statistics()
    LOGGER.info('testbench shin icdt multi - Finished collecting and plotting samples statistics')

    LOGGER.info("testbench shin icdt multi - Finished testbench for shinohara_discovery_icdt multidim")

if __name__ == "__main__":
    main(sys.argv[1:])
