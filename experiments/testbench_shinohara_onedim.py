#!/usr/bin/python3
"""Contains functions to test perfomance of shinohara_discovery_icdt on onedimensional queries and samples."""
from copy import deepcopy
import logging
import os
import re
import sys
import time
import pandas as pd
sys.path.append(".")
from sample import Sample
from query import Query
from testbench_helper_functions import _read_sample, _generate_experiments
from discovery_shinohara import shinohara_discovery_icdt, SHINOHARA_SELECT_POSITION, SHINOHARA_SELECT_OPERATION
from plot_statistics_tb_shin_multidim_discovery import _split_stats_df, _plot_testbench_shin_multi_querystringlength_to_runtime, _plot_testbench_shin_multi_combinations_to_runtime
from plot_statistics_tb_shin_multidim_samples import _collect_and_plot_sample_statistics

SIMULATION_SUPPORT_LIST = [
    0.6,
    0.8,
    1.0
]

SIMULATION_L_W_TUPLE_LIST = [
    [4, [(-1,-1),(0,10),(0,10),(0,10),(-1,-1)]],
    [5, [(-1,-1),(0,10),(0,10),(0,10),(0,10),(-1,-1)]],
    [6, [(-1,-1),(0,10),(0,10),(0,10),(0,10),(0,10),(-1,-1)]]
]

COMBINATIONS =  [   (next_position, next_operation)
                    for next_position in SHINOHARA_SELECT_POSITION
                    for next_operation in SHINOHARA_SELECT_OPERATION
                ]

FILENAME_REGEXP = '^(?P<sample_size>[^\\.]*)\\.(?P<min_trace_length>[0-9]*)\\.(?P<max_trace_length>[0-9]*)\\.(?P<type_length>[-]*[0-9]*)\\.(?P<event_dimension>[-]*[0-9]*)\\.(?P<dataset>[^\\.]*)\\.seq'

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main(args):
    """
        This testbench is able to perform multiple query discoveries using
        the function shinohara_discovery_icdt() and different query and
        discovery parameters like string length, support threshold, next
        position and next operation.

        The discovery is based on synthetic data, bu can be extended to samples
        in the directory '/samples'.

        It collects statistics during the run which are stored and plotted.

        To use the testbench, use the following command:
        python3 experiments/testbench_shinohara_onedim.py [synthetic|google-btw23] [True|False]

        Args:
            args: Array containing a
                - string defining which kind of samples will be available in
                  '/samples' for query discovery (default: 'synthetic').
                - boolean which indiactes whether samples should be generated.
                  If set to True it either generates synthetic samples or
                converts datasets in '/datasets' to samples using the function
                _dataset_to_sample(), depending on args[0].
    """
    LOGGER.info("testbench shin icdt - Start testbench for shinohara_discovery_icdt")
    LOGGER.info("testbench shin icdt - Set simulation params")

    sim_supp_list = SIMULATION_SUPPORT_LIST
    sim_dim_l_w_list = []
    if args[0] == "synthetic":
        sim_dim_l_w_list = SIMULATION_L_W_TUPLE_LIST

    if args[0] == "synthetic" and args[1] == "True":
        LOGGER.info('testbench shin icdt - Generate synthetic samples')
        #_generate_experiments(params_sample_size=[10,20,30,40,50], params_trace_length=[(5,10),(10,20)],params_type_length=1)
        _generate_experiments(params_sample_size=[20,50], params_trace_length=[(10,20)],params_type_length=1)
        LOGGER.info('testbench shin icdt - Finished building sythetic samples')

    LOGGER.info('testbench shin icdt - Start simulation')
    # Init statistic dicts and lists
    stats_shin_icdt = {}
    stats_list_shin_icdt = []

    entries = os.listdir("samples/")
    for entry in entries:
        if entry.endswith(".msgpack"):
            filename = entry[0:len(entry)-8]
            # Load sample and store sample parameter
            LOGGER.info('testbench shin icdt - Load sample %s and parse sample params', filename)
            sample = Sample(_read_sample(filename))

            parsed_params = re.match(FILENAME_REGEXP,filename)
            assert parsed_params is not None
            sample_size = int(parsed_params.group('sample_size'))
            min_trace_length = int(parsed_params.group('min_trace_length'))
            max_trace_length = int(parsed_params.group('max_trace_length'))
            type_length = int(parsed_params.group('type_length'))
            dataset = parsed_params.group('dataset')

            # Start simulation for current sample
            LOGGER.info('testbench shin icdt - Start simulation for sample %s', filename)
            for dim_l_w_tuple in sim_dim_l_w_list:
                for supp in sim_supp_list:
                    sample._get_supported_typeset(supp)
                    for (next_position, next_operation) in COMBINATIONS:
                        dim_l_w_tuple_copy = deepcopy(dim_l_w_tuple)
                        query_string_length = dim_l_w_tuple_copy[0]
                        local_window_sizes = dim_l_w_tuple_copy[1]

                        # Init query
                        query_and_discovery_params = "dim: "+str(1)+", string length: "+str(query_string_length)+", supp: "+str(supp)+", next_pos: "+str(next_position)+", next_op: "+str(next_operation)
                        LOGGER.info('testbench shin icdt - Init discovery with params %s', query_and_discovery_params)
                        query:Query = Query()
                        query.init_most_general_query_for_shinohara(query_string_length,local_window_sizes,'shinohara_icdt', sample)

                        # Start discovery for current query, sample and support threshold
                        LOGGER.info('testbench shin icdt - Start discovery for sample %s and query %s', filename, query_and_discovery_params)
                        start_time_shinohara = time.time()
                        query_and_stats = shinohara_discovery_icdt(query, sample, supp, select_position=next_position, select_operation=next_operation)
                        runtime_shinohara = time.time()-start_time_shinohara
                        LOGGER.info('testbench shin icdt - Finished discovery for sample %s and query %s', filename, query_and_discovery_params)

                        # Store collected stats
                        LOGGER.info('testbench shin icdt - Start storing collected stats for sample %s and query %s', filename, query_and_discovery_params)
                        assert query_and_stats[0] is not None
                        query = query_and_stats[0]
                        discovery_stats = query_and_stats[1]

                        stats_shin_icdt["param sample_size"] = sample_size
                        stats_shin_icdt["param min_trace_length"] = min_trace_length
                        stats_shin_icdt["param max_trace_length"] = max_trace_length
                        stats_shin_icdt["param type_length"] = type_length
                        stats_shin_icdt["param support"] = supp
                        stats_shin_icdt["param dataset"] = dataset

                        if query_and_stats[0] is not None:
                            stats_shin_icdt["mined query"] = query._query_string
                            query.set_query_repeated_variables()
                            query.set_query_typeset()
                            stats_shin_icdt["param repeated variables"] = query._query_repeated_variables
                            stats_shin_icdt["param query typeset"] = query._query_typeset
                            stats_shin_icdt["param query class"] = query._query_class
                            stats_shin_icdt["param matchtest"] = query._query_matchtest
                            stats_shin_icdt["param discovery algorithm"] = query._query_discovery_algorithm
                        else:
                            stats_shin_icdt["mined query"] = ""
                            stats_shin_icdt["param repeated variables"] = set()
                            stats_shin_icdt["param query typeset"] = set()
                            stats_shin_icdt["param query class"] = "normal"
                            stats_shin_icdt["param matchtest"] = 'regex'
                            stats_shin_icdt["param discovery algorithm"] = 'shinohara_icdt'

                        stats_shin_icdt["mining runtime"] = runtime_shinohara
                        stats_shin_icdt["param query_string_length"] = query_string_length
                        stats_shin_icdt["param local_window_sizes"] = dim_l_w_tuple[1]
                        stats_shin_icdt["param position/operation choice"] = str(next_position)+"/"+str(next_operation)

                        if query_and_stats[1] is not None:
                            stats_shin_icdt["param match test count"] = discovery_stats["param match test count"]
                            stats_shin_icdt["param avg match test runtime for single position"] = discovery_stats["param avg match test runtime for single position"]
                            stats_shin_icdt["param avg match test runtime for whole query"] = discovery_stats["param avg match test runtime for whole query"]
                            stats_shin_icdt["param supported typeset"] = discovery_stats["param supported typeset"]
                        else:
                            stats_shin_icdt["param match test count"] = 0
                            stats_shin_icdt["param avg match test runtime for single position"] = 0
                            stats_shin_icdt["param avg match test runtime for whole query"] = 0
                            stats_shin_icdt["param supported typeset"] = set()

                        stats_list_shin_icdt.append(deepcopy(stats_shin_icdt))
                        LOGGER.info('testbench shin icdt - Finished storing collected stats for sample %s and query %s', filename, query_and_discovery_params)
    LOGGER.info('testbench shin icdt - Simulation finished')

    LOGGER.info('testbench shin icdt - Start building dataframe')
    shin_stats_dataframe = pd.DataFrame()
    for stats in stats_list_shin_icdt:
        stats_df = pd.DataFrame([stats])
        shin_stats_dataframe = shin_stats_dataframe.append(stats_df)

    LOGGER.info('testbench shin icdt - Writing shinohara icdt dataframe to plots/')
    shin_stats_dataframe.to_csv('plots/dataframe_shin_icdt')

    LOGGER.info('testbench shin icdt - Start plotting')
    if args[0] == "synthetic":
        _plot_testbench_shin_multi_combinations_to_runtime(stats_df=shin_stats_dataframe,sim_supp_list=SIMULATION_SUPPORT_LIST)
        _split_stats_df(shin_stats_dataframe)
        if os.path.exists('plots/dataframe_shin_icdt_random'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="random")
        if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="fragmentation-quartered")
        if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
            stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
            _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=SIMULATION_SUPPORT_LIST, filename_base="fragmentation-gauss")

    LOGGER.info('testbench shin icdt - Start collecting and plotting samples statistics')
    _collect_and_plot_sample_statistics()
    LOGGER.info('testbench shin icdt - Finished collecting and plotting samples statistics')

    LOGGER.info("testbench shin icdt - Finished testbench for shinohara_discovery_icdt")

if __name__ == "__main__":
    main(sys.argv[1:])
