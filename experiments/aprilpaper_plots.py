import seaborn as sns
import sys
import os
import gzip
import time
import logging
import random
from copy import deepcopy
import matplotlib.pyplot as plt
import pandas as pd
import scienceplots
from itertools import product
sys.path.append(".")

from testbench_multidim import match_algos, generate_plots, non_matching_sample, change_sample
from datasets.google.FirstPaper import google_trace_generation
from datasets.finance.FirstPaper import finance_trace_generation
from sample_multidim import MultidimSample
from query_multidim import MultidimQuery
sns.set_context("paper", font_scale=3.5)
plt.style.use(['science'])


# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main(test='all'):
    if test == 'all':
        tests = ['sota', 'rl_compare']
    else:
        tests = [test]

    if 'sota' in tests:
        sample_size = -1
        trace_length = 100
        max_query_length = 6
        df_sota = sota(sample_size=sample_size, trace_length=trace_length, overwrite=False, max_query_length=max_query_length)
        file_name = f'sota_{sample_size}_{trace_length}'
        generate_plots(dataframe=df_sota, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='sota')

    if 'max_pattern' in tests:
        iterations = 4
        df_pattern = max_pattern_ratio(iterations=iterations, repetitions= 5, overwrite=True)
        file_name = f'max_pattern_{iterations}'
        generate_plots(dataframe=df_pattern, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='max_pattern')

    if 'number_of_types' in tests:
        iterations = 20
        df_types = number_of_types(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'number_of_types_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='number_of_types')

    if 'domain_size' in tests:
        iterations = 16
        df_types = domain_size(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'domain_size_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='domain_size')

    if 'trace_length' in tests:
        iterations = 100
        df_types = min_trace_length(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'trace_length_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='trace_length')

    if 'supp_alphabet' in tests:
        iterations = 10
        df_types = supp_alphabet(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'supp_alphabet_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='supported types', y='time', hue='discovery algorithm', kind='supported_aplhabet')

    if 'number_of_patterns' in tests:
        iterations = 20
        df_types = number_of_patterns(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'number_of_patterns_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='pattern types', y='time', hue='discovery algorithm', kind='number_of_patterns')

    if 'mixed_queries' in tests:
        iterations = 10
        df_types = no_mixed_queries(iterations=iterations, repetitions= 5, overwrite=False)
        file_name = f'mixed_queries_{iterations}'
        generate_plots(dataframe=df_types, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='mixed_queries')
    
    if 'synt' in tests:
        df_types = synt_plots()
        file_name = 'synt_plots'
        generate_plots(dataframe=df_types, file_name=file_name, x='iterations', y='time',
                       hue='discovery algorithm', col = 'mode', col_wrap=3,kind='synt',
                       facet_kws={'sharex': False, 'sharey': False})

    if 'rl_compare' in tests:
        df_rl = rl_compare()
        file_name = 'rl_compare'
        generate_plots(dataframe=df_rl, file_name=file_name, x='iterations', y='time', hue='discovery algorithm', kind='rl_compare')
# Real-world data plots:
def sota(sample_size:int, trace_length:int, overwrite: bool = False, max_query_length = -1) -> pd.DataFrame:
    """Experiment for the State of the Art comparison
    Args:
        sample_size (int): number of traces in the sample
        trace_length (int): number of events per trace in the sample
        overwrite (bool, optional): Option to overwrite existing results and rerun experiment. Defaults to False.

    Returns:
        Dataframe: Collected data for each iteration
    """
    file_name = f'sota_{sample_size}_{trace_length}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        repetition = 5
        discoveries = ['unified', 'separated', 'separated_ps', 'unified_ps', 'il-miner2', 'il-miner']
        for mod in ['finance', 'google']:
            for abstraction in ['Abstraction 1', 'Abstraction 2', 'Abstraction 3']:
                if mod == 'finance':
                    if abstraction == 'Abstraction 1':
                        sample_path = 'datasets/finance/FirstPaper/samples/finance_query1.txt'
                        if not os.path.isfile(sample_path):
                            finance_trace_generation.query1()

                    elif abstraction == 'Abstraction 2':
                        sample_path = 'datasets/finance/FirstPaper/samples/finance_query2.txt'
                        if not os.path.isfile(sample_path):
                            finance_trace_generation.query2()
                    else:
                        sample_path = 'datasets/finance/FirstPaper/samples/finance_query3.txt'
                        if not os.path.isfile(sample_path):
                            finance_trace_generation.query3()

                    file = open(sample_path, 'r', encoding="utf-8")
                elif mod == 'google':
                    if abstraction == 'Abstraction 1':
                        sample_path = 'datasets/google/FirstPaper/samples/google_query1.txt.gz'

                        if not os.path.isfile(sample_path):
                            google_trace_generation.query1()

                    elif abstraction == 'Abstraction 2':
                        sample_path = 'datasets/google/FirstPaper/samples/google_query2.txt.gz'
                        if not os.path.isfile(sample_path):
                            google_trace_generation.query2()
                    else:
                        sample_path = 'datasets/google/FirstPaper/samples/google_query3.txt.gz'
                        if not os.path.isfile(sample_path):
                            google_trace_generation.query3()
                    file = gzip.open(sample_path, 'rb')
                else:
                    file = open(sample_path, 'r', encoding="utf-8")
                sample_list = []
                start = time.time()
                counter = 0
                for trace1 in file:
                    # sample_set2.append(' '.join(trace1.decode().split()))
                    if mod == 'google':
                        sample_list.append(' '.join(trace1.decode().split()[-trace_length:]))
                    else:
                        sample_list.append(' '.join(trace1.split()[-trace_length:]))
                    if counter == sample_size:
                        break
                    counter += 1

                file.close()
                result = time.time() - start
                LOGGER.info('Finished reading file: %i lines in %f seconds', counter, result)
                for _ in range(repetition):
                    results, columns = match_algos(sample_list=sample_list, results=results, iterations=repetition, mod=mod,
                                          j=abstraction, discovery=discoveries, file_path=file_path, max_query_length=max_query_length)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)
    return dataframe

def rl_compare():
    file_name = 'rl_compare'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path):
        sample_path = 'datasets/google/FirstPaper/samples/google_query2_rl.txt.gz'
        if not os.path.isfile(sample_path):
            google_trace_generation.query2_rl()
        file = gzip.open(sample_path, 'rb')
        repetition = 5
        sample_list_google = []
        counter = 0
        for trace1 in file:
            sample_list_google.append(' '.join(trace1.decode().split()))
            counter += 1
            if counter == 100:
                break

        file.close()

        sample_path = 'datasets/finance/FirstPaper/samples/finance_query2_rl.txt'
        if not os.path.isfile(sample_path):
            finance_trace_generation.query2_rl()
        file = open(sample_path, 'r', encoding="utf-8")
        sample_list_finance = []
        for trace1 in file:
            sample_list_finance.append(trace1)

        file.close()

        for _ in range(repetition):
            results, columns = match_algos(sample_list=sample_list_google, results=results, iterations='google', mod='mod', j='google',
                                discovery=['unified'], file_path=file_path, only_types=True)
            results, columns = match_algos(sample_list=sample_list_finance, results=results, iterations='finance', mod='mod', j='finance',
                                discovery=['unified'], file_path=file_path, only_types=True)


        dataframe = pd.DataFrame(results, columns=columns)
        queryset_bu = dataframe.loc[dataframe.iteration == 'google']['queryset'].values[0]
        rl_google_results = pd.read_csv('experiments/results/datasets/rl_google_cpu_results.txt', sep=', ', engine='python')
        parent_dict = {}
        dict_iter = {}
        sample = MultidimSample()
        sample.set_sample(sample_list_google)
        alphabet = sample.get_sample_typeset()
        queryset_object = rl_google_results.loc[0,'queryset']
        queryset = set()
        descritptive = 0
        non_descritptive = 0
        not_matching = 0
        for querystring in queryset_object.split("'"):
            if ';' in querystring:
                queryset.add(querystring)
                query = MultidimQuery()
                query.set_query_string(querystring)
                query.set_query_matchtest('smarter')
                query.set_pos_last_type_and_variable()
                if querystring in queryset_bu:
                    descritptive += 1
                else:
                    matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter,
                                                patternset=alphabet, parent_dict=parent_dict)
                    if matching:
                        non_descritptive += 1
                    else:
                        not_matching += 1


        print(len(queryset_bu),descritptive, non_descritptive, not_matching)
        df = pd.concat([dataframe, rl_google_results]) #.to_csv(file_path)
        df.fillna({'discovery algorithm':'rl', 'iterations':'google', 'iteration':'google'}, inplace=True)
        df.to_csv(file_path)
        queryset_bu = dataframe.loc[dataframe.iteration == 'finance']['queryset'].values[0]
        rl_finance_results = pd.read_csv('experiments/results/datasets/rl_finance_cpu_results.txt', sep=', ', engine='python')
        parent_dict = {}
        dict_iter = {}
        sample = MultidimSample()
        sample.set_sample(sample_list_finance)
        alphabet = sample.get_sample_typeset()
        queryset_object = rl_finance_results.queryset.values[0]
        queryset = set()
        descritptive = 0
        non_descritptive = 0
        not_matching = 0
        for querystring in queryset_object.split("'"):
            if ';' in querystring:
                queryset.add(querystring)
                query = MultidimQuery()
                query.set_query_string(querystring)
                query.set_query_matchtest('smarter')
                query.set_pos_last_type_and_variable()
                if querystring in queryset_bu:
                    descritptive += 1
                else:
                    matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter,
                                                patternset=alphabet, parent_dict=parent_dict)
                    if matching:
                        non_descritptive += 1
                    else:
                        not_matching += 1


        print(len(queryset_bu),descritptive, non_descritptive, not_matching)
        df = pd.concat([df, rl_finance_results]) #.to_csv(file_path)
        df.fillna({'discovery algorithm':'rl', 'iterations':'finance', 'iteration':'finance'}, inplace=True)
        df.to_csv(file_path)
    else:
        df = pd.read_csv(file_path)
    return df

####################################################################################
# Synthetic data plots:
def max_pattern_ratio(iterations:int, repetitions:int, overwrite: bool = False):
    file_name = f'max_pattern_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample()
        trace_length = len(sample_list[0].split())
        domain_cnt = sample_list[0].split()[0].count(';')
        domain_list = list(range(domain_cnt))
        results= []
        for rep in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            pos_set = set(range(trace_length))
            for j in range(iterations):
                cur_domain = j% domain_cnt
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, rep)
                if not pos_set:
                    break

                pos_list = random.sample(sorted(pos_set), 1)
                if j == 0:
                    pos = min(pos_set)
                    pos_set.discard(pos)
                pos_set.difference_update(pos_list)

                new_sample_list = change_sample(new_sample_list, 0, [cur_domain], pos, pos_list)
                LOGGER.info(new_sample_list[0])
                results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations, j=j+1, mod='max pattern ratio', file_path=file_path)
        # columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time', 'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence', 'trace max type occourence', 'min pattern length', 'sample set', 'max query length']
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)
    return dataframe


def number_of_types(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'number_of_types_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample(sample_size=2, trace_length=iterations +1, domain_size=1)
        domain_cnt = sample_list[0].split()[0].count(';')
        # for i, event in enumerate(sample_list[0].split()):
            # trace_list = sample_list[1].split()
            # new_event_list = trace_list[i].split(';')
            # new_event_list[0] = event.split(';')[0]
            # new_event = ';'.join(new_event_list)
            # new_trace_list = ' '.join(sample_list[1].split()[:i] + [new_event] + sample_list[1].split()[i+1:])
            # sample_list[1] = new_trace_list


        for rep in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            for j in range(iterations):
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, rep)
                # if j == 0:
                pattern_type = 1

                # else:
                #     pattern_type = 0
                #     pos_list = [j]

                new_sample_list = change_sample(sample_list=new_sample_list, pattern_type=pattern_type, rand_domain=[0], pos=j, pos_list=[j])
                LOGGER.info(new_sample_list)
                if j%2 == 0:
                    results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations, j=j+1, mod='number of types', file_path=file_path)
        # columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time', 'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence', 'trace max type occourence', 'min pattern length', 'sample set', 'max query length']
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)

    return dataframe


def type_ratio(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'type_ratio_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample(sample_size=2, trace_length=iterations+1, domain_size=1)

        for rep in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            for j in range(iterations):
                
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, rep)
                
                new_sample_list = change_sample(sample_list=new_sample_list, pattern_type=1, rand_domain=[0],
                                                pos=iterations-j, pos_list=[iterations-j])
                
                LOGGER.info(new_sample_list)
                if j%2 == 1:
                    results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations,
                                                   j=j+1, mod='type ratio', file_path=file_path)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)

    return dataframe


def min_trace_length(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'trace_length_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample(sample_size=2, trace_length=iterations)
        new_event = sample_list[0].split()[0]
        new_trace_list = ' '.join(sample_list[0].split()[:1] + [new_event] + sample_list[0].split()[2:])
        sample_list[0] = new_trace_list

        pos = 0
        for rep in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            for j in range(5, iterations+1, 10):
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, rep)
                # new_sample_list[1] = ' '.join(sample_list[1].split()[:j+1])
                new_event = sample_list[1].split()[0]
                new_sample_list[1] = ' '.join(sample_list[1].split()[:j] + [new_event])
                LOGGER.info(new_sample_list)
                results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations, j=j+1, mod='trace length', file_path=file_path)
        # columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time', 'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence', 'trace max type occourence', 'min pattern length', 'sample set', 'max query length']
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)

    return dataframe

def supp_alphabet(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'supp_alphabet_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample()
        trace_length = len(sample_list[0].split())
        domain_cnt = sample_list[0].split()[0].count(';')
        modes = ['Type']
        results= []
        for r in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            pos_dict = {key: set(range(trace_length)) for key in range(domain_cnt)}

            for j in range(iterations):
                cur_domain = j% domain_cnt
                #Modify Sample
                LOGGER.info('Type Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, r)
                if j != 0:
                    pos = min(pos_dict[cur_domain])
                    pos_dict[cur_domain].discard(pos)
                    # used_positions.add(pos)
                    pattern_type = 1
                    pos_list = [pos]

                    new_sample_list = change_sample(new_sample_list, pattern_type, [cur_domain], pos, pos_list)
                if j%2 == 0:
                    results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations,
                                                   j=j+1, mod='supported alphabet', file_path=file_path)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)

    return dataframe

def traces(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'traces_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:

        pos = 0
        for rep in range(repetitions):
            for j in range(2, iterations+1, 100):
                sample_list = non_matching_sample(sample_size=j, trace_length=10)
                new_sample_list = change_sample(sample_list=sample_list, pattern_type=0, rand_domain=[0], pos=0, pos_list=[1])
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i', iterations, j, rep)
                # new_sample_list[1] = ' '.join(sample_list[1].split()[:j+1])
                results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=iterations, j=j, mod='trace length', file_path=file_path)
        # columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time', 'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence', 'trace max type occourence', 'min pattern length', 'sample set', 'max query length']
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)

    return dataframe

def number_of_patterns(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'number_of_patterns_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        sample_list = non_matching_sample()
        trace_length = len(sample_list[0].split())
        domain_cnt = sample_list[0].split()[0].count(';')
        results= []
        for r in range(repetitions):
            new_sample_list = deepcopy(sample_list)
            pos_dict = {key: set(range(trace_length)) for key in range(domain_cnt)}

            for j in range(iterations):
                cur_domain = j% domain_cnt
                #Modify Sample
                LOGGER.info('Pattern Queries - Query length: %i Current position: %i Repetition: %i',  iterations, j, r)
                if j != 0:
                    pos = min(pos_dict[cur_domain])
                    pos_dict[cur_domain].discard(pos)
                    pattern_type = 0
                    if pos_dict[cur_domain]:
                        stop_count = 2 # min(2, len(pos_dict[cur_domain])+1)
                        var_count = random.randrange(start=1, stop = stop_count)
                        pos_list = random.sample(sorted(pos_dict[cur_domain]), var_count)
                        pos_dict[cur_domain].difference_update(pos_list)

                    new_sample_list = change_sample(new_sample_list, pattern_type, [cur_domain], pos, pos_list)
                if j%2 == 0:
                    results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=r, j=j+1, mod="number of patterns", file_path=file_path)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)
    return dataframe

def domain_size(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'domain_size_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        results= []
        for r in range(repetitions):

            for j in range(iterations):
                sample_list = non_matching_sample(trace_length=20,domain_size=j+1)
                # event0 = f' {sample_list[0].split()[0]}' *2
                # sample_list[0] = sample_list[0]+ ' '+ sample_list[0] + event0
                # sample_list[1] = event0[1:]
                # sample_list[1] = sample_list[0]
                #new_sample_list = change_sample(sample_list, pattern_type=0, pos=0, pos_list=[8], rand_domain=list(range(j+1)))
                new_sample_list = deepcopy(sample_list)
                for dom in range(j+1):
                    new_sample_list = change_sample(new_sample_list, pattern_type=1, pos=0, pos_list=[dom], rand_domain=[dom])
                    # new_sample_list = change_sample(new_sample_list, pattern_type=1, pos=dom+1, pos_list=[dom+2], rand_domain=[dom])
                # new_sample_list = change_sample(new_sample_list, pattern_type=1, pos=4, pos_list=[4], rand_domain=list(range(j+1)))
                #Modify Sample
                LOGGER.info('Query length: %i Current position: %i Repetition: %i',  iterations, j, r)
                discovery=['unified','separated', 'unified_ps', 'separated_ps']
                if j%2 == 0:
                    results, columns = match_algos(sample_list= new_sample_list, results=results, discovery=discovery,
                                               iterations=r, j=j+1, mod="domain size", file_path=file_path)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)
    return dataframe

def no_mixed_queries(iterations:int, repetitions:int, overwrite:bool = False):
    file_name = f'mixed_queries_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    results = []
    if not os.path.isfile(file_path) or overwrite:
        results= []
        for r in range(repetitions):
            sample_list = non_matching_sample(trace_length=5,domain_size=1)
            for j in range(iterations):
                new_sample_list = deepcopy(sample_list)
                k= j+1
                event0='x; '*k
                event1='y; '*k
                event2=sample_list[0]
                new_sample_list[0] = event0 +' z; z;' + ' ' + event2
                new_sample_list[1] = event2 + ' ' + event1 + ' w; w;'
                #Modify Sample
                LOGGER.info('Query length: %i Current position: %i Repetition: %i',  iterations, j, r)
            
                results, columns = match_algos(sample_list= new_sample_list, results=results, iterations=r, j=j+1, mod="mixed_queries", file_path=file_path)
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    else:
        dataframe = pd.read_csv(file_path)
    return dataframe

def synt_plots():
    
    iterations = 100
    file_name = f'trace_length_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df1 = pd.read_csv(file_path)
    else:
        df1 = min_trace_length(iterations=iterations, repetitions= 5, overwrite=False)
    df1['mode'] = 'trace_length'

    iterations = 16
    file_name = f'domain_size_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df2 = pd.read_csv(file_path)
    else:
        df2 = domain_size(iterations=iterations, repetitions= 5, overwrite=False)
    df2['mode'] = 'domain_size'

    iterations = 20
    file_name = f'types_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df3 = pd.read_csv(file_path)
    else:
        df3 = type_ratio(iterations=iterations, repetitions= 5, overwrite=False)
    df3['mode'] = 'types'


    # iterations = 20
    # file_name = f'supp_alphabet_{iterations}'
    # file_path = f'experiments/results/datasets/{file_name}.csv'
    # if os.path.isfile(file_path):
    #     df4 = pd.read_csv(file_path)
    # else:
    #     df4 = supp_alphabet(iterations=iterations, repetitions= 5, overwrite=False)
    # df4['mode'] = 'alphabet'

    iterations = 1000
    file_name = f'traces_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df4 = pd.read_csv(file_path)
    else:
        df4 = traces(iterations=iterations, repetitions= 5, overwrite=False)
    df4['mode'] = 'traces'


    

    iterations = 10
    file_name = f'max_pattern_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df5 = pd.read_csv(file_path)
    else:
        df5 = max_pattern_ratio(iterations=iterations, repetitions= 5, overwrite=False)
    df5['mode'] = 'pattern ratio'


    iterations = 10
    file_name = f'mixed_queries_{iterations}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    if os.path.isfile(file_path):
        df6 = pd.read_csv(file_path)
    else:
        df6 = no_mixed_queries(iterations=iterations, repetitions= 5, overwrite=False)
    df6['mode'] = 'mixed queries'


    frames = [df1, df2, df3, df4, df5, df6]

    result = pd.concat(frames)
    result.to_csv('experiments/results/datasets/synt_plots.csv')
    return result

if __name__ == "__main__":
    main('synt')
