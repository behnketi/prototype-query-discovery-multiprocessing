#!/usr/bin/python3
"""Contains functions to test perfomance of the different algorithms on syntatically created multidim-samples."""
import logging
import time
import sys
import os
import random
import gzip
from copy import deepcopy
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from itertools import product
from statistics import mean

import scienceplots
sys.path.append(".")
from generator import SampleGenerator
from discovery_bu_multidim import bu_discovery_multidim
from sample_multidim import MultidimSample
from discovery_il_miner import il_miner
from discovery_bu_pts_multidim import discovery_bu_pts_multidim

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def main():
    """
        Contains the main function of this file. Runs all the created test functions for different sample sizes and trace lengths.
    """
    result_path = 'experiments/results/'
    if not os.path.isdir(result_path):
        os.mkdir(result_path)
    if len(sys.argv) == 2:
        if sys.argv[1] == 'synt':
            synt_queries(iterations=10, repetitions= 5, mode='all', discovery=['unified', 'separated','unified_ps', 'separated_ps'])
        elif sys.argv[1] == 'google':
            test_google_traces()
        elif sys.argv[1] == 'il-miner':
            test_il_miner()
        elif sys.argv[1] == 'test':
            test_queries(discovery=['unified', 'separated', 'separated_ps'])
        else:
            raise ValueError
    else:
        sample_sizes = [2]
        min_trace_length = 20

        #LOGGER.info('Max query length: %i Mixed Queries',j)
        # df = synt_queries(iterations=10, repetitions= 10, mode='all', discovery=['unified', 'separated', 'separated_ps'])
        df1 = test_queries(discovery=['separated'])
        # test_sample()
        # test_il_miner()
        # test_google_traces()

def synt_queries(iterations:int, repetitions:int, mode:str= 'all', discovery:list|None = None):
    sample_list = non_matching_sample()
    trace_length = len(sample_list[0].split())
    domain_cnt = sample_list[0].split()[0].count(';')
    modes = ['Type', 'Pattern', 'Mixed'] if mode == 'all' else [mode]
    results= []
    file_name = f'synt_queries_{repetitions}'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    for mod, r in product(modes, range(repetitions)):
        new_sample_list = deepcopy(sample_list)
        pos_dict = {key: set(range(trace_length)) for key in range(domain_cnt)}
            # cur_domain = 0
            # used_positions = set()
        for j in range(iterations):
            cur_domain = j% domain_cnt
            #Modify Sample
            LOGGER.info('%s Queries - Query length: %i Current position: %i Repetition: %i', mod , iterations, j, r)
            if mod == 'Mixed':
                pattern_type = random.random()
                cur_mod = 'Pattern' if pattern_type <0.5 else 'Type'
            else:
                cur_mod = mod
            if j != 0:
                pos = min(pos_dict[cur_domain])
                pos_dict[cur_domain].discard(pos)
                # used_positions.add(pos)

                if cur_mod == 'Type':
                    pattern_type = 1
                    pos_list = [pos]

                elif cur_mod == 'Pattern':
                    pattern_type = 0
                    # if len(used_positions)<=10:
                    if pos_dict[cur_domain]:
                        stop_count = min(2, len(pos_dict[cur_domain])+1)
                        var_count = random.randrange(start=1, stop = stop_count)
                        pos_list = random.sample(sorted(pos_dict[cur_domain]), var_count)
                        pos_dict[cur_domain].difference_update(pos_list)
                        # used_positions.update(pos_list)
                    # else:
                    #     pos_list = [pos]
                    #     pos -=1
                    #     cur_domain+=1
                    #     used_positions = set()

                else:
                    raise ValueError("Use a valid mode: 'all', 'Type', 'Pattern' or 'Mixed'")
                new_sample_list = change_sample(new_sample_list, pattern_type, [cur_domain], pos, pos_list)

            results = match_algos(sample_list= new_sample_list, results=results, iterations=r, j=j, mod=mod, discovery=discovery, file_path=file_path)

    return generate_plots(
        results=results,
        file_name='synt_queries',
        y="time",
        x="iterations",
        hue="discovery algorithm",
        kind="box",
        col='mode',
        col_wrap=3,
    )





def test_queries(discovery:list|None = None):
    results = []
    samples = []
    file_name = f'test_queries'
    file_path = f'experiments/results/datasets/{file_name}.csv'
    # samples.append(["m;b;1; t;a;1; t;a;1;", "m;b;1; m;a;1; m;a;0;", "m;a;0; t;a;1; m;b;0;"])
    # samples.append(["m;b; t;a; t;a; t;b; t;a; t;b; t;b;", "m;b; m;a; m;a; m;b; m;b; m;b;","m;a; t;a; m;b; t;b;"])
    # samples.append(['m;r;r; f;o;f; t;t;t; b;b;b; c;r;c; m;o;o; m;o;m; n;n;h; q;q;q; c;h;h;', 'm;t;g; i;h;q; p;p;p; l;l;h; r;g;a; m;h;q; k;b;o; t;j;s; c;e;e; r;o;s;'])

    # samples.append(["a;a; b;b; b;b; a;a;", "a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a;" ])
    # samples.append(["a;1; a;1; a;1; a;1; a;1; a;1; a;1;", "b;0; b;0; b;0; b;0; b;0; c;3; d;4; e;5;" ])

    # samples.append(['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;'])
    # samples.append(['o;o;b; r;r;f; b;b;b; f;s;f; t;t;b; n;s;f; l;l;f; n;n;n; q;q;b; k;k;b;', 'g;s;b; p;t;f; f;t;i; a;m;f; t;j;f; c;l;b; t;r;f; c;k;t; k;h;i; t;s;b;'])
    # samples.append(['n;n;n; d;m;n; o;o;k; k;k;k; c;i;n; d;n;c; f;f;f; o;c;g; c;o;c; c;c;c;', 'g;k;c; o;a;c; h;h;t; c;o;t; d;e;c; o;b;g; l;l;f; h;q;j; d;h;q; q;q;q;'])
    # samples.append(['SOHU;201012010900;100;70.98; QLIK;201012010900;500;24.0; BRCM;201012010900;100;44.92; PAAS;201012010900;450;38.0; ZUMZ;201012010900;100;32.0; MSFT;201012010900;15194;25.47;', 'FCFS;201012010905;295;28.79; CREE;201012010905;240;66.15; MRVL;201012010905;200;19.58; SOHU;201012010905;200;71.0; APRI;201012010905;2900;3.69; MSFT;201012010905;1105;25.47;', 'MEOH;201012010910;852;29.37; LLNW;201012010910;2000;7.11; VECO;201012010910;100;44.95; MSFG;201012010910;100;9.2; FFIV;201012010910;200;134.5; MSFT;201012010910;1588;25.48;', 'CRUS;201012010915;700;15.55; FSLR;201012010915;100;124.25; CREE;201012010915;272;66.18; WERN;201012010915;723;21.58; APOL;201012010915;200;34.34; MSFT;201012010915;23650;25.48;', 'CMCSA;201012010920;8387;20.03; BPOP;201012010920;3000;2.89; QGEN;201012010920;16400;18.4; SQQQ;201012010920;300;34.7; APRI;201012010920;2500;3.74; MSFT;201012010920;30478;25.5;', 'LSTR;201012010925;100;36.99; AVGO;201012010925;1200;26.48; LRCX;201012010925;1681;46.33; MCOX;201012010925;2000;8.46; BEBE;201012010925;100;6.48; MSFT;201012010925;64646;25.57;', 'MSCC;201012010930;25546;22.92; PRFZ;201012010930;500;60.77; MSEX;201012010930;3622;17.62; MSFG;201012010930;3103;9.21; PRFT;201012010930;2810;11.661; MSFT;201012010930;3073560;25.82;', 'PEIX;201012010935;40792;0.735; MSG;201012010935;7664;22.69; PRCP;201012010935;200;5.15; BEAV;201012010935;2100;36.45; VICL;201012010935;6300;1.99; MSFT;201012010935;2284430;25.811;', 'VICL;201012010940;3200;1.98; CBRL;201012010940;1300;53.12; BEAV;201012010940;20722;36.58; MCOX;201012010940;75814;8.2; SMTC;201012010940;9072;24.15; MSFT;201012010940;1577610;25.875;', 'POOL;201012010945;23343;21.34; GLRE;201012010945;400;28.44; PICO;201012010945;3734;28.93; GIGM;201012010945;5500;1.43; NICE;201012010945;3100;30.76; MSFT;201012010945;1649546;25.792;', 'BEBE;201012010950;22222;6.5; CRAI;201012010950;100;22.5; TOPS;201012010950;200;0.81; FCBC;201012010950;400;14.0; CCMP;201012010950;13471;41.35; MSFT;201012010950;1122574;25.82;', 'IOSP;201012010955;600;21.76; EGLE;201012010955;7300;5.04; PRFT;201012010955;800;11.76; SBGI;201012010955;2269;7.92; PEBO;201012010955;100;13.12; MSFT;201012010955;985701;25.88;', 'RUSHB;201012011000;400;18.49; EGLE;201012011000;14276;5.03; UCFC;201012011000;3087;1.19; STKL;201012011000;1108;7.0; UEIC;201012011000;400;27.7; MSFT;201012011000;1958946;25.91;', 'BEAV;201012011005;6476;36.2; IBKR;201012011005;3800;18.22; EGHT;201012011005;7403;2.76; PLXT;201012011005;1250;3.35; STKL;201012011005;2800;7.01; MSFT;201012011005;2832616;25.92;', 'BBOX;201012011010;400;36.73; SPAR;201012011010;356;5.45; VCLT;201012011010;200;77.95; NEWS;201012011010;3900;8.7; SAPE;201012011010;42598;12.55; MSFT;201012011010;1212146;25.95;', 'NLST;201012011015;100;2.37; HEES;201012011015;800;10.27; MLHR;201012011015;1100;22.32; CTHR;201012011015;1000;2.51; CFNL;201012011015;500;10.74; MSFT;201012011015;1446707;25.8801;', 'GMCR;201012011020;137205;35.87; AFSI;201012011020;12601;16.3; STKL;201012011020;1050;7.01; TIGR;201012011020;900;4.6; EXPD;201012011020;33641;54.8; MSFT;201012011020;959930;25.88;', 'FOSL;201012011025;7950;67.85; RUTH;201012011025;12700;5.08; TUES;201012011025;4499;5.29; TIGR;201012011025;900;4.6; ARCC;201012011025;11261;16.5; MSFT;201012011025;1101040;25.91;', 'CBOE;201012011030;32518;24.12; FOSL;201012011030;4900;67.94; MTSN;201012011030;500;2.91; PDFS;201012011030;200;4.02; TIGR;201012011030;950;4.55; MSFT;201012011030;1011074;25.88;', 'GILT;201012011035;1150;4.83; EHTH;201012011035;719;15.5; KONE;201012011035;6100;4.35; ATAX;201012011035;900;5.3; WAVX;201012011035;21091;2.77; MSFT;201012011035;1093057;25.9;', 'VISN;201012011040;274;3.66; WPPGY;201012011040;300;56.57; BOOM;201012011040;800;16.38; ATHN;201012011040;7157;42.93; AMKR;201012011040;40711;7.115; MSFT;201012011040;1462151;25.91;', 'TCBK;201012011045;200;14.63; ALGT;201012011045;200;51.27; PTEN;201012011045;64946;20.1502; RSTI;201012011045;1300;29.85; OSBC;201012011045;100;1.92; MSFT;201012011045;562085;25.9242;', 'ESRX;201012011050;37681;54.36; EZCH;201012011050;11261;24.93; RSTI;201012011050;1700;29.89; MDAS;201012011050;8577;19.16; NDSN;201012011050;1329;82.085; MSFT;201012011050;1092095;25.9;', 'ADSK;201012011055;27197;36.38; RPRX;201012011055;240;1.34; PNFP;201012011055;2600;10.14; STKL;201012011055;13840;6.94; DCOM;201012011055;8514;13.54; MSFT;201012011055;919029;25.87;', 'TACT;201012011100;200;8.98; SAAS;201012011100;500;3.0; NVAX;201012011100;29686;2.3; BNCL;201012011100;800;7.865; BEAV;201012011100;7880;36.25; MSFT;201012011100;1982483;25.8429;', 'SSNC;201012011105;300;20.27; ORBK;201012011105;800;11.3; MLHR;201012011105;2200;22.34; USAT;201012011105;1034;1.08; BBGI;201012011105;900;3.9; MSFT;201012011105;542749;25.89;', 'NEWS;201012011110;1800;8.79; SNAK;201012011110;360;3.9; OGXI;201012011110;2600;16.0; PSEC;201012011110;4424;9.9; ACLS;201012011110;9300;2.57; MSFT;201012011110;309251;25.91;', 'IPXL;201012011115;14471;17.9; RBCAA;201012011115;100;21.48; EGLE;201012011115;4080;5.03; SRDX;201012011115;1415;9.2; ATSG;201012011115;8400;7.64; MSFT;201012011115;940898;25.855;', 'ADVS;201012011120;900;52.47; PICO;201012011120;100;28.82; VIVO;201012011120;6600;22.57; WAVX;201012011120;1800;2.76; NTAP;201012011120;35926;51.79; MSFT;201012011120;395559;25.86;', 'CIDM;201012011125;100;1.45; AVAV;201012011125;2000;24.66; JASO;201012011125;59494;6.96; HITT;201012011125;1000;58.85; SCSS;201012011125;1730;9.13; MSFT;201012011125;357310;25.86;', 'EGHT;201012011130;10881;2.84; MTRX;201012011130;2300;10.0; BCRX;201012011130;1400;5.08; ACPW;201012011130;6696;2.145; IPAR;201012011130;200;18.79; MSFT;201012011130;336566;25.88;', 'PERY;201012011135;1447;28.22; LOGM;201012011135;10659;45.4675; ANAT;201012011135;500;79.125; BKMU;201012011135;800;4.68; LOGI;201012011135;4400;20.0; MSFT;201012011135;319956;25.915;', 'VSAT;201012011140;2100;42.04; ANAT;201012011140;250;79.06; AREX;201012011140;2400;18.75; GRMN;201012011140;7322;29.83; BGCP;201012011140;9600;7.99; MSFT;201012011140;406036;25.92;', 'MGLN;201012011145;700;49.31; AFOP;201012011145;1700;10.63; PTSI;201012011145;100;11.16; ANAT;201012011145;500;78.98; AREX;201012011145;1300;18.75; MSFT;201012011145;831835;25.91;', 'PERY;201012011150;2600;28.24; WMAR;201012011150;600;9.64; ANAT;201012011150;200;79.03; ULTI;201012011150;2100;45.81; ACWI;201012011150;1154;44.93; MSFT;201012011150;1357745;25.98;', 'IRDM;201012011155;2500;9.45; STEM;201012011155;5350;1.14; APPY;201012011155;9597;0.569; CLMS;201012011155;1200;12.0; RBCAA;201012011155;200;21.4; MSFT;201012011155;1057082;25.98;', 'MGRC;201012011200;200;28.19; OFLX;201012011200;712;16.51; JAKK;201012011200;500;19.56; OPEN;201012011200;13695;73.24; III;201012011200;1300;2.14; MSFT;201012011200;1279661;26.03;', 'ENDP;201012011205;2400;36.83; EXAC;201012011205;1100;18.0; LAYN;201012011205;2000;33.46; COHU;201012011205;400;14.92; BEAV;201012011205;3600;36.67; MSFT;201012011205;549357;26.058;', 'PRAN;201012011210;300;1.43; HTWR;201012011210;600;94.0625; SGMO;201012011210;800;4.82; SANM;201012011210;12478;10.99; ENG;201012011210;450;3.02; MSFT;201012011210;660972;26.015;', 'GIFI;201012011215;300;27.41; MSFG;201012011215;1200;9.5; SPNS;201012011215;300;2.56; LAYN;201012011215;100;33.28; ENDP;201012011215;2600;36.82; MSFT;201012011215;382112;26.0099;', 'SBSI;201012011220;4341;21.4; SPU;201012011220;300;5.03; VICL;201012011220;200;1.965; RJET;201012011220;2195;7.77; BEAT;201012011220;3100;4.05; MSFT;201012011220;381339;26.035;', 'KRNY;201012011225;100;8.68; EXEL;201012011225;6900;5.988; BEAV;201012011225;3700;36.605; CPSI;201012011225;501;47.05; JVA;201012011225;100;3.85; MSFT;201012011225;313929;26.05;', 'STMP;201012011230;1100;13.42; SBRA;201012011230;2900;17.13; TRST;201012011230;500;5.64; CTRN;201012011230;3100;23.56; PLCE;201012011230;8310;52.11; MSFT;201012011230;510181;26.01;', 'HTCH;201012011235;5600;3.09; BSPM;201012011235;2100;2.84; OSTK;201012011235;1271;15.77; PLCE;201012011235;2100;51.99; PHIIK;201012011235;500;17.64; MSFT;201012011235;383391;26.0475;', 'ATLO;201012011240;580;20.06; PRCP;201012011240;500;5.1899; INDB;201012011240;300;25.56; SBSI;201012011240;600;21.4; NDSN;201012011240;300;82.58; MSFT;201012011240;499046;26.08;', 'PSMT;201012011245;1100;34.73; GAME;201012011245;9249;5.8; TNDM;201012011245;4400;14.77; ALNY;201012011245;3204;9.36; OMEX;201012011245;100;2.0; MSFT;201012011245;657280;26.081;', 'PRFT;201012011250;5200;11.685; ANTH;201012011250;500;5.49; VGLT;201012011250;401;62.19; COKE;201012011250;500;58.33; SWKS;201012011250;29104;25.975; MSFT;201012011250;754052;26.11;', 'UTEK;201012011255;9300;19.41; PRXI;201012011255;200;1.9; NVGN;201012011255;2000;0.51; ANGO;201012011255;500;14.27; MNDO;201012011255;100;2.31; MSFT;201012011255;629128;26.13;', 'MCRL;201012011300;3313;12.86; THOR;201012011300;10532;25.72; ENDP;201012011300;3775;36.82; ISIL;201012011300;27700;13.01; TCBK;201012011300;100;14.72; MSFT;201012011300;571385;26.13;', 'ASTI;201012011305;2261;3.3; PLUS;201012011305;500;23.62; ENDP;201012011305;4933;36.73; EXLS;201012011305;300;20.91; VRSK;201012011305;2019;30.79; MSFT;201012011305;828137;26.16;', 'EMMT;201012011310;300;26.63; IPGP;201012011310;15417;30.7399; THOR;201012011310;9300;25.74; LTRE;201012011310;100;10.07; ENDP;201012011310;3110;36.77; MSFT;201012011310;1173347;26.21;', 'RLOC;201012011315;662;17.96; INFA;201012011315;5900;42.05; CWCO;201012011315;400;8.83; ENDP;201012011315;6700;36.68; THOR;201012011315;8069;25.68; MSFT;201012011315;1344335;26.12;', 'CWCO;201012011320;700;8.82; AMAG;201012011320;29716;15.69; ENDP;201012011320;6079;36.7; AMAT;201012011320;102491;12.81; THOR;201012011320;3500;25.73; MSFT;201012011320;780252;26.13;', 'APAGF;201012011325;1800;38.77; VICL;201012011325;2400;1.95; SNSS;201012011325;1170;0.3894; ALGN;201012011325;2150;17.9; LOGI;201012011325;11091;19.96; MSFT;201012011325;614637;26.12;', 'BEBE;201012011330;14348;6.28; HOMB;201012011330;804;21.24; AAPL;201012011330;104343;316.97; CBNK;201012011330;200;12.31; ANTH;201012011330;2100;5.485; MSFT;201012011330;650589;26.13;', 'INDY;201012011335;8600;30.288; ESBF;201012011335;200;14.57; HCBK;201012011335;10875;11.52; ANTH;201012011335;600;5.49; BRKL;201012011335;1700;10.08; MSFT;201012011335;459753;26.1701;', 'HTWR;201012011340;1100;94.01; AAPL;201012011340;98697;317.09; ZINC;201012011340;4774;11.61; BRKL;201012011340;3900;10.08; ANTH;201012011340;300;5.48; MSFT;201012011340;596634;26.14;', 'ADEP;201012011345;200;4.65; CLDX;201012011345;2100;4.15; ENDP;201012011345;5254;36.84; INFA;201012011345;3042;42.065; COHU;201012011345;1600;14.965; MSFT;201012011345;616279;26.17;', 'GBLI;201012011350;900;19.39; CLDX;201012011350;20143;4.125; INFA;201012011350;7400;42.02; SNSS;201012011350;3300;0.39; CRAY;201012011350;4600;7.22; MSFT;201012011350;911371;26.13;', 'IKAN;201012011355;2393;1.17; INFA;201012011355;11167;42.07; TXRH;201012011355;3200;17.19; CETV;201012011355;7655;19.07; SRCE;201012011355;400;18.97; MSFT;201012011355;396623;26.1624;', 'COBZ;201012011400;883;5.12; BKCC;201012011400;5113;11.45; MYGN;201012011400;4479;21.45; ULTA;201012011400;5372;36.02; NVAX;201012011400;2200;2.29; MSFT;201012011400;506916;26.17;', 'SQI;201012011405;500;13.4; GTLS;201012011405;5150;32.64; CHRW;201012011405;8554;75.075; ISRG;201012011405;4886;265.88; MSCC;201012011405;5860;22.92; MSFT;201012011405;486512;26.18;', 'COWN;201012011410;1452;4.24; EMMS;201012011410;1080;0.586; HSTM;201012011410;200;6.36; CHOP;201012011410;100;5.75; MSFG;201012011410;1683;9.35; MSFT;201012011410;465136;26.19;', 'GIII;201012011415;1300;27.85; QCOM;201012011415;116401;48.08; MSEX;201012011415;600;17.94; EGLE;201012011415;15212;5.07; PRCP;201012011415;100;5.18; MSFT;201012011415;506560;26.19;', 'AEIS;201012011420;4400;12.0; OPTT;201012011420;600;6.18; PCCC;201012011420;300;9.18; MCBC;201012011420;400;2.64; KERX;201012011420;13600;5.15; MSFT;201012011420;741344;26.16;', 'CHRW;201012011425;20237;74.97; SSRI;201012011425;3500;26.8; KERX;201012011425;16200;5.14; SIGM;201012011425;5400;12.08; SYBT;201012011425;400;24.15; MSFT;201012011425;568857;26.15;', 'AEIS;201012011430;2450;12.0; BIOS;201012011430;3907;4.43; CLMT;201012011430;1757;21.778; ENOC;201012011430;6200;25.85; SSRI;201012011430;2650;26.811; MSFT;201012011430;378806;26.18;', 'CLMS;201012011435;1200;12.0; SSRI;201012011435;7000;26.825; FSRV;201012011435;315;26.67; CEVA;201012011435;4100;22.85; EMCI;201012011435;500;22.22; MSFT;201012011435;748879;26.2;', 'SRCE;201012011440;100;18.97; BIIB;201012011440;10330;65.58; QCOM;201012011440;56778;48.04; PRFT;201012011440;1400;11.7; GIII;201012011440;1400;27.775; MSFT;201012011440;431381;26.2;', 'NVTL;201012011445;73406;9.13; ORIT;201012011445;11302;11.44; ACGL;201012011445;6000;90.65; RAIL;201012011445;1204;25.71; ACOR;201012011445;1500;26.42; MSFT;201012011445;486792;26.2099;', 'LNCE;201012011450;1300;23.3; LBTYA;201012011450;15130;35.8; LNBB;201012011450;1860;4.86; MNRO;201012011450;100;49.85; ENOC;201012011450;4950;25.85; MSFT;201012011450;247462;26.2;', 'FSBK;201012011455;2125;7.69; NVDA;201012011455;48972;14.255; ZAGG;201012011455;400;7.07; RICK;201012011455;2250;7.08; TWGP;201012011455;4400;26.34; MSFT;201012011455;764775;26.24;', 'CYBE;201012011500;1900;8.85; PTRY;201012011500;1700;20.56; WSBC;201012011500;3592;17.97; HYGS;201012011500;200;3.7; RPTP;201012011500;200;3.785; MSFT;201012011500;358915;26.21;', 'GTLS;201012011505;7400;32.65; KBALB;201012011505;400;6.09; UMPQ;201012011505;8366;10.98; FWLT;201012011505;40722;29.37; PTRY;201012011505;2194;20.58; MSFT;201012011505;560941;26.16;', 'MLNX;201012011510;4800;24.33; ASFI;201012011510;680;7.08; HRZN;201012011510;2700;14.745; KEYW;201012011510;7600;13.69; LMNX;201012011510;400;17.35; MSFT;201012011510;1034230;26.11;', 'EMCI;201012011515;300;22.24; MLNX;201012011515;3000;24.35; ESIO;201012011515;2100;15.09; FAST;201012011515;6748;54.94; MOFG;201012011515;1147;14.93; MSFT;201012011515;734456;26.13;', 'CVV;201012011520;200;7.4499; ZHNE;201012011520;1398;2.6; CVTI;201012011520;2260;8.88; CRESY;201012011520;5900;19.0001; PLCE;201012011520;7520;52.03; MSFT;201012011520;590900;26.17;', 'FBNC;201012011525;2520;14.49; ZN;201012011525;200;4.91; SSRI;201012011525;14990;26.87; ELGX;201012011525;1000;5.87; AAWW;201012011525;3121;55.22; MSFT;201012011525;592232;26.17;', 'BSDM;201012011530;6700;4.32; WIBC;201012011530;2404;7.27; NWS;201012011530;8942;15.68; QCOM;201012011530;199903;47.99; CPLA;201012011530;6414;56.77; MSFT;201012011530;572925;26.1711;', 'MLHR;201012011535;2400;22.53; MSG;201012011535;2479;22.86; ELON;201012011535;1465;9.76; FCBC;201012011535;1100;14.12; FBNC;201012011535;630;14.51; MSFT;201012011535;748330;26.18;', 'CEVA;201012011540;14756;22.75; SSRI;201012011540;38331;26.82; WIBC;201012011540;13491;7.27; IRWD;201012011540;1000;10.76; OSBC;201012011540;10635;1.92; MSFT;201012011540;426239;26.18;', 'IRWD;201012011545;1000;10.77; TBNK;201012011545;1500;18.56; NWBI;201012011545;79382;10.45; PESI;201012011545;200;1.49; CYTK;201012011545;3800;2.23; MSFT;201012011545;1224619;26.14;', 'PRAN;201012011550;3000;1.43; PPHM;201012011550;8192;1.59; NVEC;201012011550;1499;52.13; CLMT;201012011550;4700;21.56; PERY;201012011550;1580;27.88; MSFT;201012011550;1300261;26.07;', 'INTC;201012011555;2857728;21.49; MSCC;201012011555;44858;22.99; AETI;201012011555;200;2.4; ARCI;201012011555;100;3.45; RSYS;201012011555;5771;9.03; MSFT;201012011555;3251435;26.05;', 'STAA;201012011600;9159;5.51; BASI;201012011600;100;0.9; AEIS;201012011600;42269;11.91; MSFG;201012011600;5229;9.35; AOSL;201012011600;1509;11.76; MSFT;201012011600;2805739;26.0;', 'AEPI;201012011605;482;24.96; MSEX;201012011605;1599;17.88; TSRA;201012011605;14530;20.07; AEIS;201012011605;22812;11.93; MSFG;201012011605;6030;9.39; MSFT;201012011605;2950;26.0;', 'FLWS;201012011610;1400;1.98; MGIC;201012011610;100;6.01; ESEA;201012011610;14101;3.81; CBLI;201012011610;1411;6.7; CNIT;201012011610;4467;5.29; MSFT;201012011610;226746;26.0005;', 'MU;201012011615;45000;7.269; FLIR;201012011615;5343;27.68; NDAQ;201012011615;21165;22.57; BLDR;201012011615;1367;1.48; PGTI;201012011615;176;2.21; MSFT;201012011615;500;26.01;', 'BBOX;201012011620;105;36.96; YHOO;201012011620;1100;16.12; FBNC;201012011620;259;14.55; NFLX;201012011620;2580;199.99; FFIC;201012011620;300;13.67; MSFT;201012011620;1100;26.01;', 'TROW;201012011625;100;59.46; TSYS;201012011625;7940;4.78; XLNX;201012011625;25000;27.13; AGNC;201012011625;1662;29.41; CASY;201012011625;932;39.91; MSFT;201012011625;238;26.01;', 'IMMR;201012011630;359;5.87; ATML;201012011630;40000;10.39; CSCO;201012011630;1200;19.3; AAPL;201012011630;999;316.37; LOPE;201012011630;500;18.86; MSFT;201012011630;110500;26.09;', 'CSCO;201012011635;2348;19.3; JASO;201012011635;700;6.98; BBBY;201012011635;200;44.54; BRCD;201012011635;4970;5.05; STEM;201012011635;1000;1.12; MSFT;201012011635;5100;26.01;', 'CAVM;201012011640;3570;38.1841; UNIS;201012011640;100;5.5; JASO;201012011640;100;6.98; CYBX;201012011640;300;27.22; URRE;201012011640;1000;3.72; MSFT;201012011640;2033;26.0;', 'PNRA;201012011645;1073;101.9; GOOG;201012011645;100;563.5; TQQQ;201012011645;1000;136.15; SQNM;201012011645;200;6.35; LOPE;201012011645;500;18.86; MSFT;201012011645;16700;25.99;', 'NVDA;201012011650;1700;14.19; BIDU;201012011650;200;106.67; QQQ;201012011650;800;53.08; MU;201012011650;800;7.39; BEAV;201012011650;300;36.57; MSFT;201012011650;300;25.99;', 'NVDA;201012011655;1200;14.19; NFLX;201012011655;953;200.0; CAVM;201012011655;3570;38.18; YHOO;201012011655;300;16.11; MXIM;201012011655;400;23.81; MSFT;201012011655;100;26.0;'])
    # samples.append(['a;a;a;a;a;a; a;a;a;a;a;a; a;a;a;a;a;a; a;a;a;a;a;a;', 'a;a;a;a;a;a; a;a;a;a;a;a;'])
    # samples.append(['b; d;', 'b; d;'])
    samples.append(['p;p; o;o; t;t; k;k; i;i; a;a; s;s; b;b; p;p; g;g;', 'm;m; n;n; l;l; k;h; i;r; f;f; c;c; q;q; m;m; d;d;'])
    for iteration, sample_list in enumerate(samples):
        results, columns = match_algos(sample_list= sample_list, results=results, iterations=len(samples), j=iteration, mod='all', discovery=discovery, file_path=file_path)

    # columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time', 'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence', 'trace max type occourence', 'min pattern length', 'sample set', 'max query length']
    dataframe = pd.DataFrame(results, columns=columns)
    dataframe.to_csv(file_path)
    return generate_plots(dataframe=dataframe,
        file_name='test_queries',
        y="time",
        x="iterations",
        hue="discovery algorithm",
        kind="scatter",
    )

def test_il_miner():
    results = []

    samples = []
    samples.append(["a; b;" , "a; b;" ])
    samples.append(["a; b; c; d;", "a; b; c; d;" ])
    samples.append(["a; b; c; d; e; f;", "a; b; c; d; e; f;" ])
    samples.append(["a; b; c; d; e; f; g; h;", "a; b; c; d; e; f; g; h;" ])
    samples.append(["a; b; c; d; e; f; g; h; i; j;", "a; b; c; d; e; f; g; h; i; j;" ])
    samples.append(["a; b; c; d; e; f; g; h; i; j; k; l;", "a; b; c; d; e; f; g; h; i; j; k; l;" ])
    samples.append(["a; b; c; d; e; f; g; h; i; j; k; l; m;", "a; b; c; d; e; f; g; h; i; j; k; l; m;" ])
    # samples.append(["a; c; e; g; i; k; l;", "a; c; e; g; i; k; l;" ])
    # samples.append(["a; c; e; g; i; k; l; m;", "a; c; e; g; i; k; l; m;" ])
    # samples.append(["a; c; e; g; i; k; l; m; n;", "a; c; e; g; i; k; l; m; n;" ])
    # samples.append(["a; c; e; g; i; k; l; m; n; o;", "a; c; e; g; i; k; l; m; n; o;" ])

    for iteration, sample_list in enumerate(samples):
        LOGGER.info('Sample: %i', iteration)
        results = match_algos(sample_list= sample_list, results=results, iterations=len(samples), j=iteration, mod='all', discovery=['il-miner','il-miner2'])

    return generate_plots(
        results=results,
        file_name='test_il-miner',
        y="time",
        x="trace length",
        hue="discovery algorithm",
        kind="bar",
    )

def test_google_traces():
    results = []
    for i in range(2,3):
        # data_file = open(f'experiments/google_{i}_results.txt', 'w', encoding="utf-8")
        LOGGER.info('Started file %i', i)
        # for k in [10,50,200, 500, 1000]:
        #     LOGGER.info('Number of traces: %i', k)
        trace_lengths = [10, 50, 100]#, 200, 500]
        for j in trace_lengths:
            LOGGER.info('Length of traces: %i', j)
            file = gzip.open(f'datasets/Sigmod2024/google_query{i}.txt.gz', 'rb')
            sample_list = []
            counter = 0
            start= time.time()
            for trace1 in file:

                counter+=1
                sample_list.append(' '.join(trace1.decode().split()[-j:]))

            file.close()
            result = time.time() - start
            LOGGER.info('Finished reading file %i: %i lines in %f seconds', i, counter, result)


            results = match_algos(sample_list= sample_list, results=results, iterations=3, j=j, mod='all', discovery=['unified', 'separated'])
    return generate_plots(
        results=results,
        file_name='google_traces',
        y="time",
        x='max query length',
        hue="discovery algorithm",
        kind="scatter",
        col='iterations',
        col_wrap=3,
    )



def test_sample():
    sample = MultidimSample(["b; a; a; c; d; d; b; a; b; b;", "b; a; a; b; c; d; d; a; c; c; b; b;", "a; a; b; c; b; a; c; d; b; a;", "c; a; a; b; d; b; c; c; d; d; b; a;",\
            "b; a; c; d; d; b; b; a; c; d; d; b;"])
    result_dict1 = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False, max_query_length=10)
    print(result_dict1['queryset'])
    result_dict2 = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=True, max_query_length=10)
    print(result_dict2['queryset'])


def change_sample(sample_list:list, pattern_type:int, rand_domain:list, pos:int, pos_list:list):
    if pattern_type == -1:
        new_trace = sample_list[0]
        for pos2 in pos_list:
            new_trace = ' '.join(new_trace.split()[:pos2] + [new_trace.split()[pos]] + new_trace.split()[pos2+1:])
            sample_list[0] = new_trace
        return sample_list
    domain_cnt = len(sample_list[0].split()[pos].split(';'))
    for idx, trace in enumerate(sample_list):
        
        if pattern_type == 0:
            new_trace = trace
            for pos2 in pos_list:
                if len(rand_domain) < domain_cnt:
                    for i in rand_domain:
                        new_event = new_trace.split()[pos2].split(';')
                        new_event[i] = new_trace.split()[pos].split(';')[i]
                        new_trace = ' '.join(new_trace.split()[:pos2] + [';'.join(new_event)] + new_trace.split()[pos2+1:])
                else:
                    new_trace = ' '.join(new_trace.split()[:pos2] + [new_trace.split()[pos]] + new_trace.split()[pos2+1:])
                sample_list[idx] = new_trace
        else:
            trace0 = sample_list[0]
            trace0_list = trace0.split()
            new_trace = trace
            pos2 = pos_list[0]
            for dom in rand_domain:
                letter = trace0_list[pos].split(';')[dom]
                pos_event = new_trace.split()[pos2].split(';')
                pos_event[dom] = letter
                new_trace = ' '.join(new_trace.split()[:pos2] + [';'.join(pos_event)] + new_trace.split()[pos2+1:])
                sample_list[idx] = new_trace
    return sample_list


def match_algos(sample_list: list, results: list, iterations: int, j: int|str, mod: str,file_path: str, 
                discovery: list|None = None, max_query_length: int = - 1, only_types: bool = False) -> list:
    if not discovery:
        discovery = ['unified', 'separated', 'separated_ps', 'unified_ps']
    
    sample = MultidimSample()
    sample.set_sample(sample_list)
    sample_size = len(sample_list)

    trace_sample = MultidimSample()
    trace_sample.set_sample([sample_list[0]])

    trace_length = sample_list[0].count(' ') + 1
    sample.get_vertical_sequence_database()
    # stats = sample.sample_stats()
    len_pattern = []
    patterns = []
    trace_pattern = 0
    pattern_cnt = 0
    supp =1.0
    att_vsdb = sample.get_att_vertical_sequence_database()
    vsdb = {}
    domain_cnt = sample._sample_event_dimension
    alphabet = set()
    gen_event= ';' * domain_cnt
    gen_event_list = [i for i in gen_event]
    patternset ={}
    for domain, dom_vsdb in att_vsdb.items():
        patternset[domain] = set()
        for key, value in dom_vsdb.items():
            new_key = ''.join(gen_event_list[:domain] + [key] + gen_event_list[domain:])
            vsdb[new_key] = value
            if not only_types:
                for item in value.keys():
                    if len(value[item]) >= 2:
                        patternset[domain].add(key)
                        break

    pattern_list = [0]*sample_size
    for pos_dict in vsdb.values():
        for trace, positions in pos_dict.items():
            if len(positions) >= 2:
                pattern_list[trace] = pattern_list[trace] + 1

    sample_sized_support = sample_size
    alphabet = {symbol for symbol,value in vsdb.items() if len(value) >= sample_sized_support}
    # vsdb = sample._sample_att_vertical_sequence_database
    dim_sample_dict = sample.get_dim_sample_dict()
    dim_stats = {dim: dim_sample.sample_stats() for dim, dim_sample in dim_sample_dict.items()}
    dim_trace_sample_dict = trace_sample.get_dim_sample_dict()
    dim_trace_stats = {dim: dim_sample.sample_stats() for dim, dim_sample in dim_trace_sample_dict.items()}
    for idx, _ in enumerate(sample._sample):
        for domain in att_vsdb.keys():
            for _, value in att_vsdb[domain].items():
                if list(value.keys())[0] == idx:
                    trace_pattern = max(trace_pattern, len(value[idx]))
                    if len(value[idx]) >= 2:
                        pattern_cnt +=1
        len_pattern.append(trace_pattern)
        patterns.append(pattern_cnt)
        trace_pattern = 0
        pattern_cnt = 0
        max_type_occurence =[stats['sample type distribution ordered'][-1][1] for stats in dim_stats.values()]
        trace_max_type_occurence =[stats['sample type distribution ordered'][-1][1] for stats in dim_trace_stats.values()]
    min_pattern_len = min(len_pattern)

    for matching in discovery:
        start= time.time()
        if matching == 'unified':
            copy_sample = deepcopy(sample)
            result_dict = bu_discovery_multidim(copy_sample, 1.0, 'smarter', domain_seperated=False, 
                                                max_query_length=max_query_length, only_types=only_types)
            result= time.time()-start
            queryset1 = result_dict['queryset']
            queryset = queryset1
            # for i, querystring in enumerate(queryset):
            #     print(querystring)
            querycount = result_dict['querycount']
            searchspace = len(result_dict['matchingset'])

        elif matching == 'separated':
            copy_sample = deepcopy(sample)
            result_dict = bu_discovery_multidim(copy_sample, 1.0, 'smarter', domain_seperated=True,
                                                max_query_length=max_query_length)
            queryset2 = result_dict['queryset']
            queryset = queryset2
            result= time.time()-start
            searchspace = len(result_dict['matchingset'])
            # domain_string = ' '.join(str(e) for e in result_dict['domain_queries'])
        elif matching == 'separated_ps':
            copy_sample = deepcopy(sample)
            result_dict = bu_discovery_multidim(copy_sample, 1.0, 'pattern-split-sep', domain_seperated=True,
                                                max_query_length=max_query_length)
            result= time.time()-start
            queryset3 = result_dict['queryset']
            queryset = queryset3
            searchspace = len(result_dict['matchingset'])
            #max_query= sorted(queryset, key=len)[-1].count(' ') +1
        elif matching == 'unified_ps':
            copy_sample = deepcopy(sample)
            result_dict = discovery_bu_pts_multidim(copy_sample, 1.0, use_smart_matching=True, discovery_order='type_first',
                                                    use_tree_structure=True, max_query_length=max_query_length)[2]
            result = time.time()-start
            queryset5 = result_dict['queryset'] - {''}
            searchspace = result_dict['querycount']
            queryset = queryset5
            # assert queryset5 == queryset1
        elif matching == 'il-miner':
            copy_sample = deepcopy(sample)
            result_dict = il_miner(copy_sample, complete =False, max_query_length=max_query_length)
            queryset4 = result_dict['queryset']
            queryset = queryset4
            result= time.time()-start
            searchspace = len(result_dict['matchingset'])
            # domain_string = ' '.join(str(e) for e in result_dict['domain_queries'])
        elif matching == 'il-miner2':
            copy_sample = deepcopy(sample)
            result_dict = il_miner(copy_sample, complete =True, max_query_length=max_query_length)
            queryset5 = result_dict['queryset']
            queryset = queryset5
            result= time.time()-start
            searchspace = len(result_dict['matchingset'])
            # domain_string = ' '.join(str(e) for e in result_dict['domain_queries'])
        results.append([str(sample_size), str(trace_length),str(iterations), matching,str(result),
                        str(j), str(len(queryset)), queryset, mod, searchspace, str(max_type_occurence),
                        str(trace_max_type_occurence), min_pattern_len,
                        str(max_query_length), str(len(alphabet)), str(mean(pattern_list))])
            # generate_plots(results=results, file_name='in_progress', y="time", x="iterations", hue="discovery algorithm",kind="scatter")
        columns = ['sample size', 'trace length','iteration', 'discovery algorithm', 'time',
                   'iterations', 'queryset size', 'queryset', 'mode', 'searchspace', 'max type occourence',
                   'trace max type occourence', 'min pattern length', 'max query length',
                   'supported types', 'pattern types']
        dataframe = pd.DataFrame(results, columns=columns)
        dataframe.to_csv(file_path)
    return results, columns

def generate_plots(dataframe:pd.DataFrame, file_name:str, x:str, y:str, hue:str, 
                   col: int|None = None, col_wrap: int|None = None, kind='bar',
                   overwrite:bool = False, facet_kws:dict|None = None) -> pd.DataFrame:
    sns.set_context("paper", font_scale=4)
    plt.style.use(['science'])
    plt.rcParams['lines.linewidth'] = 2 
    if kind != 'sota':
        dataframe = dataframe.astype({'time':'float', 'iterations':'int'})
    else:
        dataframe = dataframe.astype({'time':'float'})
    result_path = 'experiments/results/datasets'
    if not os.path.isdir(result_path):
        os.mkdir(result_path)
    plot_path = 'experiments/results/plots'
    if not os.path.isdir(plot_path):
        os.mkdir(plot_path)
    hatches = {'unified':'.',
                   'separated': '/',
                   'unified_ps': '|',
                   'separated_ps': '//',
                   'il-miner': 'x',
                   'il-miner2': 'o',
                   'rl': '/',}

    if kind in {'synt_queries'}:
        grid = sns.catplot(data=dataframe, y=y, x=x, hue=hue , kind='box', col=col, col_wrap=col_wrap)
        grid.set(yscale='log')
        grid.savefig(f'experiments/results/plots/{file_name}.pdf')
    elif kind == 'sota':
        fig, grid = plt.subplots(1,2, sharey=True, layout='constrained', figsize=(12, 3))
        grid[0].set(yscale='log')
        adatped_dataframe = dataframe.loc[dataframe['discovery algorithm'] != 'il-miner']
        cart_product = list(product(adatped_dataframe['discovery algorithm'].unique(), adatped_dataframe['iterations'].unique()))
        querystring_dict = {}

        for queryset in adatped_dataframe['queryset']:
            for querystring in queryset:
                if querystring in querystring_dict:
                    continue
                querystring_dict[querystring] = 0
                for event in querystring.split():
                    querystring_dict[querystring] += sum(1 for dim in event.split(';') if dim)
        for idx, mod in enumerate(['google', 'finance']):
            data_subplot = adatped_dataframe.loc[adatped_dataframe['mode'] == mod]
            sns.barplot(data=data_subplot, y=y, x=x, hue=hue, ax=grid[idx])

            for i, p in zip(cart_product, grid[idx].patches):
                p.set(hatch=hatches[i[0]])
                if i[0] in ['unified', 'il-miner']:
                    LOGGER.debug(p.get_x())
                    queryset_sizes = data_subplot.loc[(data_subplot['discovery algorithm'] == i[0]) & (data_subplot['iterations'] == i[1])]['queryset size'].values
                    queryset = data_subplot.loc[(data_subplot['discovery algorithm'] == i[0]) & (data_subplot['iterations'] == i[1])]['queryset'].values[0]
                    if len(queryset) == 0:
                        avg_querylength = 0
                    else:
                        avg_querylength = max(
                            querystring_dict[querystring]
                            for querystring in queryset
                        )
                    queryset_size = mean([int(j) for j in queryset_sizes])

                    grid[idx].annotate(f'{queryset_size}', # \n{avg_querylength}',
                                (p.get_x() + p.get_width() / 2., 1), #p.get_height()/2),
                                ha = 'center', va = 'center',
                                #size=15,
                                xytext = (0, 10),
                                xycoords= 'data',
                                textcoords = 'offset pixels')

                if idx == 0:
                    grid[idx].legend([], [], frameon=False)
                    grid[idx].set(xlabel='Google')
                    # plt.bar_label(splot.containers[0], label_type='center')
                    fig.legend(loc='outside right')
                else:
                    grid[idx].set(ylabel=None, xlabel='Finance')
                    grid[idx].legend([], [], frameon=False)
                    # sns.move_legend(grid[idx], "upper left", bbox_to_anchor=(1, 1))
                    # fig.legend()


        fig.savefig(f'experiments/results/plots/{file_name}.pdf')
    elif kind in ['number_of_types', 'max_pattern', 'trace_length', 'number_of_patterns',
                  'supp_alphabet', 'synt', 'domain_size']:
        
        grid = sns.relplot(data=dataframe, y=y, x=x, hue=hue, style=hue, err_style='bars',
                           kind='line', col=col, col_wrap=col_wrap, facet_kws=facet_kws)
        
        if kind == 'synt':
            # grid.set(xticklabels=[])  # remove the tick labels
            grid.set(xlabel='')  # remove the axis label
            grid.set_titles("{col_name}")
        grid.set(yscale='log', yticks=[1, 100])

        grid.savefig(f'experiments/results/plots/{file_name}.pdf')
    
    else:
        fig, grid = plt.subplots()
        grid.set(yscale='log')
        
        iteration = len(dataframe.iteration.unique())
        sns.barplot(data=dataframe, y=y, x=x, hue=hue, ax=grid)
        disc_list = [disc for disc in dataframe['discovery algorithm'].unique() for _ in range(iteration)]
        for disc,patch in zip(disc_list,grid.patches):
            patch.set(hatch=hatches[disc])
        grid.legend()
        if kind == 'rl_compare':
            grid.set(xlabel=None)
        sns.move_legend(grid, "upper left", bbox_to_anchor=(1, 1))
        plt.savefig(f'experiments/results/plots/{file_name}.pdf')

    return dataframe

def non_matching_sample(sample_size:int=2, trace_length:int = 20, domain_size:int = 3) ->list:
    generator = SampleGenerator()
    generator.generate_sample_w_empty_queryset(sample_size=sample_size, min_trace_length=trace_length, max_trace_length=trace_length)
    sample_list= []
    for trace in generator._sample._sample:
        new_trace = []
        for event in trace.split():
            new_event= f"{event};" *domain_size
            new_trace.append(new_event)
        sample_list.append(' '.join(new_trace))
    return sample_list


# def profile(sample_list, discovery):

if __name__ == "__main__":
    main()
