#!/usr/bin/python3
"""Contains functions to test perfomance of the different algorithms on syntatically created multidim-samples."""
import logging
import seaborn as sns
import pandas as pd
from sample_multidim import MultidimSample
from query_multidim import MultidimQuery

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main():
    queryset = {'m;;$x0; ;;$x0;', '$x0;;$x1; $x0;;$x1;', 'm;b;;', '$x0;a;; $x0;;;', 'm;;; ;a;1;', ';a;; ;a;;'}
    sample_list = ["m;b;1; t;a;1; t;a;1;", "m;b;1; m;a;1; m;a;0;", "m;a;0; t;a;1; m;b;0;"]
    sample = MultidimSample()
    sample_range_list = list(range(len(sample_list)))
    sample.set_sample(sample_list)
    event_db = sample.get_att_vertical_sequence_database()
    instance_dictionary = {}
    for querystring in queryset:
        query = MultidimQuery()
        query.set_query_string(querystring, recalculate_attributes=False)
        instance_dictionary = query.query_pos_dict(event_db, sample, instance_dictionary, trace_list=sample_range_list)

    LOGGER.info(instance_dictionary)
    supp = supp_count(instance_dictionary, queryset)
    match = match_count(instance_dictionary, queryset)
    print(supp, match)

def supp_count(instance_dictionary, queryset):
    return [len(value) for querystring, value in instance_dictionary.items() if querystring in queryset]

def match_count(instance_dictionary:dict, queryset:set) ->list:
    return [sum(len(v) for v in dct.values()) for querystring, dct in instance_dictionary.items() if querystring in queryset]

def evaluate(queryset_dict:dict, sample_list:list):
    supp_counts = {}
    match_counts = {}
    instance_dictionary = {}
    sample = MultidimSample()
    sample.set_sample(sample_list)
    event_db = sample.get_att_vertical_sequence_database()
    sample_range_list = list(range(len(sample_list)))
    all_queries = set(querystring for queryset in queryset_dict.values() for querystring in queryset)

    for querystring in all_queries:
        query = MultidimQuery()
        query.set_query_string(querystring, recalculate_attributes=False)
        instance_dictionary = query.query_pos_dict(event_db, sample, instance_dictionary, trace_list=sample_range_list)

    for discovery, queryset in queryset_dict.items():
        supp_counts[discovery] = supp_count(instance_dictionary, queryset)
        match_counts[discovery] = match_count(instance_dictionary, queryset)
    # evaluation_plot(supp_counts, 'Support')
    # evaluation_plot(match_counts, 'Total Matches')
    evaluation_plot(match_counts, supp_counts)


def evaluation_plot(match_counts: dict, supp_counts: dict):
    # create empty lists to store the data and labels
    data = []

    # loop through the dictionary and append the values and labels to the lists
    for discovery in match_counts:
        for count in match_counts[discovery]:
            row_match = [discovery, count, 'Matches' ]
            data.append(row_match)
        for count in supp_counts[discovery]:
            row_supp = [discovery, count, 'Support' ]
            data.append(row_supp)
    df_all = pd.DataFrame(data, columns=['Discovery', 'counts', 'boxplot'])
    df_all.to_csv('experiments/datasets/match_supp.csv')
    grid = sns.catplot(data=df_all, y='counts', x='Discovery', hue='boxplot', kind='box')
    grid.set(yscale='log')
    grid.savefig(f'experiments/plots/match_supp.pdf')

    # def evaluation_plot(queryset_dict:dict, title:str):
    # # create empty lists to store the data and labels
    # all_data = []
    # all_labels = []

    # # loop through the dictionary and append the values and labels to the lists
    # for key, values in queryset_dict.items():
    #     all_data.append(values)
    #     all_labels.append(key)

    # fig, ax = plt.subplots()
    # # create the boxplot
    # ax.boxplot(all_data, labels=all_labels)

    # # add a title and axis labels
    # ax.set_title(title)
    # ax.set_xlabel("Discovery Algorithm")
    # ax.set_ylabel("Number of Matches")
    # ax.set_yscale('log')

    # # display the plot
    # fig.savefig(f'experiments/plots/{title}.pdf')
    # plt.show()




if __name__ == "__main__":
    main()
