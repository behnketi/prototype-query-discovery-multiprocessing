#!/usr/bin/python3
"""Contains tests for the class Sample"""
import sys
sys.path.append(".")

import logging
from sample import Sample
from error import InvalidTraceError, EmptySampleError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_constructor():
    """
        Test for Sample constructor.
    """
    LOGGER.info('Started test for constructor (__init__) of Sample')

    sample01 = Sample()
    assert sample01._sample == [], 'sample01: Wrong trace collection!'
    assert sample01._sample_size == 0, 'sample01: Wrong sample size!'
    assert sample01._sample_typeset == set(), 'sample01: Wrong typeset!'

    sample02 = Sample([])
    assert sample02._sample == [], 'sample02: Wrong trace collection!'
    assert sample02._sample_size == 0, 'sample02: Wrong sample size!'
    assert sample02._sample_typeset == set(), 'sample02: Wrong typeset!'

    sample03 = Sample(['ab c','d'])
    assert sample03._sample == ['ab c', 'd'], 'sample03: Wrong trace collection!'
    assert sample03._sample_size == 2, 'sample03: Wrong sample size!'
    assert sample03._sample_typeset == {'ab','c','d'}, 'sample03: Wrong typeset!'

    LOGGER.info('Finished test for constructor (__init__) of Sample')

def test_sample_stats():
    """
        Test for function sample_stats() for small samples.
    """
    LOGGER.info('Started test for sample_stats')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    sample_stats = sample0.sample_stats(factors=[1,2,3])
    assert sample_stats['sample typeset'] == {'ea', 'ca', 'ia', 'la', 'na', 'aa', 'pa', 'fa', 'ja', 'ka', 'ba', 'oa', 'ha', 'ma', 'da', 'ga'}
    assert sample_stats['sample support'] == 1.0
    assert sample_stats['sample supported typeset'] == set()
    assert sample_stats['sample #types'] == 16
    assert sample_stats['sample min trace'] == "aa ba ca da"
    assert sample_stats['sample min trace len'] == 4
    assert sample_stats['sample max trace'] == "ma na oa pa"
    assert sample_stats['sample max trace len'] == 4
    assert sample_stats['sample average trace len'] == 4.0
    assert sample_stats['sample trace length distribution'] == [('4', [1.0])]
    assert sample_stats['sample type distribution'] == {
        'ea': [0.0625], 'ca': [0.0625], 'ia': [0.0625], 'la': [0.0625],
        'na': [0.0625], 'aa': [0.0625], 'pa': [0.0625], 'fa': [0.0625],
        'ja': [0.0625], 'ka': [0.0625], 'ba': [0.0625], 'oa': [0.0625],
        'ha': [0.0625], 'ma': [0.0625], 'da': [0.0625], 'ga': [0.0625]
    }
    assert sample_stats['sample 1 #factors'] == 16
    assert sample_stats['sample 1 factor distribution'] == {
        'ca': 0.0625, 'da': 0.0625, 'ia': 0.0625, 'oa': 0.0625,
        'ea': 0.0625, 'ga': 0.0625, 'ka': 0.0625, 'pa': 0.0625,
        'na': 0.0625, 'aa': 0.0625, 'ma': 0.0625, 'ba': 0.0625,
        'ha': 0.0625, 'la': 0.0625, 'ja': 0.0625, 'fa': 0.0625
    }
    assert sample_stats['sample 2 #factors'] == 12
    assert sample_stats['sample 2 factor distribution'] == {
        'ea fa': 0.08333333333333333, 'oa pa': 0.08333333333333333,
        'ba ca': 0.08333333333333333, 'na oa': 0.08333333333333333,
        'aa ba': 0.08333333333333333, 'fa ga': 0.08333333333333333,
        'ka la': 0.08333333333333333, 'ca da': 0.08333333333333333,
        'ga ha': 0.08333333333333333, 'ia ja': 0.08333333333333333,
        'ma na': 0.08333333333333333, 'ja ka': 0.08333333333333333
    }
    assert sample_stats['sample 3 #factors'] == 8
    assert sample_stats['sample 3 factor distribution'] == {
        'na oa pa': 0.125, 'ba ca da': 0.125, 'ma na oa': 0.125,
        'ia ja ka': 0.125, 'aa ba ca': 0.125, 'ea fa ga': 0.125,
        'ja ka la': 0.125, 'fa ga ha': 0.125
    }

    sample1 = Sample()
    sample1.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    sample_stats = sample1.sample_stats(factors=[1,2,3])
    assert sample_stats['sample typeset'] == {'NN', 'bb', 'KK', 'aa', 'cc', 'dd', 'ee'}
    assert sample_stats['sample support'] == 1.0
    assert sample_stats['sample supported typeset'] == {'aa', 'bb'}
    assert sample_stats['sample #types'] == 7
    assert sample_stats['sample min trace'] == "aa cc cc bb"
    assert sample_stats['sample min trace len'] == 4
    assert sample_stats['sample max trace'] == "aa NN ee ee bb"
    assert sample_stats['sample max trace len'] == 5
    assert sample_stats['sample average trace len'] == 4.5
    assert sample_stats['sample trace length distribution'] == [('4', [0.5]), ('5', [0.5])]
    assert sample_stats['sample type distribution'] == {
        'NN': [0.05555555555555555], 'bb': [0.2222222222222222],
        'KK': [0.05555555555555555], 'aa': [0.2222222222222222],
        'cc': [0.1111111111111111], 'dd': [0.1111111111111111],
        'ee': [0.2222222222222222]
    }
    assert sample_stats['sample 1 #factors'] == 7
    assert sample_stats['sample 1 factor distribution'] == {
        'cc': 0.1111111111111111, 'aa': 0.2222222222222222,
        'ee': 0.2222222222222222, 'dd': 0.1111111111111111,
        'KK': 0.05555555555555555, 'NN': 0.05555555555555555,
        'bb': 0.2222222222222222
    }
    assert sample_stats['sample 2 #factors'] == 12
    assert sample_stats['sample 2 factor distribution'] == {
        'dd bb': 0.07142857142857142, 'aa cc': 0.07142857142857142,
        'KK dd': 0.07142857142857142, 'cc cc': 0.07142857142857142,
        'aa NN': 0.07142857142857142, 'ee ee': 0.14285714285714285,
        'dd KK': 0.07142857142857142, 'NN ee': 0.07142857142857142,
        'ee bb': 0.14285714285714285, 'aa ee': 0.07142857142857142,
        'aa dd': 0.07142857142857142, 'cc bb': 0.07142857142857142
    }
    assert sample_stats['sample 3 #factors'] == 9
    assert sample_stats['sample 3 factor distribution'] == {
        'aa cc cc': 0.1, 'cc cc bb': 0.1, 'aa ee ee': 0.1, 'ee ee bb': 0.2,
        'KK dd bb': 0.1, 'NN ee ee': 0.1, 'aa NN ee': 0.1, 'dd KK dd': 0.1,
        'aa dd KK': 0.1
    }

    sample2 = Sample()
    sample2.set_sample(["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    sample_stats = sample2.sample_stats(factors=[1,2,3])
    assert sample_stats['sample typeset'] == {'bb', 'gg', 'oo', 'ff', 'nn', 'hh', 'kk', 'jj', 'aa', 'mm', 'cc', 'dd', 'ee', 'pp', 'll', 'ii'}
    assert sample_stats['sample support'] == 1.0
    assert sample_stats['sample supported typeset'] == set()
    assert sample_stats['sample #types'] == 16
    assert sample_stats['sample min trace'] == "aa bb cc dd"
    assert sample_stats['sample min trace len'] == 4
    assert sample_stats['sample max trace'] == "mm nn oo pp"
    assert sample_stats['sample max trace len'] == 4
    assert sample_stats['sample average trace len'] == 4.0
    assert sample_stats['sample trace length distribution'] == [('4', [1.0])]
    assert sample_stats['sample type distribution'] == {
        'bb': [0.0625], 'cc': [0.0625], 'ii': [0.0625],
        'kk': [0.0625], 'oo': [0.0625], 'ee': [0.0625],
        'nn': [0.0625], 'pp': [0.0625], 'jj': [0.0625],
        'll': [0.0625], 'ff': [0.0625], 'gg': [0.0625],
        'hh': [0.0625], 'aa': [0.0625], 'mm': [0.0625],
        'dd': [0.0625]
    }
    assert sample_stats['sample 1 #factors'] == 16
    assert sample_stats['sample 1 factor distribution'] == {
        'dd': 0.0625, 'ff': 0.0625, 'jj': 0.0625, 'nn': 0.0625,
        'bb': 0.0625, 'ee': 0.0625, 'ii': 0.0625, 'll': 0.0625,
        'aa': 0.0625, 'oo': 0.0625, 'kk': 0.0625, 'cc': 0.0625,
        'gg': 0.0625, 'mm': 0.0625, 'hh': 0.0625, 'pp': 0.0625
    }
    assert sample_stats['sample 2 #factors'] == 12
    assert sample_stats['sample 2 factor distribution'] == {
        'jj kk': 0.08333333333333333, 'ii jj': 0.08333333333333333,
        'kk ll': 0.08333333333333333, 'ee ff': 0.08333333333333333,
        'aa bb': 0.08333333333333333, 'gg hh': 0.08333333333333333,
        'ff gg': 0.08333333333333333, 'cc dd': 0.08333333333333333,
        'nn oo': 0.08333333333333333, 'oo pp': 0.08333333333333333,
        'mm nn': 0.08333333333333333, 'bb cc': 0.08333333333333333
    }
    assert sample_stats['sample 3 #factors'] == 8
    assert sample_stats['sample 3 factor distribution'] == {
        'ii jj kk': 0.125, 'ee ff gg': 0.125, 'nn oo pp': 0.125,
        'bb cc dd': 0.125, 'mm nn oo': 0.125, 'aa bb cc': 0.125,
        'ff gg hh': 0.125, 'jj kk ll': 0.125
    }

    sample3 = Sample()
    sample3.set_sample(["a c c b", "a e e b", "a K d d M b", "a K e L e M b"])
    sample3.set_sample_size()
    sample3.set_sample_typeset()
    sample_stats = sample3.sample_stats(factors=[1,2,3,7,8])
    assert sample_stats['sample typeset'] == {'b', 'c', 'd', 'M', 'K', 'a', 'L', 'e'}
    assert sample_stats['sample support'] == 1.0
    assert sample_stats['sample supported typeset'] == {'a', 'b'}
    assert sample_stats['sample #types'] == 8
    assert sample_stats['sample min trace'] == "a c c b"
    assert sample_stats['sample min trace len'] == 4
    assert sample_stats['sample max trace'] == "a K e L e M b"
    assert sample_stats['sample max trace len'] == 7
    assert sample_stats['sample average trace len'] == 5.25
    assert sample_stats['sample trace length distribution'] == [('6', [0.25]), ('7', [0.25]), ('4', [0.5])]
    assert sample_stats['sample type distribution'] == {
        'b': [0.19047619047619047], 'c': [0.09523809523809523], 'd': [0.09523809523809523],
        'M': [0.09523809523809523], 'K': [0.09523809523809523], 'a': [0.19047619047619047],
        'L': [0.047619047619047616], 'e': [0.19047619047619047]
    }
    assert sample_stats['sample 1 #factors'] == 7
    assert sample_stats['sample 1 factor distribution'] == {
        'd': 0.11764705882352941, 'L': 0.058823529411764705, 'a': 0.23529411764705882,
        'e': 0.23529411764705882, 'K': 0.11764705882352941, 'c': 0.11764705882352941,
        'M': 0.11764705882352941
    }
    assert sample_stats['sample 2 #factors'] == 15
    assert sample_stats['sample 2 factor distribution'] == {
        'K e': 0.058823529411764705, 'e b': 0.058823529411764705, 'K d': 0.058823529411764705,
        'e M': 0.058823529411764705, 'e L': 0.058823529411764705, 'e e': 0.058823529411764705,
        'd M': 0.058823529411764705, 'M b': 0.11764705882352941, 'c c': 0.058823529411764705,
        'c b': 0.058823529411764705, 'd d': 0.058823529411764705, 'a K': 0.11764705882352941,
        'a e': 0.058823529411764705, 'L e': 0.058823529411764705, 'a c': 0.058823529411764705
    }
    assert sample_stats['sample 3 #factors'] == 13
    assert sample_stats['sample 3 factor distribution'] == {
        'a e e': 0.07692307692307693, 'a c c': 0.07692307692307693, 'e M b': 0.07692307692307693,
        'c c b': 0.07692307692307693, 'd M b': 0.07692307692307693, 'K e L': 0.07692307692307693,
        'a K d': 0.07692307692307693, 'a K e': 0.07692307692307693, 'L e M': 0.07692307692307693,
        'K d d': 0.07692307692307693, 'e e b': 0.07692307692307693, 'd d M': 0.07692307692307693,
        'e L e': 0.07692307692307693
    }
    LOGGER.info('Finished test for sample_stats')

def test_get_sample_min_max_trace():
    """
        Test for functions _get_sample_min_trace and _get_sample_max_trace.
    """
    LOGGER.info('Started test for _get_sample_min_trace and _get_sample_max_trace')
    sample = Sample()
    sample.set_sample(["a", "a b", "a b c"])
    assert sample.get_sample_min_trace() == "a", '1st sample: wrong min trace'
    assert sample.get_sample_max_trace() == "a b c", '1st sample: wrong max trace'

    sample.set_sample(["a;a", "a;a b;b", "a;a b;b c;c"])
    assert sample.get_sample_min_trace() == "a;a", '2nd sample: wrong min trace'
    assert sample.get_sample_max_trace() == "a;a b;b c;c", '2nd sample: wrong max trace'

    sample.set_sample(["aa", "aa bb", "aa bb cc"])
    assert sample.get_sample_min_trace() == "aa", '3rd sample: wrong min trace'
    assert sample.get_sample_max_trace() == "aa bb cc", '3rd sample: wrong max trace'

    sample.set_sample(["aaaaa", "aaaaa bbbbb", "a b c"])
    assert sample.get_sample_min_trace() == "aaaaa", '4th sample: wrong min trace'
    assert sample.get_sample_max_trace() == "a b c", '4th sample: wrong max trace'
    LOGGER.info('Finished test for _get_sample_min_trace and _get_sample_max_trace')

def test_get_sample_max_min_avg_trace():
    """
        Test for function _get_sample_max_min_avg_trace for small samples.
    """
    LOGGER.info('Started test for _get_sample_max_min_avg_trace')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    min_max_avg_trace = sample0.get_sample_max_min_avg_trace()
    assert sample0._sample == ["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"], 'sample0: Wrong trace collection!'
    assert sample0._sample_typeset == {'ha', 'da', 'la', 'ea', 'na', 'oa', 'ja', 'fa', 'ma', 'ga', 'pa', 'aa', 'ba', 'ka', 'ca', 'ia'}, 'sample0: Wrong typeset!'
    assert min_max_avg_trace[2] == 4.0, 'sample0: Wrong average trace length!'

    sample1 = Sample()
    sample1.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    min_max_avg_trace = sample1.get_sample_max_min_avg_trace()
    assert sample1._sample == ['aa cc cc bb', 'aa ee ee bb', 'aa dd KK dd bb', 'aa NN ee ee bb'], 'sample1: Wrong trace collection!'
    assert sample1._sample_typeset == {'KK', 'aa', 'dd', 'ee', 'bb', 'cc', 'NN'}, 'sample1: Wrong typeset!'
    assert min_max_avg_trace[2] == 4.5, 'sample1: Wrong average trace length!'

    sample2 = Sample()
    sample2.set_sample(["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    min_max_avg_trace = sample2.get_sample_max_min_avg_trace()
    assert sample2._sample == ["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"], 'sample2: Wrong trace collection!'
    assert sample2._sample_typeset == {'aa', 'dd', 'oo', 'ee', 'bb', 'cc', 'll', 'ff', 'nn', 'ii', 'mm', 'jj', 'pp', 'hh', 'gg', 'kk'}, 'sample2: Wrong typeset!'
    assert min_max_avg_trace[2] == 4.0, 'sample2: Wrong average trace length!'

    sample3 = Sample()
    sample3.set_sample(["a c c b", "a e e b", "a K d d M b", "a K e L e M b"])
    sample3.set_sample_size()
    sample3.set_sample_typeset()
    min_max_avg_trace = sample3.get_sample_max_min_avg_trace()
    assert sample3._sample == ["a c c b", "a e e b", "a K d d M b", "a K e L e M b"], 'sample3: Wrong trace collection!'
    assert sample3._sample_typeset == {'e', 'd', 'M', 'b', 'a', 'K', 'c', 'L'}, 'sample3: Wrong typeset!'
    assert min_max_avg_trace[2] == 5.25, 'sample3: Wrong average trace length!'
    LOGGER.info('Finished test for _get_sample_max_min_avg_trace')

def test_get_supported_typeset():
    """
        Test for function get_supported_typeset.
    """
    LOGGER.info('Started test for get_supported_typeset')
    sample0 = Sample()
    sample0.set_sample(["aa cc cc bb", "aa cc ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    assert sample0._sample == ["aa cc cc bb", "aa cc ee bb", "aa dd KK dd bb", "aa NN ee ee bb"], 'sample0: Wrong trace collection!'
    assert sample0._sample_size == 4, 'sample0: Wrong sample size!'
    assert sample0._sample_typeset == {'aa', 'cc', 'bb', 'ee', 'dd', 'KK', 'NN'}, 'sample0: Wrong typeset!'
    typeset00 = sample0.get_supported_typeset(0.0)
    typeset05 = sample0.get_supported_typeset(0.5)
    typeset10 = sample0.get_supported_typeset(1.0)
    assert typeset00 == {'bb', 'dd', 'aa', 'cc', 'NN', 'ee', 'KK'}, 'Supp 0.0: Wrong supported typeset'
    assert typeset05 == {'ee', 'bb', 'aa', 'cc'}, 'Supp 0.5: Wrong supported typeset'
    assert typeset10 == {'bb', 'aa'}, 'Supp 1.0: Wrong supported typeset'
    LOGGER.info('Finished test for get_supported_typeset')

def test_get_type_distribution():
    """
        Test for function _get_type_distribution for small samples.
    """
    LOGGER.info('Started test for _get_type_distribution')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    type_distribution = sample0._get_type_distribution()
    assert sample0._sample_typeset == {'ia', 'la', 'aa', 'ea', 'ha', 'pa', 'fa', 'oa', 'ca', 'da', 'ga', 'ja', 'ka', 'ma', 'na', 'ba'}
    assert type_distribution == {
        'ia': [0.0625], 'la': [0.0625], 'aa': [0.0625], 'ea': [0.0625],
        'ha': [0.0625], 'pa': [0.0625], 'fa': [0.0625], 'oa': [0.0625],
        'ca': [0.0625], 'da': [0.0625], 'ga': [0.0625], 'ja': [0.0625],
        'ka': [0.0625], 'ma': [0.0625], 'na': [0.0625], 'ba': [0.0625]
    }

    sample1 = Sample()
    sample1.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    type_distribution = sample1._get_type_distribution()
    assert sample1._sample_typeset == {'aa', 'cc', 'bb', 'dd', 'NN', 'KK', 'ee'}
    assert type_distribution == {
        'aa': [0.2222222222222222], 'cc': [0.1111111111111111], 'bb': [0.2222222222222222],
        'dd': [0.1111111111111111], 'NN': [0.05555555555555555], 'KK': [0.05555555555555555],
        'ee': [0.2222222222222222]
    }

    sample2 = Sample()
    sample2.set_sample(["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    type_distribution = sample2._get_type_distribution()
    assert sample2._sample_typeset == {'ii', 'aa', 'jj', 'cc', 'gg', 'nn', 'pp', 'hh', 'll', 'bb', 'oo', 'kk', 'dd', 'mm', 'ff', 'ee'}
    assert type_distribution == {
        'ii': [0.0625], 'aa': [0.0625], 'jj': [0.0625],
        'cc': [0.0625], 'gg': [0.0625], 'nn': [0.0625],
        'pp': [0.0625], 'hh': [0.0625], 'll': [0.0625],
        'bb': [0.0625], 'oo': [0.0625], 'kk': [0.0625],
        'dd': [0.0625], 'mm': [0.0625], 'ff': [0.0625],
        'ee': [0.0625]
    }

    sample3 = Sample()
    sample3.set_sample(["a c c b", "a e e b", "a K d d M b", "a K e L e M b"])
    sample3.set_sample_size()
    sample3.set_sample_typeset()
    type_distribution = sample3._get_type_distribution()
    assert sample3._sample_typeset == {'M', 'e', 'b', 'L', 'd', 'K', 'c', 'a'}
    assert type_distribution == {
        'M': [0.09523809523809523], 'e': [0.19047619047619047], 'b': [0.19047619047619047],
        'L': [0.047619047619047616], 'd': [0.09523809523809523], 'K': [0.09523809523809523],
        'c': [0.09523809523809523], 'a': [0.19047619047619047]
    }
    LOGGER.info('Finished test for _get_type_distribution')

def test_get_factor_distribution():
    """
        Test for function _get_factor_distribution for small samples.
    """
    LOGGER.info('Started test for _get_factor_distribution')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    factor_distribution = sample0._get_factor_distribution(factor_length=2)
    assert sample0._sample_typeset == {'na', 'ga', 'ka', 'oa', 'ca', 'ma', 'aa', 'ba', 'pa', 'fa', 'ea', 'ha', 'ja', 'la', 'ia', 'da'}
    assert factor_distribution == {
        'ba ca': 0.08333333333333333, 'ea fa': 0.08333333333333333,
        'aa ba': 0.08333333333333333, 'ga ha': 0.08333333333333333,
        'ka la': 0.08333333333333333, 'ma na': 0.08333333333333333,
        'oa pa': 0.08333333333333333, 'ca da': 0.08333333333333333,
        'ja ka': 0.08333333333333333, 'ia ja': 0.08333333333333333,
        'na oa': 0.08333333333333333, 'fa ga': 0.08333333333333333
    }

    sample1 = Sample()
    sample1.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    factor_distribution = sample1._get_factor_distribution(factor_length=2)
    assert sample1._sample_typeset == {'dd', 'cc', 'NN', 'ee', 'aa', 'bb', 'KK'}
    assert factor_distribution == {
        'ee ee': 0.14285714285714285, 'dd bb': 0.07142857142857142,
        'cc bb': 0.07142857142857142, 'dd KK': 0.07142857142857142,
        'KK dd': 0.07142857142857142, 'aa NN': 0.07142857142857142,
        'NN ee': 0.07142857142857142, 'cc cc': 0.07142857142857142,
        'aa ee': 0.07142857142857142, 'aa dd': 0.07142857142857142,
        'aa cc': 0.07142857142857142, 'ee bb': 0.14285714285714285
    }

    sample2 = Sample()
    sample2.set_sample(["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    factor_distribution = sample2._get_factor_distribution(factor_length=2)
    assert sample2._sample_typeset == {'pp', 'dd', 'hh', 'kk', 'cc', 'mm', 'll', 'gg', 'ee', 'oo', 'aa', 'ii', 'jj', 'bb', 'nn', 'ff'}
    assert factor_distribution == {
        'jj kk': 0.08333333333333333, 'mm nn': 0.08333333333333333,
        'oo pp': 0.08333333333333333, 'gg hh': 0.08333333333333333,
        'ee ff': 0.08333333333333333, 'ff gg': 0.08333333333333333,
        'aa bb': 0.08333333333333333, 'cc dd': 0.08333333333333333,
        'ii jj': 0.08333333333333333, 'bb cc': 0.08333333333333333,
        'kk ll': 0.08333333333333333, 'nn oo': 0.08333333333333333
    }

    sample3 = Sample()
    sample3.set_sample(["a c c b", "a e e b", "a K d d M b", "a K e L e M b"])
    sample3.set_sample_size()
    sample3.set_sample_typeset()
    factor_distribution = sample3._get_factor_distribution(factor_length=2)
    assert sample3._sample_typeset == {'d', 'L', 'K', 'M', 'e', 'a', 'c', 'b'}
    assert factor_distribution == {
        'd d': 0.058823529411764705, 'e b': 0.058823529411764705,
        'e L': 0.058823529411764705, 'K d': 0.058823529411764705,
        'd M': 0.058823529411764705, 'c c': 0.058823529411764705,
        'M b': 0.11764705882352941,  'e e': 0.058823529411764705,
        'L e': 0.058823529411764705, 'e M': 0.058823529411764705,
        'a c': 0.058823529411764705, 'c b': 0.058823529411764705,
        'a e': 0.058823529411764705, 'a K': 0.11764705882352941,
        'K e': 0.058823529411764705
    }
    LOGGER.info('Finished test for _get_factor_distribution')

def test_get_factors():
    """
        Test for function _get_factors for small sample and factor length.
    """
    LOGGER.info('Started test for _get_factors')
    sample = Sample()
    sample.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    factors = sample._get_factors(factor_length=1)
    assert factors == {'cc', 'KK', 'ee', 'aa', 'NN', 'bb', 'dd'}
    factors = sample._get_factors(factor_length=2)
    assert factors == {'cc cc', 'dd KK', 'dd bb', 'aa ee', 'NN ee', 'aa NN', 'aa dd', 'ee bb', 'ee ee', 'cc bb', 'aa cc', 'KK dd'}

    sample2 = Sample()
    sample2.set_sample(["aa cc cc bb", "aa ee ee bb ", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    try:
        factors = sample2._get_factors(factor_length=1)
    except InvalidTraceError:
        LOGGER.info('A trace ends with a whitespace.')
    LOGGER.info('Finished test for _get_factors')

def test_get_l_w_tuples():
    """
        Test for function get_l_w_tuples() for small samples.
    """
    LOGGER.info('Started test for get_l_w_tuples')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    sample0_l_w_tuples = sample0.get_l_w_tuples()
    assert sample0_l_w_tuples == [(1, [(-1, -1), (-1, -1)]), (2, [(-1, -1), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 1), (-1, -1)]), (2, [(-1, -1), (1, 1), (-1, -1)]), (3, [(-1, -1), (0, 0), (0, 0), (-1, -1)]), (4, [(-1, -1), (0, 0), (0, 0), (0, 0), (-1, -1)])]
    sample0_l_w_tuples = sample0.get_l_w_tuples(query_string_length_interval=[2,3])
    assert sample0_l_w_tuples == [(2, [(-1, -1), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 1), (-1, -1)]), (2, [(-1, -1), (1, 1), (-1, -1)]), (3, [(-1, -1), (0, 0), (0, 0), (-1, -1)])]

    sample1 = Sample()
    sample1.set_sample(["aa ba", "ea fa ga ha ca da", "ia ja ka", "ma na oa pa la"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    sample1_l_w_tuples = sample1.get_l_w_tuples()
    assert sample1_l_w_tuples == [(1, [(-1, -1), (-1, -1)]), (2, [(-1, -1), (0, 0), (-1, -1)])]
    sample1_l_w_tuples = sample1.get_l_w_tuples(query_string_length_interval=[2,3])
    assert sample1_l_w_tuples == [(2, [(-1, -1), (0, 0), (-1, -1)])]
    LOGGER.info('Finished test for get_l_w_tuples')

def test_set_sample_typeset():
    """
        Basic test for function set_sample_typeset including samples with
        typelength greater than one.
    """
    LOGGER.info('Started test for _set_sample_typeset')
    sample0 = Sample()
    sample0.set_sample(["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"])
    sample0.set_sample_size()
    sample0.set_sample_typeset()
    assert sample0._sample == ["aa ba ca da", "ea fa ga ha", "ia ja ka la", "ma na oa pa"], 'sample0: Wrong trace collection!'
    assert sample0._sample_size == 4, 'sample0: Wrong sample size!'
    assert sample0._sample_typeset == {'aa','ba','ca','da','ea','fa','ga','ha','ia','ja','ka','la','ma','na','oa','pa'}, 'sample0: Wrong sample size!'

    sample1 = Sample()
    sample1.set_sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    sample1.set_sample_size()
    sample1.set_sample_typeset()
    assert sample1._sample == ["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"], 'sample1: Wrong trace collection!'
    assert sample1._sample_size == 4, 'sample1: Wrong sample size!'
    assert sample1._sample_typeset == {'KK', 'ee', 'NN', 'bb', 'dd', 'cc', 'aa'}, 'sample1: Wrong sample size!'

    sample2 = Sample()
    sample2.set_sample(["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"])
    sample2.set_sample_size()
    sample2.set_sample_typeset()
    assert sample2._sample == ["aa bb cc dd", "ee ff gg hh", "ii jj kk ll", "mm nn oo pp"], 'sample2: Wrong trace collection!'
    assert sample2._sample_size == 4, 'sample2: Wrong sample size!'
    assert sample2._sample_typeset == {'pp', 'hh', 'ii', 'kk', 'll', 'mm', 'nn', 'jj', 'ee', 'bb', 'dd', 'cc', 'aa', 'gg', 'ff', 'oo'}, 'sample2: Wrong sample size!'

    sample3 = Sample()
    sample3.set_sample(["a c c b", "a e e b", "a K d d M b", "a K e L e M b"])
    sample3.set_sample_size()
    sample3.set_sample_typeset()
    assert sample3._sample == ["a c c b", "a e e b", "a K d d M b", "a K e L e M b"], 'sample3: Wrong trace collection!'
    assert sample3._sample_size == 4, 'sample3: Wrong sample size!'
    assert sample3._sample_typeset == {'M', 'K', 'd', 'b', 'L', 'a', 'e', 'c'}, 'sample3: Wrong sample size!'
    LOGGER.info('Finished test for _set_sample_typeset')

def test00_create_vertical_sequence_database():
    """
        Testing 'create_vertical_sequence_database': Check for detecting empty samples.
    """
    LOGGER.info('Started test 00 for create_vertical_sequence_database')

    sample = Sample()
    try:
        sample._create_vertical_sequence_database()
        raise AssertionError("Test failed: No error raised!")
    except EmptySampleError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for create_vertical_sequence_database')

def test01_create_vertical_sequence_database():
    """
        Testing 'create_vertical_sequence_database': Check for detecting index error.
    """
    LOGGER.info('Started test 01 for create_vertical_sequence_database')

    sample = Sample(["",""])
    try:
        sample._create_vertical_sequence_database([-1])
        raise AssertionError("Test failed: No error raised!")
    except IndexError:
        pass
    except Exception as err:
        raise err

    try:
        sample._create_vertical_sequence_database([0, sample._sample_size])
        raise AssertionError("Test failed: No error raised!")
    except IndexError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 01 for create_vertical_sequence_database')

def test02_create_vertical_sequence_database():
    """
        Testing 'create_vertical_sequence_database': small running example.
    """
    LOGGER.info('Started test 02 for create_vertical_sequence_database')

    sample = Sample(["b a b", "AB b C dF gh", "", "a b"])
    vertical_representation = {
            'a': {0: [1],
                  3: [0]},
            'AB':{1: [0]},
            'b': {0: [0,2],
                  1: [1],
                  3: [1]},
            'C': {1: [2]},
            'dF':{1: [3]},
            'gh':{1: [4]}
            }
    sample._create_vertical_sequence_database()
    assert sample._sample_vertical_sequence_database == vertical_representation

    LOGGER.info('Finished test 02 for create_vertical_sequence_database')

def test03_create_vertical_sequence_database():
    """
        Testing 'create_vertical_sequence_database': small running example ignoring one trace (only [0,2,3] used).
    """
    LOGGER.info('Started test 03 for create_vertical_sequence_database')

    sample = Sample(["b a b", "AB b C dF gh", "", "a b"])
    vertical_representation = {
            'a': {0: [1],
                  3: [0]},
            'b': {0: [0,2],
                  3: [1]},
            }
    sample._create_vertical_sequence_database([0,2,3])
    assert sample._sample_vertical_sequence_database == vertical_representation

    LOGGER.info('Finished test 03 for create_vertical_sequence_database')
