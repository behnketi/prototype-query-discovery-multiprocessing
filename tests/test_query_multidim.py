#!/usr/bin/python3
"""Contains tests for class MultidimQuery"""
import sys
sys.path.append(".")

import logging
from query_multidim import MultidimQuery
from sample import Sample
from error import InvalidQueryGapConstraintError,InvalidQueryLocalWindowSizeError,InvalidQueryStringLengthError,InvalidEventDimensionError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_multidim_event_flattening():
    """
        Test of all setter functions, to_normalform, to_regex, match_sample and
        check_consistence on more dimensional queries.
    """
    sample = Sample()
    sample.set_sample(["a; b; c; b;", "a; c; z; c; b;", "b; a; a; z; b; a; b;", "a; c; z; c; b; d;"])
    support = 0.5

    LOGGER.info('Started test test_event_flattening')
    query01 = MultidimQuery()
    query01.set_query_string("a; $x; $x; b;")
    query01.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query01.set_query_gap_constraints([{'z'},{'z'},{'z'}])
    query01.query_string_to_regex()
    assert query01._query_event_dimension == 1
    assert query01._query_typeset == {'a','b'}
    assert query01._query_repeated_variables == {'x'}
    assert query01._query_attribute_typesets == dict({1: {'a', 'b'}})
    assert query01._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(-1,-1)]
    assert query01._query_gap_constraints == [{'z'},{'z'},{'z'}]
    assert query01._query_string_regex == 'a;\\s(((?:[^\\s;z]+;){1})\\s){0,1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;z]+;){1})\\s){0,1}(?P=x);\\s(((?:[^\\s;z]+;){1})\\s){0,1}b;'
    assert query01.match_sample(sample,support) is False
    query01._query_gap_constraints=[]
    query01.query_string_to_normalform()
    assert query01._query_string == "a; $x0; $x0; b;"
    assert query01._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(-1,-1)]
    assert query01._query_string_length == 4
    assert query01._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P=x0);\\s(((?:[^\\s;]+;){1})\\s){0,1}b;'
    query01.check_consistency()
    assert query01.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;a; E;a; b;c;", "a;b; C;a; D;a; b;c;", "a;b; B;a; z;z; B;a; b;c;", "a;b; E;a; z;z; E;a; b;c;"])

    query02 = MultidimQuery()
    query02.set_query_string("a;b; $x;a; $x;a; b;c;")
    query02.set_query_windowsize_local([(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0)])
    query02.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}])
    query02.query_string_to_regex()
    assert query02._query_event_dimension == 2
    assert query02._query_typeset == {'a','b','c'}
    assert query02._query_attribute_typesets == dict({1: {'a', 'b'}, 2: {'a', 'b','c'}})
    assert query02._query_repeated_variables == {'x'}
    assert query02._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query02._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query02._query_string_regex == "a;b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<x>[^\\s;]+);a;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P=x);a;\\s(((?:[^\\s;z]+;){2})\\s){0,1}b;c;"
    assert query02.match_sample(sample,support) is False
    query02._query_gap_constraints=[]
    query02.query_string_to_normalform()
    assert query02._query_string == "a;b; $x0;a; $x0;a; b;c;"
    assert query02._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query02._query_string_length == 4
    assert query02._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P<x0>[^\\s;]+);a;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P=x0);a;\\s(((?:[^\\s;]+;){2})\\s){0,1}b;c;"
    query02.check_consistency()
    assert query02.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;d; F;F; E;e; b;c; d;G;","a;b; E;d; y;z; F;F; E;e; b;c; d;G;","a;b; E;d; F;G; E;e; b;c; d;H;","a;b; E;d; v;z; F;F; w;x; E;e; b;c; d;G;"])

    # non-repeated variable at the end of the query string
    query03 = MultidimQuery()
    query03.set_query_string("a;b; $x;d; $y;$y; $x;e; b;c; d;$z;")
    query03.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query03.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}])
    query03.query_string_to_regex()
    assert query03._query_event_dimension == 2
    assert query03._query_typeset == {'a','b','c','d','e'}
    assert query03._query_attribute_typesets == dict({1: {'a','b','d'}, 2: {'b','d','e','c'}})
    assert query03._query_repeated_variables == {'x','y'}
    assert query03._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query03._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query03._query_string_regex == "a;b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<x>[^\\s;]+);d;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<y>[^\\s;]+);(?P=y);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P=x);e;\\s(((?:[^\\s;z]+;){2})\\s){0,1}b;c;\\s(((?:[^\\s;z]+;){2})\\s){0,1}d;(?P<z>[^\\s;]+);"
    assert query03.match_sample(sample,support) is False
    query03._query_gap_constraints=[]
    query03.query_string_to_normalform()
    assert query03._query_string == "a;b; $x0;d; $x1;$x1; $x0;e; b;c; d;;"
    assert query03._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(1,1)]
    assert query03._query_string_length == 6
    assert query03._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P<x0>[^\\s;]+);d;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P<x1>[^\\s;]+);(?P=x1);\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P=x0);e;\\s(((?:[^\\s;]+;){2})\\s){0,1}b;c;\\s(((?:[^\\s;]+;){2})\\s){0,1}d;(?:[^\\s;]+;){1,1}"
    query03.check_consistency()
    assert query03.match_sample(sample,support) is True

    sample.set_sample(["U;b; X;d; Y;Y; X;e; b;c; d;Z;", "U;b; z;y; X;d; Y;Y; X;e; b;c; d;Z;", "U;b; X;d; a;b; c;d; Y;Y; X;e; b;c; d;Z;", "U;b; a;z; X;d; a;b; Y;Y; X;e; b;c; d;Z;"])

    # non-repeated variable at the beginning and the end of the query string
    query04 = MultidimQuery()
    query04.set_query_string("$u;b; $x;d; $y;$y; $x;e; b;c; d;$z;")
    query04.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query04.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}])
    query04.query_string_to_regex()
    assert query04._query_event_dimension == 2
    assert query04._query_typeset == {'b','c','d','e'}
    assert query04._query_attribute_typesets == dict({1: {'b','d'}, 2: {'b','d','c','e'}})
    assert query04._query_repeated_variables == {'x','y'}
    assert query04._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query04._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query04._query_string_regex == "(?P<u>[^\\s;]+);b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<x>[^\\s;]+);d;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<y>[^\\s;]+);(?P=y);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P=x);e;\\s(((?:[^\\s;z]+;){2})\\s){0,1}b;c;\\s(((?:[^\\s;z]+;){2})\\s){0,1}d;(?P<z>[^\\s;]+);"
    assert query04.match_sample(sample,support) is False
    query04._query_gap_constraints=[]
    query04.query_string_to_normalform()
    assert query04._query_string == ";b; $x0;d; $x1;$x1; $x0;e; b;c; d;;"
    assert query04._query_windowsize_local == [(1,1),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(1,1)]
    assert query04._query_string_length == 6
    assert query04._query_string_regex == "(?:[^\\s;]+;){1,1}b;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P<x0>[^\\s;]+);d;\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P<x1>[^\\s;]+);(?P=x1);\\s(((?:[^\\s;]+;){2})\\s){0,1}(?P=x0);e;\\s(((?:[^\\s;]+;){2})\\s){0,1}b;c;\\s(((?:[^\\s;]+;){2})\\s){0,1}d;(?:[^\\s;]+;){1,1}"
    query04.check_consistency()
    assert query04.match_sample(sample,support) is True

    sample.set_sample(["a;X; Y;a;", "a;X; b;c; d;e; Y;a;", "a;X; z;b; Y;a;", "a;X; z;b; Y;a;"])

    # non-repeated variable at the end of an event, followed by an event which starts with a non-repeated variable
    query05 = MultidimQuery()
    query05.set_query_string("a;$x; $y;a;")
    query05.set_query_windowsize_local([(0,0),(0,2),(0,0)])
    query05.set_query_gap_constraints([{'z'},{'z'},{'z'}])
    query05.query_string_to_regex()
    assert query05._query_event_dimension == 2
    assert query05._query_typeset == {'a'}
    assert query05._query_attribute_typesets == dict({1: {'a'}, 2: {'a'}})
    assert query05._query_repeated_variables == set()
    assert query05._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(-1,-1)]
    assert query05._query_gap_constraints == [{'z'},{'z'},{'z'}]
    assert query05._query_string_regex == "a;(?P<x>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<y>[^\\s;]+);a;"
    assert query05.match_sample(sample,support) is False
    query05._query_gap_constraints=[]
    query05.query_string_to_normalform()
    assert query05._query_string == "a;; ;a;"
    assert query05._query_windowsize_local == [(-1,-1),(2,4),(-1,-1)]
    assert query05._query_string_length == 2
    assert query05._query_string_regex == "a;(?:[^\\s;]+;){1,1}\\s(((?:[^\\s;]+;){2})\\s){0,1}(?:[^\\s;]+;){1,1}a;"
    query05.check_consistency()
    assert query05.match_sample(sample,support) is True

    sample.set_sample(["a;b; c;d;", "a;a; z;c; z;e; b;b;", "a;X; z;b; Y;a;", "a;X; z;b; Y;a;"])

    # Query string consists only of non-repeated variables
    query06 = MultidimQuery()
    query06.set_query_string("$u;$v; $w;$x;")
    query06.set_query_windowsize_local([(0,0),(0,2),(0,0)])
    query06.set_query_gap_constraints([{'z'},{'z'},{'z'}])
    query06.query_string_to_regex()
    assert query06._query_event_dimension == 2
    assert query06._query_typeset == set()
    assert query06._query_attribute_typesets == dict({1: set(), 2: set()})
    assert query06._query_repeated_variables == set()
    assert query06._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(-1,-1)]
    assert query06._query_gap_constraints == [{'z'},{'z'},{'z'}]
    assert query06._query_string_regex == "(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);"
    assert query06.match_sample(sample,support) is True
    query06._query_gap_constraints=[]
    query06.query_string_to_normalform()
    assert query06._query_string == ""
    assert query06._query_windowsize_local == [(4,6)]
    assert query06._query_string_length == 0
    assert query06._query_string_regex == "(((?:[^\\s;]+;){2})\\s){1,2}(?:[^\\s;]+;){2}"
    query06.check_consistency()
    assert query06.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;F; c;d;","a;b; z;z; z;z c;d;","a;b; E;F; E;F; E;F; E;F; c;d;","a;b; z;z; E;F; z;z; c;d;"])

    # Query string containing an event consisting only of non-repeated variables
    query07 = MultidimQuery()
    query07.set_query_string("a;b; $u;$v; c;d;")
    query07.set_query_windowsize_local([(0,0),(0,2),(0,0),(0,2),(0,0)])
    query07.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'}])
    query07.query_string_to_regex()
    assert query07._query_event_dimension == 2
    assert query07._query_typeset == {'a','b','c','d'}
    assert query07._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query07._query_repeated_variables == set()
    assert query07._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query07._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query07._query_string_regex == "a;b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}c;d;"
    assert query07.match_sample(sample,support) is False
    query07._query_gap_constraints=[]
    query07.query_string_to_normalform()
    assert query07._query_string == "a;b; c;d;"
    assert query07._query_windowsize_local == [(-1,-1),(0,0),(2,6),(0,0),(-1,-1)]
    assert query07._query_string_length == 2
    assert query07._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){1,3}c;d;"
    query07.check_consistency()
    assert query07.match_sample(sample,support) is True

    # Query string containing two consecutive events consisting only of non-repeated variables
    query08 = MultidimQuery()
    query08.set_query_string("a;b; $u;$v; $w;$x; c;d;")
    query08.set_query_windowsize_local([(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0)])
    query08.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}])
    query08.query_string_to_regex()
    assert query08._query_event_dimension == 2
    assert query08._query_typeset == {'a','b','c','d'}
    assert query08._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query08._query_repeated_variables == set()
    assert query08._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query08._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query08._query_string_regex == "a;b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}c;d;"
    query08._query_gap_constraints=[]
    query08.query_string_to_normalform()
    assert query08._query_string == "a;b; c;d;"
    assert query08._query_windowsize_local == [(-1,-1),(0,0),(4,10),(0,0),(-1,-1)]
    assert query08._query_string_length == 2
    assert query08._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){2,5}c;d;"
    query08.check_consistency()

    # Query string containing three consecutive events consisting only of non-repeated variables
    query09 = MultidimQuery()
    query09.set_query_string("a;b; $u;$v; $w;$x; $y;$z; c;d;")
    query09.set_query_windowsize_local([(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0)])
    query09.set_query_gap_constraints([{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}])
    query09.query_string_to_regex()
    assert query09._query_event_dimension == 2
    assert query09._query_typeset == {'a','b','c','d'}
    assert query09._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query09._query_repeated_variables == set()
    assert query09._query_windowsize_local == [(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)]
    assert query09._query_gap_constraints == [{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'},{'z'}]
    assert query09._query_string_regex == "a;b;\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}(?P<y>[^\\s;]+);(?P<z>[^\\s;]+);\\s(((?:[^\\s;z]+;){2})\\s){0,1}c;d;"
    query09._query_gap_constraints=[]
    query09.query_string_to_normalform()
    assert query09._query_string == "a;b; c;d;"
    assert query09._query_windowsize_local == [(-1,-1),(0,0),(6,14),(0,0),(-1,-1)]
    assert query09._query_string_length == 2
    assert query09._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){3,7}c;d;"
    query09.check_consistency()

    sample.set_sample(["a;b; E;F; G;X; c;X;", "a;b; G;X; c;X;", "a;b; c;d; e;X; c;Y;", "a;b; E;F; z;z; G;X; z;z; c;X;"])

    query10= MultidimQuery()
    query10.set_query_string("a;b; ;; ;$x; c;$x;")
    query10.set_query_windowsize_local([(-1, -1), (0, 0), (3, 7), (0, 2), (0, 0), (-1, -1)])
    query10.query_string_to_regex()
    assert query10._query_event_dimension == 2
    assert query10._query_typeset == {'a','b','c'}
    assert query10._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b'}})
    assert query10._query_repeated_variables == {'x'}
    assert query10._query_windowsize_local == [(-1, -1), (0, 0), (3, 7), (0, 2), (0, 0), (-1, -1)]
    assert query10._query_gap_constraints == []
    assert query10._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){1,3}(?:[^\\s;]+;){1,1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s){0,1}c;(?P=x);"
    assert query10.match_sample(sample, support) is True
    query10._query_gap_constraints=[]
    query10.query_string_to_normalform()
    assert query10._query_string == "a;b; ;$x0; c;$x0;"
    assert query10._query_windowsize_local == [(-1, -1), (0, 0), (3, 7), (0, 2), (0, 0), (-1, -1)]
    assert query10._query_string_length == 3
    assert query10._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s){1,3}(?:[^\\s;]+;){1,1}(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s){0,1}c;(?P=x0);"
    query10.check_consistency()
    assert query10.match_sample(sample, support) is True

    ############################################################
    sample = Sample()
    sample.set_sample(["a;b;a; a;b;a;"])
    ############################################################
    query = MultidimQuery()
    query.set_query_string(";;$x0; ;;$x0;")
    query.set_query_windowsize_local([(2,2),(2,5),(-1,-1)])
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){2}(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){2,2}(?P=x0);"
    assert match_test is True

    query._query_string=";$x0;; ;$x0;;"
    query.set_query_windowsize_local([(1,1),(2,5),(1,1)])
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){1,1}(?P<x0>[^\\s;]+);(?:[^\\s;]+;){1,1}\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){1,1}(?P=x0);(?:[^\\s;]+;){1,1}"
    assert match_test is True

    query._query_string=";b;; ;b;;"
    query.set_query_windowsize_local([(1,1),(2,5),(1,1)])
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){1,1}b;(?:[^\\s;]+;){1,1}\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){1,1}b;(?:[^\\s;]+;){1,1}"
    assert match_test is True

    query._query_string="a;;$x0; ;;$x0;"
    query.set_query_windowsize_local([(-1, -1),(1,1),(2,5),(-1, -1)])
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "a;(?:[^\\s;]+;){1}(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){2,2}(?P=x0);"
    assert match_test is True

    query._query_string=""
    query._query_event_dimension = 3
    query.set_query_string_length()
    query.set_query_windowsize_local([(6,9)])
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(((?:[^\\s;]+;){3})\\s){1,2}(?:[^\\s;]+;){3}"
    assert match_test is True

    LOGGER.info('Finished test test_event_flattening')

def test_multidim_event_flattening_wo_lws():
    """
        Test of all setter functions, to_normalform, to_regex, match_sample and
        check_consistence on more dimensional queries without local window
        sizes and gap constraints.
    """
    sample = Sample()
    sample.set_sample(["a; b; c; b;", "a; c; z; c; b;", "b; a; a; z; b; a; b;", "a; c; z; c; b; d;"])
    support = 0.5

    LOGGER.info('Started test test_event_flattening_wo_lws')
    query01 = MultidimQuery()
    query01.set_query_string("a; $x; $x; b;")
    query01.query_string_to_regex()
    assert query01._query_event_dimension == 1
    assert query01._query_typeset == {'a','b'}
    assert query01._query_repeated_variables == {'x'}
    assert query01._query_attribute_typesets == dict({1: {'a', 'b'}})
    assert query01._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s)*?(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s)*?(?P=x);\\s(((?:[^\\s;]+;){1})\\s)*?b;'
    assert query01.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;a; E;a; b;c;", "a;b; C;a; D;a; b;c;", "a;b; B;a; z;z; B;a; b;c;", "a;b; E;a; z;z; E;a; b;c;"])

    query02 = MultidimQuery()
    query02.set_query_string("a;b; $x;a; $x;a; b;c;")
    query02.query_string_to_regex()
    assert query02._query_event_dimension == 2
    assert query02._query_typeset == {'a','b','c'}
    assert query02._query_attribute_typesets == dict({1: {'a', 'b'}, 2: {'a', 'b','c'}})
    assert query02._query_repeated_variables == {'x'}
    assert query02._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<x>[^\\s;]+);a;\\s(((?:[^\\s;]+;){2})\\s)*?(?P=x);a;\\s(((?:[^\\s;]+;){2})\\s)*?b;c;"
    assert query02.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;d; F;F; E;e; b;c; d;G;","a;b; E;d; y;z; F;F; E;e; b;c; d;G;","a;b; E;d; F;G; E;e; b;c; d;H;","a;b; E;d; v;z; F;F; w;x; E;e; b;c; d;G;"])

    # non-repeated variable at the end of the query string
    query03 = MultidimQuery()
    query03.set_query_string("a;b; $x;d; $y;$y; $x;e; b;c; d;$z;")
    query03.query_string_to_regex()
    assert query03._query_event_dimension == 2
    assert query03._query_typeset == {'a','b','c','d','e'}
    assert query03._query_attribute_typesets == dict({1: {'a','b','d'}, 2: {'b','d','e','c'}})
    assert query03._query_repeated_variables == {'x','y'}
    assert query03._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<x>[^\\s;]+);d;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<y>[^\\s;]+);(?P=y);\\s(((?:[^\\s;]+;){2})\\s)*?(?P=x);e;\\s(((?:[^\\s;]+;){2})\\s)*?b;c;\\s(((?:[^\\s;]+;){2})\\s)*?d;(?P<z>[^\\s;]+);"
    assert query03.match_sample(sample,support) is True

    sample.set_sample(["U;b; X;d; Y;Y; X;e; b;c; d;Z;", "U;b; z;y; X;d; Y;Y; X;e; b;c; d;Z;", "U;b; X;d; a;b; c;d; Y;Y; X;e; b;c; d;Z;", "U;b; a;z; X;d; a;b; Y;Y; X;e; b;c; d;Z;"])

    # non-repeated variable at the beginning and the end of the query string
    query04 = MultidimQuery()
    query04.set_query_string("$u;b; $x;d; $y;$y; $x;e; b;c; d;$z;")
    query04.query_string_to_regex()
    assert query04._query_event_dimension == 2
    assert query04._query_typeset == {'b','c','d','e'}
    assert query04._query_attribute_typesets == dict({1: {'b','d'}, 2: {'b','d','c','e'}})
    assert query04._query_repeated_variables == {'x','y'}
    assert query04._query_string_regex == "(?P<u>[^\\s;]+);b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<x>[^\\s;]+);d;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<y>[^\\s;]+);(?P=y);\\s(((?:[^\\s;]+;){2})\\s)*?(?P=x);e;\\s(((?:[^\\s;]+;){2})\\s)*?b;c;\\s(((?:[^\\s;]+;){2})\\s)*?d;(?P<z>[^\\s;]+);"
    assert query04.match_sample(sample,support) is True

    sample.set_sample(["a;X; Y;a;", "a;X; b;c; d;e; Y;a;", "a;X; z;b; Y;a;", "a;X; z;b; Y;a;"])

    # non-repeated variable at the end of an event, followed by an event which starts with a non-repeated variable
    query05 = MultidimQuery()
    query05.set_query_string("a;$x; $y;a;")
    query05.query_string_to_regex()
    assert query05._query_event_dimension == 2
    assert query05._query_typeset == {'a'}
    assert query05._query_attribute_typesets == dict({1: {'a'}, 2: {'a'}})
    assert query05._query_repeated_variables == set()
    assert query05._query_string_regex == "a;(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P<y>[^\\s;]+);a;"
    assert query05.match_sample(sample,support) is True

    sample.set_sample(["a;b; c;d;", "a;a; z;c; z;e; b;b;", "a;X; z;b; Y;a;", "a;X; z;b; Y;a;"])

    # Query string consists only of non-repeated variables
    query06 = MultidimQuery()
    query06.set_query_string("$u;$v; $w;$x;")
    query06.query_string_to_regex()
    assert query06._query_event_dimension == 2
    assert query06._query_typeset == set()
    assert query06._query_attribute_typesets == dict({1: set(), 2: set()})
    assert query06._query_repeated_variables == set()
    assert query06._query_string_regex == "(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);"
    assert query06.match_sample(sample,support) is True

    sample.set_sample(["a;b; E;F; c;d;","a;b; z;z; z;z c;d;","a;b; E;F; E;F; E;F; E;F; c;d;","a;b; z;z; E;F; z;z; c;d;"])

    # Query string containing an event consisting only of non-repeated variables
    query07 = MultidimQuery()
    query07.set_query_string("a;b; $u;$v; c;d;")
    query07.query_string_to_regex()
    assert query07._query_event_dimension == 2
    assert query07._query_typeset == {'a','b','c','d'}
    assert query07._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query07._query_repeated_variables == set()
    assert query07._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"
    assert query07.match_sample(sample,support) is True
    query07.query_string_to_normalform()
    assert query07._query_string == "a;b; c;d;"
    assert query07._query_string_length == 2
    assert query07._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"

    # Query string containing two consecutive events consisting only of non-repeated variables
    query08 = MultidimQuery()
    query08.set_query_string("a;b; $u;$v; $w;$x; c;d;")
    query08.query_string_to_regex()
    assert query08._query_event_dimension == 2
    assert query08._query_typeset == {'a','b','c','d'}
    assert query08._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query08._query_repeated_variables == set()
    assert query08._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"
    assert query08.match_sample(sample,support) is True
    query08.query_string_to_normalform()
    assert query08._query_string == "a;b; c;d;"
    assert query08._query_string_length == 2
    assert query08._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"
    query08.check_consistency()

    # Query string containing three consecutive events consisting only of non-repeated variables
    query09 = MultidimQuery()
    query09.set_query_string("a;b; $u;$v; $w;$x; $y;$z; c;d;")
    query09.query_string_to_regex()
    assert query09._query_event_dimension == 2
    assert query09._query_typeset == {'a','b','c','d'}
    assert query09._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b','d'}})
    assert query09._query_repeated_variables == set()
    assert query09._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?(?P<u>[^\\s;]+);(?P<v>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P<w>[^\\s;]+);(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P<y>[^\\s;]+);(?P<z>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"
    assert query09.match_sample(sample,support) is True
    query09.query_string_to_normalform()
    assert query09._query_string == "a;b; c;d;"
    assert query09._query_string_length == 2
    assert query09._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?c;d;"

    sample.set_sample(["a;b; E;F; G;X; c;X;", "a;b; G;X; c;X;", "a;b; c;d; e;X; c;Y;", "a;b; E;F; z;z; G;X; z;z; c;X;"])

    query10= MultidimQuery()
    query10.set_query_string("a;b; ;; ;$x; c;$x;")
    query10.query_string_to_regex()
    assert query10._query_event_dimension == 2
    assert query10._query_typeset == {'a','b','c'}
    assert query10._query_attribute_typesets == dict({1: {'a','c'}, 2: {'b'}})
    assert query10._query_repeated_variables == {'x'}
    assert query10._query_string_regex == "a;b;\\s((?:[^\\s;]+;){2}\\s)*?((?:[^\\s;]+;){2})\\s((?:[^\\s;]+;){2}\\s)*?(?:[^\\s;]+;){1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?c;(?P=x);"
    assert query10.match_sample(sample, support) is True

    ############################################################
    sample = Sample()
    sample.set_sample(["a;b;a; a;b;a;"])
    ############################################################
    #query11
    query = MultidimQuery()
    query.set_query_string(";;$x0; ;;$x0;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){2}(?P<x0>[^\\s;]+);\\s((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){2}(?P=x0);"
    assert match_test is True

    #query12
    query._query_string=";$x0;; ;$x0;;"
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;)(?P<x0>[^\\s;]+);((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){1}(?P=x0);(?:[^\\s;]+;){1}"
    assert match_test is True

    #query13
    query._query_string=";b;; ;b;;"
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;)b;((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){1}b;(?:[^\\s;]+;){1}"
    assert match_test is True

    #query14
    query._query_string="a;;$x0; ;;$x0;"
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "a;(?:[^\\s;]+;){1}(?P<x0>[^\\s;]+);\\s((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){2}(?P=x0);"
    assert match_test is True

    #query15
    query._query_string=""
    query._query_event_dimension = 3
    query.set_query_string_length()
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == ""
    assert match_test is True

    LOGGER.info('Finished test test_event_flattening_wo_lws')

def test_multidim_get_missing_attributes_count():
    """
        Test of function get_missing_attributes_count().
    """
    LOGGER.info('Started test test_event_flattening')
    query01=MultidimQuery(given_query_string="a;b; $x;$x; c;d;")
    assert query01._get_missing_attributes_count()==0
    query02=MultidimQuery(given_query_string="a;b; ;; $x;$x; c;d;")
    assert query02._get_missing_attributes_count()==2
    query03=MultidimQuery(given_query_string="a;b; ;; ;$x; c;d;")
    assert query03._get_missing_attributes_count()==3
    query04=MultidimQuery(given_query_string=";b; ;; ;$x; c;;")
    assert query04._get_missing_attributes_count()==5
    query05=MultidimQuery(given_query_string=";; ;;")
    assert query05._get_missing_attributes_count()==4
    LOGGER.info('Finished test test_event_flattening')

def test_multidim_init_most_general_query_for_shinohara():
    """
        Test for function init_most_general_query_for_shinohara.
    """
    LOGGER.info('Started test test_init_most_general_query_for_shinohara')
    sample = Sample()
    query = MultidimQuery()
    try:
        query.init_most_general_query_for_shinohara(0, [(0,1),(0,1),(0,1)], 'shinohara_icdt', sample, event_dimension=1, gap_constraints=None, query_class="normal")
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    try:
        query.init_most_general_query_for_shinohara(3, None, 'shinohara_icdt', sample, event_dimension=1, gap_constraints=None, query_class="normal")
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    try:
        query.init_most_general_query_for_shinohara(4, [(0,1),(0,1),(0,1)], 'shinohara_icdt', sample, event_dimension=0, gap_constraints=None, query_class="normal")
        raise AssertionError("Test failed: No InvalidEventDimensionError raised!")
    except InvalidEventDimensionError:
        pass
    except Exception as err:
        raise err

    query01 = MultidimQuery()
    query01.init_most_general_query_for_shinohara(4, [(0,1),(0,1),(0,1)], 'shinohara_icdt', sample, event_dimension=1, gap_constraints=None, query_class="normal")
    assert query01._query_string == "$A; $B; $C; $D;", 'query01: Wrong query string!'
    assert query01._query_string_length == 4, 'query01: Wrong query string length!'
    assert query01._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(-1,-1)], 'query01: Wrong local window sizes!'
    assert query01._query_event_dimension == 1, 'query01: Wrong query event dimension!'
    assert query01._query_discovery_algorithm == 'shinohara_icdt', 'query01: Wrong query discovery algorithm!'

    query02 = MultidimQuery()
    query02.init_most_general_query_for_shinohara(3, [(0,0),(0,1),(0,0),(0,1),(0,0)], 'shinohara_icdt', sample, event_dimension=2, gap_constraints=None, query_class="normal")
    assert query02._query_string == "$A;$B; $C;$D; $E;$F;", 'query02: Wrong query string!'
    assert query02._query_string_length == 3, 'query02: Wrong query string length!'
    assert query02._query_windowsize_local == [(-1,-1),(0,0),(0,1),(0,0),(0,1),(0,0),(-1,-1)], 'query02: Wrong local window sizes!'
    assert query02._query_event_dimension == 2, 'query02: Wrong query event dimension!'
    assert query02._query_discovery_algorithm == 'shinohara_icdt', 'query02: Wrong query discovery algorithm!'

    query03 = MultidimQuery()
    query03.init_most_general_query_for_shinohara(3, [(0,0),(0,0),(0,0),(0,1),(0,0),(0,0),(0,0),(0,1),(0,0),(0,0),(0,0)], 'shinohara_icdt', sample, event_dimension=4, gap_constraints=None, query_class="normal")
    assert query03._query_string == "$A;$B;$C;$D; $E;$F;$G;$H; $I;$J;$K;$L;", 'query03: Wrong query string!'
    assert query03._query_string_length == 3, 'query03: Wrong query string length!'
    assert query03._query_windowsize_local == [(-1,-1),(0,0),(0,0),(0,0),(0,1),(0,0),(0,0),(0,0),(0,1),(0,0),(0,0),(0,0),(-1,-1)], 'query03: Wrong local window sizes!'
    assert query03._query_event_dimension == 4, 'query03: Wrong query event dimension!'
    assert query03._query_discovery_algorithm == 'shinohara_icdt', 'query03: Wrong query discovery algorithm!'
    LOGGER.info('Finished test test_init_most_general_query_for_shinohara')

def test_multidim_check_consistency():
    """
        Test for function check_consistency.
    """
    LOGGER.info('Started test test_check_consistency')
    query01 = MultidimQuery()
    query01.set_query_string("")
    query01._query_string_length = 3
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    query01._query_string_length = 0
    query01._query_windowsize_local = [(0,1),(0,1)]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query01.set_query_string("a; $x; $x; b;")
    query01.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query01._query_string_length = 3
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    query01._query_string_length = 4
    query01._query_windowsize_local = [(0,1),(0,1)]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query01._query_windowsize_local = [(0,1),(0,1),(1,0)]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query01.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query01._query_gap_constraints=[{'z'},{'z'}]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryGapConstraintError raised!")
    except InvalidQueryGapConstraintError:
        pass
    except Exception as err:
        raise err

    # Two-dimensional query
    query02 = MultidimQuery()
    query02.set_query_string("a;b; $x;a; $x;a; b;c;")
    query02._query_windowsize_local = [(0,1),(0,1)]
    try:
        query02.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query02._query_windowsize_local = [(0,0),(1,2),(0,0),(0,2),(0,0),(0,2),(0,0)]
    try:
        query02.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query02._query_windowsize_local = [(0,0),(2,0),(0,0),(0,2),(0,0),(0,2),(0,0)]
    try:
        query02.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query02.set_query_windowsize_local([(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0)])
    query02._query_gap_constraints=[{'z'},{'z'},{'z'}]
    try:
        query02.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryGapConstraintError raised!")
    except InvalidQueryGapConstraintError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test test_check_consistency')

def test_multidim_query_constructor():
    """
        Test for Query constructor.
    """
    LOGGER.info('Started test for constructor (__init__) of Query')

    gap_constraints = [{"a","b"},{"c","d"},{"e","f"},{"g","h"}]
    gap_constraints_2 = [{"z"},{"z"},{"z"},{"z"},{"z"},{"z"},{"z"},{"z"},{"z"}]
    window_size_global = 10
    window_size_local = [(0,0),(1,2),(1,4),(0,3)]
    window_size_local_2 = [(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0),(0,2),(0,0)]

    query01 = MultidimQuery()
    assert query01._query_string == "", 'query01: Wrong query string!'
    assert query01._query_string_length == 0, 'query01: Wrong query string length!'
    assert query01._query_repeated_variables == set(), 'query01: Wrong set of repeated variables!'
    assert query01._query_typeset == set(), 'query01: Wrong query typeset!'
    assert query01._query_attribute_typesets == {}, 'query01: Wrong dimensional typesets'
    assert query01._query_gap_constraints == [], 'query01: Wrong query gap constraints!'
    assert query01._query_windowsize_global == -1, 'query01: Wrong global windowsize!'
    assert query01._query_windowsize_local == [], 'query01: Wrong local window sizes!'

    query02 = MultidimQuery("$A; $B; $A; $D; c;")
    assert query02._query_string == "$A; $B; $A; $D; c;", 'query02: Wrong query string!'
    assert query02._query_string_length == 5, 'query02: Wrong query string length!'
    assert query02._query_repeated_variables == {"A"}, 'query02: Wrong set of repeated variables!'
    assert query02._query_typeset == {"c"}, 'query02: Wrong query typeset!'
    assert query02._query_attribute_typesets == dict({1: {'c'}}), 'query02: Wrong dimensional typesets'
    assert query02._query_gap_constraints == [], 'query02: Wrong query gap constraints!'
    assert query02._query_windowsize_global == -1, 'query02: Wrong global windowsize!'
    assert query02._query_windowsize_local == [], 'query02: Wrong local window sizes!'

    query03 = MultidimQuery("$A; $B; $A; $D; c;", gap_constraints)
    assert query03._query_string == "$A; $B; $A; $D; c;", 'query03: Wrong query string!'
    assert query03._query_string_length == 5, 'query03: Wrong query string length!'
    assert query03._query_repeated_variables == {"A"}, 'query03: Wrong set of repeated variables!'
    assert query03._query_typeset == {"c"}, 'query03: Wrong query typeset!'
    assert query03._query_attribute_typesets == dict({1: {'c'}}), 'query03: Wrong dimensional typesets'
    assert query03._query_gap_constraints == gap_constraints, 'query03: Wrong query gap constraints!'
    assert query03._query_windowsize_global == -1, 'query03: Wrong global windowsize!'
    assert query03._query_windowsize_local == [], 'query03: Wrong local window sizes!'

    query04 = MultidimQuery("$A; $B; $A; $D; c;", gap_constraints, window_size_global)
    assert query04._query_string == "$A; $B; $A; $D; c;", 'query04: Wrong query string!'
    assert query04._query_string_length == 5, 'query04: Wrong query string length!'
    assert query04._query_repeated_variables == {"A"}, 'query04: Wrong set of repeated variables!'
    assert query04._query_typeset == {"c"}, 'query04: Wrong query typeset!'
    assert query04._query_attribute_typesets == dict({1: {'c'}}), 'query04: Wrong dimensional typesets'
    assert query04._query_gap_constraints == gap_constraints, 'query04: Wrong query gap constraints!'
    assert query04._query_windowsize_global == window_size_global, 'query04: Wrong global windowsize!'
    assert query04._query_windowsize_local == [], 'query04: Wrong local window sizes!'

    query05 = MultidimQuery("$A;a; $B;b; $A;c; $D;d; e;f;", gap_constraints_2, window_size_global, window_size_local_2)
    assert query05._query_string == "$A;a; $B;b; $A;c; $D;d; e;f;", 'query05: Wrong query string!'
    assert query05._query_string_length == 5, 'query05: Wrong query string length!'
    assert query05._query_event_dimension == 2, 'query05: Wrong query event dimension!'
    assert query05._query_repeated_variables == {"A"}, 'query05: Wrong set of repeated variables!'
    assert query05._query_typeset == {'a','b','c','d','e','f'}, 'query05: Wrong query typeset!'
    assert query05._query_attribute_typesets == dict({1: {'e'}, 2: {'a','b','c','d','f'}}), 'query05: Wrong dimensional typesets'
    assert query05._query_gap_constraints == gap_constraints_2, 'query05: Wrong query gap constraints!'
    assert query05._query_windowsize_global == window_size_global, 'query05: Wrong global windowsize!'
    assert query05._query_windowsize_local == window_size_local_2, 'query05: Wrong local window sizes!'

    # No query string but other parameters should lead to initial query ignoring other parameters
    query06 = MultidimQuery(given_query_gap_constraints=gap_constraints, given_query_windowsize_global=window_size_global, given_query_windowsize_local=window_size_local)
    assert query06._query_string == "", 'query06: Wrong query string!'
    assert query06._query_string_length == 0, 'query06: Wrong query string length!'
    assert query06._query_repeated_variables == set(), 'query06: Wrong set of repeated variables!'
    assert query06._query_typeset == set(), 'query06: Wrong query typeset!'
    assert query06._query_attribute_typesets == {}, 'query06: Wrong dimensional typesets'
    assert query06._query_gap_constraints == [], 'query06: Wrong query gap constraints!'
    assert query06._query_windowsize_global == -1, 'query06: Wrong global windowsize!'
    assert query06._query_windowsize_local == [], 'query06: Wrong local window sizes!'

    LOGGER.info('Finished test for constructor (__init__) of Query')

##################################################
# Adapted tests from test_query.py
##################################################

def test_multidim_regex_without_local_ws():
    """
        Test for query_string_to_regex() if now local window sizes are given.
    """
    LOGGER.info("Started test test_multidim_regex_without_local_ws")
    sample = Sample()
    support = 1.0
    query = MultidimQuery()

    ############################################################
    sample.set_sample(["b; b;"])
    ############################################################
    query.set_query_string("b; b;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "b;\\s(((?:[^\\s;]+;){1})\\s)*?b;"
    assert match_test is True

    query.set_query_string("$x0; $x0;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s)*?(?P=x0);"
    assert match_test is True

    ############################################################
    sample.set_sample(["a;b; a;b;"])
    ############################################################
    query.set_query_string("a;b; a;b;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "a;b;\\s(((?:[^\\s;]+;){2})\\s)*?a;b;"
    assert match_test is True

    query.set_query_string("$x0;$x1; $x0;$x1;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?P<x0>[^\\s;]+);(?P<x1>[^\\s;]+);\\s(((?:[^\\s;]+;){2})\\s)*?(?P=x0);(?P=x1);"
    assert match_test is True

    ############################################################
    sample.set_sample(["a;b; a;b;"])
    ############################################################
    query.set_query_string("a;; a;;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "a;((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){2}\\s)*?a;(?:[^\\s;]+;){1}"
    assert match_test is True

    query.set_query_string("$x0;; $x0;;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?P<x0>[^\\s;]+);((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){2}\\s)*?(?P=x0);(?:[^\\s;]+;){1}"
    assert match_test is True

    query.set_query_string(";$x0; ;$x0;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;)(?P<x0>[^\\s;]+);\\s((?:[^\\s;]+;){2}\\s)*?(?:[^\\s;]+;){1}(?P=x0);"
    assert match_test is True

    ############################################################
    sample.set_sample(["a;b;a; a;b;a;"])
    ############################################################

    query.set_query_string(";;$x0; ;;$x0;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){2}(?P<x0>[^\\s;]+);\\s((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){2}(?P=x0);"
    assert match_test is True

    query.set_query_string(";$x0;; ;$x0;;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;)(?P<x0>[^\\s;]+);((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){1}(?P=x0);(?:[^\\s;]+;){1}"
    assert match_test is True

    query.set_query_string(";b;; ;b;;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;)b;((?:[^\\s;]+;){1}\\s)((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){1}b;(?:[^\\s;]+;){1}"
    assert match_test is True

    query.set_query_string("a;;$x0; ;;$x0;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "a;(?:[^\\s;]+;){1}(?P<x0>[^\\s;]+);\\s((?:[^\\s;]+;){3}\\s)*?(?:[^\\s;]+;){2}(?P=x0);"
    assert match_test is True

    query.set_query_string(";;; ;;;")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == "(?:[^\\s;]+;){3}\\s(?:[^\\s;]+;){3}"
    assert match_test is True

    query.set_query_string("")
    query.query_string_to_regex()
    match_test = query.match_sample(sample=sample, supp=support)
    assert query._query_string_regex == ""
    assert match_test is True

    LOGGER.info("Finished test test_multidim_regex_without_local_ws")

def test_multidim_regex():
    """
        Small test for building a regex to a given query string including some
        special cases.
    """
    LOGGER.info('Started test test_regex')
    LOGGER.info("**********************************************")
    query = MultidimQuery()
    query.set_query_string("$A; $B; $A; $D;")
    LOGGER.info('Query string:         %s',query._query_string)
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query.query_string_to_regex()
    assert query._query_string == "$A; $B; $A; $D;", 'query: Wrong query string!'
    assert query._query_string_regex == '(?P<A>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<B>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P=A);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<D>[^\\s;]+);', 'query: Wrong regex!'
    LOGGER.info('Query regex:          %s',query._query_string_regex)
    query.query_string_to_normalform()
    assert query._query_string == "$x0; $x0;", 'query: Wrong query string after to_normalform'
    assert query._query_windowsize_local == [(-1, -1), (1, 3), (1, 2)], 'query: Wrong local window sizes after to_normalform'
    LOGGER.info('Query string in NF:   %s',query._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query window sizes:   %s',query_windowsize_local)
    query.query_string_to_regex()
    assert query._query_string_regex == '(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){1,3}(?P=x0);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?:[^\\s;]+;){1}', 'query: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query NF regex:       %s',query._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query0 = MultidimQuery()
    query0.set_query_string("a; $x; $y; a;")
    LOGGER.info('Query 0 string:       %s',query0._query_string)
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query0.query_string_to_regex()
    assert query0._query_string == "a; $x; $y; a;", 'query: Wrong query string!'
    assert query0._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<y>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}a;', 'query0: Wrong regex!'
    LOGGER.info('Query 0 regex:        %s',query0._query_string_regex)
    query0.query_string_to_normalform()
    assert query0._query_string == "a; a;", 'query0: Wrong query string after to_normalform'
    assert query0._query_windowsize_local == [(-1, -1), (2, 5), (-1, -1)], 'query0: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 0 string in NF: %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)
    query0.query_string_to_regex()
    assert query0._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){2,5}a;', 'query0: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 0 NF regex:     %s',query0._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query1 = MultidimQuery()
    query1.set_query_string("a; $x; $y;")
    LOGGER.info('Query 1 string:       %s',query1._query_string)
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(2,3)])
    query1.query_string_to_regex()
    assert query1._query_string == "a; $x; $y;", 'query1: Wrong query string!'
    assert query1._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<y>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){1,2}(?:[^\\s;]+;){1}', 'query1: Wrong regex!'
    LOGGER.info('Query 1 regex:        %s',query1._query_string_regex)
    query1.query_string_to_normalform()
    assert query1._query_string == "a;", 'query1: Wrong query string after to_normalform'
    assert query1._query_windowsize_local == [(-1, -1), (4,7)], 'query1: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 1 string in NF: %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)
    query1.query_string_to_regex()
    assert query1._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){3,6}(?:[^\\s;]+;){1}', 'query1: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 1 NF regex:     %s',query1._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query2 = MultidimQuery()
    query2.set_query_string("$x; a; a; $y;")
    LOGGER.info('Query 2 string:       %s',query2._query_string)
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query2.query_string_to_regex()
    assert query2._query_string == "$x; a; a; $y;", 'query2: Wrong query string!'
    assert query2._query_string_regex == '(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<y>[^\\s;]+);', 'query2: Wrong regex!'
    LOGGER.info('Query 2 regex:        %s',query2._query_string_regex)
    query2.query_string_to_normalform()
    assert query2._query_string == "a; a;", 'query2: Wrong query string after to_normalform'
    assert query2._query_windowsize_local == [(1, 2), (0, 1), (1, 2)], 'query2: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 2 string in NF: %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)
    query2.query_string_to_regex()
    LOGGER.info(query2._query_string_regex)
    assert query2._query_string_regex == '(((?:[^\\s;]+;){1})\\s){1,2}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?:[^\\s;]+;){1}', 'query2: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 2 NF regex:     %s',query2._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query3 = MultidimQuery()
    query3.set_query_string("$x; $y; $z;")
    LOGGER.info('Query 3 string:       %s',query3._query_string)
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    query3.query_string_to_regex()
    LOGGER.info('Query 3 regex:        %s',query3._query_string_regex)
    assert query3._query_string == "$x; $y; $z;", 'query3: Wrong query string!'
    assert query3._query_string_regex == '(?P<x>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<y>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<z>[^\\s;]+);', 'query3: Wrong regex!'
    query3.query_string_to_normalform()
    assert query3._query_string == "", 'query3: Wrong query string after to_normalform'
    assert query3._query_windowsize_local == [(3,5)], 'query3: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 3 string in NF: %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)
    query3.query_string_to_regex()
    assert query3._query_string_regex == '(?:[^\\s;]+;\\s){2,4}[^\\s;]+;', 'query3: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 3 NF regex:     %s',query3._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query4 = MultidimQuery(given_query_string="; ; ; bb;")
    query4.set_query_windowsize_local([(3,6),(-1,-1)])
    query4.query_string_to_regex()
    assert query4._query_string_regex == "(?:[^\\s;]+;){1,1}\\s(((?:[^\\s;]+;){1})\\s){2,5}bb;", 'query4: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 4 regex:        %s',query4._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query5 = MultidimQuery(given_query_string=";; ;; aa;;")
    query5.set_query_windowsize_local([(4,6),(1,1)])
    query5.query_string_to_regex()
    assert query5._query_string_regex == "(?:[^\\s;]+;){2,2}\\s(((?:[^\\s;]+;){2})\\s){1,2}aa;(?:[^\\s;]+;){1,1}", 'query5: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 5 regex:        %s',query5._query_string_regex)
    LOGGER.info("**********************************************")

    # Missing events at the beginning without ; at first pos
    LOGGER.info("**********************************************")
    query6 = MultidimQuery(given_query_string="aa;;")
    query6.set_query_windowsize_local([(4,8),(1,1)])
    query6.query_string_to_regex()
    assert query6._query_string_regex == "(((?:[^\\s;]+;){2})\\s){2,4}aa;(?:[^\\s;]+;){1,1}", 'query6: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 6 regex:        %s',query6._query_string_regex)
    LOGGER.info("**********************************************")

    # Missing events in the end
    LOGGER.info("**********************************************")
    query7 = MultidimQuery(given_query_string="aa;;")
    query7.set_query_windowsize_local([(-1, -1), (5, 9)])
    query7.query_string_to_regex()
    assert query7._query_string_regex == "aa;(?:[^\\s;]+;){1,1}\\s(((?:[^\\s;]+;){2})\\s){1,3}(?:[^\\s;]+;){2}", 'query7: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 7 regex:        %s',query7._query_string_regex)
    LOGGER.info("**********************************************")

    # Missing events at the beginning with ; at first pos
    LOGGER.info("**********************************************")
    query8 = MultidimQuery(given_query_string=";bb;")
    query8.set_query_windowsize_local([(5, 9),(-1, -1)])
    query8.query_string_to_regex()
    assert query8._query_string_regex == "(?:[^\\s;]+;){2,2}\\s(((?:[^\\s;]+;){2})\\s){1,3}(?:[^\\s;]+;){1,1}bb;", 'query8: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 8 regex:        %s',query8._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query9 = MultidimQuery(given_query_string=";;; $x0;$x0;$x0; ;;aa;")
    query9.set_query_windowsize_local([(3, 6), (0, 0), (0, 0), (2, 5), (-1, -1)])
    query9.query_string_to_regex()
    assert query9._query_string_regex == "(?:[^\\s;]+;){3,3}\\s(((?:[^\\s;]+;){3})\\s){0,1}(?P<x0>[^\\s;]+);(?P=x0);(?P=x0);\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){2,2}aa;", 'query9: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 9 regex:        %s',query9._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query10 = MultidimQuery(given_query_string="aa;;; ;$x0;$x0; ;bb;;")
    query10.set_query_windowsize_local([(-1, -1), (3, 6), (0, 0), (1, 4), (1, 1)])
    query10.query_string_to_regex()
    assert query10._query_string_regex == "aa;(?:[^\\s;]+;){2,2}\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){1,1}(?P<x0>[^\\s;]+);(?P=x0);\\s(((?:[^\\s;]+;){3})\\s){0,1}(?:[^\\s;]+;){1,1}bb;(?:[^\\s;]+;){1,1}", 'query10: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 10 regex:        %s',query10._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query11 = MultidimQuery(given_query_string="aa;$B;aa; $E;$E;$F; aa;bb;aa;")
    query11.set_query_windowsize_local([(-1, -1), (0, 0), (0, 0), (0, 3), (0, 0), (0, 0), (0, 3), (0, 0), (0, 0), (-1, -1)])
    query11.query_string_to_regex()
    assert query11._query_string_regex == "aa;(?P<B>[^\\s;]+);aa;\\s(((?:[^\\s;]+;){3})\\s){0,1}(?P<E>[^\\s;]+);(?P=E);(?P<F>[^\\s;]+);\\s(((?:[^\\s;]+;){3})\\s){0,1}aa;bb;aa;", 'query11: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 11 regex:        %s',query11._query_string_regex)
    LOGGER.info("**********************************************")
    LOGGER.info('Finished test test_regex')

def test_multidim_normalform():
    """
        Small test for bilding the normalform of a given query string inluding
        some special cases.
    """
    LOGGER.info('Started test test_normalform')
    query0 = MultidimQuery()
    query0.set_query_string("a; $x; $y; a;")
    LOGGER.info('Query 0 string:       %s',query0._query_string)
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query0.query_string_to_normalform()
    assert query0._query_string == 'a; a;'
    assert query0._query_windowsize_local==[(-1, -1), (2, 5), (-1, -1)]
    LOGGER.info('Query 0 string in NF: %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)

    query1 = MultidimQuery()
    query1.set_query_string("a; $x; $y;")
    LOGGER.info('Query 1 string:       %s',query1._query_string)
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(0,1),(0,1)])
    query1.query_string_to_normalform()
    assert query1._query_string == 'a;'
    assert query1._query_windowsize_local==[(-1, -1), (2, 4)]
    LOGGER.info('Query 1 string in NF: %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)

    query2 = MultidimQuery()
    query2.set_query_string("$x; a; a; $y;")
    LOGGER.info('Query 2 string:       %s',query2._query_string)
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query2.query_string_to_normalform()
    assert query2._query_string == 'a; a;'
    assert query2._query_windowsize_local==[(1, 2), (0, 1), (1, 2)]
    LOGGER.info('Query 2 string in NF: %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)

    query3 = MultidimQuery()
    query3.set_query_string("$x; $y; $z;")
    LOGGER.info('Query 3 string:       %s',query3._query_string)
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    query3.query_string_to_normalform()
    assert query3._query_string == ''
    assert query3._query_windowsize_local==[(3,5)]
    LOGGER.info('Query 3 string in NF: %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)

    query4 = MultidimQuery()
    query4.set_query_string("$x; a; a; $x;")
    LOGGER.info('Query 4 string:       %s',query4._query_string)
    query4.set_query_string_length()
    query4.set_query_repeated_variables()
    query4.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query4.query_string_to_normalform()
    assert query4._query_string == '$x0; a; a; $x0;'
    assert query4._query_windowsize_local==[(-1, -1),(0,1),(0,1), (0,1),(-1, -1)]
    LOGGER.info('Query 4 string in NF: %s',query4._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query4._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 4 window sizes:   %s',query_windowsize_local)

    query5 = MultidimQuery()
    query5.set_query_string("$x; $a; $a; $x;")
    LOGGER.info('Query 5 string:       %s',query5._query_string)
    query5.set_query_string_length()
    query5.set_query_repeated_variables()
    query5.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query5.query_string_to_normalform()
    assert query5._query_string == '$x0; $x1; $x1; $x0;'
    assert query5._query_windowsize_local==[(-1, -1),(0,1),(0,1), (0,1),(-1, -1)]
    LOGGER.info('Query 5 string in NF: %s',query5._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query5._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 5 window sizes:   %s',query_windowsize_local)

    query6 = MultidimQuery()
    query6.set_query_string("$x; $a; $a; $x; $y; $y; $a; $c; $d; $a; $d; $c;")
    LOGGER.info('Query 6 string:       %s',query6._query_string)
    query6.set_query_string_length()
    query6.set_query_repeated_variables()
    query6.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])
    query6.query_string_to_normalform()
    assert query6._query_string == '$x0; $x1; $x1; $x0; $x2; $x2; $x1; $x3; $x4; $x1; $x4; $x3;'
    assert query6._query_windowsize_local==[(-1, -1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1, -1)]
    LOGGER.info('Query 6 string in NF: %s',query6._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query6._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 6 window sizes:   %s',query_windowsize_local)

    query7 = MultidimQuery()
    query7.set_query_string("$x0;$x1;$x2; $x0;$x1;$x2;")
    LOGGER.info('Query 7 string:       %s',query7._query_string)
    query7.set_query_string_length()
    query7.set_query_repeated_variables()
    query7.query_string_to_normalform()
    assert query7._query_string == '$x0;$x1;$x2; $x0;$x1;$x2;'
    LOGGER.info('Query 7 string in NF: %s',query7._query_string)

    query8 = MultidimQuery()
    query8.set_query_string("a; $x1; $x1; $x0; $x0;")
    LOGGER.info('Query 8 string:       %s',query8._query_string)
    query8.set_query_string_length()
    query8.set_query_repeated_variables()
    query8.query_string_to_normalform()
    assert query8._query_string == 'a; $x0; $x0; $x1; $x1;'
    LOGGER.info('Query 8 string in NF: %s',query8._query_string)

    LOGGER.info('Finished test test_normalform')

def test_multidim_match_sample_special_cases_without_gapconstraints():
    """
        Small test for the regex-based matchtest for queries.
    """
    LOGGER.info('Started test test_match_sample_special_cases_without_gapconstraints')
    sample = Sample()
    sample.set_sample(["a; b; c; a;", "a; c; b; a;", "b; a; a; c; d;", "c; a; a; b; d;"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    support = 0.5
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)
    LOGGER.info('Requested support:        %s',str(support))

    query0 = MultidimQuery()
    query0.set_query_string("a; $x; $y; a;")
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 0 string:           %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)
    result = query0.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query0._query_string_regex)
    LOGGER.info('Result of match_sample:   %s', str(result))
    LOGGER.info('Support of the query:     %s', str(query0._query_sample_support))
    matched_traces_list = [str(int) for int in query0._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query0._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query0._query_string == "a; a;", 'query0: Wrong query string after match_sample (and hence to_notmalform)'
    assert query0._query_string_regex == "a;\\s(((?:[^\\s;]+;){1})\\s){2,5}a;", 'query0: Wrong query string regex'
    assert result is True, 'query0: Wrong match_sample result'
    assert query0._query_sample_support == 0.5, 'query0: Wrong sample support'
    assert query0._query_matched_traces == [0, 1], 'query0: Wrong list of matched traces'
    assert query0._query_not_matched_traces == [2, 3], 'query0: Wrong list of not matched traces'

    query1 = MultidimQuery()
    query1.set_query_string("a; $x; $y;")
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 1 string:           %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)
    result = query1.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query1._query_string_regex)
    LOGGER.info('Result of match_sample:   %s', str(result))
    LOGGER.info('Support of the query:     %s', str(query1._query_sample_support))
    matched_traces_list = [str(int) for int in query1._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query1._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query1._query_string == "a;", 'query1: Wrong query string after match_sample (and hence to_notmalform)'
    assert query1._query_string_regex == "a;\\s(((?:[^\\s;]+;){1})\\s){1,3}(?:[^\\s;]+;){1}", 'query1: Wrong query string regex'
    assert result is True, 'query1: Wrong match_sample result'
    assert query1._query_sample_support == 1.0, 'query1: Wrong sample support'
    assert query1._query_matched_traces == [0, 1, 2, 3], 'query1: Wrong list of matched traces'
    assert query1._query_not_matched_traces == [], 'query1: Wrong list of not matched traces'

    query2 = MultidimQuery()
    query2.set_query_string("$x; a; a; $y;")
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 2 string:           %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)
    result = query2.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query2._query_string_regex)
    LOGGER.info('Result of match_sample:   %s', str(result))
    LOGGER.info('Support of the query:     %s', str(query2._query_sample_support))
    matched_traces_list = [str(int) for int in query2._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query2._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query2._query_string == "a; a;", 'query2: Wrong query string after match_sample (and hence to_notmalform)'
    LOGGER.info(query2._query_string_regex)
    assert query2._query_string_regex == "(((?:[^\\s;]+;){1})\\s){1,2}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?:[^\\s;]+;){1}", 'query2: Wrong query string regex'
    assert result is True, 'query2: Wrong match_sample result'
    assert query2._query_sample_support == 0.5, 'query2: Wrong sample support'
    assert query2._query_matched_traces == [2, 3], 'query2: Wrong list of matched traces'
    assert query2._query_not_matched_traces == [0, 1], 'query2: Wrong list of not matched traces'

    query3 = MultidimQuery()
    query3.set_query_string("$x; $y; $z;")
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 3 string:           %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)
    result = query3.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query3._query_string_regex)
    LOGGER.info('Result of match_sample:   %s', str(result))
    LOGGER.info('Support of the query:     %s', str(query3._query_sample_support))
    matched_traces_list = [str(int) for int in query3._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query3._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query3._query_string == "", 'query3: Wrong query string after match_sample (and hence to_notmalform)'
    assert query3._query_string_regex == "(?:[^\\s;]+;\\s){2,4}[^\\s;]+;", 'query3: Wrong query string regex'
    assert result is True, 'query3: Wrong match_sample result'
    assert query3._query_sample_support == 1.0, 'query3: Wrong sample support'
    assert query3._query_matched_traces == [0, 1, 2, 3], 'query3: Wrong list of matched traces'
    assert query3._query_not_matched_traces == [], 'query3: Wrong list of not matched traces'

    LOGGER.info('Finished test test_match_sample_special_cases_without_gapconstraints')

def test_multidim_match_sample_simple():
    """
        Small test for the regex-based matchtest for queries which contain gap
        constraints.
    """
    LOGGER.info('Started test test_match_sample_simple')
    query = MultidimQuery()
    query.set_query_string("a; $x; $x; $y; $z; $y; b;")
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])
    query.set_query_gap_constraints([{"K"}, {"L"}, {"M"}, {"N"}, {"O"}, {"P"}])

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    windowsize_local = ",".join(windowsize_local_list)
    gap_constraints = ' '.join(f"{str(gap)}" for gap in query._query_gap_constraints)
    LOGGER.info('Query repeated vars:      %s',repeated_variables)
    LOGGER.info('Query window sizes:       %s',windowsize_local)
    LOGGER.info('Query gap constraints:    %s',gap_constraints)

    sample = Sample()
    sample.set_sample([ "a; c; c; d; a; d; b;", "a; c; c; d; K; d; b;", "a; c; c; d; d; b;",
                        "a; c; c; d; N; d; b;", "a; c; R; S; c; d; a; d; b;"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)

    support = 0.5
    result = query.match_sample(sample, support, complete_test=True)
    LOGGER.info('In normalform?:           %s',str(query._query_is_in_normalform))
    LOGGER.info('Regex of query string:    %s',query._query_string_regex)
    LOGGER.info('Requested support:        %s',str(support))
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query._query_sample_support))
    LOGGER.info("**********************************************")
    assert query._query_string == "a; $x; $x; $y; $z; $y; b;", 'query: Wrong query string'
    assert query._query_string_length == 7, 'query: Wrong query string length'
    assert query._query_repeated_variables == {'x','y'}, 'query: Wrong set of repeated variables'
    assert query._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1,-1)], 'query: Wrong local window sizes'
    assert query._query_gap_constraints == [{"K"}, {"L"}, {"M"}, {"N"}, {"O"}, {"P"}], 'query: Wrong gap constraints'
    assert query._query_is_in_normalform is False, 'query: Should not be in normalform because of given gap constraints'
    assert query._query_string_regex == 'a;\\s(((?:[^\\s;K]+;){1})\\s){0,1}(?P<x>[^\\s;]+);\\s(((?:[^\\s;L]+;){1})\\s){0,1}(?P=x);\\s(((?:[^\\s;M]+;){1})\\s){0,1}(?P<y>[^\\s;]+);\\s(((?:[^\\s;N]+;){1})\\s){0,1}(?P<z>[^\\s;]+);\\s(((?:[^\\s;O]+;){1})\\s){0,1}(?P=y);\\s(((?:[^\\s;P]+;){1})\\s){0,1}b;', 'query: Wrong query string regex'
    assert result is True, 'query: Wrong match_sample result'
    assert query._query_sample_support == 0.6, 'query: Wrong support ot the query'

    LOGGER.info('Finished test test_match_sample_simple')

def test_multidim_setter_nf_regex_matching():
    """
        Small test for basic functionalities including setter, to_normalform,
        to_regex and matching for one simple query.
    """
    LOGGER.info('Started test test_setter_nf_regex_matching')
    query = MultidimQuery()
    query.set_query_string("a; $x; $x; $y; $z; $y; b;")
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    windowsize_local = ",".join(windowsize_local_list)
    LOGGER.info('Query repeated vars:      %s',repeated_variables)
    LOGGER.info('Query window sizes:       %s',windowsize_local)
    assert query._query_string == "a; $x; $x; $y; $z; $y; b;", 'query: Wrong query string'
    assert query._query_string_length == 7, 'query: Wrong query string length'
    assert query._query_repeated_variables == {'x','y'}, 'query: Wrong set of repeated variables'
    assert query._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1,-1)], 'query: Wrong local window sizes'

    query.query_string_to_normalform()

    LOGGER.info('NF-Query string:          %s',query._query_string)
    LOGGER.info('                          %s',str(query._query_is_in_normalform))
    LOGGER.info('NF-Query string length:   %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    windowsize_local = ",".join(windowsize_local_list)
    gap_constraints = ' '.join(f"{str(gap)}" for gap in query._query_gap_constraints)
    LOGGER.info('NF-Query repeated vars:   %s',repeated_variables)
    LOGGER.info('NF-Query window sizes:    %s',windowsize_local)
    LOGGER.info('NF-Query gap constraints: %s',gap_constraints)
    assert query._query_string == "a; $x0; $x0; $x1; $x1; b;", 'query: Wrong query string after to_normalform'
    assert query._query_is_in_normalform is True, 'query: is_in_normalform should be True but is False'
    assert query._query_string_length == 6, 'query: Wrong query string length after to_normalform'
    assert query._query_windowsize_local == [(-1, -1), (0, 1), (0, 1), (0, 1), (1, 3), (0, 1), (-1, -1)], 'query: Wrong local window sizes after to_normalform'

    sample = Sample()
    sample.set_sample([ "a; c; c; d; a; d; b;", "a; c; c; d; K; d; b;",
                        "a; c; c; d; d; b;", "a; c; c; d; N; d; b;", "a; c; R; S; c; d; a; d; b;"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)

    support = 0.5
    result = query.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query._query_string_regex)
    LOGGER.info('Requested support:        %s',str(support))
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query._query_sample_support))
    LOGGER.info("**********************************************")
    assert query._query_string_regex == 'a;\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<x0>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P=x0);\\s(((?:[^\\s;]+;){1})\\s){0,1}(?P<x1>[^\\s;]+);\\s(((?:[^\\s;]+;){1})\\s){1,3}(?P=x1);\\s(((?:[^\\s;]+;){1})\\s){0,1}b;', 'query: Wrong query string regex'
    assert result is True, 'query: Wrong match_sample result'
    assert query._query_sample_support == 0.6, 'query: Wrong support ot the query'

    LOGGER.info('Finished test test_setter_nf_regex_matching')

def test_multidim_set_query_typeset():
    """
        Small test for computing the set of type occurring in a query string.
    """
    LOGGER.info('Started test test_set_query_typeset')
    query = MultidimQuery()
    query.set_query_string('A; $X; $Y; B;')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'A','B'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    query = MultidimQuery()
    query.set_query_string('A; B; C; B;')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'A','B','C'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    query = MultidimQuery()
    query.set_query_string('AB; $X; $Y; CD;')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'AB','CD'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    LOGGER.info('Finished test test_set_query_typeset')

def test00_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare Query instance to non-
        Query instance
    """
    LOGGER.info('Started test 00 for _syntactically_equal')

    query = MultidimQuery()
    noquery = 5

    try:
        query == noquery
        raise AssertionError("Test failed: No error raised!")
    except TypeError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for _syntactically_equal')

def test02_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having different query_string_length.
    """
    LOGGER.info('Started test 02 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1._query_string_length = 5
    query2._query_string_length = 6

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 02 for _syntactically_equal')

def test03_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length but different typesets.
    """
    LOGGER.info('Started test 03 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1._query_string_length = 5
    query1._query_typeset = set(['AA', 'BC'])

    query2._query_string_length = 5
    query2._query_typeset = set(['AA', 'BB'])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 03 for _syntactically_equal')

def test04_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length and typeset but different sets of
        repeated variables.
    """
    LOGGER.info('Started test 04 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1._query_string_length = 5
    query1._query_typeset = set(['AA', 'BC'])
    query1._query_repeated_variables = set(['XX', 'Y'])

    query2._query_string_length = 5
    query2._query_typeset = set(['BC', 'AA'])
    query2._query_repeated_variables = (['XD', 'Y'])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 04 for _syntactically_equal')

def test05_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length and typeset but different strings.
    """
    LOGGER.info('Started test 05 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_typeset()

    query2.set_query_string('AA; $XX; $XX; $Y; BC;')
    query2.set_query_string_length()
    query2.set_query_typeset()

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 05 for _syntactically_equal')

def test06_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset but
        different gap constraints.
    """
    LOGGER.info('Started test 06 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_typeset()
    query1._query_gap_constraints = [['A','B'],['AB','BB'],['BC']]

    query2.set_query_string('AA; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_typeset()
    query2._query_gap_constraints = [[],['A','B'],['AB','BC'],['BC']]

    try:
        query1 == query2
        raise AssertionError("Test failed: No error raised!")
    except InvalidQueryGapConstraintError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 06 for _syntactically_equal')

def test07_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset but
        different gap constraints.
    """
    LOGGER.info('Started test 07 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_typeset()
    query1.set_query_gap_constraints([[],['A','B'],['AB','BB'],['BC']])

    query2.set_query_string('AA; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_typeset()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 07 for _syntactically_equal')

def test08_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset and gap
        constraints, but a different global window size.
    """
    LOGGER.info('Started test 08 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(6)

    query2.set_query_string('AA; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(7)

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 08 for _syntactically_equal')

def test09_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset, gap
        constraints and global window size, but different local window sizes.
    """
    LOGGER.info('Started test 09 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],['A','B'],['BC','AB'],['BC']])
    query1.set_query_windowsize_global(7)
    query1.set_query_windowsize_local([(-1,-1), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query2.set_query_string('AA; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(7)
    query2.set_query_windowsize_local([(-1,-1), (2,3), (0,4), (1,2), (0,1), (-1,-1)])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 09 for _syntactically_equal')

def test10_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset, gap
        constraints and global window size, but different local window sizes.
    """
    LOGGER.info('Started test 10 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,2), (-1,-1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 10 for _syntactically_equal')

def test11_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes, whereby only one query has local window
        sizes starting / ending with (-1,-1). Comparison is done in both
        directions.
    """
    LOGGER.info('Started test 11 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(1,2), (1,3), (0,4), (1,2), (0,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 11 for _syntactically_equal')

def test12_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes except for the local window sizes.
    """
    LOGGER.info('Started test 12 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (1,-1)])

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 12 for _syntactically_equal')

def test13_multidim__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes, whereby only one query has local window
        sizes starting / ending with (-1,-1).
    """
    LOGGER.info('Started test 13 for _syntactically_equal')

    query1 = MultidimQuery()
    query2 = MultidimQuery()

    query1.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA; $Y; $XX; $Y; $XX; BC;')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(1,2), (0,3), (0,4), (1,2), (0,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert query1 == query2, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 13 for _syntactically_equal')
