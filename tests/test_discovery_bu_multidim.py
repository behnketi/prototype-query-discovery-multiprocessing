#!/usr/bin/python3
"""Contains tests for functions in discovery_bu_multidim.py"""
import sys
import logging
import time


sys.path.append(".")

from discovery_bu_multidim import bu_discovery_multidim
from sample_multidim import MultidimSample
from discovery_il_miner import il_miner
from discovery_bu_pts_multidim import discovery_bu_pts_multidim
#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def test_discovery_bu_multidim_unified_00():
    sample_set = ["a;b;c;" , "a;b;c;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset = result_dict['queryset']
    result_set = {"a;b;c;"}

    assert queryset == result_set

def test_discovery_bu_multidim_separated_00():
    sample_set = ["a;b;c;" , "a;b;c;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {"a;b;c;"}

    assert queryset == result_set

def test_discovery_bu_multidim_separatedps_00():
    sample_set = ["a;b;c;" , "a;b;c;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'pattern-split-sep', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {"a;b;c;"}

    assert queryset == result_set

def test_discovery_bu_multidim_unified_01():
    sample_set = ["a;b;c; a;b;c;", "e;f;g; e;f;g;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset = result_dict['queryset']
    result_set = {"$x0;$x1;$x2; $x0;$x1;$x2;"}

    assert queryset == result_set

def test_discovery_bu_multidim_separated_01():
    sample_set = ["a;b;c; a;b;c;", "e;f;g; e;f;g;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {"$x0;$x1;$x2; $x0;$x1;$x2;"}

    assert queryset == result_set

def test_discovery_bu_multidim_separatedps_01():
    sample_set = ["a;b;c; a;b;c;", "e;f;g; e;f;g;" ]
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'pattern-split-sep', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {"$x0;$x1;$x2; $x0;$x1;$x2;"}

    assert queryset == result_set

def test_discovery_bu_multidim_unified_02():
    sample_set = ['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;']
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset = result_dict['queryset']
    result_set = {';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;', ';$x0;; ;b;; ;p;; ;$x0;; ;;o;', '$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;', ';;e; ;b;; ;p;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;', ';;e; ;;$x0; ;p;$x0; ;s;$x0;', ';;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;', ';$x0;; ;b;; ;$x0;; ;s;;', ';;e; ;b;; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;', '$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;', ';;e; ;;$x0; ;p;$x0; ;;o;', '$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;', '$x0;;; ;b;; $x0;;$x1; ;;$x1;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;', ';;e; ;;o; $x0;;; $x0;;o;', ';;e; ;b;; ;;o; ;;o;', ';$x0;; ;b;; ;;o; ;$x0;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;', '$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;', '$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;'}
    assert queryset == result_set

def test_discovery_bu_multidim_separated_02():
    sample_set = ['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;']
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;', ';$x0;; ;b;; ;p;; ;$x0;; ;;o;', '$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;', ';;e; ;b;; ;p;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;', ';;e; ;;$x0; ;p;$x0; ;s;$x0;', ';;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;', ';$x0;; ;b;; ;$x0;; ;s;;', ';;e; ;b;; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;', '$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;', ';;e; ;;$x0; ;p;$x0; ;;o;', '$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;', '$x0;;; ;b;; $x0;;$x1; ;;$x1;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;', ';;e; ;;o; $x0;;; $x0;;o;', ';;e; ;b;; ;;o; ;;o;', ';$x0;; ;b;; ;;o; ;$x0;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;', '$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;', '$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;'}

    assert queryset == result_set

def test_discovery_bu_multidim_separatedps_02():
    sample_set = ['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;']
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict = bu_discovery_multidim(sample, 1.0, 'pattern-split-sep', domain_seperated=True)
    queryset = result_dict['queryset']
    result_set = {';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;', ';$x0;; ;b;; ;p;; ;$x0;; ;;o;', '$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;', ';;e; ;b;; ;p;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;', ';;e; ;;$x0; ;p;$x0; ;s;$x0;', ';;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;', ';$x0;; ;b;; ;$x0;; ;s;;', ';;e; ;b;; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;', '$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;', ';;e; ;;$x0; ;p;$x0; ;;o;', '$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;', '$x0;;; ;b;; $x0;;$x1; ;;$x1;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;', ';;e; ;;o; $x0;;; $x0;;o;', ';;e; ;b;; ;;o; ;;o;', ';$x0;; ;b;; ;;o; ;$x0;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;', '$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;', '$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;'}
    
    assert queryset == result_set

def test_discovery_bu_multidim_separatedps_03():
    sample_set = ['M;M;M; o;o;o; j;o;j; d;d;d; b;b;b; E;E;E; G;G;G; y;y;y; e;e;e; x;x;x; s;s;s; B;M;B; J;J;J; D;D;D; C;C;C; K;K;K; M;L;L; h;h;h; a;a;a; t;t;M;', 'f;f;f; u;u;u; l;u;l; A;A;A; c;c;c; n;n;n; q;q;q; H;H;H; F;F;F; g;g;g; m;m;m; w;f;w; v;v;v; p;p;p; z;z;z; i;i;i; f;r;r; I;I;I; N;N;N; k;k;f;']
    sample= MultidimSample()
    sample._sample = sorted(sample_set, key=len)
    sample._sample_event_dimension = sample_set[0].split()[0].count(';')
    sample._sample_size = len(sample_set)
    sample.get_vertical_sequence_database()

    result_dict_ps = bu_discovery_multidim(sample, 1.0, 'pattern-split-sep', domain_seperated=True)
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=True)
    queryset_ps = result_dict_ps['queryset']
    queryset_bu = result_dict_bu['queryset']
    assert queryset_ps == queryset_bu

if __name__ == "__main__":
    test_discovery_bu_multidim()
