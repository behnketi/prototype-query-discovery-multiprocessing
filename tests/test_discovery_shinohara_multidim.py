#!/usr/bin/python3
"""Contains tests for functions in discovery_shinohara.py"""
import re
import sys
sys.path.append(".")

import logging
from sample_multidim import MultidimSample
from query_multidim import MultidimQuery
from discovery_shinohara_multidim import shinohara_discovery_icdt, _find_unvisited_variables

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_shinohara_discovery_icdt_multidim_for_multidimensional_queries():
    """
        Test for function shinohara_discovery_icdt for n-dimensional queries.
    """
    LOGGER.info('Started test test_shinohara_discovery_icdt')
    sample0 = MultidimSample(given_sample=[
        "aa;bb;aa; cc;cc;cc; aa;bb;aa;", "aa;bb;aa; ee;ee;ee; aa;bb;aa;",
        "aa;bb;aa; dd;dd;dd; KK;LL;MM; aa;bb;aa;", "aa;bb;aa; NN;OO;PP; ee;ee;ee; aa;bb;aa;"])
    supp = 1.0

    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with different configurations of select_position and select_operation')
    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query01.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query01.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=True,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='left_to_right',
                    select_operation='type')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           left_to_right')
    LOGGER.info('select_operation:          type')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=True,
                    select_position='left_to_right',
                    select_operation='type')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           left_to_right')
    LOGGER.info('select_operation:          type')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='right_to_left',
                    select_operation='var')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B;$C; $D;$E;$F; $G;$H;$I;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=True,
                    select_position='right_to_left',
                    select_operation='var')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with descriptive query')
    query02 = MultidimQuery(given_query_string="aa;bb;aa; $D;$D;$D; aa;bb;aa;")
    query02.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query02.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query02, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s\$[D];\$[D];\$[D];\saa;bb;aa;", query02._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query02 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query02 = MultidimQuery(given_query_string="aa;bb;aa; $D;$D;$D; aa;bb;aa;")
    query02.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query02.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query02, sample0, supp,
                    select_type_attwise=True,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s\$[D];\$[D];\$[D];\saa;bb;aa;", query02._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query02 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query containing types')
    query03 = MultidimQuery(given_query_string="aa;bb;aa; $D;$E;$F; $G;$H;$I;")
    query03.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query03.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query03, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query03._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query03 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query03 = MultidimQuery(given_query_string="aa;bb;aa; $D;$E;$F; $G;$H;$I;")
    query03.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query03.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query03, sample0, supp,
                    select_type_attwise=True,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s((\$[D];){3}|(\$[E];){3}|(\$[F];){3})\saa;bb;aa;", query03._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query03 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with repeated and non-repeated variables')
    query04 = MultidimQuery(given_query_string="$A;$B;$C; $D;$D;$D; $G;$H;$I;")
    query04.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query04.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query04, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s(\$[D];){3}\saa;bb;aa;", query04._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query04 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query04 = MultidimQuery(given_query_string="$A;$B;$C; $D;$D;$D; $G;$H;$I;")
    query04.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query04.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query04, sample0, supp,
                    select_type_attwise=True,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;aa;\s(\$[D];){3}\saa;bb;aa;", query04._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query04 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query which does not satisfy the support threshold')
    query05 = MultidimQuery(given_query_string="aa;bb;aa; aa;bb;aa; $A;$B;$C;")
    query05.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query05.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query05, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert result[0] is None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       False')
    LOGGER.info('Mined query05 string:      None')
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query05 = MultidimQuery(given_query_string="aa;bb;aa; aa;bb;aa; $A;$B;$C;")
    query05.set_query_windowsize_local([(-1,-1),(0,0),(0,0),(0,3),(0,0),(0,0),(0,3),(0,0),(0,0),(-1,-1)])
    query05.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query05, sample0, supp,
                    select_type_attwise=True,
                    select_position='random',
                    select_operation='random')
    assert result[0] is None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('select_type_attwise:       True')
    LOGGER.info('Mined query05 string:      None')
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    LOGGER.info('Finished test test_shinohara_discovery_icdt')

def test_shinohara_discovery_icdt_multidim_for_twodimensional_queries():
    """
        Test for function shinohara_discovery_icdt for n-dimensional queries.
    """
    LOGGER.info('Started test test_shinohara_discovery_icdt')
    sample0 = MultidimSample(given_sample=["aa;bb; cc;cc; aa;bb;", "aa;bb; ee;ee; aa;bb;", "aa;bb; dd;dd; KK;LL; aa;bb;", "aa;bb; NN;OO; ee;ee; aa;bb;"])
    supp = 1.0

    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with different configurations of select_position and select_operation')
    query01 = MultidimQuery(given_query_string="$A;$B; $C;$D; $E;$F;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query01.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;\s((\$[C];){2}|(\$[D];){2})\saa;bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B; $C;$D; $E;$F;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='left_to_right',
                    select_operation='type')
    assert re.match(r"aa;bb;\s((\$[C];){2}|(\$[D];){2})\saa;bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           left_to_right')
    LOGGER.info('select_operation:          type')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A;$B; $C;$D; $E;$F;")
    query01.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='right_to_left',
                    select_operation='var')
    assert re.match(r"aa;bb;\s((\$[C];){2}|(\$[D];){2})\saa;bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with descriptive query')
    query02 = MultidimQuery(given_query_string="aa;bb; $C;$C; aa;bb;")
    query02.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query02.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query02, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;\s\$[C];\$[C];\saa;bb;", query02._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('Mined query02 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query containing types')
    query03 = MultidimQuery(given_query_string="aa;bb; $C;$D; $E;$F;")
    query03.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query03.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query03, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;\s((\$[C];){2}|(\$[D];){2})\saa;bb;", query03._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query03 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with repeated and non-repeated variables')
    query04 = MultidimQuery(given_query_string="$A;$B; $C;$C; $E;$F;")
    query04.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query04.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query04, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;bb;\s\$[C];\$[C];\saa;bb;", query04._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query04 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query which does not satisfy the support threshold')
    query05 = MultidimQuery(given_query_string="aa;bb; aa;bb; $A;$B;")
    query05.set_query_windowsize_local([(-1,-1),(0,0),(0,2),(0,0),(0,2),(0,0),(-1,-1)])
    query05.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query05, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert result[0] is None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query05 string:      None')
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    LOGGER.info('Finished test test_shinohara_discovery_icdt')

def test_shinohara_discovery_icdt_multidim_for_onedimensional_queries():
    """
        Test for function shinohara_discovery_icdt for 1-dimensional queries.
    """
    LOGGER.info('Started test test_shinohara_discovery_icdt')
    sample0 = MultidimSample(given_sample=["aa; cc; cc; bb;", "aa; ee; ee; bb;", "aa; dd; KK; dd; bb;", "aa; NN; ee; ee; bb;"])
    supp = 1.0

    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with different configurations of select_position and select_operation')
    query01 = MultidimQuery(given_query_string="$A; $B; $C; $D;")
    query01.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query01.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;\s((\$[B];\s){2}|(\$[C];\s){2})bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A; $B; $C; $D;")
    query01.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='left_to_right',
                    select_operation='type')
    assert re.match(r"aa;\s((\$[B];\s){2}|(\$[C];\s){2})bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           left_to_right')
    LOGGER.info('select_operation:          type')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query01 = MultidimQuery(given_query_string="$A; $B; $C; $D;")
    query01.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_type_attwise=False,
                    select_position='right_to_left',
                    select_operation='var')
    assert re.match(r"aa;\s((\$[B];\s){2}|(\$[C];\s){2})bb;", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with descriptive query')
    query02 = MultidimQuery(given_query_string="aa; $C; $C; bb;")
    query02.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query02.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query02, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;\s\$[C];\s\$[C];\sbb;", query02._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('Mined query02 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query containing types')
    query03 = MultidimQuery(given_query_string="aa; $B; $C; $D;")
    query03.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query03.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query03, sample0, supp,
                    select_type_attwise=False,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;\s((\$[B];\s){2}|(\$[C];\s){2})bb;", query03._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query03 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with repeated and non-repeated variables')
    query04 = MultidimQuery(given_query_string="$A; $B; $B; $D;")
    query04.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query04.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query04, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa;\s\$[B];\s\$[B];\sbb;", query04._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query06 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query which does not satisfy the support threshold')
    query05 = MultidimQuery(given_query_string="aa; $B; bb; $D;")
    query05.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query05.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query05, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert result[0] is None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query07 string:      None')
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    LOGGER.info('Finished test test_shinohara_discovery_icdt')

def test_find_unvisited_variables():
    """
        Test for function _find_unvisited_variables()
    """
    LOGGER.info('Started test test_find_unvisited_variables')
    query0 = MultidimQuery(given_query_string="a; $x; $y; a;")
    assert _find_unvisited_variables(query0) == {'x;': [2], 'y;': [3]}
    query1 = MultidimQuery(given_query_string="a; $x; $y;")
    assert _find_unvisited_variables(query1) == {'x;': [2], 'y;': [3]}
    query2 = MultidimQuery(given_query_string="$x; $y; $x;")
    assert _find_unvisited_variables(query2) == {'x;': [1,3], 'y;': [2]}
    query3 = MultidimQuery(given_query_string="$x; $y; $y;")
    assert _find_unvisited_variables(query3) == {'x;': [1], 'y;': [2,3]}
    query4 = MultidimQuery(given_query_string="$x; $y; $z;")
    assert _find_unvisited_variables(query4) == {'x;': [1], 'y;': [2], 'z;': [3]}

    query5 = MultidimQuery(given_query_string="a;$x;b; c;$y;d; e;$z;f;")
    assert _find_unvisited_variables(query5) == {'x;': [2], 'y;': [5], 'z;': [8]}
    query6 = MultidimQuery(given_query_string="a;$x;b; c;$y;d; e;$x;f;")
    assert _find_unvisited_variables(query6) == {'x;': [2,8], 'y;': [5]}
    query7 = MultidimQuery(given_query_string="$x;a;b; c;$y;d; e;f;$x;")
    assert _find_unvisited_variables(query7) == {'x;': [1,9], 'y;': [5]}
    LOGGER.info('Finished test test_find_unvisited_variables')
