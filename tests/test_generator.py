#!/usr/bin/python3
"""Contains tests for class SampleGenerator"""
import sys
sys.path.append(".")

import logging
from generator import SampleGenerator

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_generate_random_sample():
    """
        Test for function generate_random_sample() with default values.
    """
    LOGGER.info('Started test test_generate_random_sample')
    sample_gen = SampleGenerator()
    sample = sample_gen.generate_random_sample()
    for elem in sample._sample:
        LOGGER.info(elem)
    LOGGER.info('Finished test test_generate_random_sample')

def test_generate_fragmentation_sample():
    """
        Test for function generate_fragmentation_*_sample.
    """
    LOGGER.info('Started test test_generate_fragmentation_sample')
    sample_gen1 = SampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=-1, type_count=-1)
    for trace in sample_gen1._sample._sample:
        LOGGER.info(trace)
        sample_gen2 = SampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=-1, type_count=-1)
    for trace in sample_gen2._sample._sample:
        LOGGER.info(trace)
    LOGGER.info('Finished test test_generate_fragmentation_sample')

def test_build_typeset():
    """
        Test for function build_typeset.
    """
    LOGGER.info('Started test test_build_typeset')
    type_length = 2
    upper_bound = 52**type_length
    sample_gen = SampleGenerator()
    typeset = sample_gen.build_typeset(upper_bound, type_length)
    LOGGER.info(typeset)
    LOGGER.info('Finished test test_build_typeset')

def test_build_fragmentation_of_1_gauss():
    """
        Test for function _build_fragmentation_of_1_gauss.
    """
    LOGGER.info('Started test test_build_fragmentation_of_1_gauss')
    sample_gen = SampleGenerator()

    counter = 2
    upper_bound = 200
    typeset = sample_gen.build_typeset(counter, upper_bound)

    distribution = sample_gen._build_fragmentation_of_1_gauss(typeset)

    for elem in distribution:
        LOGGER.info(elem)

    LOGGER.info('Finished test test_build_fragmentation_of_1_gauss')

def test_build_fragmentation_of_1_quartered():
    """
        Test for function _build_fragmentation_of_1_quartered.
    """
    LOGGER.info('Started test test_build_fragmentation_of_1_quartered')
    sample_gen = SampleGenerator()

    typeset = set()
    for i in range(0,250):
        typeset.add(i)

    distribution = sample_gen._build_fragmentation_of_1_quartered(typeset)

    for elem in distribution:
        LOGGER.info(elem)

    LOGGER.info('Finished test test_build_fragmentation_of_1_quartered')

def test_generate_sample_w_empty_queryset():
    """
        Test for function generate_sample_w_empty_queryset.
    """
    LOGGER.info('Started test test_generate_sample_w_empty_queryset')
    sample_size = 2
    min_trace_length = 100

    generator = SampleGenerator()
    generator.generate_sample_w_empty_queryset(sample_size=sample_size, min_trace_length=min_trace_length, max_trace_length=min_trace_length)

    sample = generator._sample
    vsdb = sample.get_vertical_sequence_database()
    alphabet = sample.get_supported_typeset(1.0)
    assert not alphabet
    patternset = set([key for key in vsdb.keys() for item in vsdb[key].keys() if len(vsdb[key][item]) >= 2])
    assert not patternset

    LOGGER.info('Finished test test_generate_sample_w_empty_queryset')
