#!/usr/bin/python3
"""Contains tests for functions in discovery.py"""
import sys
sys.path.append(".")

import logging
import time
from math import ceil
from sample import Sample
from query import Query
from error import EmptySampleError, InvalidQuerySupportError
from discovery_bu_pattern_type_split import _build_type_tree, _build_pattern_tree,\
        _build_mixed_query_tree, bu_pattern_type_split_discovery, _search_var_smart
from hyper_linked_tree import HyperLinkedTree

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


#####
#
# Tests for function _build_type_tree.
#
def test00__build_type_tree():
    """
        Testing '_build_type_tree': check for detecting "no frequent item exists"
    """
    LOGGER.info('Started test 00 for _build_type_tree')

    sample = Sample(["a", "b"])
    supp = 1.0
    stats = []

    # 1. ""
    # 2. -

    sample_result = {''}

    assert sample_result == _build_type_tree(stats, sample, supp)

    LOGGER.info('Finished test 00 for _build_type_tree')

def test01__build_type_tree():
    """
        Testing '_build_type_tree': small running example
    """
    LOGGER.info('Started test 01 for _build_type_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    supp = 1.0
    stats = []

    # 1. "", aa, bb
    # 2. aa bb
    # 3. -

    sample_result = {'','aa', 'bb', 'aa bb'}

    assert sample_result == _build_type_tree(stats, sample, supp)

    LOGGER.info('Finished test 01 for _build_type_tree')

def test02__build_type_tree():
    """
        Testing '_build_type_tree': small running example
    """
    LOGGER.info('Started test 02 for _build_type_tree')

    sample = Sample(["aa bb cc aa", "aa aa bb bb bb aa", "aa dd KK dd bb cc aa", "aa bb aa"])
    supp = 1.0
    stats = []

    # 1. '', aa, bb
    # 2. aa aa, aa bb, bb aa
    # 3. aa bb aa
    # 4. -

    sample_result = {'','aa', 'bb', 'aa aa', 'aa bb', 'bb aa', 'aa bb aa'}

    assert sample_result == _build_type_tree(stats, sample, supp)

    LOGGER.info('Finished test 02 for _build_type_tree')

def test03__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 0.5
    """
    LOGGER.info('Started test 03 for _build_type_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd cc dd bb", "aa NN ee ee bb"])
    supp = 0.5
    stats = []

    # 1. '', aa, bb, cc, ee
    # 2. aa bb, aa cc, aa ee, cc, bb, ee bb, ee ee
    # 3. aa cc bb, aa ee bb, aa ee ee, ee ee bb
    # 4. aa ee ee bb
    # 5. -

    sample_result = {'', 'aa', 'bb', 'cc', 'ee', 'aa bb', 'aa cc', 'aa ee', 'cc bb', 'ee bb', 'ee ee', 'aa cc bb', 'aa ee bb', 'aa ee ee', 'ee ee bb', 'aa ee ee bb'}

    assert sample_result == _build_type_tree(stats, sample, supp)

    LOGGER.info('Finished test 03 for _build_type_tree')

def test04__build_type_tree():
    """
        Testing '_build_type_tree': check for detecting "no frequent item exists" using a hyperlinked tree
    """
    LOGGER.info('Started test 04 for _build_type_tree')

    sample = Sample(["a", "b"])
    supp = 1.0
    stats = []

    # 1. ""
    # 2. -

    sample_result = {''}

    assert sample_result == _build_type_tree(stats, sample, supp, use_tree_structure=True).query_strings_to_set()

    LOGGER.info('Finished test 04 for _build_type_tree')

def test05__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 0.5 using a hyperlinked tree
    """
    LOGGER.info('Started test 05 for _build_type_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd cc dd bb", "aa NN ee ee bb"])
    supp = 0.5
    stats = []

    # 1. '', aa, bb, cc, ee
    # 2. aa bb, aa cc, aa ee, cc, bb, ee bb, ee ee
    # 3. aa cc bb, aa ee bb, aa ee ee, ee ee bb
    # 4. aa ee ee bb
    # 5. -

    sample_result = {'', 'aa', 'bb', 'cc', 'ee', 'aa bb', 'aa cc', 'aa ee', 'cc bb', 'ee bb', 'ee ee', 'aa cc bb', 'aa ee bb', 'aa ee ee', 'ee ee bb', 'aa ee ee bb'}

    assert sample_result == _build_type_tree(stats, sample, supp, use_tree_structure=True).query_strings_to_set()

    LOGGER.info('Finished test 05 for _build_type_tree')

def test06__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 1.0 using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 06 for _build_type_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd cc dd bb", "aa NN ee ee bb"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, [], {})

    # 1. '', aa, bb
    # 2. aa bb
    # 3. -

    sample_result = {'', 'aa', 'bb', 'aa bb'}

    hlt = _build_type_tree(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching)
    LOGGER.debug(hlt)
    assert sample_result == _build_type_tree(stats, sample, supp, use_tree_structure=True).query_strings_to_set()

    LOGGER.info('Finished test 06 for _build_type_tree')

def test07__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 1.0 using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 07 for _build_type_tree')

    sample = Sample(["a a a", "b a b"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, [], {})

    # 1. '', a
    # 2. -

    sample_result = {'', 'a'}

    assert sample_result == _build_type_tree(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 07 for _build_type_tree')

#####
#
# Tests for function _build_pattern_tree.
#
def test00__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example
    """
    LOGGER.info('Started test 00 for _build_pattern_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    supp = 1.0
    stats = []

    # 1. "", "$x0 $x0"
    # 2. -

    sample_result = ["", "$x0 $x0"]

    assert sample_result == sorted(_build_pattern_tree(stats, sample, supp))

    LOGGER.info('Finished test 00 for _build_pattern_tree')

def test01__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example
    """
    LOGGER.info('Started test 01 for _build_pattern_tree')

    sample = Sample(["aa cc cc bb ee ee", "aa ee ee bb ee ee", "aa dd KK dd bb ee ee", "aa NN ee ee bb ee ee"])
    supp = 1.0
    stats = []

    # 1. "", "$x0 $x0"
    # 2. "$x0 $x0 $x1 $x1"
    # 3. -

    sample_result = ["", "$x0 $x0", "$x0 $x0 $x1 $x1"]

    assert sample_result == sorted(_build_pattern_tree(stats, sample, supp))

    LOGGER.info('Finished test 01 for _build_pattern_tree')

def test02__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 02 for _build_pattern_tree')

    sample = Sample(["aa cc cc bb ee ee", "aa ee ee bb ee ee", "aa dd KK dd bb ee ee", "aa NN ee ee bb ee ee"])
    supp = 1.0
    stats = []

    # 1. "", "$x0 $x0"
    # 2. "$x0 $x0 $x1 $x1"
    # 3. -

    sample_result = ["", "$x0 $x0", "$x0 $x0 $x1 $x1"]

    hlt = _build_pattern_tree(stats, sample, supp, use_tree_structure=True)
    normalized_return_set = set()

    for string in hlt.query_strings_to_list():
        new_query = Query()
        new_query.set_query_string(string)
        new_query.query_string_to_normalform()

        normalized_return_set.add(new_query._query_string)

    assert sample_result == sorted(normalized_return_set)

    LOGGER.info('Finished test 02 for _build_pattern_tree')

def test03__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 03 for _build_pattern_tree')

    sample = Sample(["a a", "b b"])
    supp = 1.0
    stats = []

    # 1. "", "$x0 $x0"
    # 2. -

    sample_result = ["", "$x0 $x0"]

    hlt = HyperLinkedTree(2)
    _search_var_smart(None, sample, supp, hlt)

    result = hlt.query_strings_to_list()

    assert sorted(sample_result) == sorted(result)

    LOGGER.info('Finished test 03 for _build_pattern_tree')

def test04__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 04 for _build_pattern_tree')

    sample = Sample(["aa cc cc bb ee ee", "aa ee ee bb ee ee", "aa dd KK dd bb ee ee", "aa NN ee ee bb ee ee"])
    supp = 1.0
    stats = []

    # 1. "", "$x0 $x0"
    # 2. "$x0 $x0 $x1 $x1"
    # 3. -

    sample_result = ["", "$x0 $x0", "$x0 $x0 $x1 $x1"]

    hlt = HyperLinkedTree(4)
    _search_var_smart(None, sample, supp, hlt)

    result = hlt.query_strings_to_list()

    assert sorted(sample_result) == sorted(result)

    LOGGER.info('Finished test 04 for _build_pattern_tree')

def test05__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 05 for _build_pattern_tree')

    sample = Sample(["aa cc cc bb ee ee", "aa ee ee bb ee ee", "aa dd KK dd bb ee ee", "aa NN ee ee bb ee ee"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, ['cc','ee','dd'], {})

    # 1. "", "$x0 $x0"
    # 2. "$x0 $x0 $x1 $x1"
    # 3. -

    sample_result = ["", "$x0 $x0", "$x0 $x0 $x1 $x1"]

    hlt = _build_pattern_tree(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching)

    LOGGER.debug("\n%s",str(hlt))

    result = hlt.query_strings_to_list()

    assert sorted(sample_result) == sorted(result)

    LOGGER.info('Finished test 05 for _build_pattern_tree')

def test06__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 06 for _build_pattern_tree')

    sample = Sample(['e m k e p e o e q q', 'a d g a b a j a s r'])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, ['e','q','a'], {})

    # 1. "", "$x0 $x0"
    # 2. "$x0 $x0 $x1 $x1"
    # 3. -

    sample_result = ["", "$x0 $x0", "$x0 $x0 $x0", "$x0 $x0 $x1 $x1", "$x0 $x1 $x0 $x1", "$x0 $x1 $x1 $x0", "$x0 $x0 $x0 $x0"]

    hlt = _build_pattern_tree(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching)

    LOGGER.debug("\n%s",str(hlt))

    result = hlt.query_strings_to_list()

    assert sorted(sample_result) == sorted(result)

    LOGGER.info('Finished test 06 for _build_pattern_tree')

#####
#
# Tests for function _build_mixed_query_tree.
#

def test00__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 00 for _build_mixed_query_tree')

    sample = Sample(["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    supp = 1.0
    stats = []

    # 1. aa, bb
    # 2. aa bb, $x0 $x0
    # 3. aa $x0 $x0, $x0 $x0 bb
    # 4. aa $x0 $x0 bb
    # 5. -

    LOGGER.info(' > Started  _build_type_tree')
    type_tree = _build_type_tree(stats, sample, supp)
    LOGGER.info(' > Finished _build_type_tree')
    LOGGER.info(' > Started  _build_pattern_tree')
    pattern_tree = _build_pattern_tree(stats, sample, supp)
    LOGGER.info(' > Finished _build_pattern_tree')

    sample_result = ["", "aa", "bb", "aa bb", "$x0 $x0", "$x0 $x0 bb" ,"aa $x0 $x0", "aa $x0 $x0 bb"]
    LOGGER.info(' > Started  _build_mixed_query_tree')
    assert sorted(sample_result, key=lambda item: (item.count(' '),item)) ==\
            sorted(_build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree), key=lambda item: (item.count(' '),item))
    LOGGER.info(' > Finished _build_mixed_query_tree')

    LOGGER.info('Finished test 00 for _build_mixed_query_tree')

def test01__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 01 for _build_mixed_query_tree')

    sample = Sample(["a"])
    supp = 1.0
    stats = []

    sample_result = ["", "a"]
    # 1. "", a
    # 2. -

    type_tree = _build_type_tree(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree(stats, sample, supp, use_tree_structure = True)
    mixed_query_tree = _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree)

    assert sorted(sample_result, key=lambda item: (item.count(' '),item)) ==\
            sorted(mixed_query_tree.query_strings_to_list(), key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 01 for _build_mixed_query_tree')

def test02__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 02 for _build_mixed_query_tree')

    sample = Sample(["a a", "b b"])
    supp = 1.0
    stats = []

    sample_result = ["", "$x0 $x0"]
    # 1. "", x0 x0
    # 2. -

    type_tree = _build_type_tree(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree(stats, sample, supp, use_tree_structure = True)
    mixed_query_tree = _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree)

    assert sorted(sample_result, key=lambda item: (item.count(' '),item)) ==\
            sorted(mixed_query_tree.query_strings_to_list(), key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 02 for _build_mixed_query_tree')

def test03__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 03 for _build_mixed_query_tree')

    sample = Sample(["a a a", "b a b"])
    supp = 1.0
    stats = []

    sample_result = ["", "a", "$x0 $x0", "$x0 a $x0"]
    # 1. "", a, x0 x0
    # 2. x0 a x0
    # 3. -

    type_tree = _build_type_tree(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree(stats, sample, supp, use_tree_structure = True)
    mixed_tree = _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree)

    result = set()
    for vertex in mixed_tree.vertices_to_list():
        if vertex.is_frequent(ceil(supp*sample._sample_size)):
            result.add(vertex.query_string)

    LOGGER.info("Result:")
    LOGGER.info(result)
    LOGGER.info(sorted(result, key=lambda item: (item.count(' '),item)))
    LOGGER.info(sorted(sample_result, key=lambda item: (item.count(' '),item)))
    assert sorted(sample_result, key=lambda item: (item.count(' '),item)) ==\
            sorted(result, key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 03 for _build_mixed_query_tree')

def test04__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example using a smarter matching approach
    """
    LOGGER.info('Started test 04 for _build_mixed_query_tree')

    sample = Sample(["a a a", "b a b"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, [], {})

    sample_result = ["", "a", "$x0 $x0", "$x0 a $x0"]
    # 1. "", a, x0 x0
    # 2. x0 a x0
    # 3. -

    type_tree = _build_type_tree(stats, sample, supp, use_tree_structure = True, param_smart_matching=param_smart_matching)
    pattern_tree = _build_pattern_tree(stats, sample, supp, use_tree_structure = True, param_smart_matching=param_smart_matching)
    mixed_tree = _build_mixed_query_tree(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    result = set()
    for vertex in mixed_tree.vertices_to_list():
        if vertex.is_frequent(ceil(supp*sample._sample_size)):
            result.add(vertex.query_string)

    assert sorted(sample_result, key=lambda item: (item.count(' '),item)) ==\
            sorted(result, key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 04 for _build_mixed_query_tree')

#####
#
# Tests for function pattern_type_split_discovery.
#
def test00_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting empty samples
    """
    LOGGER.info('Started test 00 for pattern_type_split_discovery')

    sample = None
    supp = 1.0

    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! Missing sample not detected!")
    except EmptySampleError:
        pass
    except Exception as err:
        raise err

    sample = Sample()
    sample.set_sample([])

    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! Empty sample not detected!")
    except EmptySampleError:
        pass
    except Exception as err:
        raise err

    sample.set_sample(["a"])
    sample._sample_size = 0
    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! _sample_size is '0', but not detected!")
    except EmptySampleError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for pattern_type_split_discovery')

def test01_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting invalid query support
    """
    LOGGER.info('Started test 01 for pattern_type_split_discovery')

    sample = Sample()
    sample.set_sample(['a'])
    sample.set_sample_size()

    supp = -0.5

    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err

    supp = 1.1

    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 01 for pattern_type_split_discovery')

def test02_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting invalid optional discovery_order
    """
    LOGGER.info('Started test 02 for pattern_type_split_discovery')

    sample = Sample()
    sample.set_sample(['a'])
    sample.set_sample_size()

    supp = 1.0

    var_discovery_order = None

    try:
        bu_pattern_type_split_discovery(sample, supp, discovery_order=var_discovery_order)
        raise AssertionError ("Test failed: No error raised!")
    except NotImplementedError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 02 for pattern_type_split_discovery')

def test03_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 03 for pattern_type_split_discovery')

    sample = Sample()
    sample.set_sample(["b a b", "aB b C aB C", "", "a b"])
    sample.set_sample_size()

    supp = 1.0

    # 1. ""
    # 2. -

    sample_result = [""]

    assert sample_result == sorted(list(bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')[0]), key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 03 for pattern_type_split_discovery')

def test04_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example with supp 0.5
    """
    LOGGER.info('Started test 04 for pattern_type_split_discovery')

    sample = Sample()
    sample.set_sample(["b a b", "aB b C aB C", "", "a b a"])
    sample.set_sample_size()

    supp = 0.5

    # 1. "", "b", "a"
    # 2. "a b", "b a", "$x0 $x0"
    # 3. "$x0 b $x0"
    # 4. -

    sample_result = ["", "a", "b", "$x0 $x0", "a b", "b a", "$x0 b $x0"]

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))\
            == sorted(list(bu_pattern_type_split_discovery(sample, supp, discovery_order='type_first')[0]), key=lambda item: (item.count(' '),item))

    LOGGER.info('Finished test 04 for pattern_type_split_discovery')

def test05_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 05 for pattern_type_split_discovery')

    sample = Sample(["a a a", "b a b"])
    supp = 1.0
    stats = []

    sample_result = ["$x0 a $x0"]
    # 1. "", a, x0 x0
    # 2. x0 a x0
    # 3. -

    assert sample_result == bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0]

    LOGGER.info('Finished test 05 for pattern_type_split_discovery')

def test06_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 06 for pattern_type_split_discovery')

    sample = Sample(["b a a c d d b a b b", "b a a b c d d a c c b b", "a a b c b a c d b a", "c a a b d b c c d d b a", "b a c d d b b a c d d b"])
    supp = 1.0
    stats = []

    sample_result =['$x0 a $x1 $x2 $x2 $x1 $x0', '$x0 a $x1 $x2 $x1 $x2 $x0', '$x0 $x1 $x1 c d $x1 $x0', '$x0 $x1 $x2 $x1 $x0 $x2', '$x0 $x1 $x2 $x2 $x0 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2', '$x0 a $x1 c d $x1 $x0', '$x0 a $x1 $x1 $x0 $x0', '$x0 $x0 c $x1 $x1 $x0', 'a $x0 c $x1 $x1 $x0', 'a $x0 $x1 $x1 b $x0',\
            'a $x0 $x1 $x1 $x0 b', 'a $x0 $x1 $x0 $x1 b', '$x0 a c $x1 $x1 $x0', '$x0 a $x1 b $x1 $x0', '$x0 a $x1 $x1 b $x0', '$x0 a $x1 $x1 $x0 b', '$x0 a $x1 $x0 $x1 b',\
            '$x0 $x1 $x1 $x0 $x1', '$x0 $x1 c d $x0 $x1', '$x0 c $x0 $x1 $x1', '$x0 $x1 a $x0 $x1', '$x0 $x1 $x0 b $x1', '$x0 $x0 $x1 $x1 a', '$x0 a c d b $x0', '$x0 a b $x0 $x0',\
            'b c $x0 $x0 a', 'b $x0 b $x0 b', 'b $x0 c d $x0', 'b $x0 $x0 b a', 'b $x0 $x0 d b', 'a a c $x0 $x0', 'a a $x0 $x0 b', 'a c $x0 $x0 a', 'a $x0 b $x0 b',\
            'a $x0 d b $x0', 'a $x0 c $x0 b', '$x0 a b b $x0', '$x0 a b $x0 b', '$x0 a a d $x0', '$x0 a a c $x0', '$x0 a c $x0 b', '$x0 $x0 b b b', '$x0 c $x0 $x0',\
            '$x0 $x0 b $x0', '$x0 $x0 $x0 b', 'b b $x0 $x0', '$x0 a $x0 a', 'a a c d b', 'b c d b', 'b c d a', 'a b b b', 'a c d a', 'a b a', 'c b b', 'c a b']
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    set_b = set(result) - set(sample_result)
    set_a = set(sample_result) - set(result)
    LOGGER.info("%s/%s:%s",str(len(set_a)),str(len(result)),       str(sorted(set_a, key=lambda item: (item.count(' '),item))))
    LOGGER.info("%s/%s:%s",str(len(set_b)),str(len(sample_result)),str(sorted(set_b, key=lambda item: (item.count(' '),item))))

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
    LOGGER.info('Finished test 06 for pattern_type_split_discovery')

def test07_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 07 for pattern_type_split_discovery')

    sample = Sample(["01 12 12 01 12 01 01", "01 12 12 01 01 01", "12 12 01 01 12"])
    supp = 1.0
    stats = []

    sample_result = sorted(['$x0 $x0 01 $x0', '$x0 12 01 01 $x0', '12 $x0 01 $x0', '12 12 01 01', '01 12'], key=lambda item: (item.count(' '),item))
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    assert sample_result == result
    LOGGER.info('Finished test 07 for pattern_type_split_discovery')

def test08_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 08 for pattern_type_split_discovery')

    sample = Sample([\
            '6252279477 6252279477 6252279477 6252279477 6252279477 6252280737 515042969 515042969 6218406404 6251699911',\
            '6252225776 6218406404 6252244935 515042969 6252286168 515042969 515042969 515042969 6218406404 6238340468',\
            '515042969 6251995937 515042969 515042969 6252293124 6252263419 6252293122 515042969 515042969 6218406404',\
            '6221861800 6238340468 6251995937 6221861800 6238340468 6251995937 6218406404 6252336252 6238340468 6218406404',\
            '5715747784 6221861800 6238340468 6251995937 6221861800 6238340468 6251995937 6218406404 6252336252 6238340468'])
    supp = 1.0
    stats = []

    sample_result = ['$x0 $x1 $x0 $x1 6218406404', '$x0 $x1 $x1 $x0', '$x0 $x0 $x1 $x1', '$x0 $x0 $x0']
    sample_result = sorted(sample_result, key=lambda item: (item.count(' '),item))
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    assert sample_result == result
    LOGGER.info('Finished test 08 for pattern_type_split_discovery')

def test09_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 09 for pattern_type_split_discovery')

    sample = Sample(['5 5 5 5 5 5 1 1 2 2', '1 1 4 1 4 1 1 1 2 2', '1 1 1 1 1 1 1 1 1 2', '1 1 1 2 2 2 2 1 2 2', '1 1 1 1 2 2 2 2 1 2'])
    supp = 1.0
    stats = []

    sample_result = ['$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x1 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 1 2', '$x0 $x0 $x0 $x1 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x1', '$x0 $x0 $x0 $x0 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x0 $x1', '$x0 $x0 $x0 $x1 $x1 1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x1', '$x0 $x0 $x1 $x1 $x1 1 2',\
            '$x0 $x0 $x0 1 $x1 $x1', '$x0 $x0 $x0 $x0 $x0 2',\
            '$x0 $x0 1 1 $x1 $x1', '$x0 $x0 $x0 $x0 1 2', '$x0 $x0 1 1 2']

    sample_result = sorted(sample_result, key=lambda item: (item.count(' '),item))
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    assert sample_result == result
    LOGGER.info('Finished test 09 for pattern_type_split_discovery')

def test10_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 10 for pattern_type_split_discovery')

    sample = Sample(['2 2 2 2 2 0 0 0 0 0', '9 0 8 0 2 0 0 0 0 0', '0 0 0 0 8 2 2 0 0 0', '0 0 0 0 0 0 0 2 0 0', '0 0 0 0 0 0 0 0 2 0'] )
    supp = 1.0
    stats = []

    sample_result = ['$x0 $x0 2 $x0', '$x0 $x0 2 0', '$x0 $x0 $x0 $x0 $x0 0 0', '$x0 $x0 $x0 $x0 0 0 0', '$x0 $x0 $x0 0 0 0 0', '$x0 $x0 0 0 0 0 0']
    sample_result = sorted(sample_result, key=lambda item: (item.count(' '),item))
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    assert sample_result == result
    LOGGER.info('Finished test 10 for pattern_type_split_discovery')

def test11_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 11 for pattern_type_split_discovery')

    sample = Sample(['2 2 2 1 1 1 1 1 1 3', '4 2 2 2 1 1 1 1 2 2', '1 1 1 1 1 3 4 2 2 2', '1 3 1 2 2 2 2 1 2 2'])
    supp = 1.0
    stats = []

    sample_result = ['$x0 1 $x1 $x1 $x0', '$x0 $x0 2 $x1 $x1', '$x0 $x0 $x0 $x1 $x1 $x1', '1 1 1 $x0 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 1 $x0 $x0', '2 2 2',\
            '$x0 $x0 $x0 1 $x1 $x1', '1 $x0 $x0 1 $x1 $x1', '$x0 $x0 $x1 $x1 1 $x2 $x2', '1 1 $x0 $x0 $x1 $x1', '1 $x0 $x0 $x0 $x1 $x1', '$x0 $x0 $x0 1 $x0', '1 $x0 1 $x0',\
            '$x0 $x0 $x0 $x0 $x1 $x1', '$x0 $x0 $x0 $x0 $x0']
    sample_result = sorted(sample_result, key=lambda item: (item.count(' '),item))
    result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    assert sample_result == result
    LOGGER.info('Finished test 11 for pattern_type_split_discovery')

def test12_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
        Since the matching finds 'a.O' in 'a.O2' and 'a.O3', this example actually does not work right now.
    """
    LOGGER.info('Started test 12 for pattern_type_split_discovery')

    # test deactivated since regex '$x0 $x0' matches '01 02'

    #sample = Sample([\
    #        '05 05 05 05 05 05 05 05 05 05 05 05 05 05 05 05 a,O0 a,O1 02 02',\
    #        'a,O2 a,O3 a,O4 03 a,O5 a,O6 a,O7 a,O8 03 04 a,O9 a,10 04 a,11 04 a,12 a,13 a,14 02 02',\
    #        '04 03 a,15 03 a,16 a,17 a,18 03 04 a,19 a,20 a,21 a,22 a,23 a,24 a,25 a,26 a,27 a,28 02',\
    #        '02 a,29 02 02 02 02 02 a,30 a,31 a,32 a,33 a,34 a,35 02 02 02 02 a,36 02 02',\
    #        '02 02 a,37 02 02 02 02 02 a,38 a,39 a,40 a,41 a,42 a,43 02 02 02 02 a,44 02',\
    #        'a,45 a,46 a,47 04 02 02 02 a,48 a,49 a,50 a,51 a,52 02 02 02 a,53 04 02 a,54 02',\
    #        '03 04 a,55 a,56 5 a,57 a,58 a,59 a,60 04 a,61 a,62 04 04 a,63 a,64 02 02 02 02',\
    #        '02 02 a,65 a,66 a,67 a,68 a,69 a,70 03 04 02 02 02 a,71 a,72 a,73 a,74 02 02 02',\
    #        '02 02 02 a,75 a,76 a,77 a,78 a,79 a,80 03 04 02 02 02 a,81 a,82 a,83 a,84 02 02',\
    #        'a,85 a,86 a,87 a,88 a,89 03 04 02 02 02 a,90 03 a,91 02 02 02 02 a,92 02 02'])
    #            #'5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 2 2',\
    #            #'3 3 4 4 4 2 2',\
    #            #'4 3 3 3 4 2',\
    #            #'2 2 2 2 2 2 2 2 2 2 2 2',\
    #            #'2 2 2 2 2 2 2 2 2 2 2 2',\
    #            #'4 2 2 2 2 2 2 2 4 2 2',\
    #            #'3 4 5 4 4 4 2 2 2 2',\
    #            #'2 2 3 4 2 2 2 2 2 2',\
    #            #'2 2 2 3 4 2 2 2 2 2',\
    #            #'3 4 2 2 2 3 2 2 2 2 2 2'])

    #supp = 1.0
    #stats = []

    #result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))
    #sample_result = ['$x0 $x0 $x0 02']

    #assert sample_result == result

    LOGGER.info('Finished test 12 for pattern_type_split_discovery')

def test13_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 13 for pattern_type_split_discovery')

    sample = Sample(["b a a c d d b a b b", "b a a b c d d a c c b b", "a a b c b a c d b a", "c a a b d b c c d d b a", "b a c d d b b a c d d b"])
    supp = 1.0
    stats = []

    sample_result =['$x0 a $x1 $x2 $x2 $x1 $x0', '$x0 a $x1 $x2 $x1 $x2 $x0', '$x0 $x1 $x1 c d $x1 $x0', '$x0 $x1 $x2 $x1 $x0 $x2', '$x0 $x1 $x2 $x2 $x0 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2', '$x0 a $x1 c d $x1 $x0', '$x0 a $x1 $x1 $x0 $x0', '$x0 $x0 c $x1 $x1 $x0', 'a $x0 c $x1 $x1 $x0', 'a $x0 $x1 $x1 b $x0',\
            'a $x0 $x1 $x1 $x0 b', 'a $x0 $x1 $x0 $x1 b', '$x0 a c $x1 $x1 $x0', '$x0 a $x1 b $x1 $x0', '$x0 a $x1 $x1 b $x0', '$x0 a $x1 $x1 $x0 b', '$x0 a $x1 $x0 $x1 b',\
            '$x0 $x1 $x1 $x0 $x1', '$x0 $x1 c d $x0 $x1', '$x0 c $x0 $x1 $x1', '$x0 $x1 a $x0 $x1', '$x0 $x1 $x0 b $x1', '$x0 $x0 $x1 $x1 a', '$x0 a c d b $x0', '$x0 a b $x0 $x0',\
            'b c $x0 $x0 a', 'b $x0 b $x0 b', 'b $x0 c d $x0', 'b $x0 $x0 b a', 'b $x0 $x0 d b', 'a a c $x0 $x0', 'a a $x0 $x0 b', 'a c $x0 $x0 a', 'a $x0 b $x0 b',\
            'a $x0 d b $x0', 'a $x0 c $x0 b', '$x0 a b b $x0', '$x0 a b $x0 b', '$x0 a a d $x0', '$x0 a a c $x0', '$x0 a c $x0 b', '$x0 $x0 b b b', '$x0 c $x0 $x0',\
            '$x0 $x0 b $x0', '$x0 $x0 $x0 b', 'b b $x0 $x0', '$x0 a $x0 a', 'a a c d b', 'b c d b', 'b c d a', 'a b b b', 'a c d a', 'a b a', 'c b b', 'c a b']
    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0]

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
    LOGGER.info('Finished test 13 for pattern_type_split_discovery')

def test14_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 14 for pattern_type_split_discovery')

    sample = Sample(["a b c d e f g h i j k", "a b c d e f g h i j k" ])
    supp = 1.0
    stats = []

    sample_result =["a b c d e f g h i j k"]
    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0]

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
    LOGGER.info('Finished test 14 for pattern_type_split_discovery')

def test15_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 15 for pattern_type_split_discovery')

    sample = Sample(["a a a a a a a a", "b b b b b b b" ])
    supp = 1.0
    stats = []

    sample_result =["$x0 $x0 $x0 $x0 $x0 $x0 $x0"]
    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0]

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
    LOGGER.info('Finished test 15 for pattern_type_split_discovery')

def test16_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 16 for pattern_type_split_discovery')

    sample = Sample(['M o o d b E G y e x s M J D C K L h a t', 'f u u A c n q H F g m f v p z i r I N k'])
    supp = 1.0
    stats = []

    sample_result = ["$x0 $x1 $x1 $x0"]
    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
    LOGGER.info('Finished test 16 for pattern_type_split_discovery')

def test99_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': huge running example
        Since the sample runs more than 4 hours (last run: 8h), uncomment 'result = ...' if you like to try the test case.
    """
    LOGGER.info('Started test 99 for pattern_type_split_discovery')
    in_time = time.time()

    sample = Sample([\
            '5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 1 1 2 2',\
            '1 1 1 3 1 1 1 1 3 4 1 1 4 1 4 1 1 1 2 2',\
            '4 3 1 3 1 1 1 3 4 1 1 1 1 1 1 1 1 1 1 2',\
            '2 1 2 2 2 2 2 1 1 1 1 1 1 2 2 2 2 1 2 2',\
            '2 2 1 2 2 2 2 2 1 1 1 1 1 1 2 2 2 2 1 2',\
            '1 1 1 4 2 2 2 1 1 1 1 1 2 2 2 1 4 2 1 2',\
            '3 4 1 1 5 1 1 1 1 4 1 1 4 4 1 1 2 2 2 2',\
            '2 2 1 1 1 1 1 1 3 4 2 2 2 1 1 1 1 2 2 2',\
            '2 2 2 1 1 1 1 1 1 3 4 2 2 2 1 1 1 1 2 2',\
            '1 1 1 1 1 3 4 2 2 2 1 3 1 2 2 2 2 1 2 2'])

    supp = 1.0
    stats = []

    #result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))
    #result = sorted(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0], key=lambda item: (item.count(' '),item))

    sample_result = [\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x0 $x0', '$x0 $x0 $x0 $x0 $x0 $x0 1 1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x0 $x0 $x0 $x0 1 1 $x1 $x1', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x0 $x1 $x1', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x0 $x1 $x0 $x0', '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x0 $x1 $x1', '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x1 $x1', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x0 $x0', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x1 $x0 $x0 $x0 $x0 $x1 $x0 $x0', '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x0 $x1 $x1', '$x0 $x0 $x1 $x0 $x1 $x1 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x1 $x1', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 $x0 $x0', '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x0 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x0 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x0 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x0 $x2 $x2', '$x0 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1',\
            '$x0 $x1 $x0 $x1 $x1 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x0 $x0 $x0 $x0 $x1 $x0 $x0', '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x0 $x1 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x1 $x1', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 1 1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x0 $x1 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x1 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x0 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 1 1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 1 1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x0 $x2 $x0 $x1',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x0 $x2 $x1', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x1 $x0 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x2 $x2 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x0 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 1 1 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x0 $x2 $x0 $x1',\
            '$x0 $x0 $x0 $x0 $x1 $x2 $x1 $x2 1 1 2', '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 1 1 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x0 $x0 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x1 $x2 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x0 $x1 $x2 $x2 $x2 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x2 $x3 $x0 $x1',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x2 $x3 $x1 $x1', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x0 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x0 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x0', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1', '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 1 1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 1 1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x0', '$x0 $x0 $x0 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x1', '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x0 2',\
            '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 1 2',\
            '$x0 $x0 $x0 $x1 $x2 $x0 $x1 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x2 $x0 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x2 $x1 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x2 $x1 $x1 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x0 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 1 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x1', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 1 1 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x1', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2', '$x0 $x0 $x0 $x1 $x2 $x3 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 1 2', '$x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 1 $x2 $x2',\
            '$x0 $x0 $x1 $x0 $x0 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x0 $x0 $x1 $x2 $x1 $x3 $x3 $x2', '$x0 $x0 $x1 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x2 $x1', '$x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x1 $x0 $x1 $x1 $x1 $x1 1 $x2 $x2', '$x0 $x0 $x1 $x0 $x2 $x0 $x3 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x0', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x3 $x4 $x3 $x4 $x1 $x2', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 1 2', '$x0 $x0 $x1 $x1 $x0 $x1 $x2 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x1 $x2 $x1 $x3 $x3 $x2', '$x0 $x0 $x1 $x1 $x0 $x1 $x2 $x2 $x2 $x1 $x1', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x2 $x3 $x0 $x1',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x2 $x3 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 1 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x1 $x2 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x1 $x2 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x2 $x1 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x2 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x0 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x1', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x1 $x2 $x3 $x1', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x1 $x2 $x3 $x2', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x1 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x2 $x0 $x3', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 1 1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x0 $x2 2',\
            '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x0 $x1 $x2 $x2 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x0 $x1 $x3 $x1 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x0 $x1 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x0 $x3 $x0 $x2 $x3 $x0', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x0 $x2 $x3 $x0', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x0 $x2 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x2 $x3 $x0', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x2 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x2 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x1 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x1 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x1 $x2 $x3 $x1 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x1 $x2 $x3 $x2 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x1 $x2 $x3 $x2 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x2 $x3 $x2 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x0 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x0 $x1 $x3 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x0 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x0 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x0 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x0 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x0 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x1 $x3 $x3 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x1 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x1 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x2 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x2 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x3 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x3 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x3 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x1 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x1 $x3 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x0 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x1 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x3 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x1 $x1', '$x0 $x0 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x1 $x2 1 1 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 1 1 2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x1', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x2 $x1',\
            '$x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x0 $x0 $x1 $x1 $x1 $x1 1 $x2 $x2',\
            '$x0 $x1 $x0 $x0 $x1 $x2 $x1 $x2 1 1 2', '$x0 $x1 $x0 $x0 $x1 $x2 $x2 $x1 1 1 2', '$x0 $x1 $x0 $x0 $x2 $x0 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x2 $x1 $x2 1 1 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x1 1 1 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 1 1 2',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x2 $x3 $x4 $x3 $x4 $x2 $x1', '$x0 $x1 $x0 $x1 $x1 $x2 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x1 $x2 $x1 $x2 1 1 2',\
            '$x0 $x1 $x0 $x1 $x1 $x2 $x2 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x1 $x2 $x2 $x1 1 1 2', '$x0 $x1 $x0 $x1 $x2 $x1 $x1 $x2 1 $x3 $x3',\
            '$x0 $x1 $x0 $x1 $x2 $x1 $x2 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x2 $x1 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x2 $x1 $x2 1 1 $x3 $x3',\
            '$x0 $x1 $x0 $x1 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x2 $x2 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x2 $x2 $x1 1 1 $x3 $x3',\
            '$x0 $x1 $x0 $x1 $x2 $x2 $x2 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x1 $x2 $x2 $x2 $x1 1 1 2', '$x0 $x1 $x0 $x2 $x1 $x1 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x0 $x2 $x1 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3 $x0', '$x0 $x1 $x0 $x2 $x2 $x1 $x1 $x2 1 $x3 $x3',\
            '$x0 $x1 $x0 $x2 $x2 $x1 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x1 $x3 $x1 $x3 $x0 $x0', '$x0 $x1 $x0 $x2 $x2 $x1 $x3 $x1 $x3 $x2 $x0',\
            '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x2 $x1 1 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x0 $x2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x0 $x0', '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x0 $x2 $x3 $x3 $x1 $x2 1 1 2',\
            '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x1 2',\
            '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2', '$x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 1 2', '$x0 $x1 $x1 $x0 $x0 $x1 $x2 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x0 $x0 $x1 $x2 $x1 $x3 $x3 $x2', '$x0 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1', '$x0 $x1 $x1 $x0 $x0 $x2 $x0 $x2 1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x0 $x2 1 1 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 1 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 1 1 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x2 $x1', '$x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x0 $x1 2', '$x0 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x0 $x2 $x0 $x3 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 1 1 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x0', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x2 $x3 $x4 $x3 $x4 $x1 $x2', '$x0 $x1 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x1 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x1 1 2', '$x0 $x1 $x1 $x1 $x0 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x1 $x2 $x1 $x3 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x1 $x2 $x2 $x2 $x1 $x1', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 1 1 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 1 1 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x2 $x3 $x1 $x1',\
            '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x1 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x1',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 1 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 1 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 1 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x2 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x1 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 1 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x1 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x1 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x1 $x1 $x2 $x3 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x1 $x2 $x2 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x0 $x1 $x3 $x1 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x0 $x1 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x0 $x2',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x0 $x2 $x3 $x0', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x1 $x2 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x1 $x0 $x2 $x0 $x0', '$x0 $x1 $x1 $x2 $x1 $x1 $x1 $x0 $x2 $x1 $x0', '$x0 $x1 $x1 $x2 $x1 $x1 $x1 $x0 $x2 $x1 $x1',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x1 $x1 $x2 $x0 $x0', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x0 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x1',\
            '$x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x1 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x1 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x2 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x2 $x2 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x0 $x1',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x1 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x2 $x1', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x2 $x0 $x1', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x1 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x1', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x1 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x3 $x3 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 $x1', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x0 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x1 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x2 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x1 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x2 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x0 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x1 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x0 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x3 $x3 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x1 $x1 $x0 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x1 $x1 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x1 $x3 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x1 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x0 $x0 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x0 $x1 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x2 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x1 $x1 $x0 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x1 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x0 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x1 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x0 $x1 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x0 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x1 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x1 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x1 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x3 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x0 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x0 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x0 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x1 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x1 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x2 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x0 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x1 $x1 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x1 $x1 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x1 $x1 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x1 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x2 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x2 $x2 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x0 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3 $x1', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x0 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x3 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x0 $x1 $x2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x0 $x2 $x1',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x2 $x0 $x2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x0 $x2 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x0 $x3 $x4 $x2', '$x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x1 $x3 $x0 $x2', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x1 $x3 $x2 $x2',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x1 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x2 $x3 $x1', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x3 $x1 $x1', '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x3 $x2 $x1',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x3 $x2 $x3 $x3 $x1', '$x0 $x1 $x2 $x0 $x0 $x2 $x3 $x1 $x1 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x2 $x3 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x0 $x2 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 $x0 2',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 $x2 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 1 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x0 $x1 $x1 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x0 $x1 $x1 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x1 $x1 2', '$x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x1 $x2 2', '$x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x1 1 2', '$x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x2 $x1 2', '$x0 $x1 $x2 $x0 $x2 $x1 $x3 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x2 $x1 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x0 $x2 $x1 $x3 $x1 $x4 $x4 $x3', '$x0 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3 $x1 $x1',\
            '$x0 $x1 $x2 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x2 $x2 $x3 $x2 $x1 $x3 $x3', '$x0 $x1 $x2 $x0 $x2 $x3 $x4 $x3 $x4 $x2 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x3 $x3 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x3 $x3 $x0 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x3 $x3 $x2 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x2 $x3 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x3 $x1 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x3 $x2 $x1', '$x0 $x1 $x2 $x1 $x0 $x1 $x3 $x2 $x3 $x3 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x0 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x1 $x3 $x3 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x0 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x0 $x3 $x3 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x1 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x1 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x0 $x1 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x0 $x2 $x1', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x0 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x1 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 $x1',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x1 $x0 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x0 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x1 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x3 $x0 $x2', '$x0 $x1 $x2 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x2 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x3 $x3 $x2', '$x0 $x1 $x2 $x1 $x3 $x3 $x2 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x0 $x2 2',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x2 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 1 2',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x3 $x3 $x1', '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x3 $x3 $x2', '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x1 $x1', '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x2 $x1', '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x1 $x3 $x0 $x2', '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x1 $x3 $x2 $x2', '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x2 $x3 $x1 $x1', '$x0 $x1 $x2 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x2 $x3 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x2 $x3 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x2 $x0 $x3 $x1 $x0 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x1 $x0 $x2 $x0 $x0',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x1 $x0 $x2 $x1 $x0', '$x0 $x1 $x2 $x2 $x1 $x1 $x1 $x0 $x2 $x1 $x1', '$x0 $x1 $x2 $x2 $x1 $x1 $x1 $x1 $x2 $x0 $x0',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x0 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x0 $x1 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x1 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x1 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x2 $x1', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x1 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x1', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x1 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x3 $x0 $x2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x0 $x2 2',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 1 1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 $x0 $x2 $x1',\
            '$x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x0 $x1 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x0 $x2 $x1', '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x2 $x0 $x1 $x2', '$x0 $x1 $x2 $x2 $x2 $x2 $x2 $x2 $x0 $x2 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x1 $x0 $x1 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x0 $x1 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x0 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x1 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x0 $x4 $x1 $x3 $x4 $x1', '$x0 $x1 $x2 $x2 $x3 $x0 $x4 $x1 $x4 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x1 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x1 $x2 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x1 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x3 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x2 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x3 $x0 $x4', '$x0 $x1 $x2 $x3 $x0 $x2 $x1 $x3 $x4 $x4 $x0',\
            '$x0 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x4 $x4 $x1', '$x0 $x1 $x2 $x3 $x0 $x2 $x4 $x3 $x4 $x1 $x1', '$x0 $x1 $x2 $x3 $x1 $x1 $x1 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x1 $x1 $x2 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x3 $x1 $x1 $x2 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x1 $x2 $x2 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x2 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x3 $x2 $x1 $x1 $x0 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x2 $x1 $x1 $x1 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x1 $x1 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x1 $x2 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x2 $x2 $x1 $x0 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x2 $x1 $x1 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x2 $x1 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x2 $x2 $x2 $x0 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x2 $x2 $x1 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x2 $x2 $x2 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x1 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x3 $x0 $x4 $x2 $x1 $x4 $x3',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x1 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x1 $x1 $x0 $x2', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x1 $x3 $x0 $x2', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x2 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x1 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x1 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x1 $x0 $x2',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x0 $x2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x2 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x0 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x2 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x0 $x4 $x1', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x2 $x0 $x3', '$x0 $x1 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x0 $x2',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x0 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x0 $x4 $x4 $x2', '$x0 $x1 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x0 $x2',\
            '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x4 $x4 $x1', '$x0 $x1 $x2 $x3 $x4 $x4 $x1 $x1 $x3 $x0 $x2',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x1 $x4 $x3 $x0 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 1 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x0 $x2 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x0 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x3 $x1 $x3 $x2 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x0 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x0 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x1', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x0 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x1 $x1', '$x0 $x0 $x0 $x1 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x0 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x0 $x2 $x0 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x0 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x0 $x0 $x1 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x1 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x0 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x2 $x0 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 1 1 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x1 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x1',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x0 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x3 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 $x2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 1 1 2', '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x0 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x0 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x1 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x0 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x2 $x1 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x1',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x1 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x2 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x2 $x3 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x0 $x0 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x2',\
            '$x0 $x0 $x0 $x1 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x2', '$x0 $x0 $x1 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x2 $x0 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x2 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x1 $x1 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x0 $x2 $x1 $x3 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x0 $x2 $x0 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x0 $x2 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x0 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x0 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x0 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x2 2',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x0 2',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x3 $x0 $x2 $x1 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x2 $x3 $x4 $x0 $x1 $x4 $x2 $x3', '$x0 $x0 $x1 $x0 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x1 $x1 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 1 1 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x0 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x0 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x0 $x2 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x2 $x2 $x0 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 1 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 1 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 1 1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x0 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x1 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x1 $x0 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x0 $x1 $x2 $x2 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x1 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x2 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x1 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x0 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x1 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x3 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x0', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x4 $x3 $x0 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 1 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x1 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x1 $x2 $x2 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 1 2', '$x0 $x0 $x1 $x1 $x2 $x0 $x3 $x1 $x1 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x0 $x3 $x1 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x3 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x0 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x1 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x2 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x3 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x4 $x2', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x3 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x1 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x2 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x4 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x0 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x0 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x1 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x4 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x1 $x4 $x3 $x4 $x0',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x3 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x2 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x3 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x4 $x0', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x4 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x4 $x2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x0 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x4 $x4 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 1 1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x0 $x3 $x4 $x2 $x2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x2 $x0 $x4 $x1 $x3', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x4 $x2 $x0', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x4 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x2 $x2 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x2 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x2 $x2 $x2 $x1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x2 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x0 $x3 $x4 $x1 $x1 $x4 $x2 $x3', '$x0 $x0 $x1 $x2 $x0 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x0 $x1 $x2 $x0 $x3 $x4 $x1 $x4 $x4 $x2 $x3', '$x0 $x0 $x1 $x2 $x0 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x1 $x2 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x1 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x2 $x4 $x3 $x0 $x4',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x4 $x3 $x3 $x4 $x2', '$x0 $x0 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x2 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x4 $x3 $x2 $x4 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x4 $x3 $x3 $x2 $x4', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x4 $x3 $x3 $x4 $x2', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x0 $x4 $x2 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x3 $x4 $x2', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x4 $x2 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x4 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x4 $x4 $x2', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x1 $x4 $x4 $x2 $x3',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x2 $x0 $x4 $x1 $x3', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x4 $x1 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x4 $x3 $x4 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x0 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x2 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x2 $x2 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x1 $x3 $x0 $x2', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x1 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x3 $x0 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x3 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x2 $x4 $x3 $x0 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x4 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x2 $x1 1 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x2 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x1 $x2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x1 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x4 $x3 $x3 $x0 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x3 $x1 $x2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x1 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x1 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x1',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x3', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x0 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x1 $x3', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x2 $x0 $x4 $x1 $x3', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x2 $x1 $x4 $x0 $x3',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x2 $x1 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x4 $x0',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x0 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x3 $x0 $x2 $x0 $x3 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x1 $x3',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x1', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x3',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x1 $x3', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x0',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x1', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x3', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x3 $x2 $x2 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x4 $x2 $x0', '$x0 $x0 $x1 $x2 $x3 $x1 $x3 $x3 $x2 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x1 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x2 $x4 $x3 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x2 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x4 $x2 $x2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x4 $x2 $x3', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x4 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x3 $x4 $x4 $x3 $x2',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x4 $x1 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x1 $x4 $x4', '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x0 $x4 $x4', '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x4 $x4', '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x4 $x2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x3 $x4 $x4 $x2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x1 $x2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x2 $x1',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x1 $x3', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x4 $x1',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x3 $x0', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x4 $x0',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x4 $x1 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x4 $x2 $x0 $x4 $x1 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x4 $x3 $x1 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x4 $x3 $x1 $x4 $x4 $x2 $x3', '$x0 $x0 $x1 $x2 $x3 $x4 $x3 $x1 $x4 $x4 $x3 $x2', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x3 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x1 1 2', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x1 $x3 1 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x2 $x3 $x4 $x0 $x1 $x4 $x2 $x3', '$x0 $x1 $x0 $x0 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x1 $x0 $x2 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x0 $x2 $x2 $x0 $x0 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x0 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x0 $x2 $x2 $x1 $x1 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x0 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 1 1 $x4 $x4', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 1 1 $x4 $x4', '$x0 $x1 $x1 $x0 $x0 $x0 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x0 $x2 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x3 $x3 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x0 $x2 $x1 $x3 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x0 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 1 2',\
            '$x0 $x1 $x1 $x0 $x2 $x0 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x0 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x0 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x1 $x3 $x4 $x4', '$x0 $x1 $x1 $x0 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x1 $x2 $x2 $x1 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x2 $x1 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x2 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x1 $x2',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x2 $x2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x0 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 2',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 1 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x1 $x1', '$x0 $x1 $x1 $x1 $x0 $x1 $x2 $x2 $x2 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x0 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x0 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x0 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x0 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x2 1 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x0 $x2 $x1 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 1 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 1 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x1 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x2 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x0 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 1 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 1 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 1 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x1 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x1 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x1', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x1 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x1', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x1 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x1 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x1 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x1 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x2 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 $x4 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x2 $x4 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x0 $x4', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x1 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x1 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x3 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x1 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x1 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x1 $x2 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 1 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 1 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x1',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x0 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x1 $x4 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x0 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x2 $x4 $x0', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x1', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x0 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x3 $x2 $x0 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x4 $x2 $x3 $x0 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x1 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x0 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x2 $x4 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x1',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 1 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x0 $x2 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x0 $x4 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x1 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x4 $x2 $x0',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x1', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x0 $x1 $x3 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x2 $x2', '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x1 $x3 $x2 2',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x0 $x0 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x1 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x1 $x1 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x1 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x4 $x4 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x4 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x1 $x1 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x1 1 2',\
            '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x4 $x4 $x1 $x1', '$x0 $x1 $x1 $x2 $x0 $x3 $x4 $x1 $x1 $x4 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x0 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x0 $x3 $x4 $x1 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x2 $x0 $x3 $x4 $x2 $x1 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x0 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x0 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x2 $x4 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x2 2',\
            '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x2', '$x0 $x1 $x1 $x2 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x0 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x1 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x2 $x3 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x2 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x2 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x2 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x2 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 1 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x0 $x0 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x1 $x0 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x4 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x0 $x4 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x2 $x3 $x0 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x1 $x4 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x4 $x1',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x4 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x1 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x2 $x0 $x3 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x2 $x4 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x3 $x3 $x0 $x2 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x0 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x1 $x2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x3 $x2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x0 $x0 1 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x0 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x0 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x0 $x0 1 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x1 $x1 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x2 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x2 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x2 $x0 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x2 $x1 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x1 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x1 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x1 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x2 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x2 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x2 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x2 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x1 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x1 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x1 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x1', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x2 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x4 $x0 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x4 $x4 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 1 1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x3 $x0 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x1 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x1 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x4 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x1 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x2 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x0 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3 $x3 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x1 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x1 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 1 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x0 $x1 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x0 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x1 $x0 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x3 $x4 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x1 $x3 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x1 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x3 $x4 $x1',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x4 $x3 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x0 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x4 $x1', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x4 $x2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x1 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x2 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x0 $x0 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x3 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x4 $x0', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x2 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x0 $x0 $x2 $x4 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x0 $x2 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x1 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x4 $x2 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x4 $x2 $x2 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x0 $x4 $x2 $x4 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x1 $x0 $x2 $x2 $x4 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x4 $x1 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x4 $x3 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x0 $x3 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x1 $x3 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x1 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x0 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x1 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x3 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x4 $x1', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x1', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x2 $x0 $x1 $x2 $x4 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x0 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x2 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x2 $x2 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 1 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x1 $x1 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x4 $x1 $x4 $x0 $x2', '$x0 $x1 $x1 $x2 $x3 $x3 $x1 $x4 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x1 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x0 $x0 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x0 1 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x1', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 1 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x0 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 1 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x2 $x0 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x0 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x4 $x0',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x2 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x3 $x4 $x3 $x0', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x4 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x0 $x3 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x1 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x1 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x2 $x5 $x3 $x4 $x5 $x0', '$x0 $x1 $x1 $x2 $x3 $x4 $x3 $x0 $x2 $x3 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x0 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x0 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x2 $x3 $x0 $x0 $x4', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x2 $x3 $x0 $x0 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x5 $x2 $x0 $x4 $x5 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x1 $x1 $x2 1 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x1 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x1 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x2 $x1 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x0 $x0 $x3 $x0 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x0 $x3 $x2 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x0 $x3 $x2 $x3 $x0 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x0 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x0 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x0 $x0 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x0 $x0 $x3 $x4 $x2 $x2 $x4 $x1 $x3', '$x0 $x1 $x2 $x0 $x0 $x3 $x4 $x2 $x2 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x0 $x0 $x3 $x4 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x4 $x4', '$x0 $x1 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x2 $x3 $x4 $x4', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x3 $x2 $x0 2',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x4 $x4 $x2 $x3', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x3 $x4 $x4', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x4 $x4 $x4',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x4 $x4 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x3 $x0 $x2 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x3 $x2 1 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x2',\
            '$x0 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x0 $x3 $x3 $x0 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x3 $x3 $x2 $x2 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x0 $x3 $x3 $x3 $x2 $x0 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x3 $x3 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x0 $x3 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x0 $x3 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x0 $x3 $x3 $x4 $x2 $x2 $x4 $x1 $x3', '$x0 $x1 $x2 $x0 $x3 $x3 $x4 $x2 $x2 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x0 $x3 $x3 $x4 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x1 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x0 $x1', '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x1 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x2 $x3 $x1 $x1', '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x2 $x3 $x1 2', '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x3 $x2 $x1 2',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x3 $x3 $x0 $x1', '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x1 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x1 $x1 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x1 $x1 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x1 $x2 $x2 $x2 1 $x3 $x3', '$x0 $x1 $x2 $x1 $x0 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x1 $x0 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x1 $x0 $x3 $x4 $x2 $x2 $x4 $x1 $x3', '$x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x2 $x4 $x0 $x4 $x3', '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x2 2',\
            '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x2', '$x0 $x1 $x2 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x1 $x3 $x2 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x2 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x0 $x3 $x2 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x1 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x0 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x0 $x2 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x2 $x0 $x2 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 $x1 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 1 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x0 $x4 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x1 $x2 $x0 $x4', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x0 $x0 $x4', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x1 $x0 $x4',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x3 $x4 $x0', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x0 $x2 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x0 $x2 $x3 $x0 $x4', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x0 $x3 $x2 $x4 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x1 $x2 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x2 $x0 $x3 $x4', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x3 $x4 $x0 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x4 $x0 $x3 $x1 $x2', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x4 $x4 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x1 $x2 1 $x4 $x4', '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x2 1 $x4 $x4',\
            '$x0 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x2 $x4 $x2 $x4 $x0 $x1',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x2 $x3 $x0 $x4', '$x0 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x2 $x3 $x0 $x4',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x1 $x1', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x2 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 $x2 1 2',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x2 $x1 $x1 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x2 $x1 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x0 $x2 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x2 $x2 $x3 $x3 $x0 $x1', '$x0 $x1 $x2 $x2 $x0 $x0 $x2 $x2 $x3 $x3 $x1 $x1', '$x0 $x1 $x2 $x2 $x0 $x0 $x3 $x2 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x3 $x2 $x2 $x3 $x1 $x1', '$x0 $x1 $x2 $x2 $x0 $x0 $x3 $x2 $x3 $x3 $x0 $x1', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x0', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x2 2',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x2 $x1 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x2 $x1 $x1 $x1 1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x2 $x2 $x1 $x1 1 $x3 $x3', '$x0 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x0 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x3 $x1 $x3 1 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x3 $x2 $x2 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x0 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x1 1 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x0 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x2 $x1 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x2 $x2 $x4 $x1 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x2 $x4 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x2 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x2', '$x0 $x1 $x2 $x2 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x1 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x2 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x0 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 1 1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x4 $x4 $x1', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x4 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x3 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x1 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x2 $x2 $x2 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x2 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x2 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x3 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x3 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x2 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 1 1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x1 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x2 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x0 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x0 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x1 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 1 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x1 $x0 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x0 $x0 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x1 $x0 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x3 $x4 $x0', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x0 $x4 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x1 $x4 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x4 $x1', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x4 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x1 $x3 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x1 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x2 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x3 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x0 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x0 $x2 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x0 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x1 $x2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x3 $x2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x2 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x0',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x1 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 $x2 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x2 1 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x2 $x3 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x2 $x2 $x3 $x0', '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x2 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x2 $x2 $x0 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x2 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x2 $x2 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x2 $x3 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x2 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x1 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x4 $x4 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x0 $x3 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x1 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x3 $x0 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x1 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x1',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x0 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x1 $x0 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x2 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x4 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x4 $x0 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x2 $x1 $x0 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x2 $x1 $x3 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x4 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x4 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x4 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x2 $x0 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x2 $x1 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x1 1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x1', '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x2 $x2 $x4 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x1 $x2 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x2 $x2 $x4 $x0',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x3 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x1 $x4 $x0 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x1 $x4 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x2 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x3 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x4 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x2 $x4 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x2 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x2 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x1 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x2 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x1 $x4 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x2 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x0 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x4 $x0 $x4 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x1 $x3 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x1 $x3 $x4 $x3', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x1 $x3 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x0 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x0 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x4 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x1 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x1 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x3 $x0 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x3 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x1 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x1 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x1 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x1 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x1 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x4 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x1 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x1 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x5 $x3 $x4 $x5 $x0',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x5 $x4 $x3 $x0 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x5 $x4 $x3 $x5 $x0', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x5 $x5 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x5 $x5 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x0 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x3 $x0 $x1 $x3 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x4 $x3 $x2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x4 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x3 $x0 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x5 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x3 $x0 $x4 $x5 $x1', '$x0 $x1 $x2 $x3 $x0 $x0 $x4 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x3 $x0 $x1 $x0 $x3 $x2 1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x0 $x2 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x0 $x3 $x1 $x1 $x1 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x1 $x1 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x1 $x1 $x2 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x1 $x2 $x2 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x1 $x2 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x1 $x1 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x1 $x1 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x2 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x2 $x4 $x4 $x1 $x1', '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x0 $x3 $x2 $x2 $x4 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x2 $x4 $x1 $x1', '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x2 $x4 $x1 2',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x2 $x4 $x3 $x1', '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x4 $x2 $x1 2', '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x3 $x0 $x3 $x4 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x3 $x1 $x1 $x2 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x3 $x1 $x3 2',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x3 $x3 $x3 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x3 $x4 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x0', '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x3 $x3 $x0 $x4',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x3 $x3 $x4 $x0', '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x3 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x2 $x4 $x4 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x2 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x2 $x2 $x4 $x4 $x0 $x3 $x3', '$x0 $x1 $x2 $x3 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x2 $x2 $x0 $x3 $x4', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x2 $x3 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x2 $x3 $x0 $x4 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x3 $x3 $x0 $x2 $x4', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x2 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x2 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x2 $x0 $x3 $x3', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x3 $x0 $x2 $x3',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x3 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x3 $x1 $x2 $x4 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x2 $x2 $x0 $x4', '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x2 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x4 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x3 $x1 $x3 $x2 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x3 $x3 $x4 $x4 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x3 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x3 $x1 $x3 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x2 $x2 $x0 $x2 $x4',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x2 $x2 $x0 $x3 $x4', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x2 $x2 $x0 $x4 $x4', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x2 $x2 $x0 $x4 2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x3 $x2 $x0 $x2 $x4', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x0 $x2 $x3', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x4 $x0 $x2 $x2',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x4 $x0 $x2 $x3', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x0 $x3',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x0 $x3', '$x0 $x1 $x2 $x3 $x3 $x0 $x4 $x2 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x0 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x1 $x3 $x3', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x1 $x3', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x3 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x0 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x0 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x4 $x0 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x0', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x1 $x4 $x4 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x1 $x4 $x4 $x3 $x0', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x4 $x2 $x4 $x0 $x3', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x3 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x0 $x1 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x0 $x2 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x0 $x3 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x1 $x3 $x0 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x2 $x3 $x0 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x3 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x0 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x0 $x2 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x0 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x0 $x3 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x1 $x2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x2 $x1', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x2 $x2', '$x0 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x1 $x0 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 1 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x3', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x0 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x3 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x1 $x0 $x3 $x3',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x1 $x0 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x0 $x3 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x0 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x0 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 $x3 $x3', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x1 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x1 $x2 $x0 $x4 $x1', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x2 $x2 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x2 $x2 $x0 $x4 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x4 $x2 $x0 $x4 $x1', '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x0 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x1 $x5 $x0 $x4 $x5', '$x0 $x1 $x2 $x3 $x4 $x4 $x2 $x3 $x0 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x1 $x2 $x0 $x1 $x1',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x1 $x2 $x0 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x2 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x2 $x0 $x1 2',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x0 $x1 $x1', '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x0 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x5 $x3 $x2 $x0 $x5 $x1',\
            '$x0 $x1 $x2 $x3 $x4 $x5 $x1 $x3 $x2 $x0 $x4 $x5', '$x0 $x1 $x2 $x3 $x4 $x5 $x1 $x3 $x2 $x0 $x5 $x4', '$x0 $x1 $x2 $x3 $x4 $x5 $x2 $x3 $x0 $x5 $x4 $x1',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 1 $x3 $x3',\
            '$x0 $x0 $x0 $x0 $x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 1 $x3 $x3',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x0 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x4',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 1 2', '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 2',\
            '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 2', '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x1 2', '$x0 $x0 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x0 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x0 $x1 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 $x4 $x4', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 2',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 2',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x2 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x0 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x3',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x3', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x0 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x0 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x1 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 1 1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 1 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x4 $x2 $x4 $x3 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x1 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x0 $x3 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x1 $x2 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x0 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 2',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x2 2', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x2 $x1 2',\
            '$x0 $x0 $x0 $x1 $x2 $x3 $x2 $x4 $x4 $x1 $x3 $x5 $x5', '$x0 $x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4 $x4', '$x0 $x0 $x0 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x0 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2',\
            '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x0 $x1 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x4 $x3 $x4', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x4 $x4 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x4', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x0 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x1 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x3',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 $x3', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x0 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x1 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x2 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x2 $x3 1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x2 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x2 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x2 1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 $x2 1 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x3 1 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x2 1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 1 1 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x0 $x3 $x2 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x1', '$x0 $x0 $x1 $x1 $x2 $x0 $x3 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x3 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x3 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x3 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x4 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x4 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x4 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x4 1 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x3 $x5 $x4 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x2 $x4 $x4 $x1 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4 $x2 1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x3 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x2 $x0 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x2 $x1 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x2 1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x2 $x2 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x4 $x2 $x1 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x4 $x2 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x4 $x2 1 2', '$x0 $x0 $x1 $x2 $x0 $x1 $x3 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x4 $x3 $x3 $x2 $x4 2',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x4 $x3 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x1 $x1 $x3 $x4 $x4 $x3 $x2 $x4 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x1 $x4 $x4 $x2 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x1 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x2 $x2 $x4 $x4 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x2 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x4 $x4 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x4 $x4 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x1 $x4 $x4 $x2 $x2 2', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x2 $x4 $x4 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x1 $x3 $x3 1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x1 1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x1 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x3 $x2 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x4 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x2 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x1 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x2 $x4 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x3 $x1 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x2 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x0 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x0 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x4 $x3 $x4 1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x4 $x4 $x1 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x4 $x4 $x3 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x1 $x3 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x2 $x3 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x1 $x4 $x4 $x4',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x2 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x2 $x4 $x4 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x2 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x2 $x4 $x4 $x4 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x1 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x1 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x2 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x2 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x2 $x4 $x4 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x2 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 $x4', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x3 $x4 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x2 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x1 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x2 $x4 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x0 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x4 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x1 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x2 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x3 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x2 $x2 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x3 $x4 $x1 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x4 1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x3 $x4 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x4 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x1 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x4 $x4 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x1 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x1 $x2 $x1 $x4 $x4 $x0 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x3 $x1 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x1 $x3 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x5 $x5 $x4 $x2 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x1 $x4 $x5 $x5 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x2 $x3 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x1 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x1 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x1 $x3 $x4 $x4 $x2 $x0 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x1 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x1 $x3 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x3 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x0 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x3 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 $x4 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x3 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x0 $x1 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x0 $x1 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x3 $x0 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x2 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x1 $x5 $x4 $x2 $x5 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x2 $x2 $x4 $x3 $x1 2',\
            '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x3 $x3 $x4 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x3 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x4 $x3 $x4 $x4 $x1 $x2 2',\
            '$x0 $x0 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x4 $x1 $x2 2', '$x0 $x0 $x1 $x2 $x3 $x4 $x4 $x3 $x1 $x2 $x5 $x5 $x5', '$x0 $x0 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x1 $x5 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x4 $x5 $x1 $x3 $x5 $x4 $x2 2', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x0 $x0 $x0 $x2 $x2 $x1 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x0 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 1 $x4 $x4',\
            '$x0 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x0 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4',\
            '$x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x2 $x2 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x4 $x4 $x4', '$x0 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x2 $x4 $x4 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x1 $x3 $x2 $x2 $x4 $x4 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 $x4', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x1 $x3 $x4 $x4 $x4', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x1 $x3 $x4 $x4 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x3 $x3 $x4 $x4 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x5 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x1 $x3 $x4 $x4 $x4', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x1 $x3 $x4 $x4 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x2 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x3 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x3 $x2 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x4 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x4 $x3 $x5 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x4 $x3 $x1 1 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x5 2', '$x0 $x1 $x0 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x3 2',\
            '$x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x2 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x0 $x2 $x3 $x3 $x4 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x0 $x2 $x3 $x4 $x1 $x2 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x0 $x2 $x3 $x4 $x4 $x1 $x2 $x3 $x5 $x5 2', '$x0 $x1 $x0 $x2 $x3 $x4 $x4 $x1 $x3 $x2 $x5 $x5 2', '$x0 $x1 $x0 $x2 $x3 $x4 $x5 $x1 $x2 $x5 $x3 $x4 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x1 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x0 $x2 $x2 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x0 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x1 $x3 $x3 $x0 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x2 $x1 $x0 $x3 $x3', '$x0 $x1 $x1 $x1 $x0 $x0 $x2 $x2 $x3 $x3 $x0 $x1 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x0 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x0 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x1 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x0 $x2 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x0 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 2', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x2 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x0 $x0 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x1 $x3 $x3', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x2 1 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x1 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x3 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x4 $x3 $x4', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x2 $x3 $x4 $x4 $x3', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x4 $x4 $x2 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x1 $x0 $x2 $x3 $x4 $x4 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x0 $x3 $x3 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 1 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x4 $x4 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x1 $x0 $x3 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x3 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x2 1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x3 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x2 $x4 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x1 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x0 $x2 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x3 $x4 $x3 $x2 $x4 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x1 $x3 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x1 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x1 $x2 $x0 $x3 $x4 $x4 $x1 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x0 $x3 $x3 1 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x4 $x4 $x1 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x2 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x1 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x2 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x2 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x3 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x4 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x3 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x0 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x4 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x5 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x2 $x2 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x4 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x4 $x2 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x0 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x2 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x4 $x2 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x2 $x4 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x2 $x4 1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4 $x2 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x4 $x4 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x1 $x4 $x4 $x2 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x2 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x0 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x2 $x0 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x2 $x4 $x0 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x4 $x5 $x5 $x4 $x0 $x2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x0 $x2 $x1 $x3 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x0 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x0 $x3 $x1 $x2 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x0 $x3 $x1 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x3 $x0 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x0 $x2 $x3 $x3 $x4 $x4 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x4 $x3 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x1 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x2 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x3 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x4 $x3 $x3 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x0 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x0 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x1 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x1 $x0 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x1 $x2 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x0 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x1 $x3 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x2 $x3 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x0 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x1 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x1 $x0 $x3 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x1 $x3 $x0 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 1 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x4 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x4 $x2 $x4 $x0 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x2 $x3 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x1 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x4 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x2 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x4 $x4 $x1 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x4 $x4 $x2 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x0 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x2 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x4 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x2 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x2 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x2 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x3 $x3 $x2 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x2 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x2 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x2 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x3 $x3 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x2 $x4 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x4 $x4 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x3 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x3 $x4 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x3 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 1 1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x0 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x3 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x0 $x3 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x0 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x2 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x2 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x2 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x3 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x5 $x0 $x4 $x5 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x5 $x2 $x4 $x0 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x5 $x3 $x4 $x0 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x5 $x5 $x4 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x3 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x3 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x0 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x3 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x4 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x4 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x5 $x5 $x0 $x3 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x5 $x5 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x0 $x3 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x2 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x0 $x3 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x2 $x0 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x3 $x4 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x3 $x4 $x4 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x2 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 1 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x0 $x3 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x0 $x3 $x4',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x2 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x0 1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x3 $x4 $x0 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x0 $x4 $x3 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x0 $x4 $x5 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x0 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x3 $x0 $x5 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x5 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x2 $x0 $x4 $x3 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x2 $x0 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x2 $x5 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x0 $x0 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x0 $x1 $x4 $x4 $x2 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x5 $x5 $x3 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x1', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x5 $x5 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x2 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x2 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x2 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x2 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x1',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x5 $x5 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x4 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x1 $x1 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x0 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x5 $x1 $x0 $x4 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x0 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x0 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x1 $x4 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x1 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x3 $x0 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x4 $x4 $x2 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x5 $x2 $x0 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x5 $x2 $x5 $x0 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x0 $x1 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x0 $x2 $x1 $x5 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x0 $x2 $x2 $x5 $x5 $x4 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x0 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x1 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x1 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x1 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x1 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x1 $x3 1 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x0 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x1 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 1 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x0 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x5 $x2 $x0 $x5 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x5 $x2 $x2 $x5 $x0 $x3',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x5 $x2 $x5 $x0 $x4 $x3', '$x0 $x1 $x1 $x2 $x3 $x4 $x5 $x0 $x2 $x3 $x0 $x5 $x4', '$x0 $x1 $x1 $x2 $x3 $x4 $x5 $x0 $x3 $x2 $x0 $x5 $x4',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x5 $x2 $x3 $x0 $x4 $x5 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x2 $x4 $x4 $x0 $x3 2', '$x0 $x1 $x2 $x0 $x1 $x0 $x3 $x3 $x4 $x4 $x0 $x2 2',\
            '$x0 $x1 $x2 $x1 $x0 $x0 $x3 $x3 $x3 $x2 1 $x4 $x4', '$x0 $x1 $x2 $x1 $x0 $x3 $x4 $x4 $x3 $x2 1 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 1 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x1 $x2 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x2 $x3 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x3 $x4 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 1 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x4 $x3 $x2 $x4 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x0 $x4 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x2 $x4 $x0 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x4 $x2 $x4 $x0 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x0 $x2 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x4 $x3',\
            '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x3 $x0 $x3 $x4 $x4 $x2 1 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x1 $x0 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x1 $x0 $x4 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x3 $x1 $x3 $x0 $x4 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x1 $x3 $x0 $x4 $x4 $x5 $x5 $x2', '$x0 $x1 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5 $x2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x2 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x2 $x2 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x3 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x4 $x3 $x4 $x2 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x2 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x3 $x4 $x2 2',\
            '$x0 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x1 $x3 $x4 $x2 $x4 $x5 $x5 $x3 $x0 2', '$x0 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x1 $x4 $x2 2',\
            '$x0 $x1 $x2 $x2 $x0 $x0 $x3 $x3 $x3 $x1 1 $x4 $x4', '$x0 $x1 $x2 $x2 $x0 $x1 $x3 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x0 $x3 $x1 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x1 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x0 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x1 1 $x5 $x5', '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x4 $x2 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x4 $x3 $x2 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x1 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x0 $x3 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x1 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x2 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x2 $x4 $x4 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x3 $x3 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x3 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x4 $x5 $x5 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x4 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x4 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x3 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x0 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x1 $x0 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x2 $x0 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x0 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x1 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x0 $x4 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x0 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x2 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x0 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x1 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x1 $x5 $x5 $x4 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x2 $x3 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x4 $x3', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x1 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x1 $x0 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x1 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x0 $x3 $x2 1 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x0 $x0 $x2 $x3 $x3 $x2 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x0 $x3 $x3 $x2 1 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x2 $x3 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x2 $x3 $x3 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x2 $x4 $x4 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x2 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x3 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x2 $x2 $x0 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x2 $x4 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x3 $x2 $x0 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x4 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x3 $x4 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x3 $x3 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x3 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x2 $x2 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x0 $x3 $x3 $x3 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x1 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x1 $x4 $x3 $x3 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x0 $x4 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x3 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x2 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x0 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x2 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x2 $x0 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x0 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 $x2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x1 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x3 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x4 $x0 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x4 $x0 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x5 $x0 $x4 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x1 $x5 $x4 $x0 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x5 $x5 $x4 $x3',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x2 $x3 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x0 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x2 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x3 $x2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x0 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x0 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x3 $x4 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x3 $x4 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x1 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x0 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x0 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x3 $x1 $x4 $x4 $x0 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x1 1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x0 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x5 $x1 $x0 $x4 $x3 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x5 $x1 $x0 $x4 $x5 $x3', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x5 $x1 $x3 $x0 $x5 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x5 $x1 $x3 $x5 $x0 $x4',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x5 $x3 $x1 $x5 $x0 $x4', '$x0 $x1 $x2 $x2 $x3 $x0 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x0 $x1 $x1 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x0 $x1 $x1 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x0 $x1 $x1 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x1 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x2 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x1 $x1 $x4 $x5 $x5 $x4 $x0 $x3', '$x0 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x2 $x0 $x3 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x1', '$x0 $x1 $x2 $x2 $x3 $x2 $x3 $x0 $x4 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x3 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x3 $x4 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x3 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x2 $x4 $x4 $x0 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x1 $x1 $x0 $x4 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x1 $x1 $x0 $x4 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x1 $x1 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x3 $x1', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x3 $x4 $x4 $x3 $x1',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x4 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x4 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x1 $x3 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x1 $x5 $x4 $x5 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x3 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x2 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x3 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x3 $x5 $x5 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x4 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x5 $x5 $x1 $x4',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x2 $x5 $x5 $x4 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x3 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x3 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x3 $x4 1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x4 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x3 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x3 $x4 1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x0 $x4 $x4 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x1 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x3 $x1 $x0 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x0 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 $x2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 $x3',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5 $x1', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x0 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x4 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x3 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x5 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x5 $x5 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x2 $x3 $x0 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x0 $x1 $x3 $x4 $x5 2',\
            '$x0 $x1 $x2 $x3 $x1 $x0 $x4 $x4 $x4 $x2 $x3 $x5 $x5', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x3 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x5 $x5 $x3 $x4',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x3 $x5 $x5', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x5 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x3 $x5 $x5 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x5 $x5 $x3 $x3', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x4 $x4 $x2 $x0 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x5 $x2 $x2 $x0 $x5 $x4', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x5 $x2 $x5 $x0 $x4 2', '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x5 $x4 $x2 $x0 $x5 $x4',\
            '$x0 $x1 $x2 $x3 $x1 $x3 $x4 $x5 $x4 $x2 $x0 $x5 2', '$x0 $x1 $x2 $x3 $x1 $x4 $x0 $x2 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x3 $x1 $x4 $x4 $x4 $x3 $x0 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x1 $x4 $x5 $x0 $x4 $x3 $x5 $x2 2', '$x0 $x1 $x2 $x3 $x1 $x4 $x5 $x5 $x0 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x2 $x2 $x4 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x2 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x2 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x4 $x4 $x2 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x3 $x3 $x4 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x3 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x2 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x3 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x3 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x2 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x2 $x2 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x2 $x3 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x2 1 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x3 $x3 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x3 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x3 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x3 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x4 $x4 $x2 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x2 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x2 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x2 $x3 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x2 1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x3 $x4 $x4 $x2 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x4 $x2 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x1 $x0 $x4 $x2 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x4 $x4 $x2 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x2 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x3 $x4 $x4 $x2 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x4 $x2 $x2 $x4 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x1 $x4 $x2 $x3 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x4 $x4 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x4 $x4 1 2', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x3 $x3 $x1 $x2 $x0 $x4 $x5 $x5 $x3 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x3 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x2 $x0 $x3 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x3 $x3 $x2 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x1 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x3 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x4 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x1 $x3 $x4 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x3 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x3 $x3 $x3 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5 $x2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x3 $x4 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x0 $x3 $x4 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x3 $x3 $x3 $x1 2',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x1', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x5 $x5 $x4 $x1', '$x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x2 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x1 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x1 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x3 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x3 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x2 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x2 $x4 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x2 $x5 $x5 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x3 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x3 $x4 $x3 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x3 $x5 $x5 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x5 $x5 $x2 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x5 $x5 $x3 $x4',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x3 $x5 $x5 $x2 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x0 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x2 $x4 $x4 $x3 $x0 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x2 $x0 $x3 $x3 $x1 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x2 $x1 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x0 $x2 $x4 $x1 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x0 $x2 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x0 $x2 $x4 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x4 $x1 $x0 $x4 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x5 $x1 $x0 $x5 $x2 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x0 $x1 $x4 $x5 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x0 $x4 $x1 $x5 $x2 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x2 $x0 $x4 $x5 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x4 $x1 $x0 $x5 $x2 $x4', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x4 $x1 $x0 $x5 $x4 $x2',\
            '$x0 $x1 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x0 $x2 $x3 $x5 $x5 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x2 $x3 $x5 $x5 $x1 $x4',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x2 $x3 $x5 $x5 $x4 $x4', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x2 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x2 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x5 $x5 $x4 $x4',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x3 $x2 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x3 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3 $x3 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x1 $x0 $x4 $x2 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x1 $x1 $x5 $x2 $x3 $x5 $x0 $x4',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x0 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x0 $x5 $x5 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x3 $x2 $x5 $x5 $x4 $x0 2',\
            '$x0 $x1 $x2 $x3 $x4 $x1 $x4 $x2 $x5 $x5 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x4 $x3 $x5 $x5 $x2 $x0 2', '$x0 $x1 $x2 $x3 $x4 $x1 $x4 $x5 $x2 $x3 $x5 $x0 2',\
            '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x5 $x4 $x5 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x0 $x5 $x5 $x4 $x1 2',\
            '$x0 $x1 $x2 $x3 $x4 $x2 $x3 $x1 $x5 $x5 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x5 $x5 $x1 $x4', '$x0 $x1 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x5 $x5 $x4 $x1',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x1 $x0 $x3 $x3 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x1 $x2 $x0 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x4 $x2 $x0 $x3 $x3 $x1 $x4 2',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x5 $x1 $x2 $x0 $x5 $x1', '$x0 $x1 $x2 $x3 $x4 $x4 $x3 $x5 $x2 $x2 $x0 $x5 $x1', '$x0 $x1 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x1 $x4 $x2 2',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x2 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x4 $x4 $x4 $x1 $x3 $x0 $x2 $x5 $x5', '$x0 $x1 $x2 $x3 $x4 $x4 $x4 $x2 $x3 $x0 $x1 $x5 $x5',\
            '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x0 $x2 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x0 $x4 $x4',\
            '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x5 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x5 $x5 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x1 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x0 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x1 $x4 $x4', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x2 $x1 $x4 $x4 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x2 $x4 $x4 $x5 $x5 $x5', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x1 $x2 $x4 $x4 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x2 $x4 $x4 2', '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x4 $x4 $x4 2',\
            '$x0 $x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x4 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x0 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x0 $x1 $x2 $x3 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x0 $x0 $x2 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x0 $x2 $x2 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x0 $x2 $x3 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x2 $x2 $x3 $x3 $x4 $x4',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x4 $x4 $x4 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x5 $x5 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x2 $x4 $x3 $x3 $x1 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x2 $x3 $x3 $x1 $x4 2',\
            '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x1 $x2 2', '$x0 $x0 $x1 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x4 $x5 $x5 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x4 $x2 $x0 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x2 $x2 $x5 $x5 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x2 $x5 $x5 $x5 2',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x3 $x2 $x5 $x5 2', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x3 $x4 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x3 $x4 $x4 $x2 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x0 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x1 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x3 $x3 $x2 $x2 $x5 $x5',\
            '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x4 $x5 $x5 $x2 $x3 $x6 $x6', '$x0 $x0 $x1 $x1 $x2 $x3 $x4 $x5 $x5 $x4 $x2 $x3 $x6 $x6', '$x0 $x0 $x1 $x2 $x0 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x1 $x1 $x1 $x3 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x1 $x3 $x3 $x3 $x4 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x4 2', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x1 $x3 $x3 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x2 $x1 $x3 $x4 $x4 $x3 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x2 $x2 $x3 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x2 $x3 $x1 $x4 $x4 $x2 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x5 $x5 $x5 2', '$x0 $x0 $x1 $x2 $x2 $x3 $x1 $x3 $x4 $x4 $x5 $x5 $x6 $x6',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x2 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x5 $x5 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x1 $x4 $x4 $x5 $x5 $x5 2',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x1 $x3 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x2 $x4 $x4 $x1 1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x3 $x3 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x3 $x4 $x3 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x3 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x3 $x3 $x4 $x1 $x5 $x5', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x5 $x5 $x4 $x4 $x3 $x1 2', '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x6 $x5 $x6',\
            '$x0 $x0 $x1 $x2 $x3 $x2 $x3 $x1 $x4 $x4 $x5 $x6 $x6 $x5', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x6 $x5 $x6', '$x0 $x0 $x1 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x6 $x6 $x5',\
            '$x0 $x0 $x1 $x2 $x3 $x4 $x4 $x3 $x5 $x5 $x2 $x1 $x6 $x6', '$x0 $x1 $x0 $x2 $x0 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x1 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x2 $x3 $x1 $x4 $x4 $x2 $x3 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x1 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x0 $x2 $x2 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x1 $x0 $x2 $x2 $x2 $x0 $x1 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x0 $x2 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 2',\
            '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 1 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x2 $x0 $x3 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x4 $x4 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x2 $x3 $x3 $x3 $x4 $x4', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x1 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x2 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 1 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x4 $x2 $x0 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x3 $x2 2',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x1 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x0 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x4 $x4 $x2 $x0 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x4 $x3 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x3 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x0 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x0 $x3 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x2 $x4 $x4 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x1 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x0 $x3 $x3 $x2 $x2 $x4 $x4', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x2 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x3 $x4 $x4 $x5 $x5 $x6 $x6',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x4 $x4 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x3 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x4 $x4 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x4 $x4 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x3 $x4 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x3 $x5 $x5 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x0 $x4 $x4 $x5 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x4 $x4 $x5 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x4 $x4 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x5 $x5 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x5 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x0 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x3 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x3 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x3 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x3 $x5 $x5 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x3 $x5 $x5 1 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x4 $x3 $x3 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x1 $x3 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x2 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x3 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 1 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x3 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x5 $x5 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x1 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x3 $x5 $x5 2',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x5 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x5 $x0 $x3 $x4 $x6 $x6', '$x0 $x1 $x1 $x2 $x3 $x1 $x0 $x4 $x3 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x1 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x2 2', '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x4 $x5 $x5 1 2',\
            '$x0 $x1 $x1 $x2 $x3 $x2 $x3 $x0 $x4 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x0 $x0 $x3 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x2 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 1 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x2 $x0 $x4 $x5 $x5 $x1 $x4 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x0 $x4 $x4 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x0 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x0 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x0 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x2 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x2 $x1 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x5 $x5 $x2 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x4 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x5 $x4 $x4 $x2 $x5 2',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x2 $x6 $x6', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x2 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x1 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x2 $x5 $x5 2', '$x0 $x1 $x1 $x2 $x3 $x4 $x5 $x0 $x2 $x3 $x6 $x6 $x5 $x4',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x0 $x3 $x2 $x4 $x4 1 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x2 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x2 $x1 $x1 $x3 $x3 $x0 $x4 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x1 $x3 $x1 $x3 $x0 $x4 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x1 $x3 $x0 $x4 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x0 $x0 $x4 $x4 $x3 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x1 $x0 $x4 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x3 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x4 1 $x5 $x5',\
            '$x0 $x1 $x2 $x1 $x3 $x3 $x3 $x0 $x4 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x1 $x3 $x3 $x4 $x0 $x5 $x5 $x4 $x2 $x6 $x6', '$x0 $x1 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x2 $x6 $x6',\
            '$x0 $x1 $x2 $x1 $x3 $x4 $x4 $x0 $x5 $x5 $x3 $x2 $x6 $x6', '$x0 $x1 $x2 $x2 $x0 $x3 $x3 $x3 $x1 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x0 $x3 $x4 $x4 $x5 $x5 $x3 $x1 $x6 $x6',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x1 $x0 $x3 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x3 $x5 $x5 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x3 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x3 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x4 $x5 $x5 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x0 $x4 $x4 $x5 $x5 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x2 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x0 $x3 $x4 $x4 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x3 $x3 $x0 $x2 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x1 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x2 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x2 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x3 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x3 2',\
            '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x6 $x6', '$x0 $x1 $x2 $x2 $x1 $x3 $x4 $x0 $x4 $x5 $x5 $x3 1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x2 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x1 $x0 $x3 $x3 $x4 $x4 1 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x0 $x4 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x3 $x4 $x4 $x3 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x4 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x1 $x4 $x5 $x5 $x4 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x3 $x5 $x5 $x1 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x0 $x4 $x4 $x5 $x5 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x2 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x3 $x1 $x4 $x4', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x0 $x3 $x3 $x4 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x1 $x3 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x1 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x1 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x3 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x1 $x3 2',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x0 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x4 $x2 $x5 $x5 $x3 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x4 $x3 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x4 $x4 $x5 $x5 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x1 $x0 $x4 $x4 $x5 $x5 $x3 $x2 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x0 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x3 $x4 $x4 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x3 $x4 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x4 $x6 $x6', '$x0 $x1 $x2 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x2 $x3 2', '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x5 $x5 $x4 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x5 $x5 $x4 $x1 $x6 $x6', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x0 $x4 $x4 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x1 $x5 $x5 $x3 $x4 $x6 $x6',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x0 $x4 $x5 $x5 $x3 $x1 $x6 $x6', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 $x4 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x3 $x5 $x5 1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x1 $x5 $x5 $x3 1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x2 $x5 $x5 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x3 $x3 $x5 $x5 $x1 2',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x3 $x1 $x5 $x5', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x3 $x5 $x5 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x4 $x3 $x1 $x5 $x5',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x4 $x0 $x4 $x5 $x5 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x0 $x4 $x5 $x5 $x3 $x1 2', '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x0 $x5 $x3 $x4 $x1 $x6 $x6',\
            '$x0 $x1 $x2 $x2 $x3 $x4 $x5 $x1 $x4 $x5 $x5 $x3 $x0 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x3 $x5 $x5 $x1 $x4 2', '$x0 $x1 $x2 $x3 $x1 $x2 $x0 $x4 $x4 $x5 $x5 $x1 $x3 2',\
            '$x0 $x1 $x2 $x3 $x2 $x4 $x0 $x1 $x5 $x5 $x4 $x3 $x6 $x6', '$x0 $x1 $x2 $x3 $x3 $x0 $x1 $x0 $x4 $x4 $x2 1 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x4 $x3 $x2 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x3 $x0 $x1 $x4 $x5 $x5 $x2 $x4 2', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x4 $x4 $x2 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x3 $x1 $x0 $x4 $x5 $x5 $x2 $x4 2',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x2 $x2 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x2 $x3 $x4 $x5 $x5', '$x0 $x1 $x2 $x3 $x3 $x4 $x1 $x0 $x2 $x3 $x3 $x4 $x5 $x5',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x0 $x1 $x5 $x5 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x4 $x0 $x2 $x5 $x5 $x4 $x1 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x0 $x2 $x5 $x4 $x1 $x6 $x6',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x1 $x0 $x5 $x5 $x4 $x2 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x1 $x2 $x5 $x5 $x4 $x0 2', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x5 $x0 $x4 $x1 $x2 $x6 $x6',\
            '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x5 $x1 $x0 $x4 $x2 $x6 $x6', '$x0 $x1 $x2 $x3 $x3 $x4 $x5 $x5 $x2 $x0 $x4 $x1 $x6 $x6', '$x0 $x1 $x2 $x3 $x4 $x4 $x4 $x0 $x3 $x2 $x5 $x5 $x1 2',\
            '$x0 $x1 $x2 $x3 $x4 $x4 $x5 $x0 $x3 $x2 $x6 $x6 $x5 $x1', '$x0 $x0 $x1 $x1 $x1 $x2 $x3 $x3 $x3 $x2 $x1 $x4 $x4 $x5 $x5', '$x0 $x0 $x1 $x1 $x2 $x3 $x3 $x4 $x4 $x3 $x2 $x5 $x5 $x6 $x6',\
            '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x4 $x4 $x1 $x3 $x5 $x5 $x6 $x6', '$x0 $x0 $x1 $x2 $x2 $x3 $x4 $x5 $x5 $x4 $x4 $x3 $x1 $x6 $x6', '$x0 $x1 $x0 $x2 $x2 $x3 $x4 $x4 $x4 $x1 $x3 $x5 $x5 $x6 $x6',\
            '$x0 $x1 $x1 $x2 $x2 $x3 $x3 $x4 $x0 $x4 $x5 $x5 $x6 $x6 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x0 $x5 $x4 $x6 $x6 $x3 $x5 2', '$x0 $x1 $x1 $x2 $x2 $x3 $x4 $x5 $x0 $x5 $x4 $x3 $x6 $x6 2',\
            '$x0 $x1 $x1 $x2 $x3 $x3 $x4 $x0 $x5 $x4 $x6 $x6 $x2 $x5 2']

    try_assertion = False
    try:
        LOGGER.info("Result size: %s in time: %s", str(len(result)), str(time.time() - in_time))
        try_assertion = True
    except Exception:
        pass
    if try_assertion:
        assert set(sample_result) == set(result)

    LOGGER.info('Finished test 99 for pattern_type_split_discovery')
