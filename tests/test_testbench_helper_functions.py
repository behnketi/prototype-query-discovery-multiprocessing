"""Contains tests for functions in testbench_helper_functions.py"""
import sys
import logging
import os
sys.path.append(".")
from testbench_helper_functions import _generate_experiments, _dataset_to_sample

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

PARAMS_SAMPLE_SIZE = [25,50,100]#,100,150,200,250,300,350,400,450,500]
PARAMS_TRACE_LENGTH = [(10,50),(50,100)]#,(100,150),(150,200)]
PARAMS_TYPE_LENGTH = 1
PARAMS_DIMENSIONS = [1,2,3]#,4,5]

def test_generate_experiments_from_helper():
    """
        Test for funtion _generate_experiments() which should generate a
        directory 'samples' and samples for all given values for sample_size,
        trace_length and type_length.
    """
    LOGGER.info('Started test test_generate_experiments')
    _generate_experiments(params_sample_size=PARAMS_SAMPLE_SIZE, params_trace_length=PARAMS_TRACE_LENGTH,params_type_length=PARAMS_TYPE_LENGTH,params_dimension=PARAMS_DIMENSIONS)
    _generate_experiments(convert_datasets=True)
    _generate_experiments(convert_datasets=True,path_to_file='datasets/google/GoogleTraces_BTW23_using_ILMiner_Queries/google_query1a.txt.gz')
    LOGGER.info('Finished test test_generate_experiments')

def test_dataset_to_sample():
    """
        Test for function _dataset_to_sample()
    """
    LOGGER.info('Started test test_dataset_to_sample')
    if os.path.exists('datasets'):
        for root, dirs, files in os.walk('./'):
            for file in files:
                if file.startswith('dataset'):
                    _dataset_to_sample(str(file))
    LOGGER.info('Finished test test_dataset_to_sample')
