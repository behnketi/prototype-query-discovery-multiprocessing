#!/usr/bin/python3
"""Contains tests for one dimensional discovery"""
import sys
import time
import logging
sys.path.append(".")

from discovery_bu_pattern_type_split import bu_pattern_type_split_discovery
from discovery_bottom_up import bottom_up_discovery
from sample import Sample

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)



def test_discovery_bu_onedim():
    """Comparing runtime of one-dimensional discovery on different samples.
    """
    samples = []
    sample_set1 = ["a" , "a" ]
    samples.append(sample_set1)

    sample_set2 = ["a a", "b b" ]
    samples.append(sample_set2)
    samples.append(["b a a c d d b a b b", "b a a b c d d a c c b b", "a a b c b a c d b a", "c a a b d b c c d d b a", "b a c d d b b a c d d b"])
    samples.append(["a b c d e f g h i j", "a b c d e f g h i j" ])
    samples.append(["a b c d e f g h i j k", "k j i h g f e d c b a" ])
    samples.append(["a b c a b c", "a b d a b c" ])
    samples.append(["a a a a a a a a", "b b b b b b b" ])
    samples.append(["b a a b a b b", "b a a b b b", "a a b b a"])
    samples.append(["a b b a", "a b b a a b b a a b b a a b b a a b b a a b b a" ])
    samples.append(["a b b a", "a b b a"])
    samples.append(["a a b b", "a a b b"])
    samples.append(["a a b b", "b b a a"])
    samples.append(["a a b b", "b a a b"])
    samples.append(['2 2 2 1 1 1 1 1 1 3', '4 2 2 2 1 1 1 1 2 2', '1 1 1 1 1 3 4 2 2 2', '1 3 1 2 2 2 2 1 2 2'])
    #samples.append(['5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 1 1 2 2','1 1 1 3 1 1 1 1 3 4 1 1 4 1 4 1 1 1 2 2','4 3 1 3 1 1 1 3 4 1 1 1 1 1 1 1 1 1 1 2','2 1 2 2 2 2 2 1 1 1 1 1 1 2 2 2 2 1 2 2','2 2 1 2 2 2 2 2 1 1 1 1 1 1 2 2 2 2 1 2','1 1 1 4 2 2 2 1 1 1 1 1 2 2 2 1 4 2 1 2','3 4 1 1 5 1 1 1 1 4 1 1 4 4 1 1 2 2 2 2','2 2 1 1 1 1 1 1 3 4 2 2 2 1 1 1 1 2 2 2','2 2 2 1 1 1 1 1 1 3 4 2 2 2 1 1 1 1 2 2','1 1 1 1 1 3 4 2 2 2 1 3 1 2 2 2 2 1 2 2'])

    supp = 1.0
    for sample_set in samples:
        LOGGER.info(sample_set)
        sample= Sample()
        sample.set_sample(sample_set)
        sample.set_sample_typeset()
        querysets= []
        for matching in ['smarter', 'type-pattern']:
            start= time.time()
            if matching in [ 'finditer', 'regex']:
                queryset = set(bottom_up_discovery(sample= sample, supp=supp, matchtest = matching))
            elif matching == 'smarter':
                result_dict = bottom_up_discovery(sample = sample, supp = supp, matchtest = matching)
                queryset = set(result_dict['queryset'])
            else:
                queryset = set(bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first', use_smart_matching=True)[0])
            querysets.append(queryset)
            result = time.time() - start

            LOGGER.info('%s: %i', matching, len(queryset))
            LOGGER.info('%s: %f', matching, result)
            LOGGER.info(queryset)
        if len(querysets) >= 2:
            diff1 = querysets[0]-querysets[1]
            diff2 = querysets[1]-querysets[0]
            if len(diff1) != 0:
                LOGGER.info( diff1)
                #LOGGER.info( querysets[0] & querysets[1])

            if len(diff2) != 0:
                LOGGER.info( diff2)
            #    LOGGER.info( querysets[0] & querysets[1])
        else:
            LOGGER.info( querysets[0])
def test16_pattern_type_split_discovery():
    sample = Sample(['M o o d b E G y e x s M J D C K L h a t', 'f u u A c n q H F g m f v p z i r I N k'])
    supp = 1.0
    stats = []
    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, use_smart_matching=True,
                                              discovery_order='type_first', find_all_matching_queries = True)[0]
    result_bs = bottom_up_discovery(sample = sample, supp = supp, matchtest = 'smarter')['matchingset'].keys()
    assert result - {''} == set(result_bs)

def test_statistics():
    """Testing statistics for different samples.
    """
    samples = []
    sample_set1 = ["a" , "a" ]
    samples.append(sample_set1)

    sample_set2 = ["a a", "b b" ]
    samples.append(sample_set2)
    samples.append(["b a a c d d b a b b", "b a a b c d d a c c b b", "a a b c b a c d b a", "c a a b d b c c d d b a", "b a c d d b b a c d d b"])
    samples.append(["a b c d e f g h i j k", "a b c d e f g h i j k" ])
    samples.append(["a b c d e f g h i j k", "k j i h g f e d c b a" ])
    samples.append(["a b c a b c", "a b d a b c" ])
    samples.append(["a a a a a a a a", "b b b b b" ])
    samples.append(["b a a b a b b", "b a a b b b", "a a b b a"])
    samples.append(["a b b a", "a b b a a b b a a b b a a b b a a b b a a b b a" ])
    samples.append(["a b b a", "a b b a"])
    samples.append(["a a b b", "a a b b"])
    samples.append(["a a b b", "b b a a"])
    samples.append(["a a b b", "b a a b"])
    supp=1.0
    for sample_set in samples:
        LOGGER.info(sample_set)
        sample= Sample()
        sample.set_sample(sample_set)
        sample.set_sample_typeset()
        result_dict = bottom_up_discovery(sample=sample, supp=supp, matchtest='smarter', statistics= True )
        stats = result_dict['statistics']
        for item in stats.items():
            LOGGER.info( item)

if __name__ == "__main__":
    test_discovery_bu_onedim()
    #test_statistics()
