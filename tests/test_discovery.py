#!/usr/bin/python3
"""Contains tests for discorvery.py file"""
import sys
import logging
sys.path.append(".")
from discovery import combine_all,_find_next_position_in_query_string, _find_next_position_in_query_string_multidim, _extract_var_pre_suf, _extract_var_pre_suf_multidim, _find_attribute_index
from error import ShinoharaInvalidPositionError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_find_next_position_in_query_string():
    """
        Test for function _find_next_position_in_query_string().
    """
    LOGGER.info('Started test for _find_next_position_in_query_string')
    try:
        LOGGER.info(_find_next_position_in_query_string("$A $B $C $D $EFG", 0))
    except ShinoharaInvalidPositionError:
        pass
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 1) == 0
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 2) == 3
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 3) == 6
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 4) == 9
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 5) == 12
    assert _find_next_position_in_query_string("$A $B $C $D $EFG $H", 6) == 17
    try:
        LOGGER.info(_find_next_position_in_query_string("$A $B $C $D $EFG", 7))
    except ShinoharaInvalidPositionError:
        pass

    LOGGER.info('Finished test for _find_next_position_in_query_string')

def test_find_next_position_in_query_string_multidim():
    """
        Test for function _find_next_position_in_query_string_multidim().
    """
    LOGGER.info('Started test for _find_next_position_in_query_string_multidim')
    try:
        LOGGER.info(_find_next_position_in_query_string("$A;$B; $C;$D; $EFG;$H; $I;", 0))
    except ShinoharaInvalidPositionError:
        pass
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 1) == 0
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 2) == 3
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 3) == 7
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 4) == 10
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 5) == 14
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 6) == 19
    assert _find_next_position_in_query_string_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 7) == 23
    try:
        LOGGER.info(_find_next_position_in_query_string("$A;$B; $C;$D; $EFG;$H; $I;", 24))
    except ShinoharaInvalidPositionError:
        pass

    LOGGER.info('Finished test for _find_next_position_in_query_string_multidim')

def test_extract_var_pre_suf():
    """
        Test for function _extract_var_pre_suf().
    """
    LOGGER.info('Started test for _extract_var_pre_suf')
    try:
        _extract_var_pre_suf("$A $B $C $D $EFG", 0)
    except ShinoharaInvalidPositionError:
        pass
    try:
        _extract_var_pre_suf("$A $B $C $D $EFG", 1)
    except ShinoharaInvalidPositionError:
        pass

    # Format: (current_variable, current_variable_and_suffix, prefix, suffix)
    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 0)
    assert quadtrupel[0] == '$A'
    assert quadtrupel[1] == '$A $B $C $D $EFG $H'
    assert quadtrupel[2] == ''
    assert quadtrupel[3] == '$B $C $D $EFG $H'

    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 3)
    assert quadtrupel[0] == '$B'
    assert quadtrupel[1] == '$B $C $D $EFG $H'
    assert quadtrupel[2] == '$A'
    assert quadtrupel[3] == ' $C $D $EFG $H'

    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 6)
    assert quadtrupel[0] == '$C'
    assert quadtrupel[1] == '$C $D $EFG $H'
    assert quadtrupel[2] == '$A $B'
    assert quadtrupel[3] == ' $D $EFG $H'

    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 9)
    assert quadtrupel[0] == '$D'
    assert quadtrupel[1] == '$D $EFG $H'
    assert quadtrupel[2] == '$A $B $C'
    assert quadtrupel[3] == ' $EFG $H'

    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 12)
    assert quadtrupel[0] == '$EFG'
    assert quadtrupel[1] == '$EFG $H'
    assert quadtrupel[2] == '$A $B $C $D'
    assert quadtrupel[3] == ' $H'

    quadtrupel = _extract_var_pre_suf("$A $B $C $D $EFG $H", 17)
    assert quadtrupel[0] == '$H'
    assert quadtrupel[1] == '$H'
    assert quadtrupel[2] == '$A $B $C $D $EFG'
    assert quadtrupel[3] == ''

    try:
        _find_next_position_in_query_string("$A $B $C $D $EFG", 7)
    except ShinoharaInvalidPositionError:
        pass

    LOGGER.info('Finished test for _extract_var_pre_suf')

def test_extract_var_pre_suf_multidim():
    """
        Test for function _extract_var_pre_suf_multidim().
    """
    LOGGER.info('Started test for _extract_var_pre_suf_multidim')
    try:
        _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", -1)
    except ShinoharaInvalidPositionError:
        pass
    try:
        _extract_var_pre_suf("$A;$B; $C;$D; $EFG;$H; $I;", 1)
    except ShinoharaInvalidPositionError:
        pass

    # Format: (current_variable, current_variable_and_suffix, prefix, suffix)
    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 0)
    assert quadtrupel[0] == '$A;'
    assert quadtrupel[1] == '$A;$B; $C;$D; $EFG;$H; $I;'
    assert quadtrupel[2] == ''
    assert quadtrupel[3] == '$B; $C;$D; $EFG;$H; $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 3)
    assert quadtrupel[0] == '$B;'
    assert quadtrupel[1] == '$B; $C;$D; $EFG;$H; $I;'
    assert quadtrupel[2] == '$A;'
    assert quadtrupel[3] == ' $C;$D; $EFG;$H; $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 7)
    assert quadtrupel[0] == '$C;'
    assert quadtrupel[1] == '$C;$D; $EFG;$H; $I;'
    assert quadtrupel[2] == '$A;$B; '
    assert quadtrupel[3] == '$D; $EFG;$H; $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 10)
    assert quadtrupel[0] == '$D;'
    assert quadtrupel[1] == '$D; $EFG;$H; $I;'
    assert quadtrupel[2] == '$A;$B; $C;'
    assert quadtrupel[3] == ' $EFG;$H; $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 14)
    assert quadtrupel[0] == '$EFG;'
    assert quadtrupel[1] == '$EFG;$H; $I;'
    assert quadtrupel[2] == '$A;$B; $C;$D; '
    assert quadtrupel[3] == '$H; $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 19)
    assert quadtrupel[0] == '$H;'
    assert quadtrupel[1] == '$H; $I;'
    assert quadtrupel[2] == '$A;$B; $C;$D; $EFG;'
    assert quadtrupel[3] == ' $I;'

    quadtrupel = _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 23)
    assert quadtrupel[0] == '$I;'
    assert quadtrupel[1] == '$I;'
    assert quadtrupel[2] == '$A;$B; $C;$D; $EFG;$H; '
    assert quadtrupel[3] == ''

    try:
        _extract_var_pre_suf_multidim("$A;$B; $C;$D; $EFG;$H; $I;", 24)
    except ShinoharaInvalidPositionError:
        pass

    LOGGER.info('Finished test for _extract_var_pre_suf_multidim')

def test_find_attribute_index():
    """
        Test for function _find_attribute_index().
    """
    LOGGER.info('Started test for _find_attribute_index')
    assert _find_attribute_index("$A $B $C $D $EFG", 1) == 0
    assert _find_attribute_index("$A $B $C $D $EFG", 2) == 0
    assert _find_attribute_index("$A $B $C $D $EFG", 3) == 0
    assert _find_attribute_index("$A $B $C $D $EFG", 4) == 0
    assert _find_attribute_index("$A $B $C $D $EFG", 5) == 0

    try:
        _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 0)
    except ShinoharaInvalidPositionError:
        pass

    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 1) == 0
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 2) == 1
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 3) == 2
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 4) == 3
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 5) == 0
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 6) == 1
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 7) == 2
    assert _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 8) == 3

    try:
        _find_attribute_index("$A;$B;$C;$D; $EFG;$H;$I;$J;", 9)
    except ShinoharaInvalidPositionError:
        pass

    LOGGER.info('Finished test for _find_attribute_index')

def test00_combine_all():
    """
        Testing 'combine_all': small running example
    """
    LOGGER.info('Started test 00 for combine_all')

    iteratable1 = ['0','1']
    iteratable2 = ['a','b']

    result = [['0','1','a','b'],['0','a','1','b'],['0','a','b','1'],['a','0','1','b'],['a','0','b','1'],['a','b','0','1']]

    assert sorted(result) == sorted(combine_all(iteratable1, iteratable2))

    LOGGER.info('Finished test 00 for combine_all')
