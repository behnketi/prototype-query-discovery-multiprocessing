#!/usr/bin/python3
"""Contains tests for functions in discovery_bottom_up.py"""
import sys
sys.path.append(".")

from sample import Sample
from query import Query
from discovery_bottom_up import _next_queries, _update_dictionary, bottom_up_discovery



def test_next_queries():
    """Test Function of _next_queries
    """
    alphabet=['a', 'b']
    query1=Query()
    query2=Query()
    query2.set_query_string('a')
    query3=Query()
    query3.set_query_string('$x0 $x0')
    query4=Query()
    query4.set_query_string('$x0 a $x0 b')
    query5=Query()
    query5.set_query_string('$x0 a $x1 $x0 $x1')

    min_trace_length = 10
    print(_next_queries(query1, alphabet, min_trace_length))
    print(_next_queries(query2, alphabet, min_trace_length))
    print(_next_queries(query3, alphabet, min_trace_length))
    print(_next_queries(query4, alphabet, min_trace_length))
    print(_next_queries(query5, alphabet, min_trace_length))


def test_update_dictionary():
    """Testing the function _update_dictionary(query, matching, dictionary)
    """
    dictionary1= dict()
    dictionary3= dict()
    dictionary4= dict()
    matching=True

    query1=Query()
    query2=Query()
    query2.set_query_string('a')
    query3=Query()
    query3.set_query_string('$x0 $x0')
    query4=Query()
    query4.set_query_string('$x0 a $x0')
    query5=Query()
    query5.set_query_string('$x0 a $x1 $x0 $x1')
    query6=Query()
    query6.set_query_string('$x0 $x1 a $x1 $x0')
    query7=Query()
    query7.set_query_string('$x0 $x1 $x1 $x0')
    parent_dict = {}
    parent_dict[''] = query1
    parent_dict['a'] = query1
    parent_dict['$x0 $x0']= query1
    parent_dict['$x0 a $x0'] = query3
    parent_dict['$x0 a $x1 $x0 $x1'] = query4
    parent_dict['$x0 $x1 a $x1 $x0']= query7
    parent_dict['$x0 $x1 $x1 $x0'] = query3

    _update_dictionary(query1, matching, dictionary1, parent_dict=parent_dict)
    assert dictionary1=={'': True}


    _update_dictionary(query3, matching, dictionary3, parent_dict=parent_dict)
    assert ('$x0 $x0' in dictionary3) is True
    assert ('' in dictionary3) is False

    _update_dictionary(query4, matching, dictionary4, parent_dict=parent_dict)
    assert ('$x0 a $x0' in dictionary4) is True


def test_discovery_bottom_up():
    """Test function of discovery_bottom_up
    """
    sample=Sample()

    sample.set_sample(["b a a b a b b", "b a a b b b", "a a b b a"])
    sample.set_sample_typeset()

    result_dict=bottom_up_discovery(sample, supp=1., matchtest='smarter')
    queryset = result_dict['queryset']
    assert queryset== {'$x0 $x0 b $x0', '$x0 a b b $x0', 'a $x0 b $x0', 'a a b b', 'b a'}
    
    testsample = Sample()
    testsample.set_sample(["a a a a", "a b b b"])
    testsample.set_sample_typeset()
    result_dict = bottom_up_discovery(testsample, supp=1., matchtest='smarter')
    queryset = result_dict['queryset']
    assert queryset == {'a $x0 $x0 $x0'}

    testsample = Sample()
    testsample.set_sample(["b b a a", "a a b b"])
    testsample.set_sample_typeset()
    result_dict = bottom_up_discovery(testsample, supp=1., matchtest='smarter')
    queryset = result_dict['queryset']
    assert queryset == {'$x0 $x0 $x1 $x1', 'a a', 'b b'}


    testsample = Sample()
    testsample.set_sample(["b a b a", "a b a b"])
    testsample.set_sample_typeset()
    result_dict = bottom_up_discovery(testsample, supp=1., matchtest='smarter')
    queryset = result_dict['queryset']
    assert queryset == {'$x0 $x1 $x0 $x1', 'a b a', 'b a b'}

def test_discovery_bottom_up_01():
    testsample = Sample()
    testsample.set_sample(['e m k e p e o e q q', 'a d g a b a j a s r'])
    testsample.set_sample_typeset()
    result_dict = bottom_up_discovery(testsample, supp=1., matchtest='smarter')
    queryset = result_dict['queryset']
    assert result_dict['queryset'] == {'$x0 $x0 $x0 $x0'}