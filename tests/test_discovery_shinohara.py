#!/usr/bin/python3
"""Contains tests for functions in discovery_shinohara.py"""
import re
import sys
sys.path.append(".")

import logging
from sample import Sample
from query import Query
from discovery_shinohara import shinohara_discovery_icdt, _find_unvisited_variables

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_shinohara_discovery_icdt():
    """
        Test for function shinohara_discovery_icdt.
    """
    LOGGER.info('Started test test_shinohara_discovery_icdt')
    sample0 = Sample(given_sample=["aa cc cc bb", "aa ee ee bb", "aa dd KK dd bb", "aa NN ee ee bb"])
    supp = 1.0

    sample_list = ' '.join(sample0._sample)
    typeset = ' '.join(sample0._sample_typeset)
    LOGGER.info("**********************************************")
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample typeset:           %s',typeset)
    LOGGER.info('Requested support:         %s',str(supp))
    LOGGER.info("**********************************************")

    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with different configurations of select_position and select_operation')
    query01 = Query(given_query_string="$A $B $C $D")
    query01.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query01.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query01, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query01._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           random')
    LOGGER.info('select_operation:          random')
    LOGGER.info('Mined query01 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query02 = Query(given_query_string="$A $B $C $D")
    query02.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query02.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query02, sample0, supp,
                    select_position='left_to_right',
                    select_operation='type')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query02._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           left_to_right')
    LOGGER.info('select_operation:          type')
    LOGGER.info('Mined query02 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    query03 = Query(given_query_string="$A $B $C $D")
    query03.set_query_string_length()
    query03.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query03.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query03, sample0, supp,
                    select_position='right_to_left',
                    select_operation='var')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query03._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query03 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with descriptive query')
    query04 = Query(given_query_string="aa $C $C bb")
    query04.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query04.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query04, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query04._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query04 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query containing types')
    query05 = Query(given_query_string="aa $B $C $D")
    query05.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query05.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query05, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query05._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query05 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with repeated and non-repeated variables')
    query06 = Query(given_query_string="$A $B $B $D")
    query06.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query06.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query06, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert re.match(r"aa\s((\$[B]\s){2}|(\$[C]\s){2})bb", query06._query_string) is not None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query06 string:      %s',result[0]._query_string)
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")

    ###########################################################################################
    LOGGER.info('test_shinohara_discovery_icdt: Test algorithm with query which does not satisfy the support threshold')
    query07 = Query(given_query_string="aa $B bb $D")
    query07.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(0,1),(-1,-1)])
    query07.set_query_sample(sample0)

    result = shinohara_discovery_icdt(
                    query07, sample0, supp,
                    select_position='random',
                    select_operation='random')
    assert result[0] is None
    LOGGER.info("**********************************************")
    LOGGER.info('select_position:           right_to_left')
    LOGGER.info('select_operation:          var')
    LOGGER.info('Mined query07 string:      None')
    LOGGER.info(result[1])
    LOGGER.info("**********************************************")
    LOGGER.info('Finished test test_shinohara_discovery_icdt')

def test_find_unvisited_variables():
    """
        Test for function _find_unvisited_variables()
    """
    LOGGER.info('Started test test_find_unvisited_variables')
    query0 = Query(given_query_string="a $x $y a")
    assert _find_unvisited_variables(query0) == {'x': [2], 'y': [3]}
    query1 = Query(given_query_string="a $x $y")
    assert _find_unvisited_variables(query1) == {'x': [2], 'y': [3]}
    query2 = Query(given_query_string="$x $y $x")
    assert _find_unvisited_variables(query2) == {'x': [1,3], 'y': [2]}
    query3 = Query(given_query_string="$x $y $y")
    assert _find_unvisited_variables(query3) == {'x': [1], 'y': [2,3]}
    query4 = Query(given_query_string="$x $y $z")
    assert _find_unvisited_variables(query4) == {'x': [1], 'y': [2], 'z': [3]}
    LOGGER.info('Finished test test_find_unvisited_variables')
