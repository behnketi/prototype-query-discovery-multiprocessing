#!/usr/bin/python3
"""Contains tests for functions in discovery.py"""
import sys
sys.path.append(".")
import logging
#import time
from math import ceil
from sample_multidim import MultidimSample
#from query import Query
from error import EmptySampleError, InvalidQuerySupportError
from discovery_bu_pts_multidim import _build_type_tree_multidim, _build_pattern_tree_multidim,\
        _build_mixed_query_tree_multidim, discovery_bu_pts_multidim,\
        _syntactically_contained_event, _syntactically_contained, _find_descriptive_querystrings#, _search_var_smart
#from hyper_linked_tree import HyperLinkedTree

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


#####
#
# Tests for function _build_type_tree.
#
def test00__build_type_tree():
    """
        Testing '_build_type_tree': check for detecting "no frequent item exists"
    """
    LOGGER.info('Started test 00 for _build_type_tree')

    sample = MultidimSample(["a;", "b;"])
    supp = 1.0
    stats = []

    # 1. ""
    # 2. -

    sample_result = {''}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 00 for _build_type_tree')

def test01__build_type_tree():
    """
        Testing '_build_type_tree': small running example
    """
    LOGGER.info('Started test 01 for _build_type_tree')

    sample = MultidimSample(["aa;xx; cc;w; cc;w; bb;yy;", "aa;xx; ee;vv; ee;vv; bb;yy;", "aa;xx; dd;rr; KK;ss; dd;rr; bb;yy;", "aa;xx; NN;tt; ee;vv; ee;vv; bb;yy;"])
    supp = 1.0
    stats = []

    # 1. "", aa;;, bb;;, ;xx;, ;yy;
    # 2. aa;; bb;;, aa;xx;, aa;; ;yy;, ;xx; bb;;, bb;yy;, ;xx; ;yy;
    # 3. aa;xx; bb;; aa;; bb;yy; aa;xx; ;yy;, ;xx; bb;yy;
    # 4. aa;xx; bb;yy;
    # 5. -

    sample_result = {'','aa;;', 'bb;;',';xx;', ';yy;',\
            'aa;; bb;;', 'aa;xx;', 'aa;; ;yy;', ';xx; bb;;', 'bb;yy;', ';xx; ;yy;',\
            'aa;xx; bb;;', 'aa;; bb;yy;', 'aa;xx; ;yy;', ';xx; bb;yy;', \
            'aa;xx; bb;yy;'}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 01 for _build_type_tree')

def test02__build_type_tree():
    """
        Testing '_build_type_tree': small running example
    """
    LOGGER.info('Started test 02 for _build_type_tree')

    sample = MultidimSample(["aa;xx; bb;w; cc;a; aa;yy;",\
            "aa;yy; aa;xx; bb;x; bb;b; bb;c; aa;yy;",\
            "aa;xx; dd;d; KK;e; dd;f; bb;y; cc;g; aa;yy;",\
            "aa;xx; bb;z; aa;yy;"])
    supp = 1.0
    stats = []

    # 1. '', aa;;, bb;;, ;xx;, ;yy;
    # 2. aa;; aa;;, aa;; bb;;, aa;xx; aa;; ;yy;, bb;; aa;;, bb;; ;yy;, ;xx; aa;;, ;xx; ;yy;
    # 3. aa;; bb;; aa;;, aa;xx; bb;;, aa;xx; aa;;, aa;xx; ;yy;, bb;; aa;yy;, ;xx; bb;; aa;;, ;xx; aa;yy;
    # 4. aa;; bb;; aa;yy;, aa;xx; bb;; aa;;, aa;xx; aa;yy;, ;xx; bb;; aa;yy;, aa;xx; bb;; ;yy;
    # 5. aa;xx; bb;; aa;yy;
    # 6. -


    sample_result = {\
            '', 'aa;;', 'bb;;', ';xx;', ';yy;',\
            'aa;; aa;;', 'aa;; bb;;', 'aa;xx;', 'aa;; ;yy;', 'aa;yy;', 'bb;; aa;;', 'bb;; ;yy;', ';xx; aa;;', ';xx; bb;;', ';xx; ;yy;',\
            'aa;; bb;; aa;;', 'aa;xx; bb;;', 'aa;xx; aa;;', 'aa;; aa;yy;', 'aa;; bb;; ;yy;', ';xx; bb;; ;yy;', 'aa;xx; ;yy;', 'bb;; aa;yy;', ';xx; bb;; aa;;', ';xx; aa;yy;',\
            'aa;; bb;; aa;yy;', 'aa;xx; bb;; aa;;', 'aa;xx; aa;yy;', ';xx; bb;; aa;yy;', 'aa;xx; bb;; ;yy;',\
            'aa;xx; bb;; aa;yy;'}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp)

    sample = MultidimSample([\
            "a;m;x; b;m;x; b;n;z;",\
            "a;o;x; b;m;x; a;n;y;",\
            "b;o;z; b;m;x; b;n;z;"])
    supp = 1.0
    stats = []

    # 1. '', b;;;, ;m;;, ;n;;, ;;x;
    # 2. b;m;;, b;;x;, ;m;x;, b;; ;n;;, ;;x; ;n;;, ;;x; b;;
    # 3. b;m;x;, b;m;; ;n;;, b;;x; ;n;;, ;m;x; ;n;;
    # 4. b;m;x; ;n;;

    sample_result = {\
            '',"b;;;", ";m;;", ";n;;", ";;x;",\
            "b;m;;", "b;;x;", ";m;; ;n;;", ";m;x;", "b;;; ;n;;", ";;x; ;n;;", ";;x; b;;;",\
            "b;m;x;", "b;m;; ;n;;", "b;;x; ;n;;", ";m;x; ;n;;",\
            "b;m;x; ;n;;"}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 02 for _build_type_tree')

def test03__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 0.5
    """
    LOGGER.info('Started test 03 for _build_type_tree')

    sample = MultidimSample([\
            "aa; cc; cc; bb;",\
            "aa; ee; ee; bb;",\
            "aa; dd; cc; dd; bb;",\
            "aa; NN; ee; ee; bb;"])
    supp = 0.5
    stats = []

    # 1. '', aa, bb, cc, ee
    # 2. aa bb, aa cc, aa ee, cc, bb, ee bb, ee ee
    # 3. aa cc bb, aa ee bb, aa ee ee, ee ee bb
    # 4. aa ee ee bb
    # 5. -

    sample_result = {'', 'aa;', 'bb;', 'cc;', 'ee;', 'aa; bb;', 'aa; cc;', 'aa; ee;', 'cc; bb;', 'ee; bb;', 'ee; ee;', 'aa; cc; bb;', 'aa; ee; bb;', 'aa; ee; ee;', 'ee; ee; bb;',
            'aa; ee; ee; bb;'}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 03 for _build_type_tree')

def test04__build_type_tree():
    """
        Testing '_build_type_tree': check for detecting "no frequent item exists" using a hyperlinked tree
    """
    LOGGER.info('Started test 04 for _build_type_tree')

    sample = MultidimSample(["a;c;", "b;d;"])
    supp = 1.0
    stats = []

    # 1. ""
    # 2. -

    sample_result = {''}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()

    LOGGER.info('Finished test 04 for _build_type_tree')

def test05__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 1.0 using a hyperlinked tree
    """
    LOGGER.info('Started test 05 for _build_type_tree')

    sample = MultidimSample(["aa;vv; cc;vx; cc;vy; bb;vz;", "aa;xv; ee;xx; ee;xy; bb;xz;", "aa;yw; dd;yv; cc;yx; dd;yy; bb;zz;", "aa;zw; NN;zv; ee;zx; ee;zy; bb;zz;"])
    supp = 1.0
    stats = []

    # 1. '', aa, bb, cc, ee
    # 2. aa bb, aa cc, aa ee, cc, bb, ee bb, ee ee
    # 3. aa cc bb, aa ee bb, aa ee ee, ee ee bb
    # 4. aa ee ee bb
    # 5. -

    sample_result = {'', 'aa;;', 'bb;;', 'aa;; bb;;'}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()

    LOGGER.info('Finished test 05 for _build_type_tree')

def test06__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 1.0 using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 06 for _build_type_tree')

    sample = MultidimSample(["aa;vv; cc;vx; cc;vy; bb;vz;", "aa;xv; ee;xx; ee;xy; bb;xz;", "aa;yw; dd;yv; cc;yx; dd;yy; bb;zz;", "aa;zw; NN;zv; ee;zx; ee;zy; bb;zz;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {}, {})

    # 1. '', aa;;, bb;;
    # 2. aa;; bb;;
    # 3. -

    sample_result = {'', 'aa;;', 'bb;;', 'aa;; bb;;'}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp, param_smart_matching=param_smart_matching)
    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()
    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 06 for _build_type_tree')

def test07__build_type_tree():
    """
        Testing '_build_type_tree': small running example at supp of 1.0 using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 07 for _build_type_tree')

    sample = MultidimSample(["a;x; a;x; a;x;", "b;x; a;x; b;z;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {}, {})

    # 1. '', "a;;", ";x;"
    # 2. "a;x;", ";x; ;x;", "a;; ;x;"
    # 3. ";x; a;x;"
    # 4. -

    sample_result = {'', "a;;", ";x;", "a;x;", ";x; ;x;", ";x; a;;", ";x; a;x;"}

    assert sample_result == _build_type_tree_multidim(stats, sample, supp, param_smart_matching=param_smart_matching)
    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()
    assert sample_result == _build_type_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 07 for _build_type_tree')

#####
#
# Tests for function _build_pattern_tree.
#
def test00__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example
    """
    LOGGER.info('Started test 00 for _build_pattern_tree')

    sample = MultidimSample(["aa;xx; aa;ww; bb;xx;", "cc;yy; cc;zz; dd;yy;"])
    supp = 1.0
    stats = []

    # 1. "", "$x0;; $x0;;", ";$x0; ;$x0;"
    # 2. "$x0;$x1; $x0;; ;$x1;"
    # 2. -

    sample_result = {"", "$x0;; $x0;;", ";$x0; ;$x0;", "$x0;$x1; $x0;; ;$x1;"}

    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 00 for _build_pattern_tree')

def test01__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example
    """
    LOGGER.info('Started test 01 for _build_pattern_tree')

    sample = MultidimSample(["aa;xx; cc;yy; cc;yy; bb;zz; ee;vv; ee;xx;",\
            "aa;yy; ee;xx; ee;zz; bb;vv; ee;xx; ee;zz;",\
            "aa;zz; dd;yy; KK;xx; dd;xx; bb;xx; ee;ww; ee;vv;",\
            "aa;xx; NN;ww; ee;ww; ee;xx; bb;xx; ee;zz; ee;zz;"])
    supp = 1.0
    stats = []

    # 1. "", "$x0;; $x0;;", ";$x0; ;$x0;"
    # 2. "$x0;; $x0;; $x1;; $x1;;"
    # 3. -

    sample_result = {"", "$x0;; $x0;;", ";$x0; ;$x0;", "$x0;; $x0;; $x1;; $x1;;"}

    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp)

    LOGGER.info('Finished test 01 for _build_pattern_tree')

def test02__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 02 for _build_pattern_tree')

    sample = MultidimSample([\
            "a;vv; vv;b; ww;b; c;xx; a;yy; c;zz; a;uu;",\
            "d;av; av;e; aw;e; f;ax; d;ay; f;az; d;au;",\
            ])
    param_smart_matching = ({}, {0 : {'a','c','d','f'}, 1 : {'b', 'e'}}, {})
    supp = 1.0
    stats = []

    # 1. "", "$x0;; $x0;;", ";$x0; ;$x0;"
    # 2. "$x0;; ;$x1; ;$x1; $x0;;", ";$x0; ;$x0; $x1;; $x1;;", "$x0;; $x1;; $x0;; $x1;;", "$x0;; $x1;; $x1;; $x0;;", "$x0;; $x0;; $x0;;" 
    # 3. "$x0;; ;$x1; ;$x1; $x0;; $x0;;", ";$x0; ;$x0; $x1;; $x1;;", "$x0;; $x1;; $x0;; $x1;; $x0;;", "$x0;; ;$x1; ;$x1; $x2;; $x2;; $x0;;",
    #    "$x0;; ;$x1; ;$x1; $x2;; $x0;; $x2;;"
    # 4. "$x0;; ;$x1; ;$x1; $x2;; $x0;; $x2;; $x0;;"
    # 5. -

    sample_result = {\
            "", "$x0;; $x0;;", ";$x0; ;$x0;",\
            "$x0;; $x1;; $x1;; $x0;;", "$x0;; ;$x1; ;$x1; $x0;;", ";$x0; ;$x0; $x1;; $x1;;", "$x0;; $x1;; $x0;; $x1;;", "$x0;; $x0;; $x0;;",\
            "$x0;; ;$x1; ;$x1; $x0;; $x0;;", ";$x0; ;$x0; $x1;; $x1;;", "$x0;; $x1;; $x0;; $x1;; $x0;;", "$x0;; ;$x1; ;$x1; $x2;; $x2;; $x0;;",\
            "$x0;; ;$x1; ;$x1; $x2;; $x0;; $x2;;", ";$x0; ;$x0; $x1;; $x2;; $x1;; $x2;;", \
            "$x0;; ;$x1; ;$x1; $x2;; $x0;; $x2;; $x0;;",\
            }

    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=None).query_strings_to_set()

def test03__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 03 for _build_pattern_tree')

    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0: {'a','x'},1:{'b','y'}}, {})

    sample = MultidimSample([\
            "a;b; a;b; e;b;",\
            "x;y; x;y; z;y;"\
            ])
    sample_result = {\
            "", "$x0;; $x0;;", ";$x0; ;$x0;",\
            ";$x0; ;$x0; ;$x0;", "$x0;$x1; $x0;$x1;", "$x0;$x1; $x0;; ;$x1;", "$x0;; $x0;$x1; ;$x1;",\
            "$x0;$x1; $x0;$x1; ;$x1;"\
            }

    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False)
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False, param_smart_matching=param_smart_matching)
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 03 for _build_pattern_tree')

def test04__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree
    """
    LOGGER.info('Started test 04 for _build_pattern_tree')

    supp = 1.0
    stats = []

    # 1. "", "$x0;;; $x0;;;", ";$x0;; ;$x0;;", ";;$x0; ;;$x0;"
    # 2. "$x0;;$x1; $x0;;$x1;", "$x0;;$x1; $x0;;; ;;$x1;", ";$x0;$x1; ;$x0;$x1;", ";;$x0; ;;$x0; ;;$x0;", "$x0;;; "$x0;$x1;; ;$x1;;", "$x0;;; $x0;;$x1; ;;$x1;",
    #    ";;$x0; ;$x1;$x0; ;$x1;;",
    # 3. "$x0;;$x1; $x0;$x2;$x1; ;$x2;;", "$x0;;$x1; $x0;$x2;; ;$x2;$x1;", "$x0;;; $x0;$x1;$x2; ;$x1;$x2;", "$x0;;$x1; $x0;;$x1; ;;$x1;", ";;$x0; ;$x1;$x0; ;$x1;$x0;", 
    # 4. "$x0;;$x1; $x0;$x2;$x1; ;$x2;$x1;"
    # 5. -

    sample_result = {\
            "", "$x0;;; $x0;;;", ";$x0;; ;$x0;;", ";;$x0; ;;$x0;",\
            "$x0;;$x1; $x0;;$x1;", "$x0;;$x1; $x0;;; ;;$x1;", ";$x0;$x1; ;$x0;$x1;", ";;$x0; ;;$x0; ;;$x0;", "$x0;;; $x0;$x1;; ;$x1;;", "$x0;;; $x0;;$x1; ;;$x1;",\
            ";;$x0; ;$x1;$x0; ;$x1;;",\
            "$x0;;$x1; $x0;$x2;$x1; ;$x2;;", "$x0;;$x1; $x0;$x2;; ;$x2;$x1;", "$x0;;; $x0;$x1;$x2; ;$x1;$x2;", "$x0;;$x1; $x0;;$x1; ;;$x1;", ";;$x0; ;$x1;$x0; ;$x1;$x0;",\
            "$x0;;$x1; $x0;$x2;$x1; ;$x2;$x1;",\
            }

    #assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False)
    #assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()
    #assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False, param_smart_matching=param_smart_matching)
    #assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 04 for _build_pattern_tree')

def test05__build_pattern_tree():
    """
        Testing '_build_pattern_tree': small running example using a hyperlinked tree and a smarter matching approach
    """
    LOGGER.info('Started test 05 for _build_pattern_tree')

    sample = MultidimSample(["aa;xx; cc;xx; cc;zz; bb;yy; ee;zz; ee;oo;",\
            "aa;xx; ee;zz; ee;yy; bb;xx; ee;zz; ee;oo;",\
            "aa;xx; dd;yy; KK;WW; dd;yy; bb;yy; ee;zz; ee;pp;",\
            "aa;xx; NN;VV; ee;yy; ee;yy; bb;zz; ee;oo; ee;pp;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0 : {'cc','ee','dd'}, 1 : {'xx', 'yy', 'zz'}}, {})

    # 1. "", "$x0;; $x0;;", ";$x0; ;$x0;"
    # 2. "$x0;; $x0;; $x1;; $x1;;"
    # 3. -

    sample_result = {"", "$x0;; $x0;;", ";$x0; ;$x0;", "$x0;; $x0;; $x1;; $x1;;", ";$x0; ;$x0; $x1;; $x1;;"}

    result = _build_pattern_tree_multidim(stats, sample, supp, param_smart_matching=param_smart_matching)

    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False)
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True).query_strings_to_set()
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=False, param_smart_matching=param_smart_matching)
    assert sample_result == _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure=True, param_smart_matching=param_smart_matching).query_strings_to_set()

    LOGGER.info('Finished test 05 for _build_pattern_tree')

#####
#
# Tests for function _build_mixed_query_tree.
#

def test00__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 00 for _build_mixed_query_tree')

    sample = MultidimSample(["a;x; b;x; a;z;","a;y; b;y; a;z;","a;z; c;z; b;z; a;z;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0: {'a'}, 1: {'x','y','z'}}, {})

    # 1.T "", "a;;", ";z;", "b;;"
    # 1.P "", "$x0;; $x0;;", ";$x0; ;$x0;"
    # 1.M ""
    # 2.T "a;; a;;", "a;z;", "a;; ;z;", "a;; b;;", "b;; a;;", "b;; ;z;"
    # 2.P "$x0;$x1; ;$x1; $x0;;"
    # 2.M "a;$x0; ;$x0;", ";$x0; ;$x0; a;;", ";$x0; ;$x0; ;z;", "$x0;; $x0;z;", ";$x0; b;$x0;", "$x0;; b;; $x0;;"
    # 3.T "a;; a;z;", "a;; b;; ;z;", "a;; b;; a;;", "b;; a;z;"
    # 3.P -
    # 3.M ";$x0; ;$x0; a;z;", "a;$x0; ;$x0; a;;", "a;$x0; ;$x0; ;z;", "a;$x0; b;$x0;", ";$x0; b;$x0; a;;" ";$x0; b;$x0; ;z;", "$x0;; b;; $x0;z;", "$x0;$x1; ;$x1; $x0;z;",
    #        "$x0;$x1; b;$x1; $x0;;"
    # 4.T "a;; b;; a;z"
    # 4.P -
    # 4.M ";$x0; b;$x0; a;z;", "a;$x0; ;$x0; a;z;", "a;$x0; b;$x0; ;z;", "a;$x0; b;$x0; a;;", "$x0;$x1; b;$x1; $x0;z;"
    # 5.T -
    # 5.P -
    # 5.M "a;$x0; b;$x0; a;z;"
    sample_result = {\
            "","a;;", ";z;", "b;;",\
            "","$x0;; $x0;;", ";$x0; ;$x0;",\
            # Length 1. mixed queries
            "a;; a;;", "a;z;", "a;; ;z;", "a;; b;;", "b;; a;;", "b;; ;z;",\
            "$x0;$x1; ;$x1; $x0;;",\
            "a;$x0; ;$x0;", ";$x0; ;$x0; a;;", ";$x0; ;$x0; ;z;", "$x0;; $x0;z;", ";$x0; b;$x0;", "$x0;; b;; $x0;;",\
            "a;; a;z;", "a;; b;; ;z;", "a;; b;; a;;", "b;; a;z;",\
            # Length 3. pattern queries
            ";$x0; ;$x0; a;z;", "a;$x0; ;$x0; a;;", "a;$x0; ;$x0; ;z;", "a;$x0; b;$x0;", ";$x0; b;$x0; a;;", ";$x0; b;$x0; ;z;", "$x0;; b;; $x0;z;", "$x0;$x1; ;$x1; $x0;z;",\
                    "$x0;$x1; b;$x1; $x0;;",\
            "a;; b;; a;z;",\
            # Length 4. pattern queries
            ";$x0; b;$x0; a;z;", "a;$x0; ;$x0; a;z;", "a;$x0; b;$x0; ;z;", "a;$x0; b;$x0; a;;", "$x0;$x1; b;$x1; $x0;z;",\
            # Length 5. type queries
            # Length 5. pattern queries
            "a;$x0; b;$x0; a;z;"\
            }

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    LOGGER.info('Finished test 01 for _build_mixed_query_tree')

    LOGGER.info('Finished test 00 for _build_mixed_query_tree')

def test01__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example with only types
    """
    LOGGER.info('Started test 01 for _build_mixed_query_tree')

    sample = MultidimSample(["a;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {}, {})

    sample_result = {"", "a;"}
    # 1. "", a
    # 2. -

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    LOGGER.info('Finished test 01 for _build_mixed_query_tree')

def test02__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example pattern only
    """
    LOGGER.info('Started test 02 for _build_mixed_query_tree')

    sample = MultidimSample(["a; a;", "b; b;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0: {'a','b'}}, {})

    sample_result = {"", "$x0; $x0;"}
    # 1. "", x0; x0;
    # 2. -

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    LOGGER.info('Finished test 02 for _build_mixed_query_tree')

def test03__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example
    """
    LOGGER.info('Started test 03 for _build_mixed_query_tree')

    sample = MultidimSample(["a; a; a;", "a; b; b;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0: {'a','b'}}, {})

    sample_result = {"", "a;", "$x0; $x0;", "a; $x0; $x0;"}
    # 1. "", a, x0 x0
    # 2. a; x0; x0;
    # 3. -

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    sample = MultidimSample(["a; a; a;", "b; b; a;"])
    supp = 1.0
    stats = []

    sample_result = {"", "a;", "$x0; $x0;", "$x0; $x0; a;"}
    # 1. "", a;, x0; x0;
    # 2. x0; x0; a;
    # 3. -

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)
    mixed_query_tree = _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)

    result = set()
    for vertex in mixed_query_tree.vertices_to_list():
        if vertex.is_frequent(ceil(supp*sample._sample_size)):
            result.add(vertex.query_string)

    assert sample_result == result

    LOGGER.info('Finished test 03 for _build_mixed_query_tree')

def test04__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example using a smarter matching approach
    """
    LOGGER.info('Started test 04 for _build_mixed_query_tree')

    sample = MultidimSample(["a;x; a;y; a;x;", "b;y; a;y; b;y;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0:{'a','b'},1:{'x','y'}}, {})

    # 1. "", "a;;", "$x0;; $x0;;", ";y;", ";$x0; ;$x0;"
    # 2. "$x0;; a;; $x0;;", "a;; ;y;", ";y; a;;", "a;y;", "a;$x0; ;$x0;", ";$x0; a;; ;$x0;", ";$x0; a;$x0;", "$x0;$x1; $x0;$x1;"
    # 3. "$x0;$x1; a;; $x0;$x1;", "$x0;$x1; ;y; $x0;$x1;"
    # 4. "$x0;$x1; a;y; $x0;$x1;"
    sample_result = {\
            "", "a;;", "$x0;; $x0;;", ";y;", ";$x0; ;$x0;",\
            "$x0;; a;; $x0;;", "a;; ;y;", ";y; a;;", "a;y;", "a;$x0; ;$x0;", ";$x0; a;; ;$x0;", "$x0;; ;y; $x0;;", "$x0;; ;y; $x0;;", ";$x0; a;$x0;", "$x0;$x1; $x0;$x1;",\
            "$x0;; $x0;y;", ";$x0; ;y; ;$x0;", "$x0;y; $x0;;",\
            "$x0;$x1; a;; $x0;$x1;", "$x0;$x1; ;y; $x0;$x1;", "$x0;; a;y; $x0;;", ";$x0; a;y; ;$x0;",\
            "$x0;$x1; a;y; $x0;$x1;"}

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    LOGGER.info('Finished test 04 for _build_mixed_query_tree')

def test05__build_mixed_query_tree():
    """
        Testing '_build_mixed_query_tree': small running example using a smarter matching approach
    """
    LOGGER.info('Started test 05 for _build_mixed_query_tree')

    # a;x;s; a;y;s; a;x;s;
    # b;y;t; a;y;s; b;y;t;

    sample = MultidimSample(["a;x;s; a;y;s; a;x;s;", "b;y;t; a;y;s; b;y;t;"])
    supp = 1.0
    stats = []
    param_smart_matching = ({}, {0:{'a','b'},1:{'x','y'},2:{'s','t'}}, {})

    # 1. "", "a;;;", "$x0;;; $x0;;;", ";y;;", ";$x0;; ;$x0;;", ";;s;", ";
    # 2. "$x0;; a;; $x0;;", "a;; ;y;", ";y; a;;", "a;y;", "a;$x0; ;$x0;", ";$x0; a;; ;$x0;", ";$x0; a;$x0;", "$x0;$x1; $x0;$x1;"
    # 3. "$x0;$x1; a;; $x0;$x1;", "$x0;$x1; ;y; $x0;$x1;"
    # 4. "$x0;$x1; a;y; $x0;$x1;"
    sample_result = {\
            # 1
            '', '$x0;;; $x0;;;', ';$x0;; ;$x0;;', ';;$x0; ;;$x0;', ';;s;', ';y;;', 'a;;;',\
            # 2
            '$x0;$x1;; $x0;$x1;;', '$x0;;$x1; $x0;;$x1;', '$x0;;; $x0;y;;', '$x0;;; ;;s; $x0;;;', '$x0;;; ;y;; $x0;;;', '$x0;;; a;;; $x0;;;', '$x0;y;; $x0;;;',\
            ';$x0;$x1; ;$x0;$x1;', ';$x0;; ;$x0;s;', ';$x0;; ;;s; ;$x0;;', ';$x0;; ;y;; ;$x0;;', ';$x0;; a;$x0;;', ';$x0;; a;;; ;$x0;;', ';$x0;s; ;$x0;;', ';;$x0; ;;s; ;;$x0;',\
            ';;$x0; ;y;$x0;', ';;$x0; ;y;; ;;$x0;', ';;$x0; a;;; ;;$x0;', ';;s; ;y;;', ';y;$x0; ;;$x0;', ';y;; ;;s;', ';y;; a;;;', ';y;s;', 'a;$x0;; ;$x0;;', 'a;;; ;y;;', 'a;;s;',\
            'a;y;;',\
            # 3
            '$x0;$x1;$x2; $x0;$x1;$x2;', '$x0;$x1;; ;;s; $x0;$x1;;', '$x0;$x1;; ;y;; $x0;$x1;;', '$x0;$x1;; a;;; $x0;$x1;;', '$x0;;$x1; $x0;y;$x1;', '$x0;;$x1; ;;s; $x0;;$x1;',\
            '$x0;;$x1; ;y;; $x0;;$x1;', '$x0;;$x1; a;;; $x0;;$x1;', '$x0;;; ;y;s; $x0;;;', '$x0;;; a;;s; $x0;;;', '$x0;;; a;y;; $x0;;;', '$x0;y;$x1; $x0;;$x1;',\
            ';$x0;$x1; ;;s; ;$x0;$x1;', ';$x0;$x1; ;y;; ;$x0;$x1;', ';$x0;$x1; a;;; ;$x0;$x1;', ';$x0;; ;y;s; ;$x0;;', ';$x0;; a;$x0;s;', ';$x0;; a;;s; ;$x0;;',\
            ';$x0;; a;y;; ;$x0;;', ';;$x0; ;y;s; ;;$x0;', ';;$x0; a;;s; ;;$x0;', ';;$x0; a;y;; ;;$x0;', ';y;; a;;s;', 'a;;s; ;y;;', 'a;$x0;s; ;$x0;;', 'a;y;s;',\
            # 4
            '$x0;$x1;$x2; ;;s; $x0;$x1;$x2;', '$x0;$x1;$x2; ;y;; $x0;$x1;$x2;', '$x0;$x1;$x2; a;;; $x0;$x1;$x2;', '$x0;$x1;; ;y;s; $x0;$x1;;', '$x0;$x1;; a;;s; $x0;$x1;;',\
            '$x0;$x1;; a;y;; $x0;$x1;;', '$x0;;$x1; ;y;s; $x0;;$x1;', '$x0;;$x1; a;;s; $x0;;$x1;', '$x0;;$x1; a;y;; $x0;;$x1;', '$x0;;; a;y;s; $x0;;;',\
            ';$x0;$x1; ;y;s; ;$x0;$x1;', ';$x0;$x1; a;;s; ;$x0;$x1;', ';$x0;$x1; a;y;; ;$x0;$x1;', ';$x0;; a;y;s; ;$x0;;', ';;$x0; a;y;s; ;;$x0;',\
            # 5
            '$x0;$x1;$x2; ;y;s; $x0;$x1;$x2;', '$x0;$x1;$x2; a;;s; $x0;$x1;$x2;', '$x0;$x1;$x2; a;y;; $x0;$x1;$x2;', '$x0;$x1;; a;y;s; $x0;$x1;;', '$x0;;$x1; a;y;s; $x0;;$x1;',\
            ';$x0;$x1; a;y;s; ;$x0;$x1;',\
            # 6
            '$x0;$x1;$x2; a;y;s; $x0;$x1;$x2;'\
            }

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = False)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = False)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching)

    type_tree = _build_type_tree_multidim(stats, sample, supp, use_tree_structure = True)
    pattern_tree = _build_pattern_tree_multidim(stats, sample, supp, use_tree_structure = True)

    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree).query_strings_to_set(frequent_items_only= True)
    assert sample_result == _build_mixed_query_tree_multidim(stats, sample, supp, type_tree, pattern_tree, param_smart_matching=param_smart_matching).query_strings_to_set(True)

    LOGGER.info('Finished test 05 for _build_mixed_query_tree')


#####
#
# Tests for function _syntactically_contained
#
def test00__syntactically_contained():
    """
        Testing '_syntactically_contained': small running example
    """
    try:
        _syntactically_contained([["a","b"],["",""]], [["ab", "x"],[""]])
        raise AssertionError("No exception caught!")
    except ValueError:
        pass

    assert _syntactically_contained([],[["a","b"]])

    assert _syntactically_contained([["","",""]], [["","",""]])
    assert _syntactically_contained([["a","","c"],["d","e",""]], [["a","b","c"],["d","e","f"]])
    assert not _syntactically_contained([["a","","c"],["d","e",""]], [["a","b","c"],["d","","f"]])

    assert _syntactically_contained([["$x0","","c"],["$x0","","f"]], [["a","","c"],["a","","f"]])
    assert not _syntactically_contained([["$x0","","c"],["$x0","","f"]], [["a","","c"],["b","","f"]])
    assert not _syntactically_contained([["$x0","","c"],["$x0","","f"]], [["a","","c"],["a","","f"]], {"$x0" : "d"})

    assert _syntactically_contained([["$x3","$x4","c"],["$x3","$x5","f"]], [["$x0","$x1","c"],["$x0","$x1","f"]])
    assert _syntactically_contained([["$x3","$x4","c"],["$x3","$x5","f"]], [["$x0","$x1","c"],["$x0","$x1","f"]], {"$x4": "$x1"})
    assert not _syntactically_contained([["$x3","$x4","c"],["$x3","$x5","f"]], [["$x0","$x1","c"],["$x0","$x1","f"]], {"$x3": "$x2"})

#####
#
# Tests for function _find_decriptive_querystrings
#
def test00__find_descriptive_querystrings():
    """
        Testing '_find_descriptive_querystrings': small running example
    """
    assert {""} == _find_descriptive_querystrings({}, [""])
    assert {"a;b;"} == _find_descriptive_querystrings({}, [";;", "a;;", ";b;", "a;b;"])

    assert {"a;b; c;d;"} == _find_descriptive_querystrings({}, [";;", "a;;", ";b; c;d;", "a;b; c;;", "a;b; c;d;"])

    to_check = ["", "a;;", ";b;", ";c;", "a;b;", "a;c;", "a;b; ;c;", "a;; a;c;", "a;b; a;c;", "$x0;; $x0;;", "$x0;b; $x0;;", "$x0;; $x0;c;", "$x0;b; $x0;c;"]
    assert {"a;b; a;c;"} == _find_descriptive_querystrings({}, to_check)
    to_check = ["a;b;", "a;c;", "d;b;", "g;c;", "x;y; z;x;"]
    assert {"a;b;", "a;c;", "d;b;", "g;c;", "x;y; z;x;"} == _find_descriptive_querystrings({}, to_check)

    assert {";$x0; b;y; ;$x0;"} == _find_descriptive_querystrings({}, [";$x0; b;y; ;$x0;",";$x0; b;; ;$x0;"])
    assert {";$x0; b;y; ;$x0;"} == _find_descriptive_querystrings({}, [";$x0; b;; ;$x0;",";$x0; b;y; ;$x0;"])

#####
#
# Tests for function pattern_type_split_discovery.
#
def test00_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting empty samples
    """
    LOGGER.info('Started test 00 for pattern_type_split_discovery')

    sample = None
    supp = 1.0

    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! Missing sample not detected!")
    except EmptySampleError:
        pass

    sample = MultidimSample()

    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! Empty sample not detected!")
    except EmptySampleError:
        pass

    sample.set_sample(["a;"])
    sample._sample_size = 0
    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised! _sample_size is '0', but not detected!")
    except EmptySampleError:
        pass

    LOGGER.info('Finished test 00 for pattern_type_split_discovery')

def test01_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting invalid query support
    """
    LOGGER.info('Started test 01 for pattern_type_split_discovery')

    sample = MultidimSample()
    sample.set_sample(['a;'])
    sample.set_sample_size()

    supp = -0.5

    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised!")
    except InvalidQuerySupportError:
        pass

    supp = 1.1

    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order='type_first')
        raise AssertionError("Test failed: No error raised!")
    except InvalidQuerySupportError:
        pass

    LOGGER.info('Finished test 01 for pattern_type_split_discovery')

def test02_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': Check for detecting invalid optional discovery_order
    """
    LOGGER.info('Started test 02 for pattern_type_split_discovery')

    sample = MultidimSample()
    sample.set_sample(['a;'])
    sample.set_sample_size()

    supp = 1.0

    var_discovery_order = None

    try:
        discovery_bu_pts_multidim(sample, supp, discovery_order=var_discovery_order)
        raise AssertionError ("Test failed: No error raised!")
    except NotImplementedError:
        pass

    LOGGER.info('Finished test 02 for pattern_type_split_discovery')

def test03_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 03 for pattern_type_split_discovery')

    sample = MultidimSample()
    sample = MultidimSample(["a;x;s; a;y;s; a;x;s;", "b;y;t; a;y;s; b;y;t;"])
    sample.set_sample_size()

    supp = 1.0

    sample_result = {'a;;s; ;y;;', ';y;; a;;s;', 'a;$x0;s; ;$x0;;', ';$x0;; a;$x0;s;', '$x0;;$x1; $x0;y;$x1;', '$x0;y;$x1; $x0;;$x1;', '$x0;$x1;$x2; a;y;s; $x0;$x1;$x2;'}

    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    LOGGER.info('Finished test 03 for pattern_type_split_discovery')

def test04_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example with supp 0.5
    """
    LOGGER.info('Started test 04 for pattern_type_split_discovery')

    sample = MultidimSample()
    sample.set_sample(["b;y; a;x; b;y;", "aB;v; b;y; C;Z; aB;v; C;Z;", "", "a;x; b;y; a;x;"])
    sample.set_sample_size()

    supp = 0.5

    sample_result = {"$x0;$x1; b;y; $x0;$x1;", "a;x; b;y;", "b;y; a;x;"}

    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    supp = 1.0
    sample = MultidimSample()
    sample.set_sample(['a;b;c; a;b;c;', 'e;f;g; e;f;g;'])
    sample_result = {'$x0;$x1;$x2; $x0;$x1;$x2;'}

    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    LOGGER.info('Finished test 04 for pattern_type_split_discovery')

def test05_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 05 for pattern_type_split_discovery')

    sample = MultidimSample(["a;x; a;y; a;x;", "b;y; a;y; b;y;"])
    supp = 1.0

    # 1. "", "a;;", "$x0;; $x0;;", ";y;", ";$x0; ;$x0;"
    # 2. "$x0;; a;; $x0;;", "a;; ;y;", ";y; a;;", "a;y;", "a;$x0; ;$x0;", ";$x0; a;; ;$x0;", ";$x0; a;$x0;", "$x0;$x1; $x0;$x1;"
    # 3. "$x0;$x1; a;; $x0;$x1;", "$x0;$x1; ;y; $x0;$x1;"
    # 4. "$x0;$x1; a;y; $x0;$x1;"
    sample_result = {"a;; ;y;", ";y; a;;", "a;$x0; ;$x0;", ";$x0; a;$x0;", "$x0;; $x0;y;", "$x0;y; $x0;;", "$x0;$x1; a;y; $x0;$x1;"}

    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    # assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    LOGGER.info('Finished test 05 for pattern_type_split_discovery')

def test06_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 06 for pattern_type_split_discovery')

    sample = MultidimSample(["b; a; a; c; d; d; b; a; b; b;", "b; a; a; b; c; d; d; a; c; c; b; b;", "a; a; b; c; b; a; c; d; b; a;", "c; a; a; b; d; b; c; c; d; d; b; a;",\
            "b; a; c; d; d; b; b; a; c; d; d; b;"])
    supp = 1.0
    stats = []

    sample_result ={'$x0; a; $x1; $x2; $x2; $x1; $x0;', '$x0; a; $x1; $x2; $x1; $x2; $x0;', '$x0; $x1; $x1; c; d; $x1; $x0;', '$x0; $x1; $x2; $x1; $x0; $x2;',\
            '$x0; $x1; $x2; $x2; $x0; $x1;', '$x0; $x0; $x1; $x1; $x2; $x2;', '$x0; a; $x1; c; d; $x1; $x0;', '$x0; a; $x1; $x1; $x0; $x0;', '$x0; $x0; c; $x1; $x1; $x0;',\
            'a; $x0; c; $x1; $x1; $x0;', 'a; $x0; $x1; $x1; b; $x0;', 'a; $x0; $x1; $x1; $x0; b;', 'a; $x0; $x1; $x0; $x1; b;', '$x0; a; c; $x1; $x1; $x0;',\
            '$x0; a; $x1; b; $x1; $x0;', '$x0; a; $x1; $x1; b; $x0;', '$x0; a; $x1; $x1; $x0; b;', '$x0; a; $x1; $x0; $x1; b;', '$x0; $x1; $x1; $x0; $x1;',\
            '$x0; $x1; c; d; $x0; $x1;', '$x0; c; $x0; $x1; $x1;', '$x0; $x1; a; $x0; $x1;', '$x0; $x1; $x0; b; $x1;', '$x0; $x0; $x1; $x1; a;', '$x0; a; c; d; b; $x0;',\
            '$x0; a; b; $x0; $x0;', 'b; c; $x0; $x0; a;', 'b; $x0; b; $x0; b;', 'b; $x0; c; d; $x0;', 'b; $x0; $x0; b; a;', 'b; $x0; $x0; d; b;', 'a; a; c; $x0; $x0;',\
            'a; a; $x0; $x0; b;', 'a; c; $x0; $x0; a;', 'a; $x0; b; $x0; b;', 'a; $x0; d; b; $x0;', 'a; $x0; c; $x0; b;', '$x0; a; b; b; $x0;', '$x0; a; b; $x0; b;',\
            '$x0; a; a; d; $x0;', '$x0; a; a; c; $x0;', '$x0; a; c; $x0; b;', '$x0; $x0; b; b; b;', '$x0; c; $x0; $x0;', '$x0; $x0; b; $x0;', '$x0; $x0; $x0; b;',\
            'b; b; $x0; $x0;','$x0; a; $x0; a;', 'a; a; c; d; b;', 'b; c; d; b;', 'b; c; d; a;', 'a; b; b; b;', 'a; c; d; a;', 'a; b; a;', 'c; b; b;', 'c; a; b;'}

    #assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    #assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    #assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    LOGGER.info('Finished test 06 for pattern_type_split_discovery')

def test07_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 07 for pattern_type_split_discovery')

    sample = MultidimSample(['m;r;r; f;o;f; t;t;t; b;b;b; c;r;c; m;o;o; m;o;m; n;n;h; q;q;q; c;h;h;', 'm;t;g; i;h;q; p;p;p; l;l;h; r;g;a; m;h;q; k;b;o; t;j;s; c;e;e; r;o;s;'])
    sample = MultidimSample(['m;b; t;a; t;a; t;b; t;a; t;b; t;b;', 'm;b; m;a; m;a; m;b; m;b; m;b;', 'm;a; t;a; m;b; t;b;'])
    supp = 1.0
    stats = []

    sample_result = {''}
    #result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=False, discovery_order='type_first')[0]
    #result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=False, use_smart_matching=True, discovery_order='type_first')[0]
    #result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=False, discovery_order='type_first')[0]
    #result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    #assert sample_result == result
    LOGGER.info('Finished test 07 for pattern_type_split_discovery')

def test08_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 08 for pattern_type_split_discovery')

    sample = MultidimSample([\
            'a;a; b;b; b;b; a;a;',\
            'a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a; a;a; b;b; b;b; a;a;'])
    supp = 1.0
    stats = []

    sample_result = {'a;a; b;b; b;b; a;a;'}
    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sample_result == result
    LOGGER.info('Finished test 08 for pattern_type_split_discovery')

def test09_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 09 for pattern_type_split_discovery')

    sample = MultidimSample(['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;',\
            'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;'])
    supp = 1.0
    stats = []

    sample_result = {';$x0;; ;b;; ;;o; ;$x0;; ;;o;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;',\
            '$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;', '$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;', '$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;',\
            ';$x0;; ;b;; ;$x0;; ;s;;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;', '$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;', ';$x0;; ;b;; ;p;; ;$x0;; ;;o;',\
            '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;', ';;e; ;;$x0; ;p;$x0; ;;o;',\
            '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;', '$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;',\
            '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;', ';;e; ;;$x0; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;',\
            '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;', ';;e; ;b;; ;p;; ;;o;', ';;e; ;b;; ;;o; ;;o;',\
            ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;', ';;e; ;;o; $x0;;; $x0;;o;', ';;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;', '$x0;;; ;b;; $x0;;$x1; ;;$x1;',\
            ';;e; ;b;; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;'}

    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]
    assert sample_result == result
    LOGGER.info('Finished test 09 for pattern_type_split_discovery')

def test10_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 10 for pattern_type_split_discovery')

    sample = MultidimSample(['m;b;1; t;a;1; t;a;1;', 'm;b;1; m;a;1; m;a;0;', 'm;a;0; t;a;1; m;b;0;'] )
    supp = 1.0
    stats = []

    sample_result = {'m;b;;', '$x0;a;; $x0;;;', '$x0;;$x1; $x0;;$x1;', ';a;; ;a;;', 'm;;$x0; ;;$x0;', 'm;;; ;a;1;'}
    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sample_result == result

    LOGGER.info('Finished test 10 for pattern_type_split_discovery')

def test11_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 11 for pattern_type_split_discovery')

    sample = MultidimSample(['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;'])
    supp = 1.0
    stats = []

    sample_result = {';$x0;; ;b;; ;;o; ;$x0;; ;;o;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;', '$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;', '$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;', '$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;', ';$x0;; ;b;; ;$x0;; ;s;;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;', '$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;', ';$x0;; ;b;; ;p;; ;$x0;; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;', ';;e; ;;$x0; ;p;$x0; ;;o;', '$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;', '$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;', ';;e; ;;$x0; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;', ';;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;', ';;e; ;b;; ;p;; ;;o;', ';;e; ;b;; ;;o; ;;o;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;', ';;e; ;;o; $x0;;; $x0;;o;', ';;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;', '$x0;;; ;b;; $x0;;$x1; ;;$x1;', ';;e; ;b;; ;p;$x0; ;s;$x0;', ';;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;', '$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;'}
    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sample_result == result
    LOGGER.info('Finished test 11 for pattern_type_split_discovery')

def test12_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
        Since the matching finds 'a.O' in 'a.O2' and 'a.O3', this example actually does not work right now.
    """
    LOGGER.info('Started test 12 for pattern_type_split_discovery')


    sample = MultidimSample(['n;n;n; d;m;n; o;o;k; k;k;k; c;i;n; d;n;c; f;f;f; o;c;g; c;o;c; c;c;c;', 'g;k;c; o;a;c; h;h;t; c;o;t; d;e;c; o;b;g; l;l;f; h;q;j; d;h;q; q;q;q;'])
                            #'   ;$x0;$x1;    ;   ;$x1;    ;$x2;   ; d;$x0;$x3;    ;$x2;$x3;'
    #sample = MultidimSample(['a11;p10;p11; a21;a23;p11; a31;p12;a33; d;p10;p13; a51;p12;p13;',\
    #                         'a12;p20;p21; a22;a24;p21; a32;p22;a34; d;p20;p23; a52;p22;p23;'])
    #sample = MultidimSample(['o;a11;a21; a31;a43;p01; c;o;p01; a51;a61;c;',\
    #                         'o;a12;a22; a32;a44;p02; c;o;p02; a52;a62;c;'])

    supp = 1.0
    stats = []

    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]
    sample_result = {';;$x0; c;;$x0; d;;c; o;;g; ;;$x1; ;;$x1;', '$x0;;; o;;; $x0;;; ;;$x1; ;;$x1;', 'o;;$x0; ;;$x0; ;;f; ;$x1;; ;;$x2; ;$x1;$x2;',\
            ';;$x0; ;;$x0; $x1;$x2;; c;;; d;;c; ;;f; $x1;$x3;; ;$x2;$x4; ;$x3;$x4;', ';k;; o;;; $x0;;c; $x0;;;', 'o;;; c;;; d;;$x0; ;;$x0;',\
            'o;;; c;;; d;;c; ;;f; ;$x0;; ;;$x1; ;$x0;$x1;', ';;$x0; c;;$x0; d;;c; ;;f; ;$x1;; ;;$x2; ;$x1;$x2;', 'o;;; c;;; $x0;;c; $x0;;;', ';k;; $x0;;; o;;g; $x0;;$x1; ;;$x1;',\
            ';;c; o;;; $x0;;c; $x0;;;', ';;$x0; d;;$x0; o;;g; ;;$x1; ;;$x1;', ';;$x0; ;;$x0; c;;; $x1;;c; $x1;;;', ';$x0;; ;o;; d;$x0;$x1; ;;$x1;',\
            ';$x0;; d;;; ;$x1;; d;$x0;$x2; ;$x1;$x2;', ';;$x0; ;;$x0; ;$x1;; $x2;;c; $x2;$x1;;', ';k;; $x0;;; d;;c; o;;g; $x0;;;', ';$x0;$x1; ;;$x1; o;;; d;$x0;$x2; ;;$x2;',\
            ';k;; ;;c; ;$x0;; $x1;;c; $x1;$x0;;', ';;$x0; $x1;;$x0; d;;c; $x1;;;', ';;$x0; o;;; $x1;;$x0; o;;g; $x1;;$x2; ;;$x2;',\
            ';;$x0; ;;$x0; ;$x1;$x2; ;;$x2; d;;c; o;;g; ;$x1;$x3; ;;$x3;', 'o;;; ;$x0;; $x1;;c; $x1;$x0;;', ';;$x0; ;;$x0; c;;; d;;$x1; ;;$x1;',\
            ';;$x0; ;;$x0; $x1;$x2;$x3; ;;$x3; d;;c; ;;f; $x1;$x4;; ;$x2;$x5; ;$x4;$x5;', 'o;;; c;;; d;;c; o;;g; ;;$x0; ;;$x0;',\
            ';;$x0; $x1;;$x0; $x2;$x3;$x4; ;;$x4; $x5;;$x0; $x1;;; ;;f; $x2;$x6;; $x5;$x3;$x7; ;$x6;$x7;', ';;$x0; ;;$x0; ;;$x1; c;o;$x1; ;;c;', ';;c; ;;c; ;;c;',\
            ';;$x0; d;;$x0; o;;; d;;$x1; ;;$x1;', ';;$x0; ;;$x0; ;o;; $x1;;$x0; o;;g; $x1;;$x2; ;;$x2;', ';;$x0; d;;$x0; ;$x1;; d;;$x2; ;$x1;$x2;',\
            ';;$x0; ;;$x0; $x1;;; c;o;; $x1;;;', 'o;;; ;;$x0; c;o;$x0; ;;c;', ';k;; o;;; ;;$x0; c;;$x0;', ';k;; ;;c; ;;$x0; c;;$x0;', ';;$x0; ;;$x0; $x1;;; d;;c; o;;g; $x1;;;',\
            ';;$x0; ;;$x0; ;$x1;; c;o;; ;$x1;;', ';;$x0; ;;$x0; ;;$x1; ;;$x1; $x2;;c; $x2;;;', ';k;; c;;; $x0;;c; $x0;;;', 'o;;$x0; ;;$x0; o;;g; ;;$x1; ;;$x1;',\
            ';$x0;$x1; ;;$x1; ;$x2;; d;$x0;$x3; ;$x2;$x3;', ';;$x0; $x1;;$x0; $x2;$x3;; c;;; $x1;;; ;;f; $x2;$x4;; ;$x3;$x5; ;$x4;$x5;', 'o;;$x0; ;;$x0; d;;$x1; ;;$x1;',\
            ';;$x0; ;;$x0; ;$x1;$x2; ;;$x2; $x3;;$x0; o;;g; $x3;$x1;$x4; ;;$x4;', ';k;; ;;$x0; c;o;$x0; ;;c;', ';;$x0; ;;$x0; ;;$x1; ;;$x1; ;;$x0; d;;$x2; ;;$x2;',\
            ';k;; c;;; d;;$x0; ;;$x0;', ';;$x0; d;;$x0; o;;; ;;f; ;$x1;; ;;$x2; ;$x1;$x2;', ';k;; c;;; d;;c; ;;f; ;$x0;; ;;$x1; ;$x0;$x1;', ';;$x0; ;;$x0; ;o;; $x1;;c; $x1;;;',\
            ';;$x0; ;;$x0; ;$x1;; c;;; d;;c; o;;g; ;$x1;$x2; ;;$x2;', ';k;; ;;c; ;$x0;; c;o;; ;$x0;;', ';;$x0; c;;$x0; $x1;;c; $x1;;;', ';;c; ;;c; c;;;',\
            ';;$x0; ;;$x0; ;o;; ;;$x0; d;;$x1; ;;$x1;', ';k;; ;;c; c;o;; ;;c;', 'o;;; ;$x0;; c;o;; ;$x0;;', ';;c; o;;; ;;$x0; c;;$x0;', ';;$x0; o;;; ;;$x0; d;;$x1; ;;$x1;',\
            ';;$x0; o;;; $x1;;$x0; ;;f; ;$x2;; $x1;;$x3; ;$x2;$x3;', ';k;; $x0;;; ;;$x1; c;o;$x1; $x0;;;', 'o;;; $x0;;; d;;c; o;;g; $x0;;;', ';k;; c;;; d;;c; o;;g; ;;$x0; ;;$x0;',\
            ';k;; $x0;;; d;;c; ;;f; $x0;;;', ';;$x0; $x1;;$x0; ;;$x2; c;o;$x2; $x1;;;', ';;$x0; ;;$x0; ;o;; d;;c; ;;f; ;$x1;; ;;$x2; ;$x1;$x2;',\
            ';;$x0; $x1;;$x0; ;o;; $x2;;$x0; $x1;;; ;;f; ;$x3;; $x2;;$x4; ;$x3;$x4;', 'o;;; $x0;;; c;o;; $x0;;;', ';$x0;$x1; c;;$x1; d;$x0;$x2; ;;$x2;',\
            ';;$x0; ;;$x0; ;o;; d;;c; o;;g; ;;$x1; ;;$x1;', ';k;; $x0;;; ;;f; ;$x1;; $x0;;$x2; ;$x1;$x2;', '$x0;;$x1; c;;$x1; $x0;;; ;;$x2; ;;$x2;',\
            'o;;; $x0;;; d;;c; ;;f; $x0;;;', ';k;; o;;; c;o;; ;;c;', ';;c; o;;; c;o;; ;;c;', ';$x0;; d;;; o;;; d;$x0;$x1; ;;$x1;'}
    #sample_result = {';$x0;$x1; ;;$x1; ;$x2;; d;$x0;$x3; ;$x2;$x3;'}
    #sample_result = {'o;;; ;;$x0; c;o;$x0; ;;c;'}

    assert sample_result == result

    LOGGER.info('Finished test 12 for pattern_type_split_discovery')

def test13_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 13 for pattern_type_split_discovery')

    sample = MultidimSample(['e;q;e; m;m;m; k;p;l; e;b;s; p;p;s; e;s;s; o;e;o; e;o;s; q;q;q; q;h;o;', 'a;r;a; d;d;e; g;n;i; a;t;o; b;n;o; a;o;o; j;b;j; a;p;o; s;r;b; r;s;o;'])
    supp = 1.0
    stats = []

    sample_result = {
            ";;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;o;",\
            ";;e; ;;$x0; ;p;$x0; ;;o;",\
            ";;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;o;$x2; ;;o;",\
            ";;e; ;b;; ;p;$x0; ;s;$x0;",\
            ";$x0;; ;b;; ;;o; ;$x0;; ;;o;",\
            "$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; ;;o; $x0;;$x3; ;$x1;; ;;o;",\
            "$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x3;;; $x3;;o;",\
            "$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;o;$x3; ;$x1;; ;;o;",\
            ";;e; $x0;;$x1; ;;o; $x0;o;$x1; ;;o;",\
            ";;e; ;b;; ;p;; ;;o;",\
            "$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; $x0;;$x3; ;$x1;; ;;o;",\
            "$x0;;; $x0;;$x1; ;p;$x1; ;s;$x1;",\
            ";;e; ;b;; ;;o; ;;o;",\
            "$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; $x0;;$x2; ;;o; ;;$x2;",\
            ";;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; ;;o; ;;$x2;",\
            ";;e; ;$x0;; ;;$x1; ;$x0;$x1; $x2;;; $x2;;o;",\
            "$x0;;; ;$x1;; $x0;;$x2; ;$x1;$x2; ;s;$x2;",\
            "$x0;$x1;; $x0;;$x2; ;p;$x2; ;$x1;; ;;o;",\
            ";;e; ;$x0;; $x1;;$x2; ;$x0;$x2; ;;o; $x1;;$x2; ;;o;",\
            ";$x0;; ;b;; ;$x0;; ;s;;",\
            "$x0;;; ;b;; $x0;;$x1; ;;$x1;",\
            "$x0;;; $x0;;; ;;o; $x1;;; $x1;;o;",\
            ";;e; ;$x0;; $x1;;$x2; ;$x0;$x2; $x1;;$x2; $x1;;$x2; ;;o;",\
            "$x0;$x1;; ;b;; $x0;;; ;$x1;; ;;o;",\
            "$x0;$x1;; $x0;;$x2; ;;o; $x0;o;$x2; ;$x1;; ;;o;",\
            ";$x0;; ;b;; ;p;; ;$x0;; ;;o;",\
            "$x0;$x1;; ;$x2;; $x0;;$x3; ;$x2;$x3; $x0;;$x3; ;;o; ;$x1;; ;;o;",\
            ";;e; ;;o; $x0;;; $x0;;o;",\
            ";;e; ;$x0;; ;;$x1; ;$x0;$x1; ;s;$x1;",\
            ";;e; ;;$x0; ;p;$x0; ;s;$x0;"}
    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sample_result == result
    LOGGER.info('Finished test 13 for pattern_type_split_discovery')

def test14_pattern_type_split_discovery():
    """
        Testing 'pattern_type_split_discovery': small running example
    """
    LOGGER.info('Started test 14 for pattern_type_split_discovery')

    sample = MultidimSample(['m;m;m; m;m;m; J;J;J; m;m;m; t;t;t; M;M;M; E;E;E; m;m;m; x;x;x; l;l;l; D;D;D; o;o;o; d;d;d; F;F;F; i;i;i; g;g;g; j;j;j; k;k;k; C;C;C; I;I;I;',\
            'z;z;z; z;z;z; a;a;a; z;z;z; L;L;L; y;y;y; c;c;c; z;z;z; f;f;f; G;G;G; v;v;v; e;e;e; A;A;A; s;s;s; K;K;K; n;n;n; B;B;B; u;u;u; b;b;b; h;h;h;'])
    supp = 1.0
    stats = []

    sample_result ={'$x0;$x1;$x2; $x0;$x1;$x2; $x0;$x1;$x2; $x0;$x1;$x2;'}
    result = discovery_bu_pts_multidim(sample, supp, use_tree_structure=True, use_smart_matching=True, discovery_order='type_first')[0]

    assert sample_result == result
    LOGGER.info('Finished test 14 for pattern_type_split_discovery')

#def test15_pattern_type_split_discovery():
#    """
#        Testing 'pattern_type_split_discovery': small running example
#    """
#    LOGGER.info('Started test 15 for pattern_type_split_discovery')
#
#    sample = Sample(["a a a a a a a a", "b b b b b b b" ])
#    supp = 1.0
#    stats = []
#
#    sample_result =["$x0 $x0 $x0 $x0 $x0 $x0 $x0"]
#    result = bu_pattern_type_split_discovery(sample, supp, use_tree_structure=True, discovery_order='type_first')[0]
#
#    assert sorted(sample_result, key=lambda item: (item.count(' '),item))== sorted(result, key=lambda item: (item.count(' '),item))
#    LOGGER.info('Finished test 15 for pattern_type_split_discovery')
#
