#!/usr/bin/python3
"""Contains tests for class SampleGenerator"""
import sys
sys.path.append(".")

import logging
from generator_multidim import MultidimSampleGenerator

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_generate_random_sample_multidim():
    """
        Test for function generate_random_sample() with default values.
    """
    LOGGER.info('Started test test_generate_random_sample')
    sample_gen = MultidimSampleGenerator()

    #type_length=1, event_dimension=1
    sample = sample_gen.generate_random_sample(sample_size=10, min_trace_length=5, max_trace_length=10, type_length=1, type_count=-1, event_dimension=1)
    for elem in sample._sample:
        LOGGER.info(elem)
    #type_length=1, event_dimension=2
    sample = sample_gen.generate_random_sample(sample_size=10, min_trace_length=5, max_trace_length=10, type_length=1, type_count=-1, event_dimension=2)
    for elem in sample._sample:
        LOGGER.info(elem)
    #type_length=1, event_dimension=3
    sample = sample_gen.generate_random_sample(sample_size=10, min_trace_length=5, max_trace_length=10, type_length=1, type_count=-1, event_dimension=3)
    for elem in sample._sample:
        LOGGER.info(elem)

    #type_length=5, event_dimension=2
    sample = sample_gen.generate_random_sample(sample_size=10, min_trace_length=5, max_trace_length=5, type_length=5, type_count=-1, event_dimension=2)
    for elem in sample._sample:
        LOGGER.info(elem)
    LOGGER.info('Finished test test_generate_random_sample')

def test_generate_fragmentation_sample_multidim():
    """
        Test for function generate_fragmentation_*_sample.
    """
    LOGGER.info('Started test test_generate_fragmentation_sample')
    LOGGER.info('#################### type_length=1, event_dimension=1 ####################')
    #type_length=1, event_dimension=1
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=1)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=1, event_dimension=2 ####################')
    #type_length=1, event_dimension=2
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=2)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=1, event_dimension=3 ####################')
    #type_length=1, event_dimension=3
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=3)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=1 ####################')
    #type_length=2, event_dimension=1
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=1)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=2 ####################')
    #type_length=2, event_dimension=2
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=2)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=3 ####################')
    #type_length=2, event_dimension=3
    sample_gen1 = MultidimSampleGenerator()
    sample_gen1.generate_fragmentation_gauss_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=3)
    for trace in sample_gen1._msample._sample:
        LOGGER.info(trace)

    LOGGER.info('#################### fragmentation_quartered ####################')

    LOGGER.info('#################### type_length=1, event_dimension=1 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=1)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=1, event_dimension=2 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=2)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=1, event_dimension=3 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=1, type_count=-1, event_dimension=3)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=1 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=1)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=2 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=2)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('#################### type_length=2, event_dimension=3 ####################')
    sample_gen2 = MultidimSampleGenerator()
    sample_gen2.generate_fragmentation_quartered_sample(sample_size=10, min_trace_length=10, max_trace_length=20, type_length=2, type_count=-1, event_dimension=3)
    for trace in sample_gen2._msample._sample:
        LOGGER.info(trace)
    LOGGER.info('Finished test test_generate_fragmentation_sample')

def test_build_typeset_multidim():
    """
        Test for function build_typeset.
    """
    LOGGER.info('Started test test_build_typeset')
    type_length = 1
    upper_bound = 10
    sample_gen = MultidimSampleGenerator()
    LOGGER.info(sample_gen.build_typeset(upper_bound, type_length))

    type_length = 2
    upper_bound = 10
    sample_gen = MultidimSampleGenerator()
    LOGGER.info(sample_gen.build_typeset(upper_bound, type_length))

    type_length = 3
    upper_bound = 10
    sample_gen = MultidimSampleGenerator()
    LOGGER.info(sample_gen.build_typeset(upper_bound, type_length))

    type_length = 2
    upper_bound = 52**type_length
    sample_gen = MultidimSampleGenerator()
    sample_gen.build_typeset(upper_bound, type_length)
    LOGGER.info('Finished test test_build_typeset')

def test_generate_sample_w_empty_queryset():
    """
        Test for function generate_sample_w_empty_queryset.
    """
    LOGGER.info('Started test test_generate_sample_w_empty_queryset')
    sample_size = 2
    min_trace_length = 100

    generator = MultidimSampleGenerator()
    generator.generate_sample_w_empty_queryset(sample_size=sample_size, min_trace_length=min_trace_length, max_trace_length=min_trace_length, event_dimensions=3)

    sample = generator._msample
    vsdb = sample.get_vertical_sequence_database()
    alphabet = sample.get_supported_typeset(1.0)
    assert not alphabet
    patternset = set([key for key in vsdb.keys() for item in vsdb[key].keys() if len(vsdb[key][item]) >= 2])
    assert not patternset
