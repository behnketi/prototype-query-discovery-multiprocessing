#!/usr/bin/python3
"""Contains tests for Error classes"""
import sys
sys.path.append(".")

from error import EmptySampleError,InconsistentQueryError,QueryRegexError,InvalidQueryStringLengthError,InvalidQuerySupportError,InvalidQueryGapConstraintError,InvalidQueryGlobalWindowSizeError,InvalidQueryLocalWindowSizeError,InvalidQueryClassError,InvalidQueryDiscoveryError,InvalidQueryMatchtestError

def test_errors():
    """
        Test for different Error-Classes.
    """
    # empty sample
    try:
        raise EmptySampleError("test")
    except EmptySampleError:
        pass

    # inconsistent query
    try:
        raise InconsistentQueryError("test")
    except InconsistentQueryError:
        pass

    # error in regex of query
    try:
        raise QueryRegexError("test")
    except QueryRegexError:
        pass

    # wrong query length
    try:
        raise InvalidQueryStringLengthError("test")
    except InvalidQueryStringLengthError:
        pass

    # wrong query support
    try:
        raise InvalidQuerySupportError("test")
    except InvalidQuerySupportError:
        pass

    # wrong gap constraints
    try:
        raise InvalidQueryGapConstraintError("test")
    except InvalidQueryGapConstraintError:
        pass

    # wrong global window size
    try:
        raise InvalidQueryGlobalWindowSizeError("test")
    except InvalidQueryGlobalWindowSizeError:
        pass

    # wrong local window size
    try:
        raise InvalidQueryLocalWindowSizeError("test")
    except InvalidQueryLocalWindowSizeError:
        pass

    # undefined query class
    try:
        raise InvalidQueryClassError("test")
    except InvalidQueryClassError:
        pass

    # undefined query discovery algorithm
    try:
        raise InvalidQueryDiscoveryError("test")
    except InvalidQueryDiscoveryError:
        pass

    # undefined match test algorithm
    try:
        raise InvalidQueryMatchtestError("test")
    except InvalidQueryMatchtestError:
        pass
