"""Contains tests for functions in plot_statistics_testbench_shinohara_multidim.py"""
import logging
import os
import sys
import pandas as pd
sys.path.append(".")
from plot_statistics_tb_shin_multidim_discovery import _split_stats_df, _plot_testbench_shin_multi_querystringlength_to_runtime, _plot_testbench_shin_multi_querystringlength_to_matchtests, _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime, _plot_testbench_shin_multi_combinations_to_runtime

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_split_stats_df():
    """
        Test for function _split_stats_df() which should generate
        various csv-file containing stats per sample kind.
    """
    LOGGER.info('Started test _split_stats_df')

    if os.path.exists('plots/dataframe_shin_icdt'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt')
        _split_stats_df(stats_df)

    LOGGER.info('Finished test _split_stats_df')

def test_plot_testbench_shin_multi_querystringlength_to_runtime():
    """
        Test for function _plot_querystringlength_to_runtime() which should
        generate plots querystringlength to runtime per sample kind random,
        fragmentaion-gauss and fragmentation-quartered if corresponding csv
        files exist
    """
    LOGGER.info('Started test test_plot_testbench_shin_multi_querystringlength_to_runtime')

    if os.path.exists('plots/dataframe_shin_icdt_random'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
        _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="random")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
        _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-quartered")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
        _plot_testbench_shin_multi_querystringlength_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-gauss")

    LOGGER.info('Finished test test_plot_testbench_shin_multi_querystringlength_to_runtime')

def test_plot_testbench_shin_multi_querystringlength_to_matchtests():
    """
        Test for function
        _plot_testbench_shin_multi_querystringlength_to_matchtests() which
        should generate plots querystringlength to number of matchtests per
        sample kind random, fragmentaion-gauss and fragmentation-quartered if
        corresponding csv files exist
    """
    LOGGER.info('Started test test_plot_testbench_shin_multi_querystringlength_to_matchtests')

    if os.path.exists('plots/dataframe_shin_icdt_random'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
        _plot_testbench_shin_multi_querystringlength_to_matchtests(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="random")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
        _plot_testbench_shin_multi_querystringlength_to_matchtests(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-quartered")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
        _plot_testbench_shin_multi_querystringlength_to_matchtests(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-gauss")

    LOGGER.info('Finished test test_plot_testbench_shin_multi_querystringlength_to_matchtests')

def test_plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime():
    """
        Test for function
        _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime()
        which should generate plots querystringlength to avg matchtest runtime
        per sample kind random, fragmentaion-gauss and fragmentation-quartered
        if corresponding csv files exist
    """
    LOGGER.info('Started test test_plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime')

    if os.path.exists('plots/dataframe_shin_icdt_random'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
        _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="random")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
        _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-quartered")
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
        _plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0], filename_base="fragmentation-gauss")

    LOGGER.info('Finished test test_plot_testbench_shin_multi_querystringlength_to_avg_matchtest_runtime')

def test_plot_testbench_shin_multi_combinations_to_runtime():
    """
        Test for function _plot_testbench_shin_multi_combinations_to_runtime()
        which should generate plots combinations to runtime per sample kind
        random, fragmentaion-gauss and fragmentation-quartered if corresponding
        csv files exist.
    """
    LOGGER.info('Started test test_plot_testbench_shin_multi_combinations_to_runtime')

    if os.path.exists('plots/dataframe_shin_icdt_random'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_random')
        _plot_testbench_shin_multi_combinations_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0])
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-quartered'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-quartered')
        _plot_testbench_shin_multi_combinations_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0])
    if os.path.exists('plots/dataframe_shin_icdt_fragmentation-gauss'):
        stats_df = pd.read_csv('plots/dataframe_shin_icdt_fragmentation-gauss')
        _plot_testbench_shin_multi_combinations_to_runtime(stats_df=stats_df, sim_supp_list=[0.3,0.5,0.6,1.0])

    LOGGER.info('Finished test test_plot_testbench_shin_multi_combinations_to_runtime')
