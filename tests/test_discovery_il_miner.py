#!/usr/bin/python3
"""Contains tests for functions in discovery_il_miner.py"""
import logging
import sys
import gzip

sys.path.append(".")
from sample_multidim import MultidimSample
import discovery_il_miner as ilm
from query_multidim import MultidimQuery
from discovery_bu_multidim import bu_discovery_multidim


#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_il_miner():
    """Test functions of discovery-il-miner.
    """
    sample = MultidimSample()
    sample_set = ["b; a; a; b; a; b; b;", "b; a; a; b; b; b;", "a; a; b; b; a;"]
    sample.set_sample(sample_set)
    eventset, event_tmp_dict = ilm.eventset_vertdb(sample_set)
    LOGGER.info('Testing function eventset_vertdb')
    assert ('b;' in eventset) is True
    assert eventset['b;'] == {(';',), ('b;',)}
    assert ((';',) in event_tmp_dict) is True
    assert len(event_tmp_dict[(';',)][1]) == sample_set[0].count(' ')+1

    LOGGER.info('Testing function set_of_event_templates')
    event_template_sets = ilm.set_of_event_templates(sample_set, eventset, event_tmp_dict)
    for template in event_template_sets[0][0]:
        assert (template in event_tmp_dict) is True

    sequence_templates = ilm.pincer_search(event_template_sets, max_query_length=-1)
    LOGGER.info('Testing function pincer search')
    for seq_tuple in list(sequence_templates)[0][0]:
        assert (seq_tuple in event_tmp_dict) is True

    print(ilm.il_miner(sample))

    sample_set = ["m;b; t;a; t;a; t;b; t;a; t;b; t;b;",
                "m;b; m;a; m;a; m;b; m;b; m;b;",
                "m;a; t;a; m;b; t;b; t;a;"]

    eventset, event_tmp_dict = ilm.eventset_vertdb(sample_set)

    LOGGER.info('Testing function eventset_vertdb')
    assert ('m;b;' in eventset) is True
    assert list(eventset['m;b;']).sort() == [(';;',), ('m;;',), (';b;',), ('m;b;',)].sort()
    assert ((';;',) in event_tmp_dict) is True
    assert len(event_tmp_dict[(';;',)][1]) == sample_set[0].count(' ')+1

    LOGGER.info('Testing function set_of_event_templates')
    event_template_sets = ilm.set_of_event_templates(sample_set, eventset, event_tmp_dict)
    for template in event_template_sets[0][0]:
        assert (template in event_tmp_dict) is True

    sequence_templates = ilm.pincer_search(event_template_sets, max_query_length=-1)
    LOGGER.info('Testing function pincer search')
    for seq_tuple in list(sequence_templates)[0][0]:
        assert (seq_tuple in event_tmp_dict) is True


def test_il_miner2():
    file_path = 'datasets/finance/FirstPaper/samples/finance_query1.txt'
    file = open(file_path, 'r', encoding="utf-8")
    sample_list = []
    counter = 0
    for _, trace1 in enumerate(file):
        sample_list.append(' '.join(trace1.split()[-5:]))
        if counter == 100:
            break
        counter += 1

    file.close()
    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict = ilm.il_miner(sample, complete=True)
    queryset = result_dict['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    assert queryset == queryset_bu # {';$x0;;; ;$x0;;; ;$x1;;; GOOG;$x1;;;'}

def test_il_miner3():
    file_path = 'datasets/google/FirstPaper/samples/google_query1.txt.gz'
    file = gzip.open(file_path, 'rb')
    sample_list = []
    counter = 0
    for _, trace1 in enumerate(file):
        sample_list.append(' '.join(trace1.decode().split()[-4:]))
        if counter == 100:
            break
        counter += 1

    file.close()
    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict_il = ilm.il_miner(sample, complete=True)
    queryset_il = result_dict_il['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    parent_dict = {}
    dict_iter = {}
    domain_cnt = sample._sample_event_dimension
    att_vsdb = sample.get_att_vertical_sequence_database()
    vsdb = {}
    patternset ={}
    gen_event= ';' * domain_cnt
    gen_event_list = [i for i in gen_event]
    for domain, dom_vsdb in att_vsdb.items():
        patternset[domain] = set()
        for key, value in dom_vsdb.items():
            new_key = ''.join(gen_event_list[:domain] + [key] + gen_event_list[domain:])
            vsdb[new_key] = value
            for item in value.keys():
                if len(value[item]) >= 2:
                    patternset[domain].add(key)
                    break
    for querystring in queryset_il:
        query = MultidimQuery()
        query.set_query_string(querystring)
        query.set_query_matchtest('smarter')
        query.set_pos_last_type_and_variable()
        matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter, patternset=patternset, parent_dict=parent_dict)
        assert matching
    assert queryset_il == queryset_bu

def test_il_miner4():
    file_path = 'datasets/google/FirstPaper/samples/google_query2.txt.gz'
    file = gzip.open(file_path, 'rb')
    sample_list = []
    counter = 0
    for _, trace1 in enumerate(file):
        sample_list.append(' '.join(trace1.decode().split()[-4:]))
        if counter == 100:
            break
        counter += 1

    file.close()
    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict_il = ilm.il_miner(sample, complete=True)
    queryset_il = result_dict_il['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    parent_dict = {}
    dict_iter = {}
    domain_cnt = sample._sample_event_dimension
    att_vsdb = sample.get_att_vertical_sequence_database()
    vsdb = {}
    patternset ={}
    gen_event= ';' * domain_cnt
    gen_event_list = [i for i in gen_event]
    for domain, dom_vsdb in att_vsdb.items():
        patternset[domain] = set()
        for key, value in dom_vsdb.items():
            new_key = ''.join(gen_event_list[:domain] + [key] + gen_event_list[domain:])
            vsdb[new_key] = value
            for item in value.keys():
                if len(value[item]) >= 2:
                    patternset[domain].add(key)
                    break
    for querystring in queryset_il:
        query = MultidimQuery()
        query.set_query_string(querystring)
        query.set_query_matchtest('smarter')
        query.set_pos_last_type_and_variable()
        matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter, patternset=patternset, parent_dict=parent_dict)
        assert matching
    assert queryset_il == queryset_bu


def test_il_miner5():
    file_path = 'datasets/google/FirstPaper/samples/google_query1.txt.gz'
    file = gzip.open(file_path, 'rb')
    sample_list = []
    counter = 0
    for _, trace1 in enumerate(file):
        sample_list.append(' '.join(trace1.decode().split()[-5:]))
        if counter == 100:
            break
        counter += 1

    file.close()
    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict_il = ilm.il_miner(sample, complete=True)
    queryset_il = result_dict_il['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    assert queryset_il == queryset_bu

def test_il_miner6():
    file_path = 'datasets/finance/FirstPaper/samples/finance_query1.txt'
    with open(file_path, 'r', encoding="utf-8") as file:
        sample_list = []
        counter = 0
        fin_trace = 38
        for trace1 in file:
            if counter in  [0,fin_trace]:
                sample_list.append(' '.join(trace1.split()[-6:]))
                if counter == fin_trace:
                    break
            counter += 1

    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict_il = ilm.il_miner(sample, complete=True)
    queryset_il = result_dict_il['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    parent_dict = {}
    dict_iter = {}
    domain_cnt = sample._sample_event_dimension
    att_vsdb = sample.get_att_vertical_sequence_database()
    vsdb = {}
    patternset ={}
    gen_event= ';' * domain_cnt
    gen_event_list = list(gen_event)
    for domain, dom_vsdb in att_vsdb.items():
        patternset[domain] = set()
        for key, value in dom_vsdb.items():
            new_key = ''.join(gen_event_list[:domain] + [key] + gen_event_list[domain:])
            vsdb[new_key] = value
            for item in value.keys():
                if len(value[item]) >= 2:
                    patternset[domain].add(key)
                    break
    for querystring in queryset_il:
        query = MultidimQuery()
        query.set_query_string(querystring)
        query.set_query_matchtest('smarter')
        query.set_pos_last_type_and_variable()
        matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter, patternset=patternset, parent_dict=parent_dict)
        assert matching
    assert queryset_il == queryset_bu

def test_il_miner7():
    file_path = 'datasets/google/FirstPaper/samples/google_query2.txt.gz'
    file = gzip.open(file_path, 'rb')
    sample_list = []
    counter = 0
    for _, trace1 in enumerate(file):
        sample_list.append(' '.join(trace1.decode().split()[-5:]))
        if counter == 2:
            break
        counter += 1

    file.close()
    sample = MultidimSample()
    sample.set_sample(sample_list)
    result_dict_il = ilm.il_miner(sample, complete=True)
    queryset_il = result_dict_il['queryset']
    result_dict_bu = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False)
    queryset_bu = result_dict_bu['queryset']
    parent_dict = {}
    dict_iter = {}
    domain_cnt = sample._sample_event_dimension
    att_vsdb = sample.get_att_vertical_sequence_database()
    vsdb = {}
    patternset ={}
    gen_event= ';' * domain_cnt
    gen_event_list = [i for i in gen_event]
    for domain, dom_vsdb in att_vsdb.items():
        patternset[domain] = set()
        for key, value in dom_vsdb.items():
            new_key = ''.join(gen_event_list[:domain] + [key] + gen_event_list[domain:])
            vsdb[new_key] = value
            for item in value.keys():
                if len(value[item]) >= 2:
                    patternset[domain].add(key)
                    break
    for querystring in queryset_il:
        query = MultidimQuery()
        query.set_query_string(querystring)
        query.set_query_matchtest('smarter')
        query.set_pos_last_type_and_variable()
        matching = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter, patternset=patternset, parent_dict=parent_dict)
        assert matching
    assert queryset_il == queryset_bu