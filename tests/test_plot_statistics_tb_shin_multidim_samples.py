#!/usr/bin/python3
"""Contains tests for functions in plot_statistics.py"""
import sys
sys.path.append(".")

import os
import logging
from testbench_helper_functions import _generate_experiments
from plot_statistics_tb_shin_multidim_samples import _collect_and_plot_sample_statistics

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_collect_and_plot_sample_statistics():
    """
        Test for funtion _collect_and_plot_sample_statistics() which should
        generate samples by using _generate_experiments and create a dataframe
        containing all sample statistics and plots.
    """
    LOGGER.info('Started test collect_and_plot_sample_statistics')
    if not os.path.exists('samples'):
        os.mkdir('samples')
        _generate_experiments(params_sample_size=[10], params_trace_length=[(10,25)],params_type_length=1,params_dimension=[2])
    elif not os.listdir('samples'):
        _generate_experiments(params_sample_size=[10], params_trace_length=[(10,25)],params_type_length=1,params_dimension=[2])
    _collect_and_plot_sample_statistics()
    LOGGER.info('Finished test collect_and_plot_sample_statistics')
