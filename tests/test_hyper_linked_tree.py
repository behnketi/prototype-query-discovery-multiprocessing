#!/usr/bin/python3
"""Contains tests for class Query"""
import logging
from hyper_linked_tree import Vertex, HyperLinkedTree
from error import InvalidQuerySupportError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test00_vertex__init__():
    """
        Testing '__init__()': checks whether a vertex is initialized correctly"
    """
    LOGGER.info('Started test 00 for vertex__init__:')

    vertex = Vertex("abc")
    assert vertex.query_string == "abc"
    assert vertex.matched_traces is None
    assert vertex.child_vertices == set()
    assert vertex.parent_vertices == set()
    assert vertex._bool_found_all_parents is None

    LOGGER.info('Finished test 00 for vertex__init__:')

def test00_vertex__str__():
    """
        Testing '__str__()': checks whether a vertex is printed correctly"
    """
    LOGGER.info('Started test 00 for vertex__str__')

    root_vertex = Vertex("root")

    curr_vertex = Vertex("current")

    ch01_vertex = Vertex("child 1")
    ch02_vertex = Vertex("child 2")

    curr_vertex.matched_traces = [1,3,5]
    curr_vertex.child_vertices = set([ch01_vertex, ch02_vertex])
    curr_vertex.parent_vertices = set([root_vertex])

    result = "--- 'current'\nMatch: [1, 3, 5]\nChild: {'child 1', 'child 2'}\nParent:None - {'root'}"
    assert result == curr_vertex.__str__()

    LOGGER.info('Finished test 00 for vertex__str__')

def test00_vertex_is_frequent():
    """
        Testing 'is_frequent()': checks whether a vertex is printed correctly"
    """
    LOGGER.info('Started test 00 for is_frequent')

    vertex = Vertex("")
    vertex._bool_found_all_parents = False

    assert vertex.is_frequent(None) is False

    vertex._bool_found_all_parents = None

    assert vertex.is_frequent(None) is None

    vertex.matched_traces = [0,1]

    try:
        vertex.is_frequent(0)
        raise AssertionError("No exception raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err
    try:
        vertex.is_frequent(2.0)
        raise AssertionError("No exception raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err

    assert vertex.is_frequent(3) is False
    assert vertex.is_frequent(2) is True

    LOGGER.info('Finished test 00 for is_frequent')

def test00_hyper_linked_tree__init__():
    """
        Testing '__init__()': checks whether a hyperlinked tree is initialized correctly"
    """
    LOGGER.info('Started test 00 for hyper_linked_tree__init__')

    try:
        HyperLinkedTree(0)
        raise AssertionError("No Exception raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err

    try:
        HyperLinkedTree(-1)
        raise AssertionError("No Exception raised!")
    except InvalidQuerySupportError:
        pass
    except Exception as err:
        raise err

    try:
        HyperLinkedTree(0.5)
        raise AssertionError("No Exception raised!")
    except InvalidQuerySupportError as err:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for hyper_linked_tree__init__')

def test01_hyper_linked_tree__init__():
    """
        Testing '__init__()': checks whether a hyperlinked tree is initialized correctly"
    """
    LOGGER.info('Started test 01 for hyper_linked_tree__init__')

    new_tree = HyperLinkedTree(1)

    result = "--- ''\nMatch: [0]\nChild: {}\nParent:True - {}"

    assert result == new_tree.find_vertex("").__str__()

    LOGGER.info('Finished test 01 for hyper_linked_tree__init__')

def test00_find_vertex():
    """
        Testing 'insert_query_string()': checks an existing vertex is found by its query string
    """
    LOGGER.info('Started test 00 for find_vertex')

    hlt = HyperLinkedTree(1)

    vertex = hlt.find_vertex("")

    assert  vertex.query_string == ""

    LOGGER.info('Finished test 00 for find_vertex')

def test00_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether type_queries are inserted correctly
    """
    LOGGER.info('Started test 00 for insert_query_string')

    hlt = HyperLinkedTree(1)

    try:
        hlt.insert_query_string(None, "a")
        raise AssertionError("No exception raised!")
    except ValueError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for insert_query_string')

def test01_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether type_queries are inserted correctly
    """
    LOGGER.info('Started test 01 for insert_query_string')

    hlt = HyperLinkedTree(1)

    root = hlt.get_root()

    LOGGER.info(hlt)
    hlt.insert_query_string(root, "a")
    hlt.insert_query_string(root, "b")
    hlt.insert_query_string(root, "ab")
    hlt.insert_query_string(hlt.find_vertex("a"), "a b")

    result =  "--- ''\nMatch: [0]\nChild: {'a', 'ab', 'b'}\nParent:True - {}\n"
    result += "--- 'a'\nMatch: None\nChild: {'a b'}\nParent:True - {''}\n"
    result += "--- 'b'\nMatch: None\nChild: {}\nParent:True - {''}\n"
    result += "--- 'ab'\nMatch: None\nChild: {}\nParent:True - {''}\n"
    result += "--- 'a b'\nMatch: None\nChild: {}\nParent:True - {'a', 'b'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 01 for insert_query_string')

def test02_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether pattern_queries are inserted correctly
    """
    LOGGER.info('Started test 02 for insert_query_string')

    hlt = HyperLinkedTree(1)

    root = hlt.get_root()

    hlt.insert_query_string(root, "$x0 $x0")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x0 $x0")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x0 $x1 $x1")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x1 $x0 $x1")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x1 $x1 $x0")

    result =  "--- ''\nMatch: [0]\nChild: {'$x0 $x0'}\nParent:True - {}\n"
    result += "--- '$x0 $x0'\nMatch: None\nChild: {'$x0 $x0 $x0', '$x0 $x0 $x1 $x1', '$x0 $x1 $x0 $x1', '$x0 $x1 $x1 $x0'}\nParent:True - {''}\n"
    result += "--- '$x0 $x0 $x0'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x0 $x1 $x1'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x1 $x0 $x1'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x1 $x1 $x0'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 02 for insert_query_string')

def test03_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether mixed_queries are inserted correctly
    """
    LOGGER.info('Started test 03 for insert_query_string')

    hlt = HyperLinkedTree(1)

    root = hlt.get_root()

    hlt.insert_query_string(root, "a")
    hlt.insert_query_string(root, "$x0 $x0")
    hlt.insert_query_string(hlt.find_vertex("a"), "a a")
    hlt.insert_query_string(hlt.find_vertex("a"), "a $x0 $x0")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 a $x0")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x0 a")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x0 $x0")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x0 $x1 $x1")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x1 $x0 $x1")
    hlt.insert_query_string(hlt.find_vertex("$x0 $x0"), "$x0 $x1 $x1 $x0")

    result =  "--- ''\nMatch: [0]\nChild: {'a', '$x0 $x0'}\nParent:True - {}\n"
    result += "--- 'a'\nMatch: None\nChild: {'a a', 'a $x0 $x0'}\nParent:True - {''}\n"
    result += "--- '$x0 $x0'\nMatch: None\nChild: {'$x0 $x0 $x0', '$x0 $x0 a', '$x0 a $x0', '$x0 $x0 $x1 $x1', '$x0 $x1 $x0 $x1', '$x0 $x1 $x1 $x0'}\nParent:True - {''}\n"
    result += "--- 'a a'\nMatch: None\nChild: {}\nParent:True - {'a'}\n"
    result += "--- 'a $x0 $x0'\nMatch: None\nChild: {}\nParent:True - {'a', '$x0 $x0'}\n"
    result += "--- '$x0 a $x0'\nMatch: None\nChild: {}\nParent:True - {'a', '$x0 $x0'}\n"
    result += "--- '$x0 $x0 a'\nMatch: None\nChild: {}\nParent:True - {'a', '$x0 $x0'}\n"
    result += "--- '$x0 $x0 $x0'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x0 $x1 $x1'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x1 $x0 $x1'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}\n"
    result += "--- '$x0 $x1 $x1 $x0'\nMatch: None\nChild: {}\nParent:True - {'$x0 $x0'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 03 for insert_query_string')

def test04_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether multidim type_queries are inserted correctly
    """
    LOGGER.info('Started test 04 for insert_query_string')

    hlt = HyperLinkedTree(1,event_dimension=2)

    root = hlt.get_root()

    hlt.insert_query_string(root, "a;;", search_for_parents=True)
    hlt.insert_query_string(root, "b;;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("b;;"), "b;b;", search_for_parents=True)
    hlt.insert_query_string(root, ";a;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("a;;"), "a;a;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("a;;"), "a;; a;;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("a;;"), "a;; b;;", search_for_parents=True)

    result =  "--- ''\nMatch: [0]\nChild: {';a;', 'a;;', 'b;;'}\nParent:True - {}\n"
    result += "--- 'a;;'\nMatch: None\nChild: {'a;a;', 'a;; a;;', 'a;; b;;'}\nParent:True - {''}\n"
    result += "--- 'b;;'\nMatch: None\nChild: {'b;b;'}\nParent:True - {''}\n"
    result += "--- 'b;b;'\nMatch: None\nChild: {}\nParent:False - {'b;;'}\n"
    result += "--- ';a;'\nMatch: None\nChild: {}\nParent:True - {''}\n"
    result += "--- 'a;a;'\nMatch: None\nChild: {}\nParent:True - {';a;', 'a;;'}\n"
    result += "--- 'a;; a;;'\nMatch: None\nChild: {}\nParent:True - {'a;;'}\n"
    result += "--- 'a;; b;;'\nMatch: None\nChild: {}\nParent:True - {'a;;', 'b;;'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 04 for insert_query_string')

def test05_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether multidim pattern_queries are inserted correctly
    """
    LOGGER.info('Started test 05 for insert_query_string')

    hlt = HyperLinkedTree(1,event_dimension=2)

    root = hlt.get_root()

    hlt.insert_query_string(root, "$x0;; $x0;;", search_for_parents=True)
    hlt.insert_query_string(root, ";$x0; ;$x0;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("$x0;; $x0;;"), "$x0;; $x0;; $x0;;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("$x0;; $x0;;"), "$x0;$x1; $x0;$x1;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex(";$x0; ;$x0;"), ";$x0; $x1;$x0; $x1;;", search_for_parents=True)

    result =  "--- ''\nMatch: [0]\nChild: {'$x0;; $x0;;', ';$x0; ;$x0;'}\nParent:True - {}\n"
    result += "--- '$x0;; $x0;;'\nMatch: None\nChild: {'$x0;$x1; $x0;$x1;', '$x0;; $x0;; $x0;;'}\nParent:True - {''}\n"
    result += "--- ';$x0; ;$x0;'\nMatch: None\nChild: {';$x0; $x1;$x0; $x1;;'}\nParent:True - {''}\n"
    result += "--- '$x0;; $x0;; $x0;;'\nMatch: None\nChild: {}\nParent:True - {'$x0;; $x0;;'}\n"
    result += "--- '$x0;$x1; $x0;$x1;'\nMatch: None\nChild: {}\nParent:True - {'$x0;; $x0;;', ';$x0; ;$x0;'}\n"
    result += "--- ';$x0; $x1;$x0; $x1;;'\nMatch: None\nChild: {}\nParent:True - {'$x0;; $x0;;', ';$x0; ;$x0;'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 05 for insert_query_string')

def test06_insert_query_string():
    """
        Testing 'insert_query_string()': checks whether multidim mixed_queries are inserted correctly
    """
    LOGGER.info('Started test 06 for insert_query_string')

    hlt = HyperLinkedTree(1,event_dimension=2)

    root = hlt.get_root()

    hlt.insert_query_string(root, "a;;", search_for_parents=True)
    hlt.insert_query_string(root, ";b;", search_for_parents=True)
    hlt.insert_query_string(root, "$x0;; $x0;;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("$x0;; $x0;;"), "$x0;; a;; $x0;;", search_for_parents=True)
    hlt.insert_query_string(hlt.find_vertex("$x0;; $x0;;"), "$x0;b; $x0;;", search_for_parents=True)

    result =  "--- ''\nMatch: [0]\nChild: {';b;', 'a;;', '$x0;; $x0;;'}\nParent:True - {}\n"
    result += "--- 'a;;'\nMatch: None\nChild: {}\nParent:True - {''}\n"
    result += "--- ';b;'\nMatch: None\nChild: {}\nParent:True - {''}\n"
    result += "--- '$x0;; $x0;;'\nMatch: None\nChild: {'$x0;b; $x0;;', '$x0;; a;; $x0;;'}\nParent:True - {''}\n"
    result += "--- '$x0;; a;; $x0;;'\nMatch: None\nChild: {}\nParent:True - {'a;;', '$x0;; $x0;;'}\n"
    result += "--- '$x0;b; $x0;;'\nMatch: None\nChild: {}\nParent:True - {';b;', '$x0;; $x0;;'}"

    assert result == str(hlt)

    LOGGER.info('Finished test 06 for insert_query_string')

def test00_set_match_results():
    """
        Testing 'set_match_results()': checks whether all exceptions are thrown correctly
    """
    LOGGER.info('Started test 00 for set_match_results')

    hlt = HyperLinkedTree(1)

    root = hlt.get_root()

    hlt.set_match_results(root, True)
    hlt.set_match_results(root, False)

    try:
        hlt.set_match_results(root, None)
        raise AssertionError("No exception raised!")
    except TypeError:
        pass
    except Exception as err:
        raise err

    try:
        hlt.set_match_results(root, 5)
        raise AssertionError("No exception raised!")
    except TypeError:
        pass
    except Exception as err:
        raise err

    hlt.set_match_results(root, [])
    hlt.set_match_results(root, [4, 0, 10])
    try:
        hlt.set_match_results(root, [-1, 0])
        raise AssertionError("No exception raised!")
    except ValueError:
        pass
    except Exception as err:
        raise err

    try:
        hlt.set_match_results(root, [1, 0, 8, 9.6])
        raise AssertionError("No exception raised!")
    except ValueError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for set_match_results')
