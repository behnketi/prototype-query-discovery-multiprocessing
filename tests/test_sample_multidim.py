#!/usr/bin/python3
"""Contains tests for the class Sample"""
import sys
sys.path.append(".")
import logging
from sample_multidim import MultidimSample
from error import EmptySampleError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_set_sample_event_dimension():
    """
        Test for function set_sample_event_dimension().
    """
    LOGGER.info('Started test of function set_sample_event_dimension()')
    sample1 = MultidimSample()
    assert sample1._sample_event_dimension == 1, 'sample1: Wrong event dimension'
    sample2 = MultidimSample([])
    assert sample2._sample_event_dimension == 1, 'sample2: Wrong event dimension'
    sample3 = MultidimSample(['a; b; c;'])
    assert sample3._sample_event_dimension == 1, 'sample3: Wrong event dimension'
    sample4 = MultidimSample(['a;d; b;e; c;f;'])
    assert sample4._sample_event_dimension == 2, 'sample4: Wrong event dimension'
    sample5 = MultidimSample(['a;d; b;e; c;f;', 'a;d; b;e; c;f;'])
    assert sample5._sample_event_dimension == 2, 'sample5: Wrong event dimension'
    sample6 = MultidimSample(['a;d; b;e; c;f;', 'a;d;d; b;e;e; c;f;f;'])
    assert sample6._sample_event_dimension == 3, 'sample6: Wrong event dimension'
    sample7 = MultidimSample(['a;d; b;e; c;f;', 'a;d; b;e; c;f;f;'])
    assert sample7._sample_event_dimension == 3, 'sample7: Wrong event dimension'
    sample8 = MultidimSample(['a;d; b;e; c;f;', 'a;d;; b;e;; c;f;;'])
    assert sample8._sample_event_dimension == 3, 'sample8: Wrong event dimension'
    LOGGER.info('Finished test of function set_sample_event_dimension()')

def test_set_sample_typeset():
    """
        Test for function set_sample_typeset().
    """
    LOGGER.info('Started test of function set_sample_event_dimension()')
    #sample1 = MultidimSample()
    #assert sample1.sample_typeset == set(), 'sample1: Wrong typeset'
    #assert sample1.sample_att_typesets == dict(), 'sample1: Wrong dim typesets'
    #sample2 = MultidimSample([])
    #assert sample2.sample_typeset == set(), 'sample2: Wrong typeset'
    #assert sample2.sample_att_typesets == dict(), 'sample2: Wrong dim typesets'
    sample3 = MultidimSample(['a; b; c;'])
    assert sample3.sample_typeset == {'a','b','c'}, 'sample3: Wrong typeset'
    assert sample3.sample_att_typesets ==  {0: {'a','b','c'}}, 'sample3: Wrong dim typesets'
    sample4 = MultidimSample(['a;d; b;e; c;f;'])
    assert sample4.sample_typeset == {'a','b','c','d','e','f'}, 'sample4: Wrong typeset'
    assert sample4.sample_att_typesets ==  {0: {'a','b','c'}, 1: {'d','e','f'}}, 'sample4: Wrong dim typesets'
    sample5 = MultidimSample(['a;d; b;e; c;f;', 'a;d; b;e; c;f;'])
    assert sample5.sample_typeset == {'a','b','c','d','e','f'}, 'sample5: Wrong typeset'
    assert sample5.sample_att_typesets ==  {0: {'a','b','c'}, 1: {'d','e','f'}}, 'sample5: Wrong dim typesets'
    sample6 = MultidimSample(['a;d; b;e; c;f;', 'a;d;d; b;e;e; c;f;f;'])
    assert sample6.sample_typeset == {'a','b','c','d','e','f'}, 'sample6: Wrong typeset'
    assert sample6.sample_att_typesets ==  {0: {'a','b','c'}, 1: {'d','e','f'}, 2: {'d','e','f'}}, 'sample6: Wrong dim typesets'
    sample7 = MultidimSample(['a;d; b;e; c;f;', 'a;d; b;e; c;f;f;'])
    assert sample7.sample_typeset == {'a','b','c','d','e','f'}, 'sample7: Wrong typeset'
    assert sample7.sample_att_typesets == {0: {'a','b','c'}, 1: {'d','e','f'}, 2: {'f'}}, 'sample7: Wrong dim typesets'
    sample8 = MultidimSample(['a;d; b;e; c;f;', 'a;d;; b;e;; c;f;;'])
    assert sample8.sample_typeset == {'a','b','c','d','e','f'}, 'sample8: Wrong typeset'
    assert sample8.sample_att_typesets ==  {0: {'a','b','c'}, 1: {'d','e','f'}, 2: set()}, 'sample8: Wrong dim typesets'
    LOGGER.info('Finished test of function set_sample_event_dimension()')

def test_get_l_w_tuples():
    """
        Test for function set_sample_typeset().
    """
    LOGGER.info('Started test of function get_l_w_tuples()')
    sample1 = MultidimSample(['a; b; c; d;'])
    lwtuples = sample1.get_l_w_tuples()
    assert lwtuples == [
        (1, [(-1, -1), (-1, -1)]),
        (2, [(-1, -1), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 1), (-1, -1)]), (2, [(-1, -1), (1, 1), (-1, -1)]),
        (3, [(-1, -1), (0, 0), (0, 0), (-1, -1)]),
        (4, [(-1, -1), (0, 0), (0, 0), (0, 0), (-1, -1)])], 'sample1: Wrong l-w-tuples'
    sample2 = MultidimSample(['a;a; b;b; c;c; d;d;'])
    lwtuples = sample2.get_l_w_tuples()
    assert lwtuples == [
        (1, [(-1, -1), (0, 0), (-1, -1)]),
        (2, [(-1, -1), (0, 0), (0, 0), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (0, 1), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (1, 1), (0, 0), (-1, -1)]),
        (3, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)]),
        (4, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)])], 'sample2: Wrong l-w-tuples'
    sample3 = MultidimSample(['a;a;a; b;b;b; c;c;c; d;d;d;'])
    lwtuples = sample3.get_l_w_tuples()
    assert lwtuples == [
        (1, [(-1, -1), (0, 0), (0, 0), (-1, -1)]),
        (2, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (0, 0), (0, 1), (0, 0), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (0, 0), (1, 1), (0, 0), (0, 0), (-1, -1)]),
        (3, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)]),
        (4, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)])], 'sample3: Wrong l-w-tuples'
    sample4 = MultidimSample(['a;a;a;a; b;b;b;b; c;c;c;c; d;d;d;d;'])
    lwtuples = sample4.get_l_w_tuples()
    assert lwtuples == [
        (1, [(-1, -1), (0, 0), (0, 0), (0, 0), (-1, -1)]),
        (2, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 1), (0, 0), (0, 0), (0, 0), (-1, -1)]), (2, [(-1, -1), (0, 0), (0, 0), (0, 0), (1, 1), (0, 0), (0, 0), (0, 0), (-1, -1)]),
        (3, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)]),
        (4, [(-1, -1), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (-1, -1)])], 'sample4: Wrong l-w-tuples'
    LOGGER.info('Finished test of function get_l_w_tuples()')

def test_get_supported_typeset():
    """
        Test for function _get_supported_typeset().
    """
    LOGGER.info('Started test of _get_supported_typeset get_l_w_tuples()')
    sample1 = MultidimSample(['a; b; c;', 'a; d; e;', 'a; f; g;', 'a; h; i;'])
    assert sample1.get_sample_supported_typeset(support=1.0, attribute=0) == {'a'}

    sample2 = MultidimSample(['a;b; c;d;','a;b; g;h;'])
    assert sample2.get_sample_supported_typeset(support=1.0, attribute=0) == {'a'}
    assert sample2.get_sample_supported_typeset(support=1.0, attribute=1) == {'b'}

    sample2 = MultidimSample(['a;b; c;d;E;','a;b; g;h;E;'])
    assert sample2.get_sample_supported_typeset(support=1.0, attribute=0) == {'a'}
    assert sample2.get_sample_supported_typeset(support=1.0, attribute=1) == {'b'}
    assert sample2.get_sample_supported_typeset(support=1.0, attribute=2) == {'E'}

    sample3 = MultidimSample(['a;b; c;d;E;','a;b; g;h;E;'])
    assert sample3.get_sample_supported_typeset(support=0.5, attribute=0) == {'a','c','g'}
    assert sample3.get_sample_supported_typeset(support=0.5, attribute=1) == {'b','d','h'}
    assert sample3.get_sample_supported_typeset(support=0.5, attribute=2) == {'E'}

    LOGGER.info('Finished test of _get_supported_typeset get_l_w_tuples()')

def test_get_trace_length_distribution():
    """
        Test of function _get_trace_length_distribution().
    """
    LOGGER.info('Started test for _get_trace_length_distribution()')
    sample1 = MultidimSample(['a; b; c;', 'a; d; e;', 'a; f; g;', 'a; h; i;'])
    assert sample1._get_trace_length_distribution() == {'3': [1.0]}

    sample2 = MultidimSample(['a;b; c;d;','a;b; g;h;'])
    assert sample2._get_trace_length_distribution() == {'2': [1.0]}

    sample3 = MultidimSample(['a;b; c;d;E;','a;b; g;h;E;'])
    assert sample3._get_trace_length_distribution() == {'2': [1.0]}

    sample4 = MultidimSample(['a;b; c;d;E;','a;b; g;h;E;','a;b; c;d;E; a;b;','a;b; g;h;E; a;b;'])
    assert sample4._get_trace_length_distribution() == {'2': [0.5], '3': [0.5]}

    sample5 = MultidimSample([
        'a;a; a;a;a;','a;a; a;a;a;',
        'a;a; a;a;a; a;a;','a;a; a;a;a; a;a;',
        'a;a; a;a;a; a;a; a;a;a;','a;a; a;a;a; a;a; a;a;a;',
        'a;a; a;a;a; a;a; a;a;a; a;a;','a;a; a;a;a; a;a; a;a;a; a;a;'])
    assert sample5._get_trace_length_distribution() == {'2': [0.25], '3': [0.25], '4': [0.25], '5': [0.25]}
    LOGGER.info('Finished test of _get_trace_length_distribution()')

def test_get_type_distribution():
    """
        Test for function _get_type_distribution for small samples.
    """
    LOGGER.info('Started test for _get_type_distribution()')
    # Tests across all attributes
    sample0 = MultidimSample()
    sample0.set_sample(["aa; ba; ca; da;", "ea; fa; ga; ha;", "ia; ja; ka; la;", "ma; na; oa; pa;"])
    assert sample0._get_type_distribution() == {'ia': [0.0625], 'ha': [0.0625], 'da': [0.0625], 'ka': [0.0625], 'ba': [0.0625], 'oa': [0.0625], 'aa': [0.0625], 'ja': [0.0625], 'ea': [0.0625], 'ga': [0.0625], 'na': [0.0625], 'ca': [0.0625], 'fa': [0.0625], 'la': [0.0625], 'ma': [0.0625], 'pa': [0.0625]}

    sample1 = MultidimSample()
    sample1.set_sample(["a;a; cc; cc; b;b;", "a;a; ee; ee; b;b;", "a;a; dd; dd; b;b;", "a;a; ee; ee; b;b;"])
    assert sample1._get_type_distribution() == {'dd': [0.08333333333333333], 'b': [0.3333333333333333], 'a': [0.3333333333333333], 'ee': [0.16666666666666666], 'cc': [0.08333333333333333]}

    sample2 = MultidimSample()
    sample2.set_sample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample2._get_type_distribution() == {'aa': [0.125], 'hh': [0.125], 'gg': [0.125], 'dd': [0.125], 'cc': [0.125], 'ee': [0.125], 'bb': [0.125], 'ff': [0.125]}

    sample3 = MultidimSample()
    sample3.set_sample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample3._get_type_distribution() == {'abc': [0.16666666666666666], 'DEF': [0.16666666666666666], 'def': [0.16666666666666666], 'ghi': [0.08333333333333333], 'jkl': [0.08333333333333333], 'JKL': [0.08333333333333333], 'ABC': [0.16666666666666666], 'GHI': [0.08333333333333333]}

    # Tests for specific attributes
    sample0 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample0._get_type_distribution(attribute=0) == {'cc': [0.125], 'dd': [0.125], 'hh': [0.125], 'aa': [0.125], 'ee': [0.125], 'ff': [0.125], 'gg': [0.125], 'bb': [0.125]}

    sample1 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample1._get_type_distribution(attribute=0) == {'abc': [0.5], 'ghi': [0.25], 'GHI': [0.25]}
    assert sample1._get_type_distribution(attribute=1) == {'JKL': [0.25], 'ABC': [0.5], 'jkl': [0.25]}
    assert sample1._get_type_distribution(attribute=2) == {'def': [1.0]}
    assert sample1._get_type_distribution(attribute=3) == {'DEF': [1.0]}
    LOGGER.info('Finished test of _get_type_distribution()')

def test_get_factors():
    """
        Test for function _get_factors().
    """
    LOGGER.info('Started test for _get_factors()')
    sample = MultidimSample()
    assert sample._get_factors(factor_length=2) == set()

    #Tests for event_factor=True
    sample0 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample0._get_factors(factor_length=2) == {'aa; bb;', 'cc; dd;', 'gg; hh;', 'dd; ee;', 'ee; ff;', 'ff; gg;', 'bb; cc;'}
    sample1 = MultidimSample(["abc;ABC; def;DEF; ghi;jkl;","abc;ABC; def;DEF; GHI;JKL;"])
    assert sample1._get_factors(factor_length=2) == {'abc;ABC; def;DEF;', 'def;DEF; GHI;JKL;', 'def;DEF; ghi;jkl;'}
    sample2 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample2._get_factors(factor_length=2) == {'abc;ABC;def;DEF; GHI;JKL;', 'abc;ABC;def;DEF; ghi;jkl;'}

    #Tests for event_factor=False
    sample0 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample0._get_factors(factor_length=2,event_factors=False) == {'cc; dd;', 'ee; ff;', 'dd; ee;', 'aa; bb;', 'bb; cc;', 'ff; gg;', 'gg; hh;'}
    sample1 = MultidimSample(["abc;ABC; def;DEF; ghi;jkl;","abc;ABC; def;DEF; GHI;JKL;"])
    assert sample1._get_factors(factor_length=2,event_factors=False) == {'abc;ABC;', 'DEF; ghi;', 'ghi;jkl;', 'ABC; def;', 'DEF; GHI;', 'def;DEF;', 'GHI;JKL;'}
    sample2 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample2._get_factors(factor_length=2,event_factors=False) == {'abc;ABC;', 'ABC;def;', 'DEF; ghi;', 'ghi;jkl;', 'DEF; GHI;', 'def;DEF;', 'GHI;JKL;'}
    assert sample2._get_factors(factor_length=3,event_factors=False) == {'abc;ABC;def;', 'def;DEF; ghi;', 'ABC;def;DEF;', 'DEF; ghi;jkl;', 'DEF; GHI;JKL;', 'def;DEF; GHI;'}
    LOGGER.info('Finished test for _get_factors()')

def test_get_factor_distribution_multidim():
    """
        Test for function _get_factor_distribution().
    """
    LOGGER.info('Started test for _get_factor_distribution()')
    sample0 = MultidimSample(["aa; ba; ca; da;", "ea; fa; ga; ha;", "ia; ja; ka; la;", "ma; na; oa; pa;"])
    assert sample0._get_factor_distribution(factor_length=1, event_factors=True) == {'ja;': 0.0625, 'aa;': 0.0625, 'ka;': 0.0625, 'oa;': 0.0625, 'ea;': 0.0625, 'ba;': 0.0625, 'fa;': 0.0625, 'na;': 0.0625, 'ca;': 0.0625, 'la;': 0.0625, 'ia;': 0.0625, 'ha;': 0.0625, 'ma;': 0.0625, 'pa;': 0.0625, 'da;': 0.0625, 'ga;': 0.0625}
    assert sample0._get_factor_distribution(factor_length=2, event_factors=True) == {'fa; ga;': 0.08333333333333333, 'ga; ha;': 0.08333333333333333, 'ma; na;': 0.08333333333333333, 'ia; ja;': 0.08333333333333333, 'ka; la;': 0.08333333333333333, 'oa; pa;': 0.08333333333333333, 'ea; fa;': 0.08333333333333333, 'ba; ca;': 0.08333333333333333, 'na; oa;': 0.08333333333333333, 'aa; ba;': 0.08333333333333333, 'ca; da;': 0.08333333333333333, 'ja; ka;': 0.08333333333333333}
    assert sample0._get_factor_distribution(factor_length=3, event_factors=False) == {'ja; ka; la;': 0.125, 'ma; na; oa;': 0.125, 'ba; ca; da;': 0.125, 'ia; ja; ka;': 0.125, 'aa; ba; ca;': 0.125, 'fa; ga; ha;': 0.125, 'na; oa; pa;': 0.125, 'ea; fa; ga;': 0.125}

    sample1 = MultidimSample(["a;a; cc; cc; b;b;", "a;a; ee; ee; b;b;", "a;a; dd; dd; b;b;", "a;a; ee; ee; b;b;"])
    assert sample1._get_factor_distribution(factor_length=1, event_factors=True) == {'ee;': 0.25, 'b;b;': 0.25, 'dd;': 0.125, 'a;a;': 0.25, 'cc;': 0.125}
    assert sample1._get_factor_distribution(factor_length=2, event_factors=True) == {
        'a;a; cc;': 0.08333333333333333, 'cc; cc;': 0.08333333333333333, 'ee; ee;': 0.16666666666666666,
        'a;a; ee;': 0.16666666666666666, 'dd; dd;': 0.08333333333333333, 'a;a; dd;': 0.08333333333333333,
        'ee; b;b;': 0.16666666666666666, 'dd; b;b;': 0.08333333333333333, 'cc; b;b;': 0.08333333333333333}
    assert sample1._get_factor_distribution(factor_length=3, event_factors=False) == {
        'a; cc; cc;': 0.0625, 'a; dd; dd;': 0.0625, 'cc; cc; b;': 0.0625,
        'a; ee; ee;': 0.125, 'ee; ee; b;': 0.125, 'dd; dd; b;': 0.0625,
        'a;a; cc;': 0.0625, 'ee; b;b;': 0.125, 'a;a; dd;': 0.0625,
        'dd; b;b;': 0.0625, 'cc; b;b;': 0.0625, 'a;a; ee;': 0.125}

    sample2 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample2._get_factor_distribution(factor_length=1, event_factors=True) == {'hh;': 0.125, 'bb;': 0.125, 'gg;': 0.125, 'dd;': 0.125, 'aa;': 0.125, 'ff;': 0.125, 'cc;': 0.125, 'ee;': 0.125}
    assert sample2._get_factor_distribution(factor_length=2, event_factors=True) == {
        'ff; gg;': 0.14285714285714285,'bb; cc;': 0.14285714285714285,
        'aa; bb;': 0.14285714285714285,'cc; dd;': 0.14285714285714285,
        'gg; hh;': 0.14285714285714285,'ee; ff;': 0.14285714285714285,
        'dd; ee;': 0.14285714285714285}
    assert sample2._get_factor_distribution(factor_length=3, event_factors=False) == {
        'cc; dd; ee;': 0.16666666666666666, 'bb; cc; dd;': 0.16666666666666666,
        'ff; gg; hh;': 0.16666666666666666, 'aa; bb; cc;': 0.16666666666666666,
        'dd; ee; ff;': 0.16666666666666666, 'ee; ff; gg;': 0.16666666666666666}

    sample3 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample3._get_factor_distribution(factor_length=1, event_factors=True) == {'ghi;jkl;': 0.25, 'abc;ABC;def;DEF;': 0.5, 'GHI;JKL;': 0.25}
    assert sample3._get_factor_distribution(factor_length=2, event_factors=True) == {'abc;ABC;def;DEF; GHI;JKL;': 0.5, 'abc;ABC;def;DEF; ghi;jkl;': 0.5}
    assert sample3._get_factor_distribution(factor_length=3, event_factors=False) == {
        'abc;ABC;def;': 0.25, 'ABC;def;DEF;': 0.25,
        'def;DEF; ghi;': 0.125, 'DEF; GHI;JKL;': 0.125,
        'def;DEF; GHI;': 0.125, 'DEF; ghi;jkl;': 0.125}

    sample4 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    assert sample4._get_factor_distribution(factor_length=1, event_factors=True) == {'ff;': 0.125, 'dd;': 0.125, 'gg;': 0.125, 'cc;': 0.125, 'ee;': 0.125, 'aa;': 0.125, 'bb;': 0.125, 'hh;': 0.125}
    assert sample4._get_factor_distribution(factor_length=2, event_factors=True) == {
        'dd; ee;': 0.14285714285714285, 'ff; gg;': 0.14285714285714285,
        'bb; cc;': 0.14285714285714285,'ee; ff;': 0.14285714285714285,
        'aa; bb;': 0.14285714285714285, 'cc; dd;': 0.14285714285714285,
        'gg; hh;': 0.14285714285714285}
    assert sample4._get_factor_distribution(factor_length=3, event_factors=False) == {
        'aa; bb; cc;': 0.16666666666666666, 'cc; dd; ee;': 0.16666666666666666,
        'bb; cc; dd;': 0.16666666666666666, 'dd; ee; ff;': 0.16666666666666666,
        'ee; ff; gg;': 0.16666666666666666, 'ff; gg; hh;': 0.16666666666666666}

    sample5 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    assert sample5._get_factor_distribution(factor_length=1, event_factors=True) == {'ghi;jkl;': 0.25, 'abc;ABC;def;DEF;': 0.5, 'GHI;JKL;': 0.25}
    assert sample5._get_factor_distribution(factor_length=2, event_factors=True) == {'abc;ABC;def;DEF; GHI;JKL;': 0.5, 'abc;ABC;def;DEF; ghi;jkl;': 0.5}
    assert sample5._get_factor_distribution(factor_length=3, event_factors=False) == {
        'def;DEF; ghi;': 0.125, 'abc;ABC;def;': 0.25,
        'DEF; GHI;JKL;': 0.125, 'DEF; ghi;jkl;': 0.125,
        'ABC;def;DEF;': 0.25, 'def;DEF; GHI;': 0.125}
    LOGGER.info('Finished test of _get_factor_distribution()')

def test_sample_stats_multidim():
    """
        Test for function sample_stats().
    """
    LOGGER.info('Started test for sample_stats()')
    sample = MultidimSample()
    try:
        sample.sample_stats()
    except EmptySampleError:
        pass

    sample0 = MultidimSample(["aa; ba; ca; da;", "ea; fa; ga; ha;", "ia; ja; ka; la;", "ma; na; oa; pa;"])

    stats = sample0.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 4
    assert stats['sample typeset'] == {'ma', 'oa', 'ca', 'fa', 'ha', 'pa', 'da', 'ka', 'na', 'ea', 'ba', 'ia', 'ja', 'aa', 'la', 'ga'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == set()
    assert stats['sample #types'] == 16
    assert stats['sample min trace'] == 'aa; ba; ca; da;'
    assert stats['sample min trace len'] == 4
    assert stats['sample max trace'] == 'ma; na; oa; pa;'
    assert stats['sample max trace len'] == 4
    assert stats['sample average trace len'] == 4.0
    assert stats['sample trace length distribution'] == [('4', [1.0])]
    assert stats['sample type distribution'] == {
            'ma': [0.0625], 'oa': [0.0625], 'ca': [0.0625], 'fa': [0.0625],
            'ha': [0.0625], 'pa': [0.0625], 'da': [0.0625], 'ka': [0.0625],
            'na': [0.0625], 'ea': [0.0625], 'ba': [0.0625], 'ia': [0.0625],
            'ja': [0.0625], 'aa': [0.0625], 'la': [0.0625], 'ga': [0.0625]
    }
    assert stats['sample 3 factor distribution'] == {
        'ia; ja; ka;': 0.125, 'aa; ba; ca;': 0.125, 'ba; ca; da;': 0.125,
        'ja; ka; la;': 0.125, 'fa; ga; ha;': 0.125, 'na; oa; pa;': 0.125,
        'ma; na; oa;': 0.125, 'ea; fa; ga;': 0.125
    }
    assert stats['sample 3 #factors'] == 8
    stats = sample0.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'ia; ja; ka;': 0.125, 'aa; ba; ca;': 0.125, 'ba; ca; da;': 0.125,
        'ja; ka; la;': 0.125, 'fa; ga; ha;': 0.125, 'na; oa; pa;': 0.125,
        'ma; na; oa;': 0.125, 'ea; fa; ga;': 0.125
    }
    assert stats['sample 3 #factors'] == 8

    sample1 = MultidimSample(["a;a; cc; cc; b;b;", "a;a; ee; ee; b;b;", "a;a; dd; dd; b;b;", "a;a; ee; ee; b;b;"])
    stats = sample1.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 4
    assert stats['sample typeset'] == {'b', 'dd', 'a', 'cc', 'ee'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == {'b', 'a'}
    assert stats['sample #types'] == 5
    assert stats['sample min trace'] == 'a;a; cc; cc; b;b;'
    assert stats['sample min trace len'] == 4
    assert stats['sample max trace'] == 'a;a; ee; ee; b;b;'
    assert stats['sample max trace len'] == 4
    assert stats['sample average trace len'] == 4.0
    assert stats['sample trace length distribution'] == [('4', [1.0])]
    assert stats['sample type distribution'] == {
            'b': [0.3333333333333333], 'dd': [0.08333333333333333],
            'a': [0.3333333333333333], 'cc': [0.08333333333333333],
            'ee': [0.16666666666666666]
    }
    assert stats['sample 3 factor distribution'] == {
        'a;a; dd; dd;': 0.125, 'ee; ee; b;b;': 0.25, 'a;a; cc; cc;': 0.125,
        'cc; cc; b;b;': 0.125, 'a;a; ee; ee;': 0.25, 'dd; dd; b;b;': 0.125
    }
    assert stats['sample 3 #factors'] == 6
    stats = sample1.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'a;a; dd;': 0.0625, 'a;a; cc;': 0.0625, 'ee; ee; b;': 0.125,
        'a; ee; ee;': 0.125, 'dd; dd; b;': 0.0625, 'dd; b;b;': 0.0625,
        'a;a; ee;': 0.125, 'a; cc; cc;': 0.0625, 'ee; b;b;': 0.125,
        'a; dd; dd;': 0.0625, 'cc; cc; b;': 0.0625, 'cc; b;b;': 0.0625
    }
    assert stats['sample 3 #factors'] == 12

    sample2 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    stats = sample2.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 2
    assert stats['sample typeset'] == {'dd', 'bb', 'ff', 'gg', 'hh', 'aa', 'cc', 'ee'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == {'dd', 'bb', 'ff', 'gg', 'hh', 'aa', 'cc', 'ee'}
    assert stats['sample #types'] == 8
    assert stats['sample min trace'] == 'aa; bb; cc; dd; ee; ff; gg; hh;'
    assert stats['sample min trace len'] == 8
    assert stats['sample max trace'] == 'aa; bb; cc; dd; ee; ff; gg; hh;'
    assert stats['sample max trace len'] == 8
    assert stats['sample average trace len'] == 8.0
    assert stats['sample trace length distribution'] == [('8', [1.0])]
    assert stats['sample type distribution'] == {
            'dd': [0.125], 'bb': [0.125], 'ff': [0.125], 'gg': [0.125],
            'hh': [0.125], 'aa': [0.125], 'cc': [0.125], 'ee': [0.125]
    }
    assert stats['sample 3 factor distribution'] == {
        'ff; gg; hh;': 0.16666666666666666, 'bb; cc; dd;': 0.16666666666666666,
        'ee; ff; gg;': 0.16666666666666666, 'aa; bb; cc;': 0.16666666666666666,
        'dd; ee; ff;': 0.16666666666666666, 'cc; dd; ee;': 0.16666666666666666
    }
    assert stats['sample 3 #factors'] == 6
    stats = sample2.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'ff; gg; hh;': 0.16666666666666666, 'bb; cc; dd;': 0.16666666666666666,
        'ee; ff; gg;': 0.16666666666666666, 'aa; bb; cc;': 0.16666666666666666,
        'dd; ee; ff;': 0.16666666666666666, 'cc; dd; ee;': 0.16666666666666666
    }
    assert stats['sample 3 #factors'] == 6

    sample3 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    stats = sample3.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 2
    assert stats['sample typeset'] == {'ghi', 'JKL', 'GHI', 'jkl', 'abc', 'def', 'DEF', 'ABC'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == {'def', 'DEF', 'ABC', 'abc'}
    assert stats['sample #types'] == 8
    assert stats['sample min trace'] == 'abc;ABC;def;DEF; ghi;jkl;'
    assert stats['sample min trace len'] == 2
    assert stats['sample max trace'] == 'abc;ABC;def;DEF; GHI;JKL;'
    assert stats['sample max trace len'] == 2
    assert stats['sample average trace len'] == 2.0
    assert stats['sample trace length distribution'] == [('2', [1.0])]
    assert stats['sample type distribution'] == {
            'ghi': [0.08333333333333333], 'JKL': [0.08333333333333333], 'GHI': [0.08333333333333333],
            'jkl': [0.08333333333333333], 'abc': [0.16666666666666666], 'def': [0.16666666666666666],
            'DEF': [0.16666666666666666], 'ABC': [0.16666666666666666]
    }
    assert stats['sample 3 factor distribution'] == {}
    assert stats['sample 3 #factors'] == 0
    stats = sample3.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'def;DEF; ghi;': 0.125, 'def;DEF; GHI;': 0.125, 'ABC;def;DEF;': 0.25,
        'abc;ABC;def;': 0.25, 'DEF; GHI;JKL;': 0.125, 'DEF; ghi;jkl;': 0.125
    }
    assert stats['sample 3 #factors'] == 6

    sample4 = MultidimSample(["aa; bb; cc; dd; ee; ff; gg; hh;", "aa; bb; cc; dd; ee; ff; gg; hh;"])
    stats = sample4.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 2
    assert stats['sample typeset'] == {'dd', 'bb', 'ff', 'gg', 'hh', 'aa', 'cc', 'ee'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == {'dd', 'bb', 'ff', 'gg', 'hh', 'aa', 'cc', 'ee'}
    assert stats['sample #types'] == 8
    assert stats['sample min trace'] == 'aa; bb; cc; dd; ee; ff; gg; hh;'
    assert stats['sample min trace len'] == 8
    assert stats['sample max trace'] == 'aa; bb; cc; dd; ee; ff; gg; hh;'
    assert stats['sample max trace len'] == 8
    assert stats['sample average trace len'] == 8.0
    assert stats['sample trace length distribution'] == [('8', [1.0])]
    assert stats['sample type distribution'] == {
            'dd': [0.125], 'bb': [0.125], 'ff': [0.125],
            'gg': [0.125], 'hh': [0.125], 'aa': [0.125],
            'cc': [0.125], 'ee': [0.125]
    }
    assert stats['sample 3 factor distribution'] == {
        'ff; gg; hh;': 0.16666666666666666, 'bb; cc; dd;': 0.16666666666666666,
        'ee; ff; gg;': 0.16666666666666666, 'aa; bb; cc;': 0.16666666666666666,
        'dd; ee; ff;': 0.16666666666666666, 'cc; dd; ee;': 0.16666666666666666
    }
    assert stats['sample 3 #factors'] == 6
    stats = sample4.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'ff; gg; hh;': 0.16666666666666666, 'bb; cc; dd;': 0.16666666666666666,
        'ee; ff; gg;': 0.16666666666666666, 'aa; bb; cc;': 0.16666666666666666,
        'dd; ee; ff;': 0.16666666666666666, 'cc; dd; ee;': 0.16666666666666666
    }
    assert stats['sample 3 #factors'] == 6

    sample5 = MultidimSample(["abc;ABC;def;DEF; ghi;jkl;","abc;ABC;def;DEF; GHI;JKL;"])
    stats = sample5.sample_stats(support=1.0, factors=[3], event_factors=True)
    assert stats['sample size'] == 2
    assert stats['sample typeset'] == {'ghi', 'JKL', 'GHI', 'jkl', 'abc', 'def', 'DEF', 'ABC'}
    assert stats['sample support'] == 1.0
    assert stats['sample supported typeset'] == {'def', 'DEF', 'ABC', 'abc'}
    assert stats['sample #types'] == 8
    assert stats['sample min trace'] == 'abc;ABC;def;DEF; ghi;jkl;'
    assert stats['sample min trace len'] == 2
    assert stats['sample max trace'] == 'abc;ABC;def;DEF; GHI;JKL;'
    assert stats['sample max trace len'] == 2
    assert stats['sample average trace len'] == 2.0
    assert stats['sample trace length distribution'] == [('2', [1.0])]
    assert stats['sample type distribution'] == {
            'ghi': [0.08333333333333333], 'JKL': [0.08333333333333333], 'GHI': [0.08333333333333333],
            'jkl': [0.08333333333333333], 'abc': [0.16666666666666666], 'def': [0.16666666666666666],
            'DEF': [0.16666666666666666], 'ABC': [0.16666666666666666]
    }
    assert stats['sample 3 factor distribution'] == {}
    assert stats['sample 3 #factors'] == 0
    stats = sample5.sample_stats(support=1.0, factors=[3], event_factors=False)
    assert stats['sample 3 factor distribution'] == {
        'def;DEF; ghi;': 0.125, 'def;DEF; GHI;': 0.125, 'ABC;def;DEF;': 0.25,
        'abc;ABC;def;': 0.25, 'DEF; GHI;JKL;': 0.125, 'DEF; ghi;jkl;': 0.125
    }
    assert stats['sample 3 #factors'] == 6

    LOGGER.info('Finished test for sample_stats()')

def test_calc_vertical_sequence_databases():
    """
        Testing 'create_vertical_sequence_databases'
    """
    LOGGER.info('Started test for create_vertical_sequence_databases')

    sample = MultidimSample()
    try:
        sample.calc_vertical_sequence_databases()
        raise AssertionError("Test failed: No error raised!")
    except EmptySampleError:
        pass
    except Exception as err:
        raise err

    sample = MultidimSample()
    sample._sample = ["",""]
    sample._sample_size = 2
    try:
        sample.calc_vertical_sequence_databases(trace_index_list=[-1])
        raise AssertionError("Test failed: No error raised!")
    except IndexError:
        pass

    try:
        sample.calc_vertical_sequence_databases(trace_index_list=[0, sample._sample_size])
        raise AssertionError("Test failed: No error raised!")
    except IndexError:
        pass

    sample = MultidimSample(["b;a; a;a; b;c;", "AB;ee; b;c; C;c; dF;z; gh;xy;", "", "a;a; b;b"])
    vertical_type_representation = {
            'a' :   {0: {0 : [1],
                         1 : [0,1]},
                     3: {0 : [0,1]}},
            'AB':   {1: {0 : [0]}},
            'b' :   {0: {0 : [0],
                         2 : [0]},
                     1: {1 : [0]},
                     3: {1 : [0,1]}},
            'c' :   {0: {2 : [1]},
                     1: {1 : [1],
                         2 : [1]}},
            'C' :   {1: {2 : [0]}},
            'dF':   {1: {3 : [0]}},
            'ee':   {1: {0 : [1]}},
            'gh':   {1: {4 : [0]}},
            'xy':   {1: {4 : [1]}},
            'z' :   {1: {3 : [1]}}
            }
    vertical_att_representation = {
            0 : {'a': {0: [1],
                       3: [0]},
                 'AB':{1: [0]},
                 'b': {0: [0,2],
                       1: [1],
                       3: [1]},
                 'C': {1: [2]},
                 'dF':{1: [3]},
                 'gh':{1: [4]}
                 },
            1 : {'a': {0: [0,1],
                       3: [0]},
                 'b': {3: [1]},
                 'c': {0: [2],
                       1: [1,2]},
                 'ee':{1: [0]},
                 'xy':{1: [4]},
                 'z': {1: [3]}
                 }
            }

    sample.calc_vertical_sequence_databases()
    assert sample._sample_vertical_sequence_database == vertical_type_representation
    assert sample._sample_att_vertical_sequence_database == vertical_att_representation

    sample = MultidimSample(["b;y; a;x; b;y;", "AB;a; b;v; C;W; dF;WE; gh;bla", "", "a;x; b;y;"])
    vertical_type_representation = {
            'a' : {0 : {1 : [0]},
                   3 : {0 : [0]}},
            'b' : {0 : {0 : [0],
                        2 : [0]},
                   3 : {1 : [0]}},
            'x' : {0 : {1 : [1]},
                   3 : {0 : [1]}},
            'y' : {0 : {0 : [1],
                        2 : [1]},
                   3 : {1 : [1]}}

            }
    vertical_att_representation = {
            0 : {'a': {0: [1],
                       3: [0]},
                 'b': {0: [0,2],
                       3: [1]}
                 },
            1 : {'x': {0: [1],
                       3: [0]},
                 'y': {0: [0,2],
                       3: [1]}
                 }
            }
    sample.calc_vertical_sequence_databases([0,2,3])
    assert sample._sample_vertical_sequence_database == vertical_type_representation
    assert sample._sample_att_vertical_sequence_database == vertical_att_representation

    sample = MultidimSample(["", "a;a; b;b"])
    vertical_type_representation = {
            'a' :   {1: {0 : [0,1]}},
            'b' :   {1: {1 : [0,1]}}
            }
    vertical_att_representation = {
            0 : {'a': {1: [0]},
                 'b': {1: [1]}
                 },
            1 : {'a': {1: [0]},
                 'b': {1: [1]},
                 }
            }
    assert sample._sample_vertical_sequence_database is None
    assert sample._sample_att_vertical_sequence_database is None

    assert sample.sample_vertical_sequence_database == vertical_type_representation
    assert sample.sample_att_vertical_sequence_database == vertical_att_representation

    sample.sample_vertical_sequence_database = {"type" : 0}
    sample.sample_att_vertical_sequence_database = {"att" : 0}

    assert sample.sample_vertical_sequence_database == {"type" : 0}
    assert sample.sample_att_vertical_sequence_database == {"att" : 0}

    LOGGER.info('Finished test for create_vertical_sequence_databases')
