#!/usr/bin/python3
"""Contains tests for class Query"""
import sys
sys.path.append(".")

import logging
from query import Query, string_to_normalform
from sample import Sample
from error import InvalidQueryGapConstraintError,InvalidQueryLocalWindowSizeError,InvalidQueryStringLengthError

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('DEBUG')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_init_most_general_query_for_shinohara():
    """
        Test for function init_most_general_query_for_shinohara.
    """
    LOGGER.info('Started test test_init_most_general_query_for_shinohara')
    sample = Sample()
    query = Query()
    try:
        query.init_most_general_query_for_shinohara(0, [(0,1),(0,1),(0,1)], 'shinohara_icdt', sample, gap_constraints=None, query_class="normal")
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    try:
        query.init_most_general_query_for_shinohara(3, None, 'shinohara_icdt', sample, gap_constraints=None, query_class="normal")
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err

    query01 = Query()
    query01.init_most_general_query_for_shinohara(4, [(0,1),(0,1),(0,1)], 'shinohara_icdt', sample, gap_constraints=None, query_class="normal")
    assert query01._query_string == "$A $B $C $D", 'query01: Wrong query string!'
    assert query01._query_string_length == 4, 'query01: Wrong query string length!'
    assert query01._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(-1,-1)], 'query01: Wrong local window sizes!'
    assert query01._query_discovery_algorithm == 'shinohara_icdt', 'query01: Wrong query discovery algorithm!'

    LOGGER.info('Finished test test_init_most_general_query_for_shinohara')

def test_check_consistency():
    """
        Test for function check_consistency.
    """
    LOGGER.info('Started test test_check_consistency')
    query01 = Query()
    query01.set_query_string("")
    query01._query_string_length = 3
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    query01._query_string_length = 0
    query01._query_windowsize_local = [(0,1),(0,1)]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query01.set_query_string("a $x $x b")
    query01.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query01._query_string_length = 3
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryStringLengthError raised!")
    except InvalidQueryStringLengthError:
        pass
    except Exception as err:
        raise err
    query01._query_string_length = 4
    query01._query_windowsize_local = [(0,1),(0,1)]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryLocalWindowSizeError raised!")
    except InvalidQueryLocalWindowSizeError:
        pass
    except Exception as err:
        raise err
    query01.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query01._query_gap_constraints=[{'z'},{'z'}]
    try:
        query01.check_consistency()
        raise AssertionError("Test failed: No InvalidQueryGapConstraintError raised!")
    except InvalidQueryGapConstraintError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test test_check_consistency')

def test_query_constructor():
    """
        Test for Query constructor.
    """
    LOGGER.info('Started test for constructor (__init__) of Query')

    gap_constraints = [{"a","b"},{"c","d"},{"e","f"},{"g","h"}]
    window_size_global = 10
    window_size_local = [(0,0),(1,2),(1,4),(0,3)]

    query01 = Query()
    assert query01._query_string == "", 'query01: Wrong query string!'
    assert query01._query_string_length == 0, 'query01: Wrong query string length!'
    assert query01._query_repeated_variables == set(), 'query01: Wrong set of repeated variables!'
    assert query01._query_typeset == set(), 'query01: Wrong query typeset!'
    assert query01._query_gap_constraints == [], 'query01: Wrong query gap constraints!'
    assert query01._query_windowsize_global == -1, 'query01: Wrong global windowsize!'
    assert query01._query_windowsize_local == [], 'query01: Wrong local window sizes!'

    query02 = Query("$A $B $A $D c")
    assert query02._query_string == "$A $B $A $D c", 'query02: Wrong query string!'
    assert query02._query_string_length == 5, 'query02: Wrong query string length!'
    assert query02._query_repeated_variables == {"A"}, 'query02: Wrong set of repeated variables!'
    assert query02._query_typeset == {"c"}, 'query02: Wrong query typeset!'
    assert query02._query_gap_constraints == [], 'query02: Wrong query gap constraints!'
    assert query02._query_windowsize_global == -1, 'query02: Wrong global windowsize!'
    assert query02._query_windowsize_local == [], 'query02: Wrong local window sizes!'

    query03 = Query("$A $B $A $D c", gap_constraints)
    assert query03._query_string == "$A $B $A $D c", 'query03: Wrong query string!'
    assert query03._query_string_length == 5, 'query03: Wrong query string length!'
    assert query03._query_repeated_variables == {"A"}, 'query03: Wrong set of repeated variables!'
    assert query03._query_typeset == {"c"}, 'query03: Wrong query typeset!'
    assert query03._query_gap_constraints == gap_constraints, 'query03: Wrong query gap constraints!'
    assert query03._query_windowsize_global == -1, 'query03: Wrong global windowsize!'
    assert query03._query_windowsize_local == [], 'query03: Wrong local window sizes!'

    query04 = Query("$A $B $A $D c", gap_constraints, window_size_global)
    assert query04._query_string == "$A $B $A $D c", 'query04: Wrong query string!'
    assert query04._query_string_length == 5, 'query04: Wrong query string length!'
    assert query04._query_repeated_variables == {"A"}, 'query04: Wrong set of repeated variables!'
    assert query04._query_typeset == {"c"}, 'query04: Wrong query typeset!'
    assert query04._query_gap_constraints == gap_constraints, 'query04: Wrong query gap constraints!'
    assert query04._query_windowsize_global == window_size_global, 'query04: Wrong global windowsize!'
    assert query04._query_windowsize_local == [], 'query04: Wrong local window sizes!'

    # No query string but other parameters should lead to initial query ignoring other parameters
    query05 = Query(given_query_gap_constraints=gap_constraints, given_query_windowsize_global=window_size_global, given_query_windowsize_local=window_size_local)
    assert query05._query_string == "", 'query05: Wrong query string!'
    assert query05._query_string_length == 0, 'query05: Wrong query string length!'
    assert query05._query_repeated_variables == set(), 'query05: Wrong set of repeated variables!'
    assert query05._query_typeset == set(), 'query05: Wrong query typeset!'
    assert query05._query_gap_constraints == [], 'query05: Wrong query gap constraints!'
    assert query05._query_windowsize_global == -1, 'query05: Wrong global windowsize!'
    assert query05._query_windowsize_local == [], 'query05: Wrong local window sizes!'

    LOGGER.info('Finished test for constructor (__init__) of Query')

def test_regex():
    """
        Small test for building a regex to a given query string including some
        special cases.
    """
    LOGGER.info('Started test test_regex')
    LOGGER.info("**********************************************")
    query = Query()
    query.set_query_string("$A $B $A $D")
    LOGGER.info('Query string:         %s',query._query_string)
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query.query_string_to_regex()
    assert query._query_string == "$A $B $A $D", 'query: Wrong query string!'
    assert query._query_string_regex == '(?P<A>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P<B>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P=A)\\s(?:[^\\s]+\\s){0,1}(?P<D>\\S+)', 'query: Wrong regex!'
    LOGGER.info('Query regex:          %s',query._query_string_regex)
    query.query_string_to_normalform()
    assert query._query_string == "$x0 $x0", 'query: Wrong query string after to_normalform'
    assert query._query_windowsize_local == [(-1, -1), (1, 3), (1, 2)], 'query: Wrong local window sizes after to_normalform'
    LOGGER.info('Query string in NF:   %s',query._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query window sizes:   %s',query_windowsize_local)
    query.query_string_to_regex()
    assert query._query_string_regex  == '(?P<x0>\\S+)\\s(?:[^\\s]+\\s){1,3}(?P=x0)\\s(?:[^\\s]+\\s){0,1}[^\\s]+', 'query: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query NF regex:       %s',query._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query0 = Query()
    query0.set_query_string("a $x $y a")
    LOGGER.info('Query 0 string:       %s',query0._query_string)
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query0.query_string_to_regex()
    assert query0._query_string == "a $x $y a", 'query: Wrong query string!'
    assert query0._query_string_regex  == 'a\\s(?:[^\\s]+\\s){0,1}(?P<x>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P<y>\\S+)\\s(?:[^\\s]+\\s){0,1}a', 'query0: Wrong regex!'
    LOGGER.info('Query 0 regex:        %s',query0._query_string_regex)
    query0.query_string_to_normalform()
    assert query0._query_string == "a a", 'query0: Wrong query string after to_normalform'
    assert query0._query_windowsize_local == [(-1, -1), (2, 5), (-1, -1)], 'query0: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 0 string in NF: %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)
    query0.query_string_to_regex()
    assert query0._query_string_regex == 'a\\s(?:[^\\s]+\\s){2,5}a', 'query0: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 0 NF regex:     %s',query0._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query1 = Query()
    query1.set_query_string("a $x $y")
    LOGGER.info('Query 1 string:       %s',query1._query_string)
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(-1,-1),(0,1),(0,1),(2,3)])
    query1.query_string_to_regex()
    assert query1._query_string == "a $x $y", 'query1: Wrong query string!'
    assert query1._query_string_regex == 'a\\s(?:[^\\s]+\\s){0,1}(?P<x>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P<y>\\S+)\\s(?:[^\\s]+\\s){1,2}[^\\s]+', 'query1: Wrong regex!'
    LOGGER.info('Query 1 regex:        %s',query1._query_string_regex)
    query1.query_string_to_normalform()
    assert query1._query_string == "a", 'query1: Wrong query string after to_normalform'
    assert query1._query_windowsize_local == [(-1, -1), (4,7)], 'query1: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 1 string in NF: %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)
    query1.query_string_to_regex()
    assert query1._query_string_regex == 'a\\s(?:[^\\s]+\\s){3,6}[^\\s]+', 'query3: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 1 NF regex:     %s',query1._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query2 = Query()
    query2.set_query_string("$x a a $y")
    LOGGER.info('Query 2 string:       %s',query2._query_string)
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query2.query_string_to_regex()
    assert query2._query_string == "$x a a $y", 'query2: Wrong query string!'
    assert query2._query_string_regex == '(?P<x>\\S+)\\s(?:[^\\s]+\\s){0,1}a\\s(?:[^\\s]+\\s){0,1}a\\s(?:[^\\s]+\\s){0,1}(?P<y>\\S+)', 'query2: Wrong regex!'
    LOGGER.info('Query 2 regex:        %s',query2._query_string_regex)
    query2.query_string_to_normalform()
    assert query2._query_string == "a a", 'query2: Wrong query string after to_normalform'
    assert query2._query_windowsize_local == [(1, 2), (0, 1), (1, 2)], 'query2: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 2 string in NF: %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)
    query2.query_string_to_regex()
    assert query2._query_string_regex == '(?:[^\\s]+\\s){1,2}a\\s(?:[^\\s]+\\s){0,1}a\\s(?:[^\\s]+\\s){0,1}[^\\s]+', 'query2: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 2 NF regex:     %s',query2._query_string_regex)
    LOGGER.info("**********************************************")

    LOGGER.info("**********************************************")
    query3 = Query()
    query3.set_query_string("$x $y $z")
    LOGGER.info('Query 3 string:       %s',query3._query_string)
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    query3.query_string_to_regex()
    LOGGER.info('Query 3 regex:        %s',query3._query_string_regex)
    assert query3._query_string == "$x $y $z", 'query3: Wrong query string!'
    assert query3._query_string_regex == '(?P<x>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P<y>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P<z>\\S+)', 'query3: Wrong regex!'
    query3.query_string_to_normalform()
    assert query3._query_string == "", 'query3: Wrong query string after to_normalform'
    assert query3._query_windowsize_local == [(3,5)], 'query3: Wrong local window sizes after to_normalform'
    LOGGER.info('Query 3 string in NF: %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)
    query3.query_string_to_regex()
    assert query3._query_string_regex == '(?:[^\\s]+\\s){2,4}[^\\s]+', 'query3: Wrong regex after to_normalform and to_regex'
    LOGGER.info('Query 3 NF regex:     %s',query3._query_string_regex)

    LOGGER.info('Finished test test_regex')

def test_normalform():
    """
        Small test for bilding the normalform of a given query string inluding
        some special cases.
    """
    LOGGER.info('Started test test_normalform')
    query0 = Query()
    query0.set_query_string("a $x $y a")
    LOGGER.info('Query 0 string:       %s',query0._query_string)
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    query0.query_string_to_normalform()
    assert query0._query_string == 'a a'
    assert query0._query_windowsize_local==[(-1, -1), (2, 5), (-1, -1)]
    LOGGER.info('Query 0 string in NF: %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)

    query1 = Query()
    query1.set_query_string("a $x $y")
    LOGGER.info('Query 1 string:       %s',query1._query_string)
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(0,1),(0,1)])
    query1.query_string_to_normalform()
    assert query1._query_string == 'a'
    assert query1._query_windowsize_local==[(-1, -1), (2, 4)]
    LOGGER.info('Query 1 string in NF: %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)

    query2 = Query()
    query2.set_query_string("$x a a $y")
    LOGGER.info('Query 2 string:       %s',query2._query_string)
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query2.query_string_to_normalform()
    assert query2._query_string == 'a a'
    assert query2._query_windowsize_local==[(1, 2), (0, 1), (1, 2)]
    LOGGER.info('Query 2 string in NF: %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)

    query3 = Query()
    query3.set_query_string("$x $y $z")
    LOGGER.info('Query 3 string:       %s',query3._query_string)
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    query3.query_string_to_normalform()
    assert query3._query_string == ''
    assert query3._query_windowsize_local==[(3,5)]
    LOGGER.info('Query 3 string in NF: %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)

    query4 = Query()
    query4.set_query_string("$x a a $x")
    LOGGER.info('Query 4 string:       %s',query4._query_string)
    query4.set_query_string_length()
    query4.set_query_repeated_variables()
    query4.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query4.query_string_to_normalform()
    assert query4._query_string == '$x0 a a $x0'
    assert query4._query_windowsize_local==[(-1, -1),(0,1),(0,1), (0,1),(-1, -1)]
    LOGGER.info('Query 4 string in NF: %s',query4._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query4._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 4 window sizes:   %s',query_windowsize_local)

    query5 = Query()
    query5.set_query_string("$x $a $a $x")
    LOGGER.info('Query 5 string:       %s',query5._query_string)
    query5.set_query_string_length()
    query5.set_query_repeated_variables()
    query5.set_query_windowsize_local([(0,1),(0,1), (0,1)])
    query5.query_string_to_normalform()
    assert query5._query_string == '$x0 $x1 $x1 $x0'
    assert query5._query_windowsize_local==[(-1, -1),(0,1),(0,1), (0,1),(-1, -1)]
    LOGGER.info('Query 5 string in NF: %s',query5._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query5._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 5 window sizes:   %s',query_windowsize_local)

    query6 = Query()
    query6.set_query_string("$x $a $a $x $y $y $a $c $d $a $d $c")
    LOGGER.info('Query 6 string:       %s',query6._query_string)
    query6.set_query_string_length()
    query6.set_query_repeated_variables()
    query6.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])
    query6.query_string_to_normalform()
    assert query6._query_string == '$x0 $x1 $x1 $x0 $x2 $x2 $x1 $x3 $x4 $x1 $x4 $x3'
    assert query6._query_windowsize_local==[(-1, -1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1, -1)]
    LOGGER.info('Query 6 string in NF: %s',query6._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query6._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 6 window sizes:   %s',query_windowsize_local)

    LOGGER.info('Finished test test_normalform')

def test_match_sample_special_cases_without_gapconstraints():
    """
        Small test for the regex-based matchtest for queries.
    """
    LOGGER.info('Started test test_match_sample_special_cases_without_gapconstraints')
    sample = Sample()
    sample.set_sample(["a b c a", "a c b a", "b a a c d", "c a a b d"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    support = 0.5
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)
    LOGGER.info('Requested support:        %s',str(support))

    query0 = Query()
    query0.set_query_string("a $x $y a")
    query0.set_query_string_length()
    query0.set_query_repeated_variables()
    query0.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 0 string:           %s',query0._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query0._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 0 window sizes:   %s',query_windowsize_local)
    result = query0.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query0._query_string_regex)
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query0._query_sample_support))
    matched_traces_list = [str(int) for int in query0._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query0._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query0._query_string == "a a", 'query0: Wrong query string after match_sample (and hence to_notmalform)'
    assert query0._query_string_regex == "a\\s(?:[^\\s]+\\s){2,5}a", 'query0: Wrong query string regex'
    assert result is True, 'query0: Wrong match_sample result'
    assert query0._query_sample_support == 0.5, 'query0: Wrong sample support'
    assert query0._query_matched_traces == [0, 1], 'query0: Wrong list of matched traces'
    assert query0._query_not_matched_traces == [2, 3], 'query0: Wrong list of not matched traces'

    query1 = Query()
    query1.set_query_string("a $x $y")
    query1.set_query_string_length()
    query1.set_query_repeated_variables()
    query1.set_query_windowsize_local([(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 1 string:           %s',query1._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query1._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 1 window sizes:   %s',query_windowsize_local)
    result = query1.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query1._query_string_regex)
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query1._query_sample_support))
    matched_traces_list = [str(int) for int in query1._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query1._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query1._query_string == "a", 'query1: Wrong query string after match_sample (and hence to_notmalform)'
    assert query1._query_string_regex == "a\\s(?:[^\\s]+\\s){1,3}[^\\s]+", 'query1: Wrong query string regex'
    assert result is True, 'query1: Wrong match_sample result'
    assert query1._query_sample_support == 1.0, 'query1: Wrong sample support'
    assert query1._query_matched_traces == [0, 1, 2, 3], 'query1: Wrong list of matched traces'
    assert query1._query_not_matched_traces == [], 'query1: Wrong list of not matched traces'

    query2 = Query()
    query2.set_query_string("$x a a $y")
    query2.set_query_string_length()
    query2.set_query_repeated_variables()
    query2.set_query_windowsize_local([(0,1),(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 2 string:           %s',query2._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query2._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 2 window sizes:   %s',query_windowsize_local)
    result = query2.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query2._query_string_regex)
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query2._query_sample_support))
    matched_traces_list = [str(int) for int in query2._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query2._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query2._query_string == "a a", 'query2: Wrong query string after match_sample (and hence to_notmalform)'
    assert query2._query_string_regex == "(?:[^\\s]+\\s){1,2}a\\s(?:[^\\s]+\\s){0,1}a\\s(?:[^\\s]+\\s){0,1}[^\\s]+", 'query2: Wrong query string regex'
    assert result is True, 'query2: Wrong match_sample result'
    assert query2._query_sample_support == 0.5, 'query2: Wrong sample support'
    assert query2._query_matched_traces == [2, 3], 'query2: Wrong list of matched traces'
    assert query2._query_not_matched_traces == [0, 1], 'query2: Wrong list of not matched traces'

    query3 = Query()
    query3.set_query_string("$x $y $z")
    query3.set_query_string_length()
    query3.set_query_repeated_variables()
    query3.set_query_windowsize_local([(0,1),(0,1)])
    LOGGER.info("**********************************************")
    LOGGER.info('Query 3 string:           %s',query3._query_string)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query3._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query 3 window sizes:   %s',query_windowsize_local)
    result = query3.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query3._query_string_regex)
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query3._query_sample_support))
    matched_traces_list = [str(int) for int in query3._query_matched_traces]
    matched_traces = ' '.join(matched_traces_list)
    LOGGER.info('Matched traces:           %s',matched_traces)
    not_matched_traces_list = [str(int) for int in query3._query_not_matched_traces]
    not_matched_traces = ' '.join(not_matched_traces_list)
    LOGGER.info('Matched traces:           %s',not_matched_traces)
    LOGGER.info("**********************************************")
    assert query3._query_string == "", 'query3: Wrong query string after match_sample (and hence to_notmalform)'
    assert query3._query_string_regex == "(?:[^\\s]+\\s){2,4}[^\\s]+", 'query3: Wrong query string regex'
    assert result is True, 'query3: Wrong match_sample result'
    assert query3._query_sample_support == 1.0, 'query3: Wrong sample support'
    assert query3._query_matched_traces == [0, 1, 2, 3], 'query3: Wrong list of matched traces'
    assert query3._query_not_matched_traces == [], 'query3: Wrong list of not matched traces'

    LOGGER.info('Finished test test_match_sample_special_cases_without_gapconstraints')

def test_match_sample_simple():
    """
        Small test for the regex-based matchtest for queries which contain gap
        constraints.
    """
    LOGGER.info('Started test test_match_sample_simple')
    query = Query()
    query.set_query_string("a $x $x $y $z $y b")
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])
    query.set_query_gap_constraints([{"K"}, {"L"}, {"M"}, {"N"}, {"O"}, {"P"}])

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    LOGGER.info('Query repeated vars:      %s',repeated_variables)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query window sizes:       %s',query_windowsize_local)
    gap_constraints = ' '.join(f"{str(gap)}" for gap in query._query_gap_constraints)
    LOGGER.info('Query gap constraints:   %s',gap_constraints)

    sample = Sample()
    sample.set_sample(["a c c d a d b", "a c c d K d b", "a c c d d b", "a c c d N d b", "a c R S c d a d b"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)

    support = 0.5
    result = query.match_sample(sample, support, complete_test=True)
    LOGGER.info('In normalform?:           %s',str(query._query_is_in_normalform))
    LOGGER.info('Regex of query string:    %s',query._query_string_regex)
    LOGGER.info('Requested support:        %s',str(support))
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query._query_sample_support))
    LOGGER.info("**********************************************")
    assert query._query_string == "a $x $x $y $z $y b", 'query: Wrong query string'
    assert query._query_string_length == 7, 'query: Wrong query string length'
    assert query._query_repeated_variables == {'x','y'}, 'query: Wrong set of repeated variables'
    assert query._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1,-1)], 'query: Wrong local window sizes'
    assert query._query_gap_constraints == [{"K"}, {"L"}, {"M"}, {"N"}, {"O"}, {"P"}], 'query: Wrong gap constraints'
    assert query._query_is_in_normalform is False, 'query: Should not be in normalform because of given gap constraints'
    assert query._query_string_regex == 'a\\s(?:[^\\sK]+\\s){0,1}(?P<x>\\S+)\\s(?:[^\\sL]+\\s){0,1}(?P=x)\\s(?:[^\\sM]+\\s){0,1}(?P<y>\\S+)\\s(?:[^\\sN]+\\s){0,1}(?P<z>\\S+)\\s(?:[^\\sO]+\\s){0,1}(?P=y)\\s(?:[^\\sP]+\\s){0,1}b', 'query: Wrong query string regex'
    assert result is True, 'query: Wrong match_sample result'
    assert query._query_sample_support == 0.6, 'query: Wrong support ot the query'

    LOGGER.info('Finished test test_match_sample_simple')

def test_match_sample_smarter():
    """Small test for the smarter-based matchtest."""
    sample = Sample()
    sample.set_sample(["a c c a a", "b d d b b"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    dict_iter = {}
    parent_dict = {}
    query = Query()
    query.set_query_string('$x0 $x1 $x1 $x0 $x0')
    query.set_query_matchtest('smarter')
    # query.set_pos_last_type_and_variable()
    patternset ={ 'a', 'b', 'c', 'd'}
    matching1 = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter,patternset=patternset, parent_dict=parent_dict)
    assert matching1 == True

    parent = Query()
    parentstring = '$x0 $x1 $x1 $x0'
    parent.set_query_string(parentstring)
    parent_dict[query._query_string] = parent
    matching2 = query.match_sample(sample=sample, supp=1.0, dict_iter=dict_iter,patternset=patternset, parent_dict=parent_dict)
    assert matching2 == True



def test_setter_nf_regex_matching():
    """
        Small test for basic functionalities including setter, to_normalform,
        to_regex and matching for one simple query.
    """
    LOGGER.info('Started test test_setter_nf_regex_matching')
    query = Query()
    query.set_query_string("a $x $x $y $z $y b")
    query.set_query_string_length()
    query.set_query_repeated_variables()
    query.set_query_windowsize_local([(0,1),(0,1),(0,1),(0,1),(0,1),(0,1)])

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    LOGGER.info('Query repeated vars:      %s',repeated_variables)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('Query window sizes:   %s',query_windowsize_local)
    assert query._query_string == "a $x $x $y $z $y b", 'query: Wrong query string'
    assert query._query_string_length == 7, 'query: Wrong query string length'
    assert query._query_repeated_variables == {'x','y'}, 'query: Wrong set of repeated variables'
    assert query._query_windowsize_local == [(-1,-1),(0,1),(0,1),(0,1),(0,1),(0,1),(0,1),(-1,-1)], 'query: Wrong local window sizes'

    query.query_string_to_normalform()

    LOGGER.info('NF-Query string:          %s',query._query_string)
    LOGGER.info('                          %s',str(query._query_is_in_normalform))
    LOGGER.info('NF-Query string length:   %s',str(query._query_string_length))
    repeated_variables = ' '.join(query._query_repeated_variables)
    LOGGER.info('NF-Query repeated vars:      %s',repeated_variables)
    query_windowsize_local_list = [f"({str(tup[0])},{str(tup[1])})" for tup in query._query_windowsize_local]
    query_windowsize_local = ",".join(query_windowsize_local_list)
    LOGGER.info('NF-Query window sizes:   %s',query_windowsize_local)
    gap_constraints = ' '.join(f"{str(gap)}" for gap in query._query_gap_constraints)
    LOGGER.info('NF-Query gap constraints:   %s',gap_constraints)
    assert query._query_string == "a $x0 $x0 $x1 $x1 b", 'query: Wrong query string after to_normalform'
    assert query._query_is_in_normalform is True, 'query: is_in_normalform should be True but is False'
    assert query._query_string_length == 6, 'query: Wrong query string length after to_normalform'
    assert query._query_windowsize_local == [(-1, -1), (0, 1), (0, 1), (0, 1), (1, 3), (0, 1), (-1, -1)], 'query: Wrong local window sizes after to_normalform'

    sample = Sample()
    sample.set_sample(["a c c d a d b", "a c c d K d b", "a c c d d b", "a c c d N d b", "a c R S c d a d b"])
    sample.set_sample_size()
    sample.set_sample_typeset()
    sample_list = ' '.join(sample._sample)
    LOGGER.info('Sample:                   %s',sample_list)
    LOGGER.info('Sample size:              %s',str(sample._sample_size))
    typeset = ' '.join(sample._sample_typeset)
    LOGGER.info('Sample typeset:           %s',typeset)

    support = 0.5
    result = query.match_sample(sample, support, complete_test=True)
    LOGGER.info('Regex of query string:    %s',query._query_string_regex)
    LOGGER.info('Requested support:        %s',str(support))
    LOGGER.info('Result of match_sample:   %s',str(result))
    LOGGER.info('Support of the query:     %s',str(query._query_sample_support))
    LOGGER.info("**********************************************")
    assert query._query_string_regex == 'a\\s(?:[^\\s]+\\s){0,1}(?P<x0>\\S+)\\s(?:[^\\s]+\\s){0,1}(?P=x0)\\s(?:[^\\s]+\\s){0,1}(?P<x1>\\S+)\\s(?:[^\\s]+\\s){1,3}(?P=x1)\\s(?:[^\\s]+\\s){0,1}b', 'query: Wrong query string regex'
    assert result is True, 'query: Wrong match_sample result'
    assert query._query_sample_support == 0.6, 'query: Wrong support ot the query'

    LOGGER.info('Finished test test_setter_nf_regex_matching')

def test_set_query_typeset():
    """
        Small test for computing the set of type occurring in a query string.
    """
    LOGGER.info('Started test test_set_query_typeset')
    query = Query()
    query.set_query_string('A $X $Y B')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'A','B'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    query = Query()
    query.set_query_string('A B C B')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'A','B','C'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    query = Query()
    query.set_query_string('AB $X $Y CD')
    query.set_query_string_length()
    query.set_query_typeset()
    assert query._query_typeset == {'AB','CD'}, 'query: Wrong typeset'

    LOGGER.info("**********************************************")
    LOGGER.info('Query string:             %s',query._query_string)
    LOGGER.info('Query string length:      %s',str(query._query_string_length))
    typeset = ' '.join(query._query_typeset)
    LOGGER.info('Query typeset:            %s',typeset)
    LOGGER.info("**********************************************")

    LOGGER.info('Finished test test_set_query_typeset')

def test00__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare Query instance to non-
        Query instance
    """
    LOGGER.info('Started test 00 for _syntactically_equal')

    query = Query()
    noquery = 5

    try:
        query == noquery
        raise AssertionError("Test failed: No error raised!")
    except TypeError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 00 for _syntactically_equal')

def test01__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two basic Query
        instances.
    """
    LOGGER.info('Started test 01 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query2._query_class = 'not_normal'

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 01 for _syntactically_equal')

def test02__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having different query_string_length.
    """
    LOGGER.info('Started test 02 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1._query_string_length = 5
    query2._query_string_length = 6

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 02 for _syntactically_equal')

def test03__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length but different typesets.
    """
    LOGGER.info('Started test 03 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1._query_string_length = 5
    query1._query_typeset = set(['AA', 'BC'])

    query2._query_string_length = 5
    query2._query_typeset = set(['AA', 'BB'])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 03 for _syntactically_equal')

def test04__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length and typeset but different sets of
        repeated variables.
    """
    LOGGER.info('Started test 04 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1._query_string_length = 5
    query1._query_typeset = set(['AA', 'BC'])
    query1._query_repeated_variables = set(['XX', 'Y'])

    query2._query_string_length = 5
    query2._query_typeset = set(['BC', 'AA'])
    query2._query_repeated_variables = (['XD', 'Y'])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 04 for _syntactically_equal')

def test05__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string_length and typeset but different strings.
    """
    LOGGER.info('Started test 05 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_typeset()

    query2.set_query_string('AA $XX $XX $Y BC')
    query2.set_query_string_length()
    query2.set_query_typeset()

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 05 for _syntactically_equal')

def test06__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset but
        different gap constraints.
    """
    LOGGER.info('Started test 06 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_typeset()
    query1._query_gap_constraints = [['A','B'],['AB','BB'],['BC']]

    query2.set_query_string('AA $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_typeset()
    query2._query_gap_constraints = [[],['A','B'],['AB','BC'],['BC']]

    try:
        query1 == query2
        raise AssertionError("Test failed: No error raised!")
    except InvalidQueryGapConstraintError:
        pass
    except Exception as err:
        raise err

    LOGGER.info('Finished test 06 for _syntactically_equal')

def test07__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset but
        different gap constraints.
    """
    LOGGER.info('Started test 07 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_typeset()
    query1.set_query_gap_constraints([[],['A','B'],['AB','BB'],['BC']])

    query2.set_query_string('AA $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_typeset()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 07 for _syntactically_equal')

def test08__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset and gap
        constraints, but a different global window size.
    """
    LOGGER.info('Started test 08 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(6)

    query2.set_query_string('AA $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(7)

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 08 for _syntactically_equal')

def test09__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset, gap
        constraints and global window size, but different local window sizes.
    """
    LOGGER.info('Started test 09 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],['A','B'],['BC','AB'],['BC']])
    query1.set_query_windowsize_global(7)
    query1.set_query_windowsize_local([(-1,-1), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query2.set_query_string('AA $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(7)
    query2.set_query_windowsize_local([(-1,-1), (2,3), (0,4), (1,2), (0,1), (-1,-1)])

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 09 for _syntactically_equal')

def test10__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same query_string and -_length, the same typeset, gap
        constraints and global window size, but different local window sizes.
    """
    LOGGER.info('Started test 10 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $Y $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA $Y $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,2), (-1,-1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are treated as equal!"

    LOGGER.info('Finished test 10 for _syntactically_equal')

def test11__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes, whereby only one query has local window sizes
        starting / ending with (-1,-1). Comparison is done in both directions.
    """
    LOGGER.info('Started test 11 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $Y $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA $Y $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(1,2), (1,3), (0,4), (1,2), (0,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 11 for _syntactically_equal')

def test12__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes except for the local window sizes.
    """
    LOGGER.info('Started test 12 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $Y $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA $Y $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])
    query2.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (1,-1)])

    assert not query1 == query2, "Test failed: queries are not treated as equal!"
    assert not query2 == query1, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 12 for _syntactically_equal')

def test13__syntactically_equal():
    """
        Test of function _syntactically_equal(): Compare two Query instances
        having the same attributes, whereby only one query has local window
        sizes starting / ending with (-1,-1).
    """
    LOGGER.info('Started test 13 for _syntactically_equal')

    query1 = Query()
    query2 = Query()

    query1.set_query_string('AA $Y $XX $Y $XX BC')
    query1.set_query_string_length()
    query1.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query1.set_query_windowsize_global(8)
    query1.set_query_windowsize_local([(-1,-1), (1,2), (0,3), (0,4), (1,2), (0,1), (-1,-1)])

    query1._query_typeset = set(['BC','AA'])
    query1._query_repeated_variables = set(['XX','Y'])

    query2.set_query_string('AA $Y $XX $Y $XX BC')
    query2.set_query_string_length()
    query2.set_query_gap_constraints([[],[],['A','B'],['AB','BC'],['BC']])
    query2.set_query_windowsize_global(8)
    query2.set_query_windowsize_local([(1,2), (0,3), (0,4), (1,2), (0,1)])

    query2.set_query_typeset()
    query2.set_query_repeated_variables()

    assert query1 == query2, "Test failed: queries are not treated as equal!"

    LOGGER.info('Finished test 13 for _syntactically_equal')

def test00_string_to_normalform():
    """
        Testing 'string_to_normalform': small running example with
        type_queries.
    """
    LOGGER.info('Started test 00 for string_to_normalform')

    string = ""
    assert string == string_to_normalform(string)[0]
    string = "a b cd b"
    assert string == string_to_normalform(string)[0]

    LOGGER.info('Finished test 00 for string_to_normalform')

def test01_string_to_normalform():
    """
        Testing 'string_to_normalform': small running example with
        pattern_queries.
    """
    LOGGER.info('Started test 01 for string_to_normalform')

    string = "$x0 $x0"
    assert string == string_to_normalform(string)[0]
    string = "$x0 $x0 $x0"
    assert string == string_to_normalform(string)[0]
    string = "$x1  $x0 $x1 $x0"
    assert "$x0 $x1 $x0 $x1" == string_to_normalform(string)[0]

    string = "$x0 $x0 $x1"
    assert "$x0 $x0" == string_to_normalform(string)[0]
    string = "$x0 $x1 $x1"
    assert "$x0 $x0" == string_to_normalform(string)[0]

    LOGGER.info('Finished test 01 for string_to_normalform')

def test02_string_to_normalform():
    """
        Testing 'string_to_normalform': small running example with
        mixed_queries.
    """
    LOGGER.info('Started test 02 for query_string_to_normalform')

    string = "a bc $x0 f $x1 g g $x1"
    assert "a bc f $x0 g g $x0" == string_to_normalform(string)[0]

    LOGGER.info('Finished test 02 for string_to_normalform')

def test_pos_last_type_and_variable():
    """
        Test for the function _pos_last_type_and_variable() for different
        queries.
    """
    query1=Query()
    query2=Query()
    query2.set_query_string('a')
    query2.set_pos_last_type_and_variable()
    query3=Query()
    query3.set_query_string('$x0 $x0')
    query3.set_pos_last_type_and_variable()
    query4=Query()
    query4.set_query_string('$x0 $x0 b')
    query4.set_pos_last_type_and_variable()
    query5=Query()
    query5.set_query_string('$x0 a $x1 $x0 $x1')
    query5.set_pos_last_type_and_variable()
    query6=Query()
    query6.set_query_string('$x0 $x1 a $x0 $x1')
    query6.set_pos_last_type_and_variable()
    query7=Query()
    query7.set_query_string('$x0 $x1 $x1 $x0 $x0')
    query7.set_pos_last_type_and_variable()
    assert tuple(query1._pos_last_type_and_variable.tolist())== (-1,-1,-1)
    assert tuple(query2._pos_last_type_and_variable.tolist())== (0,-1,-1 )
    assert tuple(query3._pos_last_type_and_variable.tolist())== (-1, 0, 1)
    assert tuple(query4._pos_last_type_and_variable.tolist())==(2, 0, 1)
    assert tuple(query5._pos_last_type_and_variable.tolist())== (1, 2, 4)
    assert tuple(query6._pos_last_type_and_variable.tolist())== (2, 1, 4)
    assert tuple(query7._pos_last_type_and_variable.tolist())== (-1, 1, 2)

def test_parent():
    """
        Test for the function _pos_last_type_and_variable() for different
        queries.
    """
    query1=Query()
    query2=Query()
    query2.set_query_string('a', recalculate_attributes=False)
    query2.set_pos_last_type_and_variable()
    query3=Query()
    query3.set_query_string('$x0 $x0', recalculate_attributes=False)
    query3.set_pos_last_type_and_variable()
    query4=Query()
    query4.set_query_string('$x0 $x0 b', recalculate_attributes=False)
    query4.set_pos_last_type_and_variable()
    query5=Query()
    query5.set_query_string('$x0 a $x1 $x0 $x1', recalculate_attributes=False)
    query5.set_pos_last_type_and_variable()
    query6=Query()
    query6.set_query_string('$x0 $x1 $x1 $x0', recalculate_attributes=False)
    query6.set_pos_last_type_and_variable()
    query7=Query()
    query7.set_query_string('$x0 $x1 $x1 $x0 $x0')
    # query7.set_pos_last_type_and_variable()
    assert query1._parent()._query_string == ''
    assert query2._parent()._query_string == ''
    assert query3._parent()._query_string == ''
    assert query4._parent()._query_string == '$x0 $x0'
    assert query5._parent()._query_string == '$x0 a $x0'
    assert query6._parent()._query_string == '$x0 $x0'
    assert query7._parent()._query_string == '$x0 $x0 $x0'