"""Contains tests for testbench_shinohara_onedim.py"""
import logging
import os
import sys
sys.path.append(".")
from experiments.testbench_shinohara_onedim import main

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def test_testbench_shinohara_onedim():
    """
        Runs testbench with synthetic data. See testbench_shinohara_onedim.py
        for a more detailed description.
    """
    LOGGER.info("Started test test_testbench_shinohara_onedim")
    if os.path.exists('samples/'):
        filelist = [ f for f in os.listdir('samples/') ]
        for file in filelist:
            os.remove(os.path.join('samples/', file))
    else:
        os.mkdir('samples')
    main(["synthetic","True"])
    LOGGER.info("Finished test test_testbench_shinohara_onedim")
