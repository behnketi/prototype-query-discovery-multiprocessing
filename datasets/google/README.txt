original/

	jobId.zip
	jobId-binary_files.zip
	machineId.zip
	machineId-binary_files.zip
	username.zip
	username-binary_files.zip

	During a preprocessing to split the data into traces, we extract three 
	different datasets: Information about job and machine IDs as well as
	usernames combined with the respective states of their tasks.

	Within a preprocessed dataset each line represents a trace, whereby each 
	trace in dataset_i_jobId_TypeEvent or dataset_i_machineId_TypeEvent depicts
	the history	of states regarding the tasks of a corresponding job ID or 
	machine ID,	respectively. Note that the tasks of a machine ID may be 
	associated to different job IDs. Each trace in the preprocessed username
	datasets contains information about the progress of tasks of a single user.
	
	Each trace in dataset_i_jobId_Priority or dataset_i_machineId_Priority 
	represents the history of states and priorities for the tasks of a single
	job ID or machine ID, respectively. Each trace in the username dataset group
	contains information about the progress of tasks and the corresponding 
	priority of a user. 
	
	*-binary_files.zip contain the corresponding samples stored as binary files.


adapted/

	adapted-jobId.zip
	adapted-jobId-binary_files.zip
	adapted-machineId.zip
	adapted-machineId-binary_files.zip
	adapted-username.zip
	adapted-username-binary_files.zip
	
	Based on original/*. Each trace has length >=10.
	
	*-binary_files.zip contain the corresponding samples stored as binary files.
	

adapted2/

	adapted2-jobId.zip
	adapted2-jobId-binary_files.zip
	adapted2-machineId.zip
	adapted2-machineId-binary_files.zip
	adapted2-username.zip
	adapted2-username-binary_files.zip
	
	Based on original/*. Traces which only include Event Types 0 (SUBMIT) and 1
	(SCHEDULE) are excluded.
	
	*-binary_files.zip contain the corresponding samples stored as binary files.


adapted3/

	adapted3-jobId.zip
	adapted3-jobId-binary_files.zip
	adapted3-machineId.zip
	adapted3-machineId-binary_files.zip
	adapted3-username.zip
	adapted3-username-binary_files.zip
	adapted3_1-username.zip
	adapted3_1-username-binary_files.zip
	
	Based on original/*. Contains only traces which are marked in 
	overview_based_on_original.ods, i.e. traces with a length which occurs often.
	
	adapted3_1-* contains all trace from adapted3-username, excluding very long
	traces.
	
	*-binary_files.zip contain the corresponding samples stored as binary files.


Priority/

	Within this directory only *_Priority-Datesets have been considered.

	original2/
		original2-jobId.zip
		original2-machineId.zip
		original2-username.zip

		New trace format: each trace now has double length, e.g. 0 0 1 1 is now
		represented as 0 H 0 L 1 L 1 H. This matches the definition in the 
		Introduction of the ICDT Paper Submission, Nov. 2021.


	adapted2_1/
	
		adapted2_1-jobId.zip
		adapted2_1-machineId.zip
		adapted2_1-username.zip
		
		adapted2_1-jobId-tupleFormat.zip
		adapted2_1-machineId-tupleFormat.zip
		adapted2_1-username-tupleFormat.zip
		
		adapted2_1-jobId-tupleFormat_restricted_length.zip
		adapted2_1-machineId-tupleFormat_restricted_length.zip
		adapted2_1-username-tupleFormat_restricted_length.zip
		
		Based on original2*. Traces which only include Event Types 0 (SUBMIT), 1
		(SCHEDULE) and 4 (FINISHED) are excluded.
		
		The traces in *-tupleFormat* use a slightly adapted format, which is 
		e.g. 0H 0L 1L 1H instead of 0 H 0 L 1 L 1 H.
		
		*-tupleFormat_restricted_length contains only trace with specific trace
		lengths.
	
	
	adapted3_1/
		adapted3_1-jobId.zip
		adapted3_1-machineId.zip
		adapted3_1-username.zip
		adapted3_1-username-without_long_traces.zip

		Based on original2. Contains only traces which are marked in 
		overview_based_on_original.ods (based on original!), i.e. traces with a
		length which occurs often.
		
		Note that overview_based_on_original.ods was not extended by adapted3_1!
		We consider the same traces but with the new format, i.e. all trace
		lenghts in overview_based_on_original.ods have to be multiplied with 2.
