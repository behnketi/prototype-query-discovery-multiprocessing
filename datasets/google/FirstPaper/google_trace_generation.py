"""_summary_

Returns:
    _type_: _description_
"""
import sys
import glob
import os.path
import gzip
import logging
import numpy as np


import pandas as pd
pd.set_option('chained_assignment',None)

sys.path.append(".")

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)



def query1():
    taskpath = 'datasets/google/Data/task_events'
    all_files = np.sort(glob.glob(taskpath + "/*.csv.gz"))
    columns= ['timestamp', 'missing', 'jobId', 'taskId', 'machineId', 'status', 'username', 'schedule', 'priority','CPU_req', 'RAM_req', 'diskspace_req', 'constraints']
    file_path = gzip.GzipFile("datasets/google/FirstPaper/samples/google_query1.txt.gz", 'wb')
    for i, file in enumerate(all_files[:20]):
        event_list = []

        data_frame=pd.read_csv( file, header= None, names=columns, dtype={2: str, 4: str})
        if i == 0:
            data_frame = data_frame.loc[data_frame.timestamp>0]
        LOGGER.info('%s, length: %i',file, data_frame.shape[0])
        df5 = data_frame.loc[data_frame['status'] == 5]
        df5_grouped = df5.groupby(['machineId','jobId'])
        LOGGER.info('%i, %i', df5_grouped.ngroups, df5.shape[0])
        for name, group in df5_grouped:

            # df1 = df.loc[(df['machineId'] == name[0]) & (df['jobId'] == name[1]) & (df['status']==1)]
            df0 = data_frame.loc[(data_frame['jobId'] == name[1]) & (data_frame['status']==0)]
            if df0.shape[0] >1:
                min_idx = df0.index.min()
                for idx in df0.index:
                    if idx in event_list:
                        break
                    for idx5 in group.index:
                        if idx > idx5 and idx5 > min_idx and (idx -min_idx <1000):
                            event_list.append(idx)
                            break
        create_traces(data_frame, event_list, file_path)
    file_path.close()

def query2():
    file_path = gzip.GzipFile("datasets/google/FirstPaper/samples/google_query2.txt.gz", 'wb')
    taskpath = 'datasets/google/Data/task_events'
    all_files = np.sort(glob.glob(taskpath + "/*.csv.gz"))
    columns= ['timestamp', 'missing', 'jobId', 'taskId', 'machineId', 'status', 'username', 'schedule', 'priority','CPU_req', 'RAM_req', 'diskspace_req', 'constraints']
    for idx, file in enumerate(all_files[:2]):
        # event_list = []
        LOGGER.info(file)
        data_frame=pd.read_csv( file, header= None, names=columns, dtype={2: str, 4: str})
        data_frame.rename(columns={'timestamp':'Time'}, inplace=True)
        if idx == 0:
            data_frame = data_frame.loc[data_frame.Time>0]

        for machine_id in data_frame.machineId.unique():
            event_list = []
            df4_machine = data_frame.loc[(data_frame['machineId'] == machine_id) & (data_frame['status'] == 4)]
            if df4_machine.shape[0] >=4:
                start = df4_machine.Time.values[-4]
                end = df4_machine.Time.values[-1]
                df_machine = data_frame.loc[(data_frame['machineId'] == machine_id) & (data_frame['Time']>= start) & (data_frame['Time']<= end)]
                event_list.append(df4_machine.index.values[-1])
                create_traces(df_machine, event_list, file_path)
    file_path.close()


def query2_rl():
    file_path = gzip.GzipFile("datasets/google/FirstPaper/samples/google_query2_rl.txt.gz", 'wb')
    taskpath = 'datasets/google/Data/task_events'
    all_files = np.sort(glob.glob(taskpath + "/*.csv.gz"))
    columns = ['timestamp', 'missing', 'jobId', 'taskId', 'machineId', 'status', 'username', 'schedule', 'priority', 'CPU_req', 'RAM_req', 'diskspace_req', 'constraints']
    csv_columns = ['Substream_ID', 'timestamp', 'jobId', 'machineId', 'status', 'priority', 'Critical']
    data_csv = []
    substream_count = 0
    for idx, file in enumerate(all_files[:2]):
        if substream_count == 100:
            break
        # event_list = []
        LOGGER.info(file)
        dataframe = pd.read_csv(file, header=None, names=columns, dtype={2: str, 4: str})
        if idx == 0:
            dataframe = dataframe.loc[dataframe.timestamp > 0]

        for machine_id in dataframe.machineId.unique():
            event_list = []
            df4_machine = dataframe.loc[(dataframe['machineId'] == machine_id) & (dataframe['status'] == 4)]
            if df4_machine.shape[0] >= 4:
                start = df4_machine.timestamp.values[0]
                end = df4_machine.timestamp.values[-1]
                df_machine = dataframe.loc[(dataframe['machineId'] == machine_id) & (dataframe['timestamp']>= start) & (dataframe['timestamp'] <= end)]
                event_list.append(df4_machine.index.values[-1])
                cols4mining = ['status', 'priority']
                for idx in sorted(event_list):
                    trace = df_machine[cols4mining].values
                    LOGGER.debug(trace.shape[0])
                    for event in trace:
                        for domain in event:
                            content = str(domain) + ';'
                            file_path.write(content.encode())
                        file_path.write(b' ')
                    file_path.write(b'\n')

                events_of_interest = []
                for idx in df_machine.index:
                    if idx in event_list:
                        events_of_interest.append(True)
                    else:
                        events_of_interest.append(False)
                df_machine['Critical'] = events_of_interest
                df_machine['Substream_ID'] = substream_count
                substream_count += 1

                data_csv.extend(df_machine[csv_columns].values)
                if substream_count == 100:
                    break
    file_path.close()

    df_csv = pd.DataFrame(data=data_csv, columns=csv_columns)
    # df_csv.rename(columns={'index': 'Time'}, inplace=True)
    df_csv.index.names = ['Time']
    df_csv.to_csv("datasets/google/FirstPaper/samples/google_query2_rl.csv")



def query3():
    file_path = gzip.GzipFile("datasets/google/FirstPaper/samples/google_query3.txt.gz", 'wb')
    taskpath = 'datasets/google/Data/task_events'
    all_files = np.sort(glob.glob(taskpath + "/*.csv.gz"))
    columns = ['timestamp', 'missing', 'jobId', 'taskId', 'machineId', 'status', 'username', 'schedule', 'priority', 'CPU_req', 'RAM_req', 'diskspace_req', 'constraints']
    for idx, file in enumerate(all_files[:11]):
        event_list = []
        LOGGER.info(file)
        data_frame = pd.read_csv(file, header=None, names=columns, dtype={2: str, 4: str})
        if idx == 0:
            data_frame = data_frame.loc[data_frame.timestamp > 0]
        for machine_id in data_frame.machineId.unique():
            df2 = data_frame.loc[(data_frame['status'] == 2) & (data_frame['machineId'] == machine_id)]
            for job_id in df2.jobId.unique():
                df1_job_id = data_frame.loc[(data_frame.jobId == job_id) & (data_frame['status'] == 1) & (data_frame.machineId == machine_id)]
                indeces2 = df2.loc[(data_frame.jobId == job_id)].index
                for idx2 in indeces2:
                    if df1_job_id.shape[0] > 0:
                        indeces1 = df1_job_id.index
                        for idx1 in indeces1:
                            if idx2 in event_list:
                                break
                            if idx2-idx1 <= 1000:
                                df_machine = data_frame[(data_frame.index > idx1) & (data_frame.index < idx2) & (data_frame.machineId == machine_id)]
                                if df_machine.shape[0] > 0:
                                    event_list.append(idx2)
        create_traces(data_frame, event_list, file_path)
    file_path.close()


def create_traces(data_frame, event_list, file):
    cols4mining = ['jobId', 'machineId', 'status', 'priority']
    for idx in sorted(event_list):
        trace = data_frame.loc[(data_frame.index <= idx) & (data_frame.index >= idx - 1000)][cols4mining].values
        LOGGER.debug(trace.shape[0])
        for event in trace:
            for domain in event:
                content= str(domain) + ';'
                file.write( content.encode())
            file.write(b' ')
        file.write(b'\n')




if __name__ == "__main__":
    # query1()
    query2_rl()
    # query2
    # query3()