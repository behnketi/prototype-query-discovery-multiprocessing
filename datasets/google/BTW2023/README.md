The whole trace generation is implemented in the file google_trace_generation.py using task event information of the Google Cluster Traces[1].

The dimension of the task events is reduced to the following 5 attributes: 'jobId', 'taskId', 'machineId', 'types','priority'
The script partitions the task event according to three different traces:

### Query 1
```
Pattern SEQ(Task a, Task b, Task c)

    Where a.status = c.status = 1 And b.status = 5

    And a.job = b.job And a.job = c.job

    And a.machine = b.machine

    And a.machine = c.machine

    Within 1000 seconds
```
### Query 2
```
Pattern SEQ(Task a, Task b, Task c, Task d)

    Where a.machine=b.machine

    And a.machine=c.machine

    And a.machine=d.machine

    And a.status=4 And b.status=4

    And c.status=4 And d.status=4

    Within 100 minutes
```

### Query 3
```
Pattern SEQ(Task a, Task b, Task c)

    Where a.machine=b.machine And a.job=c.job

    And a.status=1 And b.status=1 And c.status=2

    Within 100 seconds

```

The task events are partitioned in such a way that the traces are not overlapping which means that each task events only appears in at most one of the traces.
In the file google_query1.txt.gz are all the traces for query 1. This means that the pattern that describes query 1 can be discovered in all the traces of that file. For query 2 and query 3 this holds for the respective files. 


[1] Charles Reiss, John Wilkes, and Joseph L Hellerstein. Google cluster-usage traces: format+ schema. Google Inc., White Paper, pages 1–14, 2011.