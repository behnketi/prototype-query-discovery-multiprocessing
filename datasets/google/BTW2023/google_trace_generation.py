"""_summary_

Returns:
    _type_: _description_
"""
import sys
import gzip
import logging
import pandas as pd
import numpy as np
import glob
import os.path
import seaborn as sb
import matplotlib.pyplot as plt
import time
from datetime import datetime
sys.path.append(".")

pd.set_option('display.precision', 10)

#Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

def main():
    """main function
    """

    # Creating non-overlapping traces including all 500 google tables
    taskpath = 'datasets/google/Data/task_events'
    all_files = np.sort(glob.glob(taskpath + "/*.csv.gz"))
    oldtime1=0
    oldtime2=0
    oldtime3=0
    for file1, file2 in zip(all_files[1:-1], all_files[2:]):
        LOGGER.info(file1)
        te,te2 = data_prep(file1= file1,file2=file2)
        df_query1=query1(te, te2, overlap = True)
        oldtime1 = create_traces('query1', df_query1, te, te2, 2000, overlap=True, old_time=oldtime1)

        df_query2 = query2(te,te2, overlap=True)
        oldtime2 = create_traces('query2', df_query2, te, te2, 2000, overlap=True, old_time=oldtime2)

        df_query3 = query3(te,te2, overlap=True)
        oldtime3 = create_traces('query3', df_query3, te, te2, 100, overlap=True, old_time=oldtime3)


def data_prep(file1_id='001', file2_id ='002', file1 = None, file2 = None):
    """_summary_
    1. timestamp
    2. missing info
    3. job ID
    4. task index - within the job
    5. machine ID
    6. event type
    7. user name
    8. scheduling class
    9. priority
    10. resource request for CPU cores
    11. resource request for RAM
    12. resource request for local disk space
    13. different-machine constraint
    """
    taskpath = 'datasets/google/Data/task_events'
    columns= ['timestamp', 'missing', 'jobId', 'taskId', 'machineId', 'types', 'username', 'schedule', 'priority','CPU_req', 'RAM_req', 'diskspace_req', 'constraints']
    cols4mining= ['timestamp', 'jobId', 'taskId', 'machineId', 'types','priority']

    assert int(file1_id) in range(499)
    assert int(file1_id) +1 == int(file2_id)
    if not file1:
        file1= f'{taskpath}/part-00{file1_id}-of-00500.csv.gz'
    if not file2:
        file2=f'{taskpath}/part-00{file2_id}-of-00500.csv.gz'
    
    
    df1=pd.read_csv( file1, header= None, names=columns, dtype={2: str})
    df2= pd.read_csv( file2, header= None, names=columns, dtype={2: str})
    task_events2= pd.concat([df1,df2], ignore_index=True)
    task_events = pd.read_csv( file2, header= None, names=columns, dtype={2: str})
    task_events['timestamp'] = task_events['timestamp'].div(1000000)
    machine_idx=task_events.machineId.dropna().index.tolist()
    task_events= task_events.iloc[machine_idx]
    task_events['index']= range(task_events.shape[0])
    te= task_events[cols4mining]

    task_events2['timestamp'] = task_events2['timestamp'].div(1000000)
    machine_idx=task_events2.machineId.dropna().index.tolist()
    task_events2= task_events2.iloc[machine_idx]
    task_events2['index']= range(task_events2.shape[0])
    te2= task_events2[cols4mining]

    return te, te2

def query1(te, te2, overlap= False):
    """Pattern SEQ(Task a, Task b, Task c)

    Where a.status = c.status = 1 And b.status = 5

    And a.job = b.job And a.job = c.job

    And a.machine = b.machine

    And a.machine = c.machine

    Within 1000 seconds

    Args:
        te (_type_): _description_
        te2 (_type_): _description_
    """
    traces=[]
    eventlist= []
    min_time = []
    max_time = []
    indeces = []
    no_overlap_time= 0 
    for idx in te.index:
        end_time = te.loc[idx, 'timestamp']
        status = te.loc[idx, 'types']
        #start_time = time - 3600
        if end_time >= no_overlap_time and status == 1:
            df = te2.loc[(te2.timestamp<= end_time) & (te2.timestamp> 0)]
            machine = te.loc[idx, 'machineId']
            job = te.loc[idx, 'jobId']
            df_1= df.loc[(df.machineId == machine) & (df.jobId == job) & (df.types == 5)]
            if df_1.shape[0] >0:
                idx2 = df_1.index.values[-1]
                df_2 =  df.loc[(df.machineId == machine) & (df.jobId == job) & (df.types == 1) & (df.index < idx2)]
                if df_2.shape[0] >0:
                    #for i in df_2.index:
                    i = df_2.index[-1]
                    time_min= df_2.loc[i, 'timestamp']
                    if end_time-time_min <= 1000:
                        if overlap:
                            no_overlap_time= end_time+ 2000
                        min_time.append(time_min)
                        max_time.append(end_time)
                        indeces.append(idx)
    dataframe= create_dataframe('query1', max_time, max_time, indeces)
    return dataframe

def query2(te, te2, overlap = False):
    """Pattern SEQ(Task a, Task b, Task c, Task d)

    Where a.machine=b.machine

    And a.machine=c.machine

    And a.machine=d.machine

    And a.status=4 And b.status=4

    And c.status=4 And d.status=4

    Within 100 minutes

    Args:
        te (_type_): _description_
        te2 (_type_): _description_
    """
    traces=[]
    eventlist= []
    min_time = []
    max_time = []
    indeces = []
    no_overlap_time = 0
    for idx in te.index:

        end_time = te.loc[idx, 'timestamp']
        status = te.loc[idx, 'types']

        if status == 4 and end_time >= no_overlap_time:
            df = te2.loc[(te2.timestamp<= end_time)]
            machine = te.loc[idx, 'machineId']
            df_1= df.loc[(df.machineId == machine) & (df.timestamp >0)]
            if df_1.shape[0] >=4:
                if overlap:
                    no_overlap_time= end_time+ 2000
                i= df_1.index[-4]
                min_time.append(df_1.loc[i, 'timestamp'])
                max_time.append(end_time)
                indeces.append(idx)
    dataframe= create_dataframe('query2', max_time, max_time, indeces)
    return dataframe

def query3(te, te2, overlap= False):
    """Pattern SEQ(Task a, Task b, Task c)

    Where a.machine=b.machine And a.job=c.job

    And a.status=1 And b.status=1 And c.status=2

    Within 100 seconds

    Args:
        te (_type_): _description_
        te2 (_type_): _description_
    """
    traces=[]
    eventlist= []
    min_time = []
    max_time = []
    indeces = []
    no_overlap_time = 0
    for idx in te.index:
        end_time = te.loc[idx, 'timestamp']
        status = te.loc[idx, 'types']
        #start_time = time - 3600
        if status == 2 and end_time> no_overlap_time:
            df = te2.loc[(te2.timestamp<= end_time)]
            machine = te.loc[idx, 'machineId']
            job = te.loc[idx, 'jobId']
            df_1= df.loc[(df.machineId == machine) & (df.jobId == job) & (df.types == 1)]
            if df_1.shape[0] >0:
                idx2 = df_1.index.values[0]
                df_2 =  df.loc[(df.machineId == machine) & (df.types == 1) & (df.jobId != job) & (df.index > idx2)]
                if df_2.shape[0] >0:
                    #for i in df_2.index:
                    if overlap:
                        no_overlap_time= end_time+ 150
                    i = df_2.index[-1]
                    min_time.append(df_2.loc[i, 'timestamp'])
                    max_time.append(end_time)
                    indeces.append(idx)
    dataframe= create_dataframe('query3', max_time, max_time, indeces)
    return dataframe

def create_dataframe(file_name, min_time, max_time, indeces):
    """_summary_

    Args:
        min_time (_type_): _description_
        max_time (_type_): _description_
        indeces (_type_): _description_
    """
    df = pd.DataFrame(min_time, columns=['min_time'])
    df['max_time'] = max_time
    df['idx'] = indeces
    df_sorted = df.sort_values(by='min_time', ignore_index= True )
    df_sorted['diff'] = df_sorted.max_time - df_sorted.min_time
    overlap=[]
    for idx in df_sorted.index:
        start_time= df_sorted.loc[idx, 'min_time']
        end_time= df_sorted.loc[idx, 'max_time']
        
        overlap.append(df_sorted.loc[(df_sorted.min_time>= start_time) & (df_sorted.min_time<=end_time)].shape[0])
    df_sorted['overlap'] = overlap
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%Y%m%d%H%M%S")
    df_sorted.to_csv(f'datasets/google/BTW2023/samples/{str_date_time}_google_{file_name}.csv')
    return df_sorted

def create_traces(file_name, df_sorted, te, te2, event_duration, overlap=False, old_time = 0):
    """_summary_

    Args:
        file_name (_type_): _description_
        df_sorted (_type_): _description_
        event_duration (_type_): _description_
    """
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    str_date_time = date_time.strftime("%Y%m%d%H%M%S")
    if overlap:
        f = gzip.GzipFile(f"datasets/google/BTW2023/samples/google_{file_name}.txt.gz", 'ab')
    else:
        f = gzip.GzipFile(f"datasets/google/BTW2023/samples/google_{file_name}_{str_date_time}.txt.gz", 'wb')
    for idx in df_sorted.loc[df_sorted['diff']<event_duration].idx:
        end_time = te.loc[idx, 'timestamp']
        time2= end_time- 1.5 * event_duration
        trace= te2.loc[(te2.timestamp<= end_time) & (te2.timestamp >= time2) ].values
        if overlap:
            if old_time < time2:
                old_time = end_time
                for event in trace:
                    for domain in event[1:]:
                        content= str(domain) + ';'
                        f.write( content.encode())
                    f.write(b' ')
                f.write(b'\n')
        else:
            for event in trace:
                for domain in event[1:]:
                    content= str(domain) + ';'
                    f.write( content.encode())
                f.write(b' ')
            f.write(b'\n')

    f.close()
    return old_time

if __name__ == "__main__":
    main()
