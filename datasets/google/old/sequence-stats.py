#!/usr/bin/python3
"""Contains functions to plot statistics"""
import os
import glob
import collections

seqpath = [
    #'original2/original2-jobId/jobId', 'original2/original2-machineId/machineId', 'original2/original2-username/username',
    'Priority/adapted2_1/adapted2_1-jobId-tupleFormat/', 'Priority/adapted2_1/adapted2_1-machineId-tupleFormat/', 'Priority/adapted2_1/adapted2_1-username-tupleFormat/'#,
    #'adapted3_1/adapted3_1-jobId/jobId', 'adapted3_1/adapted3_1-machineId/machineId', 'adapted3_1/adapted3_1-username/username',
]

def adapted(seqpath):
    for seq in seqpath:
        all_files = glob.glob(seq + "/*.txt")
        for f in all_files:
            new_file = f[:len(f)-4] + "-traceCount_typeFreq_tracelengthFreq.txt"
            command_type_freq = "tr ' ' '\n' < "+str(f)+" | sort | uniq -c > "+str(new_file)
            os.system(command_type_freq)
            command_new_line = "echo "" >> "+str(new_file)
            os.system(command_new_line)

            trace_length_dict = dict()
            with open(f) as read_file:
                lines = read_file.readlines()
                for line in lines:
                    length_counter = line.count(" ")
                    if str(length_counter) in trace_length_dict:
                        trace_length_dict[str(length_counter)] = trace_length_dict[str(length_counter)] +1
                    else:
                        trace_length_dict[str(length_counter)] = 1

            trace_length_dict_int = {int(k) : v for k, v in trace_length_dict.items()}
            trace_length_dict_int_sorted = collections.OrderedDict(sorted(trace_length_dict_int.items()))

            with open(new_file, 'a') as write_file:
                for k, v in trace_length_dict_int_sorted.items():
                    line = '{}, {}'.format(k, v) 
                    print(line, file=write_file)

adapted(seqpath)
