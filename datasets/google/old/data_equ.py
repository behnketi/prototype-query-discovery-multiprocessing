import pandas as pd
import numpy as np
import glob
import matplotlib.pyplot as plt
import os


seqpath = ['jobId', 'machineId', 'username' ]

#print(len(all_files))
#df_files = (pd.read_csv(f, header=0, names=columns) for f in all_files)

def adapted(seqpath):
    for seq in seqpath:
        all_files = glob.glob(seq + "/*.txt")
        for f in all_files:
            new_file=str('adapted/'+f)
            if not os.path.exists(new_file):
                os.system(str("cat %s | awk ' NF>9 {print $0}' >%s" %(f, new_file)) )
            os.system(str("cat %s | wc -l " %f) )
            os.system(str("cat %s | wc -l " %new_file) )



def adapted2(seqpath):
    for seq in seqpath:
        all_files = glob.glob(seq + "/*.txt")
        for f in all_files:
            new_file=str('adapted2/'+f)
            if not os.path.exists(new_file):
                os.system(str("cat %s | grep [2-8] >%s" %(f, new_file)) )
            os.system(str("cat %s | wc -l " %f) )
            os.system(str("cat %s | wc -l " %new_file) )
    return

def adapted3():
    all_files = glob.glob("jobId/*.txt")
    for f in all_files:
        new_file=str('adapted3/'+f)
        if not os.path.exists(new_file):
            os.system(str("cat %s | awk ' NF<501 {print $0}' >%s" %(f, new_file)) )
        #os.system(str("cat %s | wc -l " %f) )
        #os.system(str("cat %s | wc -l " %new_file) )
        
    all_files = glob.glob("username/*.txt")
    for f in all_files:
        new_file=str('adapted3/'+f)
        if not os.path.exists(new_file):
            os.system(str("cat %s | awk ' (NF<100001) {print $0}'   | awk ' !(NF>1000 && NF<10001) {print $0}' >%s" %(f, new_file)) )
        #os.system(str("cat %s | wc -l " %f) )
        #os.system(str("cat %s | awk  '{print NF}'" %new_file) )
        
    for f in all_files:
        new_file=str('adapted3_1/'+f)
        if not os.path.exists(new_file):
            os.system(str("cat %s | awk ' NF <1001 {print $0}' >%s" %(f, new_file)) )
            
    
    
    all_files = glob.glob("machineId/*.txt")
    for f in all_files:
        new_file=str('adapted3/'+f)
        if not os.path.exists(new_file):
            if '32' in f:
                os.system(str("cat %s | awk ' NF>10 {print $0}'   | awk ' NF<2001 {print $0}' >%s" %(f, new_file)) )
            else:
                os.system(str("cat %s | awk ' NF>10 {print $0}'   | awk ' NF<501 {print $0}' >%s" %(f, new_file)) )
            
            
    

adapted3()