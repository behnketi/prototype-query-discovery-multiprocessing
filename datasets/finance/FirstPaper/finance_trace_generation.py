"""_summary_

Returns:
    _type_: _description_
"""
import sys
import logging
import os.path
import pandas as pd

sys.path.append(".")

# Logger Configuration:
LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel('INFO')
FILE_HANDLER = logging.StreamHandler()
FORMATTER = logging.Formatter(LOG_FORMAT)
FILE_HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(FILE_HANDLER)


def traces_by_date():

    finance_data_5 = pd.read_csv('datasets/finance/Data/NASDAQ_20101201_5.txt')
    finance_data_5.sort_values(['<date>', '<ticker>'], ignore_index=True, inplace=True)
    columns = ['<ticker>', '<open>', '<high>', '<low>', '<close>', '<vol>']
    file_name = "datasets/finance/FirstPaper/samples/finance_traces_by_date.txt"
    file = open(file_name, 'w', encoding="utf-8")
    for date in finance_data_5['<date>'].unique():
        trace = finance_data_5.loc[(finance_data_5['<date>'] == date)][columns].values

        for event in trace:
            for domain in event:
                content = str(domain) + ';'
                file.write(content)
            file.write(' ')
        file.write('\n')


def traces_by_ticker():
    finance_data_5 = pd.read_csv('datasets/finance/Data/NASDAQ_20101201_5.txt')
    finance_data_5.sort_values(['<date>', '<ticker>'], ignore_index=True, inplace=True)
    columns = ['<date>', '<open>', '<high>', '<low>', '<close>', '<vol>']
    finance_data_5.rename(mapper={'<date>': 'Time'}, inplace=True)
    file_name = "datasets/finance/FirstPaper/samples/ticker/finance_ticker_all_0.txt"
    file = open(file_name, 'w', encoding="utf-8")
    for ticker in finance_data_5['<ticker>'].unique():
        trace = finance_data_5.loc[(finance_data_5['<ticker>'] == ticker)][columns].values

        for event in trace:
            for domain in event:
                content = str(domain) + ';'
                file.write(content)
            file.write(' ')
        file.write('\n')

    for i in [3, 5, 10, 100]:

        for j in ['ew', 'ed']:
            if j == 'ed':
                finance_data_5[f'close_{j}_{i}'] = pd.qcut(finance_data_5['<close>'], q=i, labels=False, duplicates='drop')
                finance_data_5[f'vol_{j}_{i}'] = pd.qcut(finance_data_5['<vol>'], q=i, labels=False, duplicates='drop')
                finance_data_5[f'open_{j}_{i}'] = pd.qcut(finance_data_5['<open>'], q=i, labels=False, duplicates='drop')
                finance_data_5[f'high_{j}_{i}'] = pd.qcut(finance_data_5['<high>'], q=i, labels=False, duplicates='drop')
                finance_data_5[f'low_{j}_{i}'] = pd.qcut(finance_data_5['<low>'], q=i, labels=False, duplicates='drop')
            else:
                finance_data_5[f'close_{j}_{i}'] = pd.cut(finance_data_5['<close>'], bins=i, labels=False, duplicates='drop')
                finance_data_5[f'vol_{j}_{i}'] = pd.cut(finance_data_5['<vol>'], bins=i, labels=False, duplicates='drop')
                finance_data_5[f'open_{j}_{i}'] = pd.cut(finance_data_5['<open>'], bins=i, labels=False, duplicates='drop')
                finance_data_5[f'high_{j}_{i}'] = pd.cut(finance_data_5['<high>'], bins=i, labels=False, duplicates='drop')
                finance_data_5[f'low_{j}_{i}'] = pd.cut(finance_data_5['<low>'], bins=i, labels=False, duplicates='drop')

            file_name = f"datasets/finance/FirstPaper/samples/ticker/finance_ticker_{j}_{i}.txt"
            if not os.path.isfile(file_name):
                file = open(file_name, 'w', encoding="utf-8")
                LOGGER.info('Started writing file %s', file_name)
                columns = ['<ticker>', '<date>', f'open_{j}_{i}', f'high_{j}_{i}', f'low_{j}_{i}', f'close_{j}_{i}', f'vol_{j}_{i}']

                for ticker in finance_data_5['<ticker>'].unique():
                    trace = finance_data_5.loc[(finance_data_5['<ticker>'] == ticker)][columns].values

                    for event in trace:
                        for domain in event:
                            content = str(domain) + ';'
                            file.write(content)
                        file.write(' ')
                    file.write('\n')
                # create_traces(finance_data_5, results, file, columns)



def query1(overwrite: bool = False):
    """
    Pattern SEQ(Stock a, Stock b)
    Where a.ticker=b.ticker=’GOOG’
    And a.low>b.low

    Args:
        overwrite (bool, optional): _description_. Defaults to False.
    """
    finance_data_5 = pd.read_csv('datasets/finance/Data/NASDAQ_20101201_5.txt')
    finance_data_5.sort_values('<date>', ignore_index=True, inplace=True)
    finance_goog = finance_data_5.loc[finance_data_5['<ticker>'] == 'GOOG'].index.values
    results = []
    for count, idx in enumerate(finance_goog[1:], start=1):
        if idx - finance_goog[count - 1] <= 2000:
            results.append(idx)

    
    dir_path = "datasets/finance/FirstPaper/samples/"
    # Check whether the specified path exists or not
    isExist = os.path.exists(dir_path)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(dir_path)
    file_name = "datasets/finance/FirstPaper/samples/finance_query1.txt"
    if not os.path.isfile(file_name) or overwrite:
        file = open(file_name, 'w', encoding="utf-8")
        LOGGER.info('Started writing file %s', file)
        columns = ['<ticker>', '<date>', '<vol>', '<close>']
        create_traces(finance_data_5, results, file, columns)

    for i in [1000, 100, 10]:
        finance_data_5[f'close_{i}'] = pd.qcut(
            finance_data_5['<close>'], q=i, duplicates='drop')
        file_name = f"datasets/finance/FirstPaper/samples/finance_query1_close{i}.txt"
        if not os.path.isfile(file_name) or overwrite:
            file = open(file_name, 'w', encoding="utf-8")
            LOGGER.info('Started writing file %s', file)
            columns = ['<ticker>', '<date>', '<vol>', f'close_{i}']
            create_traces(finance_data_5, results, file, columns)


def query2(overwrite: bool = False):
    """
    Pattern SEQ(Stock a, Stock b, Stock c)
    Where a.ticker=’AAPL’ And b.ticker=’GOOG’
    And c.ticker=’MSFT’

    Args:
        overwrite (bool, optional): _description_. Defaults to False.
    """
    finance_data_5 = pd.read_csv('datasets/finance/Data/NASDAQ_20101201_5.txt')
    finance_data_5.sort_values('<date>', ignore_index=True, inplace=True)
    finance_aapl = finance_data_5.loc[finance_data_5['<ticker>'] == 'AAPL']['<date>'].values
    finance_goog = finance_data_5.loc[finance_data_5['<ticker>'] == 'GOOG']['<date>'].values
    finance_msft = finance_data_5.loc[finance_data_5['<ticker>'] == 'MSFT']['<date>'].values

    results_time = set(finance_aapl) & set(finance_goog) & set(finance_msft)
    results = []
    for timestamp in results_time:
        results.append(
            finance_data_5.loc[
             (finance_data_5['<ticker>'] == 'MSFT') &
             (finance_data_5['<date>'] == timestamp)].index.values[0])

    dir_path = "datasets/finance/FirstPaper/samples/"
    # Check whether the specified path exists or not
    isExist = os.path.exists(dir_path)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(dir_path)
    file_name = "datasets/finance/FirstPaper/samples/finance_query2.txt"
    if not os.path.isfile(file_name) or overwrite:
        file = open(file_name, 'w', encoding="utf-8")
        LOGGER.info('Started writing file %s', file)
        columns = ['<ticker>', '<date>', '<vol>', '<close>']
        create_traces(finance_data_5, results, file, columns)

    for i in [1000, 100, 10]:
        finance_data_5[f'close_{i}'] = pd.qcut(
            finance_data_5['<close>'], q=i, duplicates='drop')
        file_name = f"datasets/finance/FirstPaper/samples/finance_query2_close{i}.txt"
        if not os.path.isfile(file_name) or overwrite:
            file = open(file_name, 'w', encoding="utf-8")
            LOGGER.info('Started writing file %s', file)
            columns = ['<ticker>', '<date>', '<vol>', f'close_{i}']
            create_traces(finance_data_5, results, file, columns)


def query3(overwrite: bool = False):
    """
    Pattern
    SEQ(Stock a, Stock b, Stock c, Stock d)
    Where a.vol=b.vol And a.vol=c.vol
    And a.vol=d.vol

    Args:
        overwrite (bool, optional): _description_. Defaults to False.
    """
    finance_data_5 = pd.read_csv('datasets/finance/Data/NASDAQ_20101201_5.txt')
    finance_data_5.sort_values('<date>', ignore_index=True, inplace=True)
    results = []
    finance_data_grouped = finance_data_5.groupby(['<date>', '<vol>'])
    LOGGER.info('%i, %i',
                finance_data_grouped.ngroups,
                finance_data_5.shape[0])
    for _, group in finance_data_grouped:
        if group.shape[0] >= 4:
            indeces = group.index.values[3:]
            results.extend(indeces)
    dir_path = "datasets/finance/FirstPaper/samples/"
    # Check whether the specified path exists or not
    isExist = os.path.exists(dir_path)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(dir_path)
    file_name = "datasets/finance/FirstPaper/samples/finance_query3.txt"
    if not os.path.isfile(file_name) or overwrite:
        file = open(file_name, 'w', encoding="utf-8")
        LOGGER.info('Started writing file %s', file)
        columns = ['<ticker>', '<date>', '<vol>', '<close>']
        create_traces(finance_data_5, results, file, columns)

    for i in [1000, 100, 10]:
        finance_data_5[f'close_{i}'] = pd.qcut(
            finance_data_5['<close>'], q=i, duplicates='drop')
        file_name = f"datasets/finance/FirstPaper/samples/finance_query3_close{i}.txt"
        if not os.path.isfile(file_name) or overwrite:
            file = open(file_name, 'w', encoding="utf-8")
            LOGGER.info('Started writing file %s', file)
            columns = ['<ticker>', '<date>', '<vol>', f'close_{i}']
            create_traces(finance_data_5, results, file, columns)


def create_traces(data_frame, event_list, file, cols4mining):
    for idx in sorted(event_list):
        trace = data_frame.loc[(data_frame.index <= idx) &
                               (data_frame.index >= idx-1000)][cols4mining].values

        for event in trace:
            for domain in event:
                content = str(domain) + ';'
                file.write(content)
            file.write(' ')
        file.write('\n')

def create_sample(query, sample_size, trace_length):

    original = f"datasets/finance/FirstPaper/samples/finance_{query}.txt"
    file_path = f"datasets/finance/FirstPaper/samples/finance_{query}_{sample_size}_{trace_length}.txt"
    if os.path.isfile(original) and not os.path.isfile(file_path):
        original_file = open(original, 'r', encoding="utf-8")
        file = open(file_path, 'w', encoding="utf-8")
        counter = 0
        for trace1 in original_file:
            # sample_set2.append(' '.join(trace1.decode().split()))
            file.write(' '.join(trace1.split()[-trace_length:]))
            file.write('\n')
            if counter == sample_size:
                break
            counter += 1
        file.close()
        original_file.close()

if __name__ == "__main__":
    query1()
    query2()
    query3()
    # traces_by_date()
    # traces_by_ticker()
    # create_sample('query3', 100, 10)
